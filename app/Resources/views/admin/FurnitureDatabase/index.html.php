
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>{% block title %}Welcome admin hello!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />
    </head>
    <body>
        {%  if is_granted('ROLE_ADMIN') %}
            <a href="/admin/users">admin page</a>
        {% endif %}
        <a href="/logout">logout</a>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>