-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: furniture_enterprise
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `furniture_enterprise`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `furniture_enterprise` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `furniture_enterprise`;

--
-- Table structure for table `app_grants`
--

DROP TABLE IF EXISTS `app_grants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_grants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_grant` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_81AEAA56433D8AD0` (`_grant`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_grants`
--

LOCK TABLES `app_grants` WRITE;
/*!40000 ALTER TABLE `app_grants` DISABLE KEYS */;
INSERT INTO `app_grants` VALUES (1,'debtor','debtor grant',1,'2018-01-30 10:08:46','2018-01-30 10:08:46'),(2,'article','article grant',1,'2018-01-30 10:09:02','2018-01-30 10:09:02'),(3,'order','order grant',1,'2018-01-30 10:09:18','2018-01-30 10:09:18'),(4,'supplier','supplier grant',1,'2018-01-30 10:09:33','2018-01-30 10:09:33');
/*!40000 ALTER TABLE `app_grants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_users`
--

DROP TABLE IF EXISTS `app_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `phonenumber` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C2502824F85E0677` (`username`),
  UNIQUE KEY `UNIQ_C2502824E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_users`
--

LOCK TABLES `app_users` WRITE;
/*!40000 ALTER TABLE `app_users` DISABLE KEYS */;
INSERT INTO `app_users` VALUES (1,'5mict','$2y$13$LsMaJUy9dk1FVVY8uGmSdu5RC59pyRN94IB24gFhNMLMHpDjn4.9q','john','smith','+3811234567','john.smith.sneg@gmail.com',1,'2018-01-30 00:00:00','2018-01-30 10:11:33'),(2,'admin','$2y$13$4IzcJ9bhVGe5NKu1aMu.neUFPHENTEEUV8MMfZoERbGOe8V0UrwS6','Stijn','van Limpt','','info@dejonginterieur.com',1,'2018-01-30 16:46:23','2018-01-30 16:46:23');
/*!40000 ALTER TABLE `app_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_category_id` int(11) DEFAULT NULL,
  `article_sub_category_id` int(11) DEFAULT NULL,
  `article_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `article_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `article_name_dutch` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `article_name_german` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `purchase_price` double NOT NULL,
  `selling_price` double NOT NULL,
  `selling_price_with_pdv` double NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `picture_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_selling_price` double NOT NULL,
  `total_purchase_price` double NOT NULL,
  `profit_percents` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_BFDD3168FC5788D4` (`article_number`),
  UNIQUE KEY `name_size_constraint` (`article_name`,`size`),
  KEY `IDX_BFDD316888C5F785` (`article_category_id`),
  KEY `IDX_BFDD3168147AD8E2` (`article_sub_category_id`),
  KEY `search_article_name` (`article_name`),
  KEY `search_article_number` (`article_number`),
  KEY `search_purchase_price` (`purchase_price`),
  KEY `search_selling_price` (`selling_price`),
  KEY `search_selling_price_with_pdv` (`selling_price_with_pdv`),
  KEY `search_total_selling_price` (`total_selling_price`),
  KEY `search_profit_percents` (`profit_percents`),
  CONSTRAINT `FK_BFDD3168147AD8E2` FOREIGN KEY (`article_sub_category_id`) REFERENCES `articles_fixed_tables_sub_category` (`id`),
  CONSTRAINT `FK_BFDD316888C5F785` FOREIGN KEY (`article_category_id`) REFERENCES `articles_fixed_tables_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (2,3,12,'1','iron table','iron table - dutch','','1x2x3',100,200,300,'some dinning table. gemaakt van gebakken eiken','stoOdKovanogGvozdja03.jpg','2018-01-31 12:36:09','2018-02-07 22:25:17','admin',0,0,0),(3,4,17,'C 217','cooler','koeler','','40 x40 x40',10,20,36,'Geweischaal','FullSizeRender (1).jpg','2018-02-01 11:16:19','2018-02-01 11:16:19','admin',0,0,0);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_fixed_tables_category`
--

DROP TABLE IF EXISTS `articles_fixed_tables_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_fixed_tables_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_1169C4C55E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_fixed_tables_category`
--

LOCK TABLES `articles_fixed_tables_category` WRITE;
/*!40000 ALTER TABLE `articles_fixed_tables_category` DISABLE KEYS */;
INSERT INTO `articles_fixed_tables_category` VALUES (4,'antler articles'),(2,'cabinets'),(9,'decoration articles'),(5,'lighting'),(8,'mirrors'),(1,'seating furniture'),(7,'suitcases'),(3,'tables'),(6,'taxidermy');
/*!40000 ALTER TABLE `articles_fixed_tables_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_fixed_tables_category_translations`
--

DROP TABLE IF EXISTS `articles_fixed_tables_category_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_fixed_tables_category_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_category_id` int(11) DEFAULT NULL,
  `locale_id` int(11) DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_2C731E8588C5F785` (`article_category_id`),
  KEY `IDX_2C731E85E559DFD1` (`locale_id`),
  CONSTRAINT `FK_2C731E8588C5F785` FOREIGN KEY (`article_category_id`) REFERENCES `articles_fixed_tables_category` (`id`),
  CONSTRAINT `FK_2C731E85E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_fixed_tables_category_translations`
--

LOCK TABLES `articles_fixed_tables_category_translations` WRITE;
/*!40000 ALTER TABLE `articles_fixed_tables_category_translations` DISABLE KEYS */;
INSERT INTO `articles_fixed_tables_category_translations` VALUES (1,1,1,'Seating furniture'),(2,1,2,'Zitmeubels'),(3,2,1,'Cabinets'),(4,2,2,'Kasten'),(5,3,1,'Tables'),(6,3,2,'Tafels'),(7,4,1,'Antler articles'),(8,4,2,'Gewei artikelen'),(9,5,1,'Lighting'),(10,5,2,'Verlinchting'),(11,6,1,'Taxidermy'),(12,6,2,'Taxidermy'),(13,7,1,'Suitcases'),(14,7,2,'Koffers'),(15,8,1,'Mirrors'),(16,8,2,'Spiegels'),(17,9,1,'Decoration articles'),(18,9,2,'Decoratie artikelen');
/*!40000 ALTER TABLE `articles_fixed_tables_category_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_fixed_tables_sub_category`
--

DROP TABLE IF EXISTS `articles_fixed_tables_sub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_fixed_tables_sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_category_id` int(11) DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_DDF0938C5E237E06` (`name`),
  KEY `IDX_DDF0938C88C5F785` (`article_category_id`),
  CONSTRAINT `FK_DDF0938C88C5F785` FOREIGN KEY (`article_category_id`) REFERENCES `articles_fixed_tables_category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_fixed_tables_sub_category`
--

LOCK TABLES `articles_fixed_tables_sub_category` WRITE;
/*!40000 ALTER TABLE `articles_fixed_tables_sub_category` DISABLE KEYS */;
INSERT INTO `articles_fixed_tables_sub_category` VALUES (1,1,'dining chair'),(2,1,'couche'),(3,1,'armchair'),(4,1,'pouf'),(5,1,'stool bench'),(6,1,'bar stool'),(7,2,'sideboard'),(8,2,'cabinet'),(9,2,'bedroom dresser'),(10,2,'side table'),(11,3,'coffee tables'),(12,3,'dining tables'),(13,4,'antler pendant light'),(14,4,'antler wall lamp'),(15,4,'antler standing lamp'),(16,4,'antler ceiling lamp'),(17,4,'antler champagne cooler'),(18,4,'antler bowl'),(19,4,'antler candle stick'),(20,4,'antler seating furniture'),(21,4,'antler table'),(22,4,'antler coat rack'),(23,4,'antler decoration'),(24,5,'table lamp'),(25,5,'wall lamp'),(26,5,'pendant light'),(27,5,'standing lamp'),(28,6,'full mounts'),(29,6,'shoulder mounts'),(30,6,'birds'),(31,6,'insects'),(32,6,'antler and skulls'),(33,6,'animal hides'),(34,6,'various items'),(35,7,'suitcase antique'),(36,7,'suitcase animal skin'),(37,7,'suitcase leather'),(38,8,'mirror_wood'),(39,8,'mirror upholstered'),(40,9,'decoration article');
/*!40000 ALTER TABLE `articles_fixed_tables_sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_fixed_tables_sub_category_translations`
--

DROP TABLE IF EXISTS `articles_fixed_tables_sub_category_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_fixed_tables_sub_category_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_sub_category_id` int(11) DEFAULT NULL,
  `locale_id` int(11) DEFAULT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_9E5117BB147AD8E2` (`article_sub_category_id`),
  KEY `IDX_9E5117BBE559DFD1` (`locale_id`),
  CONSTRAINT `FK_9E5117BB147AD8E2` FOREIGN KEY (`article_sub_category_id`) REFERENCES `articles_fixed_tables_sub_category` (`id`),
  CONSTRAINT `FK_9E5117BBE559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_fixed_tables_sub_category_translations`
--

LOCK TABLES `articles_fixed_tables_sub_category_translations` WRITE;
/*!40000 ALTER TABLE `articles_fixed_tables_sub_category_translations` DISABLE KEYS */;
INSERT INTO `articles_fixed_tables_sub_category_translations` VALUES (1,1,1,'Dining chair'),(2,1,2,'Eetkamerstoel'),(3,2,1,'Couche'),(4,2,2,'Zitbank'),(5,3,1,'armchair'),(6,3,2,'Fauteuil'),(7,4,1,'Pouf'),(8,4,2,'Poef'),(9,5,1,'Stool/Bench'),(10,5,2,'Krukjes/Banken'),(11,6,1,'Bar stool'),(12,6,2,'Barkruk'),(13,7,1,'Sideboard'),(14,7,2,'Dressoir'),(15,8,1,'Cabinet'),(16,8,2,'Kast'),(17,9,1,'Bedroom dresser'),(18,9,2,'Nachtkastje'),(19,10,1,'Side table'),(20,10,2,'Wandtafel'),(21,11,1,'Coffee tables'),(22,11,2,'Salontafel'),(23,12,1,'Dining tables'),(24,12,2,'Eetkamertafel'),(25,13,1,'Antler Pendant light'),(26,13,2,'Hanglamp gewei'),(27,14,1,'Antler wall lamp'),(28,14,2,'Wandlamp gewei'),(29,15,1,'Antler standing lamp'),(30,15,2,'Vloerlamp gewei'),(31,16,1,'Antler ceiling lamp'),(32,16,2,'Plafondlamp gewei'),(33,17,1,'Antler champagne cooler'),(34,17,2,'Champagne koeler gewei'),(35,18,1,'Antler bowl'),(36,18,2,'Schaal gewei'),(37,19,1,'Antler candle stick'),(38,19,2,'Kandelaar gewei'),(39,20,1,'Antler seating furniture'),(40,20,2,'Zitmeubel gewei'),(41,21,1,'Antler table'),(42,21,2,'Tafel gewei'),(43,22,1,'Antler coat rack'),(44,22,2,'Gewei kapstok'),(45,23,1,'Antler decoration'),(46,23,2,'Gewei decoratie'),(47,24,1,'Table lamp'),(48,24,2,'Tafellamp'),(49,25,1,'Wall lamp'),(50,25,2,'Wandlamp'),(51,26,1,'Pendant light'),(52,26,2,'Hanglamp'),(53,27,1,'Standing lamp'),(54,27,2,'Vloerlamp'),(55,28,1,'Fullmounts'),(56,28,2,'Fullmounts'),(57,29,1,'Shoulder mounts'),(58,29,2,'Shoulder mounts'),(59,30,1,'Birds'),(60,30,2,'Vogels'),(61,31,1,'Insects'),(62,31,2,'Insecten'),(63,32,1,'Antler & Skulls'),(64,32,2,'Gewei & Schedels'),(65,33,1,'Animal hides'),(66,33,2,'Dierhuiden'),(67,34,1,'Various items'),(68,34,2,'Diverse naturalia'),(69,35,1,'Suitcase antique'),(70,35,2,'Koffer antiek'),(71,36,1,'Suitcase animal skin'),(72,36,2,'Koffer huid'),(73,37,1,'Suitcase leather'),(74,37,2,'Koffer leer'),(75,38,1,'Mirror wood'),(76,38,2,'Spiegel hout'),(77,39,1,'Mirror upholstered'),(78,39,2,'Spiegel bekleed'),(79,40,1,'Decoration article'),(80,40,2,'Decoratie artikel');
/*!40000 ALTER TABLE `articles_fixed_tables_sub_category_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_paid`
--

DROP TABLE IF EXISTS `articles_paid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles_paid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `order_article_id` int(11) DEFAULT NULL,
  `purchase_price` double NOT NULL,
  `selling_price` double NOT NULL,
  `profit_percents` double NOT NULL,
  `number_of_articles` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A7726C7FC14E7BC9` (`order_article_id`),
  KEY `IDX_A7726C7F7294869C` (`article_id`),
  KEY `IDX_A7726C7F12469DE2` (`category_id`),
  KEY `IDX_A7726C7FF7BFE87C` (`sub_category_id`),
  KEY `search_created` (`created`),
  KEY `search_category_sub_category_created` (`category_id`,`sub_category_id`,`created`),
  CONSTRAINT `FK_A7726C7F12469DE2` FOREIGN KEY (`category_id`) REFERENCES `articles_fixed_tables_category` (`id`),
  CONSTRAINT `FK_A7726C7F7294869C` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `FK_A7726C7FC14E7BC9` FOREIGN KEY (`order_article_id`) REFERENCES `orders_article` (`id`),
  CONSTRAINT `FK_A7726C7FF7BFE87C` FOREIGN KEY (`sub_category_id`) REFERENCES `articles_fixed_tables_sub_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles_paid`
--

LOCK TABLES `articles_paid` WRITE;
/*!40000 ALTER TABLE `articles_paid` DISABLE KEYS */;
/*!40000 ALTER TABLE `articles_paid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors`
--

DROP TABLE IF EXISTS `debtors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `end_user_type_id` int(11) DEFAULT NULL,
  `how_did_you_find_us_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `number_extension` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `btw` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `web_site` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `banking_account_number` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `total_purchase_price` double NOT NULL,
  `total_selling_price` double NOT NULL,
  `profit_percents` double NOT NULL,
  `communication_language` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_2A8D8D685E237E06` (`name`),
  UNIQUE KEY `UNIQ_2A8D8D689C631D21` (`banking_account_number`),
  UNIQUE KEY `address_contraint` (`street`,`number`,`number_extension`,`post_code`,`city`,`country`),
  KEY `IDX_2A8D8D685DF1F0D2` (`end_user_type_id`),
  KEY `IDX_2A8D8D68651949C` (`how_did_you_find_us_id`),
  KEY `search_email` (`email`),
  KEY `search_city` (`city`),
  KEY `search_name` (`name`),
  KEY `search_selling_price` (`total_selling_price`),
  KEY `search_purchase_price` (`total_purchase_price`),
  KEY `search_profit_percents` (`profit_percents`),
  CONSTRAINT `FK_2A8D8D685DF1F0D2` FOREIGN KEY (`end_user_type_id`) REFERENCES `debtors_fixed_tables_end_user_type` (`id`),
  CONSTRAINT `FK_2A8D8D68651949C` FOREIGN KEY (`how_did_you_find_us_id`) REFERENCES `debtors_fixed_tables_how_did_you_find_us` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors`
--

LOCK TABLES `debtors` WRITE;
/*!40000 ALTER TABLE `debtors` DISABLE KEYS */;
INSERT INTO `debtors` VALUES (1,3,3,'John Smith Dutch Company','info@dejonginterieur.com','ulica ','1','','18000','Nis','Netherlands','some btw','','12345678','2018-01-31 12:39:32','2018-02-08 10:38:36','admin',0,0,0,'en'),(2,3,1,'Willem-Jan Memerda','willemjan@memerda.com','Berghof ','10','','5671EH','Nuenen','Netherlands','NL143218256B01','Nvt','NL44ABNA12345678','2018-02-02 11:03:02','2018-02-02 11:03:02','admin',0,0,0,'nl'),(3,1,1,'John Smith Dutch Private','john.smith.sneg@gmail.com','ulica','1','3','11000','city','Netherlands','','','1234567890123','2018-02-08 10:43:51','2018-02-08 10:43:51','admin',0,0,0,'en'),(4,3,1,'John Smith Germany Company','john.smith.sneg@gmail.com','ulica ','2','4','11000','belgrade','Germany','german btw number','','123','2018-02-08 10:50:51','2018-02-08 10:50:51','admin',0,0,0,'en');
/*!40000 ALTER TABLE `debtors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_contact_person`
--

DROP TABLE IF EXISTS `debtors_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_id` int(11) DEFAULT NULL,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D449DB55B043EC6B` (`debtor_id`),
  KEY `name_debtor_indx` (`name`,`debtor_id`),
  CONSTRAINT `FK_D449DB55B043EC6B` FOREIGN KEY (`debtor_id`) REFERENCES `debtors` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_contact_person`
--

LOCK TABLES `debtors_contact_person` WRITE;
/*!40000 ALTER TABLE `debtors_contact_person` DISABLE KEYS */;
INSERT INTO `debtors_contact_person` VALUES (1,1,'John Smith','john.smith.sneg@gmail.com','+38112345678','2018-01-31 12:39:57','2018-01-31 12:39:57','admin'),(2,2,'Willem-Jan Memerda','willemjan@memerda.com','06-25047227','2018-02-02 11:05:36','2018-02-02 11:05:36','admin'),(3,3,'john smith contact ','john.smith.sneg@gmail.com','+381123456789','2018-02-08 10:44:41','2018-02-08 10:44:41','admin'),(4,4,'john smith in germany','john.smith.sneg@gmail.com','+38112345678','2018-02-08 10:51:17','2018-02-08 10:51:17','admin');
/*!40000 ALTER TABLE `debtors_contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_delivery_address`
--

DROP TABLE IF EXISTS `debtors_delivery_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_delivery_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_id` int(11) DEFAULT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `street` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `number_extension` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C3093222B043EC6B` (`debtor_id`),
  KEY `IDX_C30932224F8A983C` (`contact_person_id`),
  CONSTRAINT `FK_C30932224F8A983C` FOREIGN KEY (`contact_person_id`) REFERENCES `debtors_contact_person` (`id`),
  CONSTRAINT `FK_C3093222B043EC6B` FOREIGN KEY (`debtor_id`) REFERENCES `debtors` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_delivery_address`
--

LOCK TABLES `debtors_delivery_address` WRITE;
/*!40000 ALTER TABLE `debtors_delivery_address` DISABLE KEYS */;
INSERT INTO `debtors_delivery_address` VALUES (1,1,1,'ulica','1','','18000','Nis','Serbia','This is description','2018-01-31 12:40:31','2018-01-31 12:40:31','admin'),(4,3,3,'ulica','1','3','11000','city','Netherlands','here si some delivery address','2018-02-08 10:45:13','2018-02-08 10:45:13','admin'),(5,4,4,'ulica','2','4','11000','belgrade','Germany','this is some german guy','2018-02-08 10:51:54','2018-02-08 10:51:54','admin');
/*!40000 ALTER TABLE `debtors_delivery_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_delivery_address_phone_number`
--

DROP TABLE IF EXISTS `debtors_delivery_address_phone_number`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_delivery_address_phone_number` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_address_id` int(11) DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F2AF03F0EBF23851` (`delivery_address_id`),
  CONSTRAINT `FK_F2AF03F0EBF23851` FOREIGN KEY (`delivery_address_id`) REFERENCES `debtors_delivery_address` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_delivery_address_phone_number`
--

LOCK TABLES `debtors_delivery_address_phone_number` WRITE;
/*!40000 ALTER TABLE `debtors_delivery_address_phone_number` DISABLE KEYS */;
/*!40000 ALTER TABLE `debtors_delivery_address_phone_number` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_fixed_tables_end_user_type`
--

DROP TABLE IF EXISTS `debtors_fixed_tables_end_user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_fixed_tables_end_user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_4E0035E55E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_fixed_tables_end_user_type`
--

LOCK TABLES `debtors_fixed_tables_end_user_type` WRITE;
/*!40000 ALTER TABLE `debtors_fixed_tables_end_user_type` DISABLE KEYS */;
INSERT INTO `debtors_fixed_tables_end_user_type` VALUES (3,'Company'),(1,'Private'),(2,'Resale');
/*!40000 ALTER TABLE `debtors_fixed_tables_end_user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_fixed_tables_end_user_type_translations`
--

DROP TABLE IF EXISTS `debtors_fixed_tables_end_user_type_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_fixed_tables_end_user_type_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_end_user_type_id` int(11) DEFAULT NULL,
  `locale_id` int(11) DEFAULT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_23A38273A339886C` (`debtor_end_user_type_id`),
  KEY `IDX_23A38273E559DFD1` (`locale_id`),
  CONSTRAINT `FK_23A38273A339886C` FOREIGN KEY (`debtor_end_user_type_id`) REFERENCES `debtors_fixed_tables_end_user_type` (`id`),
  CONSTRAINT `FK_23A38273E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_fixed_tables_end_user_type_translations`
--

LOCK TABLES `debtors_fixed_tables_end_user_type_translations` WRITE;
/*!40000 ALTER TABLE `debtors_fixed_tables_end_user_type_translations` DISABLE KEYS */;
INSERT INTO `debtors_fixed_tables_end_user_type_translations` VALUES (1,1,1,'Private'),(2,1,2,'Privaat'),(3,1,3,'Privatgelände'),(4,2,1,'Resale'),(5,2,2,'Wederverkoop'),(6,2,3,'Wiederverkauf'),(7,3,1,'Company'),(8,3,2,'Bedrijf'),(9,3,3,'Unternehmen');
/*!40000 ALTER TABLE `debtors_fixed_tables_end_user_type_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_fixed_tables_how_did_you_find_us`
--

DROP TABLE IF EXISTS `debtors_fixed_tables_how_did_you_find_us`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_fixed_tables_how_did_you_find_us` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_6DDC6CE05E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_fixed_tables_how_did_you_find_us`
--

LOCK TABLES `debtors_fixed_tables_how_did_you_find_us` WRITE;
/*!40000 ALTER TABLE `debtors_fixed_tables_how_did_you_find_us` DISABLE KEYS */;
INSERT INTO `debtors_fixed_tables_how_did_you_find_us` VALUES (1,'Google'),(4,'Mouth to mouth'),(3,'Social Networks'),(2,'Worlds Fair');
/*!40000 ALTER TABLE `debtors_fixed_tables_how_did_you_find_us` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_fixed_tables_how_did_you_find_us_translations`
--

DROP TABLE IF EXISTS `debtors_fixed_tables_how_did_you_find_us_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_fixed_tables_how_did_you_find_us_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) DEFAULT NULL,
  `how_did_you_find_us_id` int(11) DEFAULT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_3E719352E559DFD1` (`locale_id`),
  KEY `IDX_3E719352651949C` (`how_did_you_find_us_id`),
  CONSTRAINT `FK_3E719352651949C` FOREIGN KEY (`how_did_you_find_us_id`) REFERENCES `debtors_fixed_tables_how_did_you_find_us` (`id`),
  CONSTRAINT `FK_3E719352E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_fixed_tables_how_did_you_find_us_translations`
--

LOCK TABLES `debtors_fixed_tables_how_did_you_find_us_translations` WRITE;
/*!40000 ALTER TABLE `debtors_fixed_tables_how_did_you_find_us_translations` DISABLE KEYS */;
INSERT INTO `debtors_fixed_tables_how_did_you_find_us_translations` VALUES (1,1,1,'Google'),(2,2,1,'Google'),(3,3,1,'Google'),(4,1,2,'Worlds Fair'),(5,2,2,'Wereldenbeurs'),(6,3,2,'Weltausstellung'),(7,1,3,'Social Networks'),(8,2,3,'Sociale netwerken'),(9,3,3,'Soziale Netzwerke'),(10,1,4,'Mouth to mouth'),(11,2,4,'Mond op mond'),(12,3,4,'Mund zu Mund');
/*!40000 ALTER TABLE `debtors_fixed_tables_how_did_you_find_us_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `debtors_notes`
--

DROP TABLE IF EXISTS `debtors_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtors_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_id` int(11) DEFAULT NULL,
  `note` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_C1B1ED4B043EC6B` (`debtor_id`),
  KEY `search_updated` (`updated`),
  CONSTRAINT `FK_C1B1ED4B043EC6B` FOREIGN KEY (`debtor_id`) REFERENCES `debtors` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `debtors_notes`
--

LOCK TABLES `debtors_notes` WRITE;
/*!40000 ALTER TABLE `debtors_notes` DISABLE KEYS */;
INSERT INTO `debtors_notes` VALUES (1,1,'let op klant krijgt 10% korting op alle orders','admin','2018-02-01 15:29:17','2018-02-01 15:29:17'),(2,2,'Klant wil graag uur vooraf worden gebeld','admin','2018-02-02 11:05:04','2018-02-02 11:05:04');
/*!40000 ALTER TABLE `debtors_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fixed_tables_locale`
--

DROP TABLE IF EXISTS `fixed_tables_locale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fixed_tables_locale` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_50DC57E5D4DB71B5` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fixed_tables_locale`
--

LOCK TABLES `fixed_tables_locale` WRITE;
/*!40000 ALTER TABLE `fixed_tables_locale` DISABLE KEYS */;
INSERT INTO `fixed_tables_locale` VALUES (1,'en'),(3,'ge'),(2,'nl');
/*!40000 ALTER TABLE `fixed_tables_locale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_id` int(11) DEFAULT NULL,
  `contact_person_id` int(11) DEFAULT NULL,
  `delivery_status_id` int(11) DEFAULT NULL,
  `payment_status_id` int(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  `order_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_datum` datetime NOT NULL,
  `order_supervisor` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `debtor_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `order_status_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL,
  `discount` double NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_purchase_amount` double NOT NULL,
  `total_selling_amount` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E52FFDEE551F0F81` (`order_number`),
  KEY `IDX_E52FFDEEB043EC6B` (`debtor_id`),
  KEY `IDX_E52FFDEE4F8A983C` (`contact_person_id`),
  KEY `IDX_E52FFDEE2F924C2F` (`delivery_status_id`),
  KEY `IDX_E52FFDEE28DE2F95` (`payment_status_id`),
  KEY `IDX_E52FFDEE5AA1164F` (`payment_method_id`),
  KEY `IDX_E52FFDEED7707B45` (`order_status_id`),
  KEY `search_order_supervisor` (`order_supervisor`),
  KEY `search_order_datum` (`order_datum`),
  KEY `search_debtor_name` (`debtor_name`),
  KEY `search_order_status_name` (`order_status_name`),
  KEY `search_delivery_time` (`delivery_time`),
  CONSTRAINT `FK_E52FFDEE28DE2F95` FOREIGN KEY (`payment_status_id`) REFERENCES `orders_fixed_tables_payment_status` (`id`),
  CONSTRAINT `FK_E52FFDEE2F924C2F` FOREIGN KEY (`delivery_status_id`) REFERENCES `orders_fixed_tables_delivery_status` (`id`),
  CONSTRAINT `FK_E52FFDEE4F8A983C` FOREIGN KEY (`contact_person_id`) REFERENCES `debtors_contact_person` (`id`),
  CONSTRAINT `FK_E52FFDEE5AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `orders_fixed_tables_payment_method` (`id`),
  CONSTRAINT `FK_E52FFDEEB043EC6B` FOREIGN KEY (`debtor_id`) REFERENCES `debtors` (`id`),
  CONSTRAINT `FK_E52FFDEED7707B45` FOREIGN KEY (`order_status_id`) REFERENCES `orders_fixed_tables_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,1,1,1,2,1,3,'lj2fbpZPDo','2018-01-31 13:17:47','admin','John Smith','offer-emailed','2018-02-21 00:00:00',0,'2018-01-31 13:17:47','2018-01-31 13:17:47',0,0),(2,1,1,1,2,1,7,'VYYIiF55el','2018-02-01 10:53:37','admin','John Smith','invoice-sent','2018-02-01 10:53:37',0,'2018-02-01 10:53:01','2018-02-01 10:53:37',200,936),(3,2,2,1,2,3,3,'ivXCrz1LL4','2018-02-02 11:09:10','admin','Willem-Jan Memerda','offer-emailed','2018-02-02 11:09:10',0,'2018-02-02 11:08:50','2018-02-02 11:09:10',100,200),(4,1,1,1,2,1,1,'7tfcT0dPgQ','2018-02-08 10:40:04','admin','John Smith Dutch Company','new order','2018-02-08 10:40:04',0,'2018-02-08 10:38:18','2018-02-08 10:40:04',230,460),(5,3,3,1,2,1,1,'3pypP3emS4','2018-02-08 10:46:09','admin','John Smith Dutch Private','new order','2018-02-25 00:00:00',0,'2018-02-08 10:46:09','2018-02-08 10:46:09',300,900),(6,4,4,1,2,1,1,'YuUmA8Mjic','2018-02-08 10:52:21','admin','John Smith Germany Company','new order','2018-02-28 00:00:00',0,'2018-02-08 10:52:21','2018-02-08 10:52:21',100,200);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_administrative_note`
--

DROP TABLE IF EXISTS `orders_administrative_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_administrative_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `note` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_B910C0348D9F6D38` (`order_id`),
  KEY `search_updated` (`updated`),
  CONSTRAINT `FK_B910C0348D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_administrative_note`
--

LOCK TABLES `orders_administrative_note` WRITE;
/*!40000 ALTER TABLE `orders_administrative_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_administrative_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_article`
--

DROP TABLE IF EXISTS `orders_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `to_supplier_status_id` int(11) DEFAULT NULL,
  `color` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `material` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `in_stock` int(11) NOT NULL,
  `order_from_supplier` int(11) NOT NULL,
  `order_article_purchase_price` double NOT NULL,
  `order_article_price` double NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `delivered` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_time` datetime NOT NULL,
  `commission` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F34F7C1D7294869C` (`article_id`),
  KEY `IDX_F34F7C1D8D9F6D38` (`order_id`),
  KEY `IDX_F34F7C1D2ADD6D8C` (`supplier_id`),
  KEY `IDX_F34F7C1DA3A08F0B` (`to_supplier_status_id`),
  KEY `search_delivery_time` (`delivery_time`),
  CONSTRAINT `FK_F34F7C1D2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`),
  CONSTRAINT `FK_F34F7C1D7294869C` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `FK_F34F7C1D8D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_F34F7C1DA3A08F0B` FOREIGN KEY (`to_supplier_status_id`) REFERENCES `orders_fixed_tables_to_supplier_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_article`
--

LOCK TABLES `orders_article` WRITE;
/*!40000 ALTER TABLE `orders_article` DISABLE KEYS */;
INSERT INTO `orders_article` VALUES (1,2,1,1,1,'red','iron',1,1,1,0,0,'2018-01-31 13:18:21','2018-01-31 13:18:21','admin','details','elivered','1900-01-01 00:00:00',''),(2,2,2,1,1,'bruin','ijzer',1,0,1,0,300,'2018-02-01 10:55:44','2018-02-01 10:55:44','admin','','','2018-02-15 00:00:00','jong'),(3,3,2,1,1,'natuur','ijzer met gewei',1,1,0,0,36,'2018-02-01 11:20:04','2018-02-01 11:20:04','admin','','','1900-01-01 00:00:00',''),(4,3,3,1,1,'zilver','Ijzer',2,1,1,0,0,'2018-02-02 11:09:42','2018-02-02 11:09:42','admin','','','1900-01-01 00:00:00',''),(5,2,3,1,1,'c','i',1,1,1,100,200,'2018-02-06 10:21:53','2018-02-06 10:21:53','admin','d','a','1900-01-01 00:00:00',''),(6,2,2,1,1,'red','iron',2,1,1,100,300,'2018-02-08 10:19:54','2018-02-08 10:19:54','admin','d','d','1900-01-01 00:00:00',''),(7,2,4,1,1,'red','iron',2,1,1,100,200,'2018-02-08 10:39:32','2018-02-08 10:39:32','admin','d','d','1900-01-01 00:00:00',''),(8,3,4,1,1,'red','iron',3,1,2,10,20,'2018-02-08 10:42:09','2018-02-08 10:42:09','admin','d','d','1900-01-01 00:00:00',''),(9,2,5,1,1,'red','iron',3,1,1,100,300,'2018-02-08 10:46:43','2018-02-08 10:46:43','admin','d','d','1900-01-01 00:00:00',''),(10,2,6,1,1,'','',1,1,0,100,200,'2018-02-08 10:53:01','2018-02-08 10:53:01','admin','d','d','1900-01-01 00:00:00','');
/*!40000 ALTER TABLE `orders_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_delivery_status`
--

DROP TABLE IF EXISTS `orders_fixed_tables_delivery_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_delivery_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_16106AC25E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_delivery_status`
--

LOCK TABLES `orders_fixed_tables_delivery_status` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_delivery_status` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_delivery_status` VALUES (2,'collected'),(4,'deliver'),(1,'new'),(3,'sent');
/*!40000 ALTER TABLE `orders_fixed_tables_delivery_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_delivery_status_translations`
--

DROP TABLE IF EXISTS `orders_fixed_tables_delivery_status_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_delivery_status_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) DEFAULT NULL,
  `delivery_status_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_A77408D4E559DFD1` (`locale_id`),
  KEY `IDX_A77408D42F924C2F` (`delivery_status_id`),
  CONSTRAINT `FK_A77408D42F924C2F` FOREIGN KEY (`delivery_status_id`) REFERENCES `orders_fixed_tables_delivery_status` (`id`),
  CONSTRAINT `FK_A77408D4E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_delivery_status_translations`
--

LOCK TABLES `orders_fixed_tables_delivery_status_translations` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_delivery_status_translations` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_delivery_status_translations` VALUES (1,1,1,'new'),(2,2,1,'nieuwe'),(3,3,1,'neu'),(4,1,2,'collected'),(5,2,2,'verzamelde'),(6,3,2,'gesammelt'),(7,1,3,'sent'),(8,2,3,'verzonden'),(9,3,3,'geschickt'),(10,1,4,'deliver'),(11,2,4,'leveren'),(12,3,4,'liefern');
/*!40000 ALTER TABLE `orders_fixed_tables_delivery_status_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_mail_status`
--

DROP TABLE IF EXISTS `orders_fixed_tables_mail_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_mail_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_DCC819E95E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_mail_status`
--

LOCK TABLES `orders_fixed_tables_mail_status` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_mail_status` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_mail_status` VALUES (3,'invoice-sent'),(1,'offer-emailed'),(2,'order-confirmation-emailed'),(4,'order-from-supplier');
/*!40000 ALTER TABLE `orders_fixed_tables_mail_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_mail_status_translations`
--

DROP TABLE IF EXISTS `orders_fixed_tables_mail_status_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_mail_status_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) DEFAULT NULL,
  `order_mail_status_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_7A163810E559DFD1` (`locale_id`),
  KEY `IDX_7A1638107B5DCE6E` (`order_mail_status_id`),
  CONSTRAINT `FK_7A1638107B5DCE6E` FOREIGN KEY (`order_mail_status_id`) REFERENCES `orders_fixed_tables_mail_status` (`id`),
  CONSTRAINT `FK_7A163810E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_mail_status_translations`
--

LOCK TABLES `orders_fixed_tables_mail_status_translations` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_mail_status_translations` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_mail_status_translations` VALUES (1,1,4,'order to supplier'),(2,2,4,'bestelling naar leverancier'),(3,3,4,'Bestellung an den Lieferanten'),(4,1,1,'offer emailed'),(5,2,1,'bieden gemaild'),(6,3,1,'angebot per e-mail'),(7,1,2,'order confirmation emailed'),(8,2,2,'orderbevestiging gemaild'),(9,3,2,'auftragsbestätigung per e-mail'),(10,1,3,'invoice sent'),(11,2,3,'factuur verzonden'),(12,3,3,'rechnung versandt');
/*!40000 ALTER TABLE `orders_fixed_tables_mail_status_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_payment_method`
--

DROP TABLE IF EXISTS `orders_fixed_tables_payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_A68172715E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_payment_method`
--

LOCK TABLES `orders_fixed_tables_payment_method` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_payment_method` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_payment_method` VALUES (3,'bank'),(4,'cash'),(1,'credit card'),(2,'pin');
/*!40000 ALTER TABLE `orders_fixed_tables_payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_payment_method_translations`
--

DROP TABLE IF EXISTS `orders_fixed_tables_payment_method_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_payment_method_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) DEFAULT NULL,
  `payment_method_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_21D6928DE559DFD1` (`locale_id`),
  KEY `IDX_21D6928D5AA1164F` (`payment_method_id`),
  CONSTRAINT `FK_21D6928D5AA1164F` FOREIGN KEY (`payment_method_id`) REFERENCES `orders_fixed_tables_payment_method` (`id`),
  CONSTRAINT `FK_21D6928DE559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_payment_method_translations`
--

LOCK TABLES `orders_fixed_tables_payment_method_translations` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_payment_method_translations` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_payment_method_translations` VALUES (1,1,1,'credit card'),(2,2,1,'kredietkaart'),(3,3,1,'Kreditkarte'),(4,1,2,'PIN'),(5,2,2,'PIN'),(6,3,2,'PIN'),(7,1,3,'bank'),(8,2,3,'bank'),(9,3,3,'Bank'),(10,1,4,'cash'),(11,2,4,'contant geld'),(12,3,4,'Kasse');
/*!40000 ALTER TABLE `orders_fixed_tables_payment_method_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_payment_status`
--

DROP TABLE IF EXISTS `orders_fixed_tables_payment_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_payment_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_83D82D0D5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_payment_status`
--

LOCK TABLES `orders_fixed_tables_payment_status` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_payment_status` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_payment_status` VALUES (2,'not paid'),(1,'paid');
/*!40000 ALTER TABLE `orders_fixed_tables_payment_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_payment_status_translations`
--

DROP TABLE IF EXISTS `orders_fixed_tables_payment_status_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_payment_status_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) DEFAULT NULL,
  `order_payment_status_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_E5F0B9BAE559DFD1` (`locale_id`),
  KEY `IDX_E5F0B9BA3CBD65CE` (`order_payment_status_id`),
  CONSTRAINT `FK_E5F0B9BA3CBD65CE` FOREIGN KEY (`order_payment_status_id`) REFERENCES `orders_fixed_tables_payment_status` (`id`),
  CONSTRAINT `FK_E5F0B9BAE559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_payment_status_translations`
--

LOCK TABLES `orders_fixed_tables_payment_status_translations` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_payment_status_translations` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_payment_status_translations` VALUES (1,1,1,'paid'),(2,2,1,'betaald'),(3,3,1,'bezahlt'),(4,1,2,'not paid'),(5,2,2,'niet betaald'),(6,3,2,'nicht bezahlt');
/*!40000 ALTER TABLE `orders_fixed_tables_payment_status_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_status`
--

DROP TABLE IF EXISTS `orders_fixed_tables_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_827A3AC5E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_status`
--

LOCK TABLES `orders_fixed_tables_status` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_status` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_status` VALUES (6,'canceled'),(7,'invoice-sent'),(1,'new order'),(2,'offer'),(3,'offer-emailed'),(4,'order-confirmation'),(5,'order-confirmation-emailed');
/*!40000 ALTER TABLE `orders_fixed_tables_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_status_translations`
--

DROP TABLE IF EXISTS `orders_fixed_tables_status_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_status_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) DEFAULT NULL,
  `order_status_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_A65AD37FE559DFD1` (`locale_id`),
  KEY `IDX_A65AD37FD7707B45` (`order_status_id`),
  CONSTRAINT `FK_A65AD37FD7707B45` FOREIGN KEY (`order_status_id`) REFERENCES `orders_fixed_tables_status` (`id`),
  CONSTRAINT `FK_A65AD37FE559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_status_translations`
--

LOCK TABLES `orders_fixed_tables_status_translations` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_status_translations` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_status_translations` VALUES (1,1,1,'new order'),(2,2,1,'nieuwe bestelling'),(3,3,1,'neue bestellung'),(4,1,2,'offer'),(5,2,2,'aanbod'),(6,3,2,'angebot'),(7,1,3,'offer emailed'),(8,2,3,'bieden gemaild'),(9,3,3,'angebot per e-mail'),(10,1,4,'order confirmation'),(11,2,4,'order bevestiging'),(12,3,4,'bestellbestätigung'),(13,1,5,'order confirmation emailed'),(14,2,5,'orderbevestiging gemaild'),(15,3,5,'auftragsbestätigung per e-mail'),(16,1,6,'canceled'),(17,2,6,'geannuleerd'),(18,3,6,'abgebrochen'),(19,1,7,'invoice sent'),(20,2,7,'factuur verzonden'),(21,3,7,'rechnung versandt');
/*!40000 ALTER TABLE `orders_fixed_tables_status_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_to_supplier_status`
--

DROP TABLE IF EXISTS `orders_fixed_tables_to_supplier_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_to_supplier_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_78E3ABC75E237E06` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_to_supplier_status`
--

LOCK TABLES `orders_fixed_tables_to_supplier_status` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_to_supplier_status` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_to_supplier_status` VALUES (2,'collected'),(4,'deliver'),(5,'finished'),(1,'new'),(3,'sent');
/*!40000 ALTER TABLE `orders_fixed_tables_to_supplier_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_fixed_tables_to_supplier_status_translations`
--

DROP TABLE IF EXISTS `orders_fixed_tables_to_supplier_status_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_fixed_tables_to_supplier_status_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale_id` int(11) DEFAULT NULL,
  `order_to_supplier_status_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_locale_constraint` (`name`,`locale_id`),
  KEY `IDX_AE551A16E559DFD1` (`locale_id`),
  KEY `IDX_AE551A1696ABE3A7` (`order_to_supplier_status_id`),
  CONSTRAINT `FK_AE551A1696ABE3A7` FOREIGN KEY (`order_to_supplier_status_id`) REFERENCES `orders_fixed_tables_to_supplier_status` (`id`),
  CONSTRAINT `FK_AE551A16E559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `fixed_tables_locale` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_fixed_tables_to_supplier_status_translations`
--

LOCK TABLES `orders_fixed_tables_to_supplier_status_translations` WRITE;
/*!40000 ALTER TABLE `orders_fixed_tables_to_supplier_status_translations` DISABLE KEYS */;
INSERT INTO `orders_fixed_tables_to_supplier_status_translations` VALUES (1,1,1,'new'),(2,2,1,'nieuwe'),(3,3,1,'neu'),(4,1,2,'collected'),(5,2,2,'verzamelde'),(6,3,2,'gesammelt'),(7,1,3,'sent'),(8,2,3,'verzonden'),(9,3,3,'geschickt'),(10,1,4,'deliver'),(11,2,4,'leveren'),(12,3,4,'liefern'),(13,1,5,'finished'),(14,2,5,'afgewerkt'),(15,3,5,'fertig');
/*!40000 ALTER TABLE `orders_fixed_tables_to_supplier_status_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_intern_note`
--

DROP TABLE IF EXISTS `orders_intern_note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_intern_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `note` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_D47E17348D9F6D38` (`order_id`),
  KEY `search_updated` (`updated`),
  CONSTRAINT `FK_D47E17348D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_intern_note`
--

LOCK TABLES `orders_intern_note` WRITE;
/*!40000 ALTER TABLE `orders_intern_note` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_intern_note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_mail`
--

DROP TABLE IF EXISTS `orders_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_mail_status_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `order_article_id` int(11) DEFAULT NULL,
  `send_to` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mail_subject` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `mail_body` varchar(20000) COLLATE utf8_unicode_ci NOT NULL,
  `attachment_file_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_FFF36DE37B5DCE6E` (`order_mail_status_id`),
  KEY `IDX_FFF36DE38D9F6D38` (`order_id`),
  KEY `IDX_FFF36DE3C14E7BC9` (`order_article_id`),
  KEY `search_order_article_mail_status` (`order_article_id`,`order_mail_status_id`),
  CONSTRAINT `FK_FFF36DE37B5DCE6E` FOREIGN KEY (`order_mail_status_id`) REFERENCES `orders_fixed_tables_mail_status` (`id`),
  CONSTRAINT `FK_FFF36DE38D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `FK_FFF36DE3C14E7BC9` FOREIGN KEY (`order_article_id`) REFERENCES `orders_article` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_mail`
--

LOCK TABLES `orders_mail` WRITE;
/*!40000 ALTER TABLE `orders_mail` DISABLE KEYS */;
INSERT INTO `orders_mail` VALUES (1,1,1,NULL,'john.smith.sneg@gmail.com','','Dear Sir, Madam\r\n\r\nThank you for your inquiry.\r\nIn the attachment you will find our offer.\r\nIf you have any questions or comments regarding this offer, please feel free to contact us.\r\n\r\n\r\nWith kind regards\r\n\r\nStijn van Limpt\r\nPhone Number: ','qG46vr0FJoGVvoq4Bym9kmaBbQhE0r.pdf','2018-02-01 06:18:19'),(2,1,2,NULL,'info@dejonginterieur.com','test','Dear Sir, Madam\r\n\r\nThank you for your inquiry.\r\nIn the attachment you will find our offer.\r\nIf you have any questions or comments regarding this offer, please feel free to contact us.\r\n\r\n\r\nWith kind regards\r\n\r\nStijn van Limpt\r\nPhone Number: ','YRrVuJOVcj6hZSY2C0fXm7fI5S5XXo.pdf','2018-02-01 11:06:44'),(3,4,2,2,'john.smith.sneg@gmail.com','','undefined\r\n\r\nWith kind regards\r\n\r\nStijn van Limpt\r\nPhone Number: ','n6WDeI0hdJdtaeRaTdSHSt5moh0u3p.pdf','2018-02-01 11:08:25'),(4,1,2,NULL,'info@dejonginterieur.com','test','Beste Heer, mevrouw.\r\n\r\nHartelijk dank voor uw aanvraag.\r\nIn de bijlage vindt u onze offerte.\r\nAls u vragen en/ of opmerkingen heeft over deze offerte, dan horen we het graag.\r\n\r\nHello Stijn just tesing email features.\r\n\r\nAleksandar Cvetkovic\r\n\r\nMet vriendelijke groet\r\n\r\nStijn van Limpt\r\nTelefoonnummer: ','mVfORJyGFKZM2ZE01h1hfnpv6unbrl.pdf','2018-02-01 12:11:33'),(5,1,3,NULL,'willemjan@memerda.com','Orderbevestiging','Beste Heer, mevrouw.\r\n\r\nHartelijk dank voor uw aanvraag.\r\nIn de bijlage vindt u onze offerte.\r\nAls u vragen en/ of opmerkingen heeft over deze offerte, dan horen we het graag.\r\n\r\n\r\n\r\nMet vriendelijke groet\r\n\r\nWillem-Jan Memerda\r\nTelefoonnummer: 06-25047227','9gjUVdt9d6PXAOGNVYGbYYHWA9vCUh.pdf','2018-02-02 11:10:58'),(6,3,2,NULL,'john.smith.sneg@gmail.com','hello factire','Dear Sir, Madam\r\n\r\n Thank you for the order.\r\nIn the attachment you will find our invoice.\r\nAfter we have received your payment we will send you the goods (at the end of the invoice you will find our payment details).\r\nPlease let us know if you have anymore questions.\r\n\r\n\r\nWith kind regards\r\n\r\nStijn van Limpt\r\nPhone Number: ','OG0m8UtAXuBJuO1sKRr3vyuzqjiFWi.pdf','2018-02-07 22:13:55'),(7,3,2,NULL,'john.smith.sneg@gmail.com','new invoice','Dear Sir, Madam\r\n\r\n Thank you for the order.\r\nIn the attachment you will find our invoice.\r\nAfter we have received your payment we will send you the goods (at the end of the invoice you will find our payment details).\r\nPlease let us know if you have anymore questions.\r\n\r\nhelo helo\r\n\r\nWith kind regards\r\n\r\nStijn van Limpt\r\nPhone Number: ','d7w3HDXDFUFgFxztkNmeGf4ijSHVGW.pdf','2018-02-07 22:27:09'),(8,3,2,NULL,'john.smith.sneg@gmail.com','','Dear Sir, Madam\r\n\r\n Thank you for the order.\r\nIn the attachment you will find our invoice.\r\nAfter we have received your payment we will send you the goods (at the end of the invoice you will find our payment details).\r\nPlease let us know if you have anymore questions.\r\n\r\n\r\nWith kind regards\r\n\r\nStijn van Limpt\r\nPhone Number: ','JLYJ77EBIC5jlA7n2SeuVK2qjtKB8G.pdf','2018-02-07 23:12:58');
/*!40000 ALTER TABLE `orders_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_paid`
--

DROP TABLE IF EXISTS `orders_paid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_paid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `debtor_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `purchase_price` double NOT NULL,
  `selling_price` double NOT NULL,
  `profit_percent` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_535B6A938D9F6D38` (`order_id`),
  KEY `IDX_535B6A93B043EC6B` (`debtor_id`),
  KEY `search_created` (`created`),
  KEY `search_debtor_created` (`debtor_id`,`created`),
  CONSTRAINT `FK_535B6A938D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `FK_535B6A93B043EC6B` FOREIGN KEY (`debtor_id`) REFERENCES `debtors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_paid`
--

LOCK TABLES `orders_paid` WRITE;
/*!40000 ALTER TABLE `orders_paid` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders_paid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `number_extension` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `post_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `communication_language` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_AC28B95C5E237E06` (`name`),
  UNIQUE KEY `address_contraint` (`street`,`number`,`number_extension`,`post_code`,`city`,`country`),
  KEY `search_name` (`name`),
  KEY `search_email` (`email`),
  KEY `search_post_code` (`post_code`),
  KEY `search_city` (`city`),
  KEY `search_country` (`country`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'John Smith Supplier','john.smith.sneg@gmail.com','ulica','1','','18000','Nis','Serbia','2018-01-31 12:41:10','2018-02-01 12:09:41','admin','en');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers_contact_person`
--

DROP TABLE IF EXISTS `suppliers_contact_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers_contact_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A129C3B92ADD6D8C` (`supplier_id`),
  KEY `name_supplier_indx` (`name`,`supplier_id`),
  CONSTRAINT `FK_A129C3B92ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers_contact_person`
--

LOCK TABLES `suppliers_contact_person` WRITE;
/*!40000 ALTER TABLE `suppliers_contact_person` DISABLE KEYS */;
INSERT INTO `suppliers_contact_person` VALUES (1,1,'John Smith','john.smith.sneg@gmail.com','+38112345678','2018-01-31 12:41:44','2018-01-31 12:41:44','admin');
/*!40000 ALTER TABLE `suppliers_contact_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers_notes`
--

DROP TABLE IF EXISTS `suppliers_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `note` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_585CCB2C2ADD6D8C` (`supplier_id`),
  KEY `search_updated` (`updated`),
  CONSTRAINT `FK_585CCB2C2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers_notes`
--

LOCK TABLES `suppliers_notes` WRITE;
/*!40000 ALTER TABLE `suppliers_notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `suppliers_notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_connection`
--

DROP TABLE IF EXISTS `test_connection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_connection` (
  `first_name` varchar(20) DEFAULT NULL,
  `last_name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_connection`
--

LOCK TABLES `test_connection` WRITE;
/*!40000 ALTER TABLE `test_connection` DISABLE KEYS */;
INSERT INTO `test_connection` VALUES ('first_name','last_name');
/*!40000 ALTER TABLE `test_connection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_54FCD59FA76ED395` (`user_id`),
  CONSTRAINT `FK_54FCD59FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `app_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1,'ROLE_SUPER_ADMIN'),(2,2,'ROLE_ADMIN');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-12 11:31:34
