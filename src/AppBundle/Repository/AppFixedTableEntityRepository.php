<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/18/2017
 * Time: 1:31 AM
 */

namespace AppBundle\Repository;

use AppBundle\Repository\Exceptions\TableDoesNotExistsException;
use Doctrine\ORM\EntityManagerInterface;

abstract class AppFixedTableEntityRepository extends AppEntityRepository
{
    /**
     * @return string
     * @throws TableDoesNotExistsException
     */
    public function create_table() : string
    {
        $table_name = $this->getClassMetadata()->getTableName();
        $schema_manager = $this->getEntityManager()->getConnection()->getSchemaManager();
        $tables = $schema_manager->listTableNames();
        if (!in_array($table_name, $tables)) {
            throw new TableDoesNotExistsException(_('Table does not exist'));
        }
        $table_rows = $this->get_table_rows_without_id();
        $entity_name = $this->getEntityName();
        foreach (constant($entity_name . '::TABLE_ROWS') as $table_row) {
            if (!in_array($table_row, $table_rows)) {
                $entity = static::from_model($table_row, $this->getEntityManager());
                $this->getEntityManager()->persist($entity);
            }
        }
        $this->getEntityManager()->flush();
        return $table_name;
    }
    /**
     * @return array
     */
    abstract public function get_table_rows_without_id() : array;
    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return mixed
     */
    abstract public static function from_model(array $data, EntityManagerInterface $em);
}