<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/2/2017
 * Time: 7:15 PM
 */

namespace AppBundle\Repository;

use AppBundle\Repository\Exceptions\ColumnDoesNotHaveIndexException;
use AppBundle\Repository\Exceptions\OrderByDirectionException;
use Doctrine\ORM\EntityRepository;

class AppEntityRepository extends EntityRepository
{
    protected function getDefaultPageNumber(): int
    {
        return 1;
    }

    protected function getDefaultPageSize(): int
    {
        return 12;
    }

    protected function getDefaultSortDirection() : string
    {
        return 'asc';
    }

    protected function getPrimaryKeyColumnName() : string
    {
        return $this->getClassMetadata()->getSingleIdentifierColumnName();
    }

    protected function check_ordering_consistency(string $sort_by = null, string $sort_direction = null)
    {
        if($sort_direction != 'asc' && $sort_direction != 'desc') {
            throw new OrderByDirectionException(_('Order direction can be only ascending - asc, descending - desc'));
        }
        $table = $this->getClassMetadata()->getTableName();
        $indexes = $this->getEntityManager()->getConnection()->getSchemaManager()->listTableIndexes($table);
        $index_found = false;
        foreach($indexes as $index) {
            $columns  = $index->getColumns();
            if(sizeof($columns) == 1) {
                if(in_array($sort_by, $columns)) {
                    $index_found = true;
                    break;
                }
            }
        }
        if(!$index_found) {
            throw new ColumnDoesNotHaveIndexException(_('Column does not have an index'));
        }
    }

    protected function getTotalNumberOfRecords() : int
    {
        $query = "SELECT COUNT(1) FROM " . $this->getClassName() . " d";
        return $this->getEntityManager()->createQuery($query)
            ->getSingleScalarResult();
    }

    protected function getTotalNumberOfPages(int $page_size) : int
    {
        $query = "SELECT COUNT(1) FROM " . $this->getClassName() . " d";
        $num_of_records = $this->getEntityManager()->createQuery($query)
            ->getSingleScalarResult();
        return ceil($num_of_records/floatval($page_size));
    }
}