<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/17/2017
 * Time: 4:33 PM
 */

namespace AppBundle\Repository\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class LocaleRepository extends AppFixedTableEntityRepository
{
    public function get_supported_languages() : array
    {
        return LocaleEntity::LANGUAGES;
    }

    protected function get_database_languages() : array
    {
        $query = "SELECT l.language FROM " . $this->getClassName() . " l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    public function get_table_rows_without_id() : array
    {
        return $this->get_database_languages();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return LocaleEntity
     */
    public static function from_model(array $data, EntityManagerInterface $em) : LocaleEntity
    {
        $locale_entity = new LocaleEntity();
        $locale_entity->setLanguage($data['language']);
        return $locale_entity;
    }
}