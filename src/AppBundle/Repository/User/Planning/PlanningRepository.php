<?php
namespace AppBundle\Repository\User\Planning;

use AppBundle\Repository\AppEntityRepository;

class PlanningRepository extends AppEntityRepository
{
    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return array
     */
    public function get_title_dates(\DateTime $start, \DateTime $end)
    {
        $query = "SELECT p FROM " . $this->getClassName() . " p WHERE p.date >= '" . $start->format('Y-m-d') . "' AND p.date <= '" . $end->format('Y-m-d') . "'";
        $planning_titles = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        $titles_ret = [];
        foreach($planning_titles as $title){
            $titles_ret[] = [
                'id' => $title['id'],
                'title' => $title['title'],
                'start' => $title['date']->format('Y-m-d'),
                'end' => $title['date']->format('Y-m-d'),
                'color' => '#7b0022',
                'name' => 'title',
                'background' => '#7b0022',
            ];
        }
        return $titles_ret;
    }

    /**
     * @param int $title_id
     * @return array
     */
    public function get_title(int $title_id)
    {
        $planing = $this->find(['id' => $title_id]);
        $data = [
            'id' => $planing->getId(),
            'title' => $planing->getTitle(),
            'date' => $planing->getDateString()
        ];
        return $data;
    }
}