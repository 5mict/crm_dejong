<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/13/2017
 * Time: 9:48 PM
 */

namespace AppBundle\Repository\User\Suppliers;

use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Repository\AppEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\BrowserKit\Request;

class SupplierRepository extends AppEntityRepository
{
    /**
     * @param int $page_number
     * @param int $page_size
     * @param string|null $sort_by
     * @param string|null $sort_direction
     * @return array
     */
    public function get_suppliers(int $page_number, int $page_size, string $sort_by = null, string $sort_direction = null) : array
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getPrimaryKeyColumnName();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $suppliers = $this->findBy([], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$suppliers, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction];
    }

    /**
     * @return array
     */
    public function get_suppliersData()
    {
       return $this->findAll();
    }

    /**
     * @return array
     */
    public function get_supplier_names()
    {
        $query = "SELECT s.name FROM " . $this->getClassName() . " s";
        $supplier_names = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return array_column($supplier_names, 'name');
    }
}