<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/11/2018
 * Time: 9:30 PM
 */

namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Controller\User\Orders\Exceptions\OrderStatusNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderToSupplierStatusNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderToSupplierStatusTranslationRepository extends AppFixedTableEntityRepository
{
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name, l.language FROM " . $this->getClassName() . " e JOIN e.locale l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderToSupplierStatusTranslationEntity
     * @throws LocaleEntityNotFoundException
     * @throws OrderToSupplierStatusNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderToSupplierStatusTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $order_to_supplier_status_entity = $em->getRepository(OrderToSupplierStatusEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($order_to_supplier_status_entity)) {
            throw new OrderToSupplierStatusNotFoundException(_('Order to supplier status not found'));
        }
        $order_to_supplier_status_translation_entity = $em->getRepository(OrderToSupplierStatusTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($order_to_supplier_status_translation_entity)) {
            $order_to_supplier_status_translation_entity = new OrderToSupplierStatusTranslationEntity();
            $order_to_supplier_status_translation_entity->setName($data['name']);
            $order_to_supplier_status_translation_entity->setLocale($locale_entity);
        }
        $order_to_supplier_status_translation_entity->setOrderStatus($order_to_supplier_status_entity);
        return $order_to_supplier_status_translation_entity;
    }
}