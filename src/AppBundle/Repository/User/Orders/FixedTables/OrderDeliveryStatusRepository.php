<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 1:17 PM
 */

namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderDeliveryStatusRepository extends AppFixedTableEntityRepository
{
    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT ds.name as name, dst.name as trans FROM \AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusTranslationEntity dst JOIN dst.delivery_status ds WHERE dst.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name FROM " . $this->getClassName() . " e";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderDeliveryStatusEntity
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderDeliveryStatusEntity
    {
        $order_delivery_status_entity = $em->getRepository(OrderDeliveryStatusEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($order_delivery_status_entity)) {
            $order_delivery_status_entity = new OrderDeliveryStatusEntity();
            $order_delivery_status_entity->setName($data['name']);
        }
        return $order_delivery_status_entity;
    }
}