<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/11/2018
 * Time: 9:26 PM
 */

namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderToSupplierStatusRepository extends AppFixedTableEntityRepository
{
    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT os.name as name, ost.name as trans FROM \AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusTranslationEntity ost JOIN ost.order_to_supplier_status os WHERE ost.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name FROM " . $this->getClassName() . " e";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderToSupplierStatusEntity
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderToSupplierStatusEntity
    {
        $order_to_supplier_status_entity = $em->getRepository(OrderToSupplierStatusEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($order_to_supplier_status_entity)) {
            $order_to_supplier_status_entity = new OrderToSupplierStatusEntity();
            $order_to_supplier_status_entity->setName($data['name']);
        }
        return $order_to_supplier_status_entity;
    }
}