<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 1:44 PM
 */

namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Controller\User\Orders\Exceptions\OrderMailStatusEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderPaymentMethodEntityNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderPaymentMethodTranslationRepository extends AppFixedTableEntityRepository
{
    /**
     * @return array
     */
    public function get_table_rows_without_id(): array
    {
        $query = "SELECT e.name, l.language FROM " . $this->getClassName() . " e JOIN e.locale l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }
    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderPaymentMethodTranslationEntity
     * @throws LocaleEntityNotFoundException
     * @throws OrderPaymentMethodEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderPaymentMethodTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $order_payment_method_entity = $em->getRepository(OrderPaymentMethodEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($order_payment_method_entity)) {
            throw new OrderPaymentMethodEntityNotFoundException(_('Payment method entity not found'));
        }
        $order_payment_method_translation_entity = $em->getRepository(OrderPaymentMethodTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($order_payment_method_translation_entity)) {
            $order_payment_method_translation_entity = new OrderPaymentMethodTranslationEntity();
            $order_payment_method_translation_entity->setName($data['name']);
            $order_payment_method_translation_entity->setLocale($locale_entity);
        }
        $order_payment_method_translation_entity->setPaymentMethod($order_payment_method_entity);
        return $order_payment_method_translation_entity;
    }
}