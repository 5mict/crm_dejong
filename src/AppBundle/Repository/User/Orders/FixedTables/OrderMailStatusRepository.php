<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/25/2017
 * Time: 5:34 PM
 */

namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Controller\User\Orders\Exceptions\OrderMailStatusEntityNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderMailStatusRepository extends AppFixedTableEntityRepository
{

    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT ds.name as name, dst.name as trans FROM \AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusTranslationEntity dst JOIN dst.delivery_status ds WHERE dst.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name FROM " . $this->getClassName() . " e";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderMailStatusEntity
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderMailStatusEntity
    {
        $order_mail_status_entity = $em->getRepository(OrderMailStatusEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($order_delivery_status_entity)) {
            $order_mail_status_entity = new OrderMailStatusEntity();
            $order_mail_status_entity->setName($data['name']);
        }
        return $order_mail_status_entity;
    }
}
