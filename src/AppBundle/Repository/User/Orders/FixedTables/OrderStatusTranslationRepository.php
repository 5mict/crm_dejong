<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 4:07 PM
 */
namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Controller\User\Orders\Exceptions\OrderStatusNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderStatusTranslationRepository extends AppFixedTableEntityRepository
{
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name, l.language FROM " . $this->getClassName() . " e JOIN e.locale l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderStatusTranslationEntity
     * @throws LocaleEntityNotFoundException
     * @throws OrderStatusNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderStatusTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $order_status_entity = $em->getRepository(OrderStatusEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($order_status_entity)) {
            throw new OrderStatusNotFoundException(_('Order status not found'));
        }
        $order_status_translation_entity = $em->getRepository(OrderStatusTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($order_status_translation_entity)) {
            $order_status_translation_entity = new OrderStatusTranslationEntity();
            $order_status_translation_entity->setName($data['name']);
            $order_status_translation_entity->setLocale($locale_entity);
        }
        $order_status_translation_entity->setOrderStatus($order_status_entity);
        return $order_status_translation_entity;
    }
}