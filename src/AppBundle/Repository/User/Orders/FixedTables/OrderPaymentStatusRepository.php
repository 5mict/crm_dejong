<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 1:51 PM
 */
namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Controller\User\Orders\Exceptions\OrderMailStatusEntityNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderPaymentStatusRepository extends AppFixedTableEntityRepository
{

    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT ps.name as name, pst.name as trans FROM \AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusTranslationEntity pst JOIN pst.order_payment_status ps WHERE pst.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name FROM " . $this->getClassName() . " e";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderPaymentStatusEntity
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderPaymentStatusEntity
    {
        $order_payment_status_entity = $em->getRepository(OrderPaymentStatusEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($order_payment_status_entity)) {
            $order_payment_status_entity = new OrderPaymentStatusEntity();
            $order_payment_status_entity->setName($data['name']);
        }
        return $order_payment_status_entity;
    }
}
