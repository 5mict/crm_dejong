<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 4:06 PM
 */

namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Controller\User\Orders\Exceptions\OrderDeliveryStatusEntityNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderDeliveryStatusTranslationRepository extends AppFixedTableEntityRepository
{
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name, l.language FROM " . $this->getClassName() . " e JOIN e.locale l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderDeliveryStatusTranslationEntity
     * @throws LocaleEntityNotFoundException
     * @throws OrderDeliveryStatusEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderDeliveryStatusTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $order_delivery_status_entity = $em->getRepository(OrderDeliveryStatusEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($order_delivery_status_entity)) {
            throw new OrderDeliveryStatusEntityNotFoundException(_('Delivery status entity not found'));
        }
        $order_delivery_status_translation_entity = $em->getRepository(OrderDeliveryStatusTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($order_delivery_status_translation_entity)) {
            $order_delivery_status_translation_entity = new OrderDeliveryStatusTranslationEntity();
            $order_delivery_status_translation_entity->setName($data['name']);
            $order_delivery_status_translation_entity->setLocale($locale_entity);
        }
        $order_delivery_status_translation_entity->setDeliveryStatus($order_delivery_status_entity);
        return $order_delivery_status_translation_entity;
    }
}