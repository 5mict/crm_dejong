<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 1:40 PM
 */

namespace AppBundle\Repository\User\Orders\FixedTables;

use AppBundle\Controller\User\Orders\Exceptions\OrderMailStatusEntityNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class OrderPaymentMethodRepository extends AppFixedTableEntityRepository
{
    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT pm.name as name, pmt.name as trans FROM \AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodTranslationEntity pmt JOIN pmt.payment_method pm WHERE pmt.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name FROM " . $this->getClassName() . " e";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }
    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return OrderPaymentMethodEntity
     */
    public static function from_model(array $data, EntityManagerInterface $em) : OrderPaymentMethodEntity
    {
        $order_payment_method_entity = $em->getRepository(OrderPaymentMethodEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($order_payment_method_entity)) {
            $order_payment_method_entity = new OrderPaymentMethodEntity();
            $order_payment_method_entity->setName($data['name']);
        }
        return $order_payment_method_entity;
    }
}
