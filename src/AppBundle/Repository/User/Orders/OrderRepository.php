<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/8/2017
 * Time: 8:40 PM
 */

namespace AppBundle\Repository\User\Orders;

use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Repository\AppEntityRepository;

/**
 * Class OrderRepository
 * @package AppBundle\Repository\User\Orders
 */
class OrderRepository extends AppEntityRepository
{
    /**
     * @return int
     */
    public function getDefaultPageSize(): int
    {
        return 12;
    }
    /**
     * @return string
     */
    public function getDefaultSortingColumn() : string
    {
        return 'order_datum';
    }
    /**
     * @return string
     */
    public function getDefaultSortDirection(): string
    {
        return 'desc';
    }
    /**
     * @param int $page_number
     * @param int $page_size
     * @param string|null $sort_by
     * @param string|null $sort_direction
     * @return array
     */
    public function get_orders(int $page_number, int $page_size, string $sort_by = null, string $sort_direction = null) : array
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getDefaultSortingColumn();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $orders = $this->findBy([], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$orders, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction];
    }

    /**
     * @return array
     */
    public function get_active_order_statuses()
    {
        $query = "SELECT DISTINCT d.order_status_name FROM " . $this->getClassName() . " d WHERE d.order_status_name != 'post calculation ready' AND d.order_status_name != 'canceled'";
        $order_unique_statuses = array_column($this->getEntityManager()->createQuery($query)
            ->getArrayResult(), 'order_status_name');
        return $order_unique_statuses;
    }

    public function get_finished_order_statuses()
    {
        $query = "SELECT DISTINCT d.order_status_name FROM " . $this->getClassName() . " d WHERE d.order_status_name = 'post calculation ready' OR d.order_status_name = 'canceled'";
        $order_unique_statuses = array_column($this->getEntityManager()->createQuery($query)
            ->getArrayResult(), 'order_status_name');
        return $order_unique_statuses;
    }


    /**
     * @return array
     */
    public function get_activeOrdersData()
    {
        $query = "SELECT o FROM " . $this->getClassName(). " o WHERE o.order_status_name != 'post calculation ready' AND o.order_status_name != 'canceled' ORDER BY o.id DESC";
        $orders_data = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $orders_data;
    }

    /**
     * @return array
     */
    public function get_finishedOrdersData()
    {
        $query = "SELECT o FROM " . $this->getClassName(). " o WHERE o.order_status_name = 'post calculation ready' OR o.order_status_name = 'canceled' ORDER BY o.id DESC";
        $orders_data = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $orders_data;
    }

    /**
     * @return array
     */
    public function get_invoicesData()
    {
        $query = "SELECT o FROM " . $this->getClassName(). " o WHERE  o.order_status_name = '" . OrderStatusEntity::INVOICE_SENT ."' OR o.order_status_name = '" . OrderStatusEntity::POST_CALCULATION_READY . "' ORDER BY o.order_invoice_number DESC";
        $orders_data = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $orders_data;
    }

    public function get_invoiceFinishedData()
    {
        $query = "SELECT o FROM " . $this->getClassName(). " o JOIN o.payment_status p WHERE p.name = 'paid' AND ( o.order_status_name = '" . OrderStatusEntity::INVOICE_SENT ."' OR o.order_status_name = '" . OrderStatusEntity::POST_CALCULATION_READY . "') ORDER BY o.order_invoice_number DESC";
        $orders_data = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $orders_data;
    }
    /**
     * @param OrderStatusEntity $order_status
     * @param int $page_number
     * @param int $page_size
     * @param string|null $sort_by
     * @param string|null $sort_direction
     * @return array
     */
    /*public function get_invoices()
    {
        echo '3';

    }*/
    public function get_invoices(OrderStatusEntity $order_status, int $page_number, int $page_size, string $sort_by = null, string $sort_direction = null) : array
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getDefaultSortingColumn();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPagesForInvoices($order_status, $page_size);

        $orders = $this->findBy(['order_status' => $order_status], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$orders, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction];
    }
    /**
     * @param OrderStatusEntity $order_status
     * @param int $page_size
     * @return float
     */
    public function getTotalNumberOfPagesForInvoices(OrderStatusEntity $order_status, int $page_size)
    {
        $query = "SELECT COUNT(1) FROM " . $this->getClassName() . " d JOIN d.order_status os WHERE os.name = '" . $order_status->getName() . "'";
        $num_of_records = $this->getEntityManager()->createQuery($query)
            ->getSingleScalarResult();
        return ceil($num_of_records/floatval($page_size));
    }
    /**
     * @return array
     */
    public function get_debtors_by_delivery_time() : array
    {
        $query = "SELECT o.id as id, o.debtor_name as debtor_name, o.delivery_time as delivery_time FROM " . $this->getClassName() . " o WHERE o.order_status_name != '". OrderStatusEntity::FINISHED ."' AND o.order_status_name != '". OrderStatusEntity::CANCELED ."' AND o.order_status_name !='" . OrderStatusEntity::POST_CALCULATION_READY ."' AND o.delivery_time >= '" . ((new \DateTime('now'))->format('Y-m-d')) . "' ORDER BY o.delivery_time ASC";
        $orders = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $orders;
    }
    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @param string $locale
     * @return array
     */
    public function get_order_events(\DateTime $start, \DateTime $end, string $locale) : array
    {
        $query = "SELECT o FROM " . $this->getClassName() . " o WHERE o.order_status_name != '". OrderStatusEntity::FINISHED ."' AND o.order_status_name != '". OrderStatusEntity::CANCELED ."' AND o.order_status_name !='" . OrderStatusEntity::POST_CALCULATION_READY ."' AND o.delivery_time >= '" . $start->format('Y-m-d') . "' AND o.delivery_time <= '" . $end->format('Y-m-d') . "'";
        $orders = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        $orders_ret = [];
        foreach($orders as $order) {
            $orders_ret[] = [
                'id' => $order['id'],
                'title' => $this->get_title($order, $locale),
                'start' => $order['delivery_time']->format('Y-m-d'),
                'end' => $order['delivery_time']->format('Y-m-d'),
                'color' => '#377ea3',
                'name' => 'order'
            ];
        }
        return $orders_ret;
    }
    /**
     * @param array $order
     * @param string $locale
     * @return string
     */
    protected function get_title(array $order, string $locale) : string
    {
        switch($locale) {
            case LocaleEntity::ENGLISH:
                return "Order for: " . $order['debtor_name'] . "\nSupervisor: " . $order['order_supervisor'] . "\nStatus: " . OrderStatusTranslationEntity::get_translation($order['order_status_name'], LocaleEntity::ENGLISH);
                break;
            case LocaleEntity::DUTCH:
                return "Bestelling voor: " . $order['debtor_name'] . "\nOrderbegeleider: " . $order['order_supervisor'] . "\nStatus: " . OrderStatusTranslationEntity::get_translation($order['order_status_name'], LocaleEntity::DUTCH);
                break;
            case LocaleEntity::GERMAN:
                return "Bestellung für: " . $order['debtor_name'] . "\nSupervisor: " . $order['order_supervisor'] . "\nStatus: " . OrderStatusTranslationEntity::get_translation($order['order_status_name'], LocaleEntity::GERMAN);
                break;
            default:
                return "Order for: " . $order['debtor_name'] . "\nSupervisor: " . $order['order_supervisor'] . "\nStatus: " . OrderStatusTranslationEntity::get_translation($order['order_status_name'], LocaleEntity::ENGLISH);
        }
    }
    /**
     * @param OrderEntity $order_entity
     * @return OrderEntity
     */
    public function update_total_amounts(OrderEntity $order_entity) : OrderEntity
    {
        $query = "SELECT oa.order_article_purchase_price * oa.quantity as purchase_price, oa.order_article_price * oa.quantity as selling_price FROM AppBundle\Entity\User\Orders\OrderArticleEntity oa JOIN oa.order o WHERE o.id = " . $order_entity->getId();
        $prices = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        $total_purchase_price = 0;
        $total_selling_price = 0;
        foreach($prices as $price) {
            $total_purchase_price += $price['purchase_price'];
            $total_selling_price += $price['selling_price'];
        }
        $order_entity->setTotalPurchaseAmount($total_purchase_price);
        $order_entity->setTotalSellingAmount($total_selling_price);
        return $order_entity;
    }
}