<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 2/12/2018
 * Time: 3:00 PM
 */

namespace AppBundle\Repository\User\Orders;

use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Orders\InvoiceNumbers;
use AppBundle\Repository\AppEntityRepository;
use AppBundle\Repository\Exceptions\TableDoesNotExistsException;
use Doctrine\ORM\EntityManagerInterface;

class InvoiceNumberRepository extends AppEntityRepository
{
    /**
     * @param string $end_user_type
     * @return array
     * @throws \Exception
     */
    public function get_invoice_number(string $end_user_type) : array
    {
        $query_select = "SELECT a.private_invoice_number, a.company_invoice_number FROM order_invoice_numbers a WHERE a.id = 1;";
        switch ($end_user_type) {
            case DebtorEndUserTypeEntity::PRIVATE :
                $query_update = "UPDATE furniture_enterprise.order_invoice_numbers SET company_invoice_number = (company_invoice_number + 1) WHERE id = 1;";
                break;
            case DebtorEndUserTypeEntity::COMPANY :
                $query_update = "UPDATE furniture_enterprise.order_invoice_numbers SET company_invoice_number = (company_invoice_number + 1) WHERE id = 1; ";
                break;
            default:
                throw new \Exception(_('Unknown end user type'));
        }
        $conn = mysqli_connect("localhost","furniture_user","furniture_nl","furniture_enterprise");
        if(mysqli_connect_errno()) {
            throw new \Exception(mysqli_connect_error());
        }
        $result = '';
        mysqli_begin_transaction($conn);
        try {
            mysqli_query($conn, $query_update);
            $result = mysqli_query($conn, $query_select);
            $result = mysqli_fetch_row($result);
            mysqli_commit($conn);
        } catch(\Exception $e) {
            mysqli_rollback($conn);
            mysqli_close($conn);
            throw $e;
        }
        mysqli_close($conn);
        switch ($end_user_type) {
            case DebtorEndUserTypeEntity::PRIVATE :
                $in = intval($result[1]);
                $invoice_number = [];
                $invoice_number[0] = "10600 / " . $in;
                $invoice_number[1] = $in;
                return $invoice_number;
                break;
            case DebtorEndUserTypeEntity::COMPANY :
                $in = intval($result[1]);
                $invoice_number = [];
                $invoice_number[0] = "10597 / " . $in;
                $invoice_number[1] = $in;
                return $invoice_number;
                break;
            default:
                throw new \Exception(_('Unknown end user type'));
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function get_order_number() : string
    {
        $year = $this->reset_order_counter();
        $query_select = "SELECT order_number FROM order_invoice_numbers WHERE id = 1;";
        $query_update = "UPDATE order_invoice_numbers SET order_number = (order_number + 1) WHERE id = 1;";
        $conn = mysqli_connect("localhost","furniture_user","furniture_nl","furniture_enterprise");        if(mysqli_connect_errno()) {
            throw new \Exception(mysqli_connect_error());
        }
        $result = '';
        mysqli_begin_transaction($conn);
        try {
            mysqli_query($conn, $query_update);
            $result = mysqli_query($conn, $query_select);
            $result = mysqli_fetch_row($result);
            mysqli_commit($conn);
        } catch(\Exception $e) {
            mysqli_rollback($conn);
            mysqli_close($conn);
            throw $e;
        }
        $on = intval($result[0]);
        return $year . ' / ' . $on;
    }

    protected function reset_order_counter() : string
    {
        $year = (new \DateTime('now'))->format('Y');
        $query_select = "SELECT order_numbering_year FROM order_invoice_numbers WHERE id = 1;";
        $query_update = "UPDATE order_invoice_numbers SET order_number = 0, order_numbering_year = " . $year;
        $conn = mysqli_connect("localhost","furniture_user","furniture_nl","furniture_enterprise");        if(mysqli_connect_errno()) {
            throw new \Exception(mysqli_connect_error());
        }
        $result = mysqli_query($conn, $query_select);
        $result = mysqli_fetch_row($result);
        if(trim($result[0]) != $year) {
            try {
                mysqli_begin_transaction($conn);
                mysqli_query($conn, $query_update);
                mysqli_commit($conn);
            } catch(\Exception $e) {
                mysqli_rollback($conn);
                mysqli_close($conn);
                throw $e;
            }
        }
        mysqli_close($conn);
        return $year;
    }

    /**
     * @return string
     * @throws TableDoesNotExistsException
     */
    public function create_table()
    {
        $table_name = $this->getClassMetadata()->getTableName();
        $schema_manager = $this->getEntityManager()->getConnection()->getSchemaManager();
        $tables = $schema_manager->listTableNames();
        if (!in_array($table_name, $tables)) {
            throw new TableDoesNotExistsException(_('Table does not exist'));
        }
        $ine = $this->getEntityManager()->getRepository(InvoiceNumbers::class)
            ->findOneBy(['id' => 1]);
        if(empty($ine)) {
            $ine = new InvoiceNumbers();
            $ine->setId(1);
            $ine->setCompanyInvoiceNumber(0);
            $ine->setPrivateInvoiceNumber(0);
            $ine->setOrderNumber(0);
            $ine->setOrderNumberingYear((new \DateTime('now'))->format('Y'));
            $this->getEntityManager()->persist($ine);
            $this->getEntityManager()->flush();
        }
        return $this->getClassName();
    }
}