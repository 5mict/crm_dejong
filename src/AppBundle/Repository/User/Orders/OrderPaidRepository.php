<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/4/2018
 * Time: 5:34 PM
 */

namespace AppBundle\Repository\User\Orders;

use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Repository\AppEntityRepository;
use AppBundle\Utils\DateHelper;
use AppBundle\ViewModels\User\Orders\OrdersView;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\BrowserKit\Request;

class OrderPaidRepository extends AppEntityRepository
{
    public function getDefaultPageSize(): int
    {
        return 12;
    }

    public function getDefaultSortingColumn() : string
    {
        return 'created';
    }

    public function getDefaultSortDirection(): string
    {
        return 'desc';
    }

    public function get_orders(int $page_number, int $page_size, string $sort_by = null, string $sort_direction = null, array $criteria = []) : array
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getDefaultSortingColumn();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $orders = $this->findBy($criteria, [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$orders, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction];
    }

    public function get_orders_paid(DebtorEntity $debtor, \DateTime $start, \DateTime $end, string $locale)
    {
        $query = "SELECT op.created as created, op.purchase_price as purchase_price, op.selling_price as selling_price, op.profit_percent as profit FROM " . $this->getClassName() . " op WHERE op.created >= '" . $start->format('Y-m-d H:i:o') . "' AND op.created <= '" . $end->format('Y-m-d H:i:o') . "' AND op.debtor = " . $debtor->getId() . " ORDER BY op.created ASC";
        $orders_paid = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        $difference_in_days = ($end->getTimestamp() - $start->getTimestamp())/(24*60*60);
        //$orders_paid = $this->mock_data();
        if($difference_in_days < 32) {
            return $this->get_data_in_days($orders_paid, $start, $end, $difference_in_days, $locale);
        }
        if($difference_in_days < 3*31 + 2) {
            return $this->get_data_in_weeks($orders_paid, $start, $end, $difference_in_days, $locale);
        }
        if($difference_in_days < 3*365 + 2) {
            return $this->get_data_in_months($orders_paid, $start, $end, $difference_in_days, $locale);
        }
        return $this->get_data_in_years($orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_days($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 day', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_weeks($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 week', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_months($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 month', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_years($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 year', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data(string $interval, array $orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        $current = clone $start;
        $current_end = clone $start;
        $this->set_current_time($interval, $current, $current_end);
        $xAxis = [];
        $purchase_prices = [];
        $selling_prices = [];
        $profit = [];
        $purchase_price = 0;
        $selling_price = 0;
        for($i = 0; $i < sizeof($orders_paid); ++$i) {
            $order_paid = $orders_paid[$i];
            $order_created = $order_paid['created'];
            if($start > $order_created) {
                continue;
            }
            if($end < $order_created) {
                break;
            }
            if($current_end < $order_created) {
                $xAxis[] = $this->get_xaxis($interval, $current, $locale);
                $purchase_prices[] = round($purchase_price, 2);
                $selling_prices[] = round($selling_price, 2);
                $profit[] = round($selling_price - $purchase_price, 2);
                $purchase_price = 0;
                $selling_price = 0;
                $this->modify_current_time($interval, $current, $current_end);
                --$i;
                continue;
            }
            $purchase_price += $order_paid['purchase_price'];
            $selling_price += $order_paid['selling_price'];
        }
        if($current < $end) {
            $xAxis[] = $this->get_xaxis($interval, $current, $locale);
            $purchase_prices[] = round($purchase_price, 2);
            $selling_prices[] = round($selling_price, 2);
            $profit[] = round($selling_price - $purchase_price, 2);
            $this->modify_current_time($interval, $current, $current_end);
        }
        while($current < $end) {
            $xAxis[] = $this->get_xaxis($interval, $current, $locale);
            $purchase_prices[] = round(0, 2);
            $selling_prices[] = round(0, 2);
            $profit[] = round(0, 2);
            $this->modify_current_time($interval, $current, $current_end);
        }
        return ['xAxis' => $xAxis, 'purchase_prices' => $purchase_prices, 'selling_prices' => $selling_prices, 'profit' => $profit];
    }

    protected function modify_current_time(string $interval, \DateTime $current, \DateTime $current_end)
    {
        switch($interval) {
            case '+1 day':
                $current->modify('+1 day');
                $current_end->modify('+1 day');
                break;
            case '+1 week':
                $current->modify('+1 week');
                $current_end->modify('+1 week');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 month':
                $current->modify('first day of next month');
                $current_end->modify('last day of next month');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 year':
                $current->modify('first day of January next year');
                $current_end->modify('last day of December next year');
                $current_end->setTime(23, 59, 59);
                break;
            default:
                throw new \Exception(_('Unknown formatting'));
        }
    }

    protected function set_current_time(string $interval, \DateTime $current, \DateTime $current_end)
    {
        switch($interval) {
            case '+1 day':
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 week':
                $current_end->modify('+6 days');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 month':
                $current_end->modify('last day of this month');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 year':
                $current_end->modify('last day of December this year');
                $current_end->setTime(23, 59, 59);
                break;
            default:
                throw new \Exception(_('Unknown formatting'));
        }
    }

    protected function get_xaxis(string $interval, \DateTime $current, string $locale) : string
    {
        switch($interval) {
            case '+1 day':
                $day = $current->format('d');
                $month = DateHelper::get_month_with_translation(intval($current->format('m')) - 1, $locale);
                return $day . '-' . $month;
            case '+1 week':
                $week = $current->format('W');
                $year = $current->format('Y');
                return $week . '-' . $year;
            case '+1 month':
                $month = DateHelper::get_month_with_translation(intval($current->format('m')) - 1, $locale);
                $year = $current->format('Y');
                return $month . '-' . $year;
            case '+1 year':
                $year = $current->format('Y');
                return $year;
            default:
                throw new \Exception(_('Unknown formatting'));
        }
    }

    protected function mock_data()
    {
        $ret = [];
        $now = new \DateTime('now');
        $current = new \DateTime('now');
        $days = 3*366;
        $current->modify('-' . $days . ' day');
        for($i = 0; $i < 3*$days + 40; ++$i) {
            if($i % 3 == 0) {
                $current->modify('+1 day');
            }
            $ret[] = ['created' => (clone $current), 'purchase_price' => 1, 'selling_price' => 1+1.*1/10. ];
        }
        return $ret;
    }
}