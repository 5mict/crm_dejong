<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 1:17 PM
 */

namespace AppBundle\Repository\User\Orders;

use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Repository\AppEntityRepository;
use function Composer\Autoload\includeFile;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class OrderArticleRepository extends AppEntityRepository
{
    /**
     * @return int
     */
    public function getDefaultPageNumber() : int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function getDefaultPageSize(): int
    {
        return 12;
    }

    /**
     * @return string
     */
    public function getDefaultSortDirection(): string
    {
        return 'desc';
    }

    /**
     * @param int $supplier_id
     * @param int $page_size
     * @return int
     */
    protected function getTotalNumberOfPagesForSupplier(int $supplier_id, int $page_size) : int
    {
        $query = "SELECT COUNT(1) FROM " . $this->getClassName() . " d where d.supplier = " . $supplier_id;
        $num_of_records = $this->getEntityManager()->createQuery($query)
            ->getSingleScalarResult();
        return ceil($num_of_records/floatval($page_size));
    }

    /**
     * @param int $supplier_id
     * @return array
     */
    public function get_order_articles_for_supplierData(int $supplier_id)
    {
        $query = "SELECT oa FROM " . $this->getClassName() . " oa JOIN oa.to_supplier_status s WHERE oa.supplier = '" . $supplier_id . "' AND oa.commission != '' AND s.name !='finished'";
        $order_articles_data = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $order_articles_data;
    }

    /**
     * @param int $order_id
     * @return array
     * @internal param int $order_number
     * @internal param int $order_id
     * @internal param int $supplier_status
     */
    public function get_order_articles_for_supplierDataOrder(int $order_id)
    {
        $query = "SELECT oa FROM " . $this->getClassName() . " oa JOIN oa.order o WHERE o.id = '" . $order_id . "' AND oa.commission != ''";
        $order_articles_data = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $order_articles_data;
    }

    /**
     * @param int $page_number
     * @param int $page_size
     * @param string|null $sort_by
     * @param string|null $sort_direction
     * @return array
     */
    public function get_order_articles(int $page_number = 0, int $page_size = 0, string $sort_by = null, string $sort_direction = null)
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getPrimaryKeyColumnName();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $order_articles = $this->findBy([], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$order_articles, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction] ;
    }

    /**
     * @param \Datetime $start
     * @param \Datetime $end
     * @param string $locale
     * @return array
     */
    public function get_supplier_events(\Datetime $start, \Datetime $end, string $locale)
    {
        $query = "SELECT oa.id as id, o.id as order_id, oa.commission as commission, s.name as supplier_name, oa.delivery_time as delivery_time FROM " . $this->getClassName() . " oa JOIN oa.supplier s JOIN oa.order o WHERE o.order_status_name != '". OrderStatusEntity::FINISHED ."' AND o.order_status_name != '". OrderStatusEntity::CANCELED ."' AND o.order_status_name !='" . OrderStatusEntity::POST_CALCULATION_READY ."' AND oa.delivery_time >= '" . $start->format('Y-m-d') . "' AND oa.delivery_time <= '" . $end->format('Y-m-d') . "' AND oa.order_from_supplier > 0";
        $suppliers = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        $suppliers_ret =[];
        foreach($suppliers as $supplier) {
            if (!$this->supplier_order_repeat_on_same_date($supplier, $suppliers_ret)) {
                $suppliers_ret[] = [
                    'id' => $supplier['id'],
                    'order_id' => $supplier['order_id'],
                    'title' => $this->get_title($supplier, $locale),
                    'start' => $supplier['delivery_time']->format('Y-m-d'),
                    'end' => $supplier['delivery_time']->format('Y-m-d'),
                    'color' => '#bb9b51',
                    'name' => 'supplier',
                    'supplier_name' => $supplier['supplier_name']
                ];
            }
        }
        return $suppliers_ret;
    }

    /**
     * @param array $supplier
     * @param string $locale
     * @return string
     */
    public function get_title(array $supplier, string $locale){
        switch($locale){
            case LocaleEntity::ENGLISH:
                return "Supplier name: " . $supplier['supplier_name'] . "\nCommission: " . $supplier['commission'];
                break;
            default:
                return "Naam leverancier: " . $supplier['supplier_name'] . "\nCommissie: " . $supplier['commission'];
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function get_suppliers_by_delivery_time()
    {
        $query = "SELECT oa.id as id, o.id as order_id, s.id as supplier_id, s.name as supplier_name, oa.delivery_time as delivery_time FROM " . $this->getClassName() . " oa JOIN oa.supplier s JOIN oa.order o WHERE o.order_status_name != '". OrderStatusEntity::CANCELED ."' AND o.order_status_name !='" . OrderStatusEntity::POST_CALCULATION_READY ."' AND oa.delivery_time >= '" . ((new \DateTime('now'))->format('Y-m-d')) . "' AND oa.order_from_supplier > 0 ORDER BY oa.delivery_time ASC";
        $orders = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        $data = [] ;
        foreach($orders as $order){
            $data[$order['order_id']] = [
                'id' => $order['id'],
                'order_id' => $order['order_id'],
                'supplier_id' => $order['supplier_id'],
                'supplier_name' => $order['supplier_name'],
                'delivery_time' => $order['delivery_time']
            ];
        }
        return $data;
    }

    /**
     * @param int $supplier_id
     * @return array
     */
    public function get_finished_suppliers(int $supplier_id)
    {
        $query = "SELECT oa FROM " . $this->getClassName() . " oa JOIN oa.to_supplier_status s WHERE oa.supplier = '" . $supplier_id . "' AND s.name = 'finished' ORDER BY oa.delivery_time ASC";
        $orders = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $orders;
    }

    /**
     * @param int $order_id
     * @return array
     */
    public function get_order_suppliers(int $order_id)
    {
        $order_articles = $this->findBy(['order' => $order_id]);
        $suppliers_data = [];
        foreach($order_articles as $order_article){
            $supplier = $order_article->getSupplier();
            if(!in_array($supplier->getName(), array_column($suppliers_data, 'name'))){
                $suppliers_data[] = [
                    'supplier_id' => $supplier->getId(),
                    'name' => $supplier->getName(),
                    'language' => $supplier->getCommunicationLanguage()
                ];
            }
        }
        return $suppliers_data;
    }

    /**
     * @param int $order_id
     * @param int $supplier_id
     * @return array
     */
    public function get_order_articles_for_supplier(int $order_id,  int $supplier_id)
    {
        $query = "SELECT oa FROM " . $this->getClassName() . " oa WHERE oa.supplier = '" . $supplier_id . "' AND oa.order = '" . $order_id . "' AND oa.order_from_supplier > 0";
        $order_articles = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $order_articles;
    }

    /**
     * @param $start
     * @param $end
     * @param $locale
     * @return array
     */
    public function get_article_statistics($start, $end, $locale)
    {
        $query = "SELECT oa FROM " . $this->getClassName() . " oa JOIN oa.order o WHERE o.created >= '" . $start . "' AND o.created <= '" . $end . "' AND ( o.order_status_name = '". OrderStatusEntity::FINISHED ."' OR o.order_status_name ='" . OrderStatusEntity::POST_CALCULATION_READY ."')";
        $order_articles = $this->getEntityManager()->createQuery($query)
            ->getResult();
        dump(array('sds'=>$order_articles));
        $data = [];
        foreach($order_articles as $order_article){
            $article = $order_article->getArticle();
            if($locale == "en"){
                $name = $article->getArticleName();
            } else {
                $name = $article->getArticleNameDutch();
            }
            if(isset($data[$name])){
                $data[$name]['total_selling'] = $data[$name]['total_selling'] + $order_article->getTotalSellingPrice();
                $data[$name]['total_purchase'] = $data[$name]['total_purchase'] + $order_article->getTotalPurchasePrice();
            }else{
                $data[$name] = [
                    'name' => $name,
                    'total_selling' => $order_article->getTotalSellingPrice(),
                    'total_purchase' => $order_article->getTotalPurchasePrice(),
                ];
            }
        }
        usort($data, function($a, $b) {
            return $b['total_selling'] <=> $a['total_selling'];
        });
        return $data;
    }

    /**
     * @return array
     */
    public function run_script()
    {
        $query = "SELECT oa FROM " . $this->getClassName() . " oa JOIN oa.order o WHERE ( o.order_status_name = '". OrderStatusEntity::FINISHED ."' OR o.order_status_name ='" . OrderStatusEntity::POST_CALCULATION_READY ."')";
        $order_articles = $this->getEntityManager()->createQuery($query)
            ->getResult();

        $data = [];
        foreach($order_articles as $order_article){
            $article = $order_article->getArticle();
            if(isset($data[$article->getId()])){
                $data[$article->getId()]['total_selling'] = $data[$article->getId()]['total_selling'] + $order_article->getTotalSellingPrice();
                $data[$article->getId()]['total_purchase'] = $data[$article->getId()]['total_purchase'] + $order_article->getTotalPurchasePrice();
            }else{
                $data[$article->getId()] = [
                    'id' => $article->getId(),
                    'total_selling' => $order_article->getTotalSellingPrice(),
                    'total_purchase' => $order_article->getTotalPurchasePrice(),
                ];
            }

            $articleEntity = $this->getEntityManager()
                ->getRepository(ArticleEntity::class)
                ->find($id = $article->getId());
            $articleEntity->setTotalPurchasePrice($tp = $data[$id]['total_purchase']);
            $articleEntity->setTotalSellingPrice($ts = $data[$id]['total_selling']);
            ($ts - $tp) == 0 || $ts == 0 || $tp == 0 ? $articleEntity->setProfitPercents(0) : $articleEntity->setProfitPercents(($ts - $tp) / $tp * 100, 2);
            $this->getEntityManager()->persist($articleEntity);
            $this->getEntityManager()->flush();
        }

        usort($data, function($a, $b) {
            return $b['total_selling'] <=> $a['total_selling'];
        });

        return $data;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function run_script2()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT debtors.id, orders.id as order_id, sum(order_article_purchase_price * quantity) as purchase, sum(order_article_price * quantity) as sale FROM furniture_enterprise.orders_article INNER JOIN furniture_enterprise.orders INNER JOIN furniture_enterprise.debtors WHERE order_id = orders.id AND debtors.id = orders.debtor_id AND (orders.order_status_name = 'finished' OR orders.order_status_name = 'post calculation ready') GROUP BY debtors.id;";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();
        foreach ($data as $debtor) {
            $debtorEntity = $this->getEntityManager()
                ->getRepository(DebtorEntity::class)
                ->find($id = $debtor['id']);
            $debtorEntity->setTotalPurchasePrice($tp = $debtor['purchase']);
            $debtorEntity->setTotalSellingPrice($ts = $debtor['sale']);
            ($ts - $tp) == 0 || $ts == 0 || $tp == 0 ? $debtorEntity->setProfitPercents(0) : $debtorEntity->setProfitPercents(round(($ts - $tp) / $tp * 100, 2));
            $this->getEntityManager()->persist($debtorEntity);
            $this->getEntityManager()->flush();
        }
        return $data;
    }


    /**
     * @param $start
     * @param $end
     * @param $locale
     * @return array
     */
    public function get_article_category_statistics($start, $end, $locale)
    {
        $query = "SELECT oa FROM " . $this->getClassName() . " oa JOIN oa.order o WHERE o.created >= '" . $start . "' AND o.created <= '" . $end . "' AND ( o.order_status_name = '". OrderStatusEntity::FINISHED ."' OR o.order_status_name ='" . OrderStatusEntity::POST_CALCULATION_READY ."' )";
        $order_articles = $this->getEntityManager()->createQuery($query)
            ->getResult();
        $data = [];
        foreach($order_articles as $order_article){
            $article = $order_article->getArticle();
            if($locale == "en"){
                $name = ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), "en");
            } else {
                $name = ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), "nl");
            }
            if(isset($data[$name])){
                $data[$name]['number_sold'] = $data[$name]['number_sold'] + $order_article->getQuantity();
                $data[$name]['total_selling'] = $data[$name]['total_selling'] + $order_article->getTotalSellingPrice();
                $data[$name]['total_purchase'] = $data[$name]['total_purchase'] + $order_article->getTotalPurchasePrice();
            }else{
                $data[$name] = [
                    'name' => $name,
                    'number_sold' => $order_article->getQuantity(),
                    'total_selling' => $order_article->getTotalSellingPrice(),
                    'total_purchase' => $order_article->getTotalPurchasePrice(),
                ];
            }
        }
        usort($data, function($a, $b) {
            return $b['total_selling'] <=> $a['total_selling'];
        });
        return $data;
    }

    /**
     * @param $order_article_id
     * @return string
     */
    public function get_order_article_description( $order_article_id ) : string
    {
        $query = "SELECT oa.description FROM " . $this->getClassName() . " oa WHERE oa.id = " . $order_article_id;
        $order_article_description = $this->getEntityManager()->createQuery($query)
            ->getResult();
        error_log(json_encode($order_article_description));
        return $order_article_description[0]["description"];
    }

    /**
     * @param $order_article_id
     * @param $order_article_description
     */
    public function set_order_article_description( $order_article_id, $order_article_description )
    {
        //$query = "SELECT oa FROM " . $this->getClassName() . " oa JOIN oa.order o WHERE o.created >= '" . $start . "' AND o.created <= '" . $end . "' AND ( o.order_status_name = '". OrderStatusEntity::FINISHED ."' OR o.order_status_name ='" . OrderStatusEntity::POST_CALCULATION_READY ."' )";
        //$order_articles = $this->getEntityManager()->createQuery($query)
        //    ->getResult();
        return;
    }

    /**
     * @param $supplier
     * @param array $suppliers
     * @return bool
     */
    public function supplier_order_repeat_on_same_date($supplier, array $suppliers)
    {
        if (in_array($supplier['supplier_name'], array_column($suppliers, 'supplier_name')) &&
            in_array($supplier['order_id'],  array_column($suppliers, 'order_id')) &&
            in_array($supplier['delivery_time']->format('Y-m-d'), array_column($suppliers, 'start')))
        {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return int
     * @throws DBALException
     */
    public function generateCustomOrderArticleNumber($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "SELECT MAX(custom_order_article_number) 
                FROM furniture_enterprise.orders_article 
                WHERE order_id = " . $id . ";";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchColumn();
        return $data + 1;
    }
}