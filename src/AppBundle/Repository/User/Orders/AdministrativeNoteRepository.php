<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 1:16 PM
 */

namespace AppBundle\Repository\User\Orders;

use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Repository\AppEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\BrowserKit\Request;

class AdministrativeNoteRepository extends AppEntityRepository
{
    private $order_id;

    public function getDefaultPageNumber() : int
    {
        return 1;
    }

    public function getDefaultPageSize(): int
    {
        return 10;
    }

    public function getDefaultSortDirection(): string
    {
        return 'desc';
    }

    protected function getTotalNumberOfPages(int $page_size) : int
    {
        $query = "SELECT COUNT(1) FROM " . $this->getClassName() . " d where d.order = " . $this->order_id;
        $num_of_records = $this->getEntityManager()->createQuery($query)
            ->getSingleScalarResult();
        return ceil($num_of_records/floatval($page_size));
    }

    public function get_notes(int $order_id, int $page_number = 0, int $page_size = 0, string $sort_by = null, string $sort_direction = null) : array
    {
        $this->order_id = $order_id;
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = 'updated';
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $notes = $this->findBy(['order' => $order_id], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction] ;
    }
}