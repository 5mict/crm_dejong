<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/25/2017
 * Time: 3:44 PM
 */

namespace AppBundle\Repository\User\Orders;

use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Repository\AppEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\BrowserKit\Request;

class OrderMailRepository extends AppEntityRepository
{
    /**
     * @param int $order_id
     * @param int $mail_status_id
     * @return array
     */
    public function get_last_email(int $order_id, int $mail_status_id)
    {
        $query = "SELECT om.order_details as order_details, om.created as created FROM " . $this->getClassName() . " om WHERE om.order = " . $order_id . " AND om.order_mail_status = " . $mail_status_id . "ORDER BY om.id DESC";
        $order_mail_details = $this->getEntityManager()->createQuery($query)
           ->getArrayResult();
        if(empty($order_mail_details)){
            $email = [];
        }else{
            $email['details'] = trim(preg_replace('/\s+/', ' ', $order_mail_details[0]['order_details']));
            $email['created'] = $order_mail_details[0]['created']->format('d-m-Y');
        }
        return $email;
    }
}