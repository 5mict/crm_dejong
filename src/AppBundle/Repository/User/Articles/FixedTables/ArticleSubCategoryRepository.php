<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/19/2017
 * Time: 8:22 PM
 */

namespace AppBundle\Repository\User\Articles\FixedTables;

use AppBundle\Controller\User\Articles\Exceptions\ArticleCategoryNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class ArticleSubCategoryRepository extends AppFixedTableEntityRepository
{
    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT aa.name as name, aat.name as trans FROM \AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryTranslationEntity aat JOIN aat.article_sub_category aa WHERE aat.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    public function get_with_translation(ArticleCategoryEntity $article_category_entity, string $locale)
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT aa.name as name, aat.name as trans FROM \AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryTranslationEntity aat JOIN aat.article_sub_category aa WHERE aat.locale = '" . $locale_entity->getId() . "' and aa.article_category = '" . $article_category_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id(): array
    {
        $query = "SELECT a.name FROM " . $this->getClassName() . " a";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return ArticleSubCategoryEntity
     * @throws ArticleCategoryNotFoundException
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : ArticleSubCategoryEntity
    {
        $article_category_entity = $em->getRepository(ArticleCategoryEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($article_category_entity)) {
            throw new ArticleCategoryNotFoundException(_('Article category not found'));
        }
        $article_sub_category_entity = $em->getRepository(ArticleSubCategoryEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($article_sub_category_entity)) {
            $article_sub_category_entity = new ArticleSubCategoryEntity();
            $article_sub_category_entity->setName($data['name']);
        }
        $article_sub_category_entity->setArticleCategory($article_category_entity);
        return $article_sub_category_entity;
    }
}