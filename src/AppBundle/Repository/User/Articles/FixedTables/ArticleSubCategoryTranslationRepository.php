<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 6:15 PM
 */

namespace AppBundle\Repository\User\Articles\FixedTables;

use AppBundle\Controller\User\Articles\Exceptions\ArticleSubCategoryNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class ArticleSubCategoryTranslationRepository extends AppFixedTableEntityRepository
{
    /**
     * @return array
     */
    public function get_table_rows_without_id(): array
    {
        $query = "SELECT a.name, c.name as reference, l.language FROM " . $this->getClassName() . " a JOIN a.locale l JOIN a.article_sub_category c";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return ArticleSubCategoryTranslationEntity
     * @throws ArticleSubCategoryNotFoundException
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : ArticleSubCategoryTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $article_sub_category_entity = $em->getRepository(ArticleSubCategoryEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($article_sub_category_entity)) {
            throw new ArticleSubCategoryNotFoundException(_('Article sub category not found'));
        }
        $article_sub_category_translation_entity = $em->getRepository(ArticleSubCategoryTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($article_sub_translation_category_entity)) {
            $article_sub_category_translation_entity = new ArticleSubCategoryTranslationEntity();
            $article_sub_category_translation_entity->setName($data['name']);
            $article_sub_category_translation_entity->setLocale($locale_entity);
        }
        $article_sub_category_translation_entity->setArticleSubCategory($article_sub_category_entity);
        return $article_sub_category_translation_entity;
    }
}