<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 4:55 PM
 */

namespace AppBundle\Repository\User\Articles\FixedTables;

use AppBundle\Controller\User\Articles\Exceptions\ArticleCategoryNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class ArticleCategoryTranslationRepository extends AppFixedTableEntityRepository
{
    public function get_table_rows_without_id(): array
    {
        $query = "SELECT a.name, l.language FROM " . $this->getClassName() . " a JOIN a.locale l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    public static function from_model(array $data, EntityManagerInterface $em) : ArticleCategoryTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $article_category_entity = $em->getRepository(ArticleCategoryEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($article_category_entity)) {
            throw new ArticleCategoryNotFoundException(_('Article category not found'));
        }
        $article_category_translation_entity = $em->getRepository(ArticleCategoryTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($article_category_translation_entity)) {
            $article_category_translation_entity = new ArticleCategoryTranslationEntity();
            $article_category_translation_entity->setName($data['name']);
            $article_category_translation_entity->setLocale($locale_entity);
        }
        $article_category_translation_entity->setArticleCategory($article_category_entity);
        return $article_category_translation_entity;
    }

}