<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 1:13 PM
 */

namespace AppBundle\Repository\User\Articles\FixedTables;

use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class ArticleCategoryRepository extends AppFixedTableEntityRepository
{
    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT a.name as name, at.name as trans FROM \AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity at JOIN at.article_category a WHERE at.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }

    /**
     * @return array
     */
    public function get_table_rows_without_id(): array
    {
        $query = "SELECT a.name FROM " . $this->getClassName() . " a";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    public static function from_model(array $data, EntityManagerInterface $em)
    {
        $article_category_entity = $em->getRepository(ArticleCategoryEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($article_category_entity)) {
            $article_category_entity = new ArticleCategoryEntity();
            $article_category_entity->setName($data['name']);
        }
        return $article_category_entity;
    }
}