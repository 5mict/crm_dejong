<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/4/2018
 * Time: 6:01 PM
 */

namespace AppBundle\Repository\User\Articles;

use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryTranslationEntity;
use AppBundle\Repository\AppEntityRepository;
use AppBundle\Utils\DateHelper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\BrowserKit\Request;

class ArticlePaidRepository extends AppEntityRepository
{
    public function getDefaultPageNumber() : int
    {
        return 1;
    }

    public function getDefaultPageSize(): int
    {
        return 12;
    }

    public function get_articles(int $page_number, int $page_size, string $sort_by = null, string $sort_direction = null) : array
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getPrimaryKeyColumnName();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $articles = $this->findBy([], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$articles, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction] ;
    }

    public function get_articles_paid(
        ArticleCategoryEntity $category_entity,
        $sub_category_entity,
        \DateTime $start,
        \DateTime $end,
        string $locale
    ) {
        if(empty($sub_category_entity)) {
            $query = "SELECT op.created as created, op.purchase_price as purchase_price, op.selling_price as selling_price, op.profit_percents as profit FROM " . $this->getClassName() . " op WHERE op.created >= '" . $start->format('Y-m-d H:i:o') . "' AND op.created <= '" . $end->format('Y-m-d H:i:o') . "' AND op.category = " . $category_entity->getId() . " AND op.sub_category = 'null' ORDER BY op.created ASC";
        } else {
            $query = "SELECT op.created as created, op.purchase_price as purchase_price, op.selling_price as selling_price, op.profit_percents as profit FROM " . $this->getClassName() . " op WHERE op.created >= '" . $start->format('Y-m-d H:i:o') . "' AND op.created <= '" . $end->format('Y-m-d H:i:o') . "' AND op.category = " . $category_entity->getId() . " AND op.sub_category = " . $sub_category_entity->getId() . " ORDER BY op.created ASC";
        }
        $articles_paid = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        $difference_in_days = ($end->getTimestamp() - $start->getTimestamp())/(24*60*60);

        //$articles_paid = $this->mock_data();
        if($difference_in_days < 32) {
            return $this->get_data_in_days($articles_paid, $start, $end, $difference_in_days, $locale);
        }
        if($difference_in_days < 3*31 + 2) {
            return $this->get_data_in_weeks($articles_paid, $start, $end, $difference_in_days, $locale);
        }
        if($difference_in_days < 3*365 + 2) {
            return $this->get_data_in_months($articles_paid, $start, $end, $difference_in_days, $locale);
        }
        return $this->get_data_in_years($articles_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_days($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 day', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_weeks($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 week', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_months($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 month', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data_in_years($orders_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        return $this->get_data('+1 year', $orders_paid, $start, $end, $difference_in_days, $locale);
    }

    protected function get_data(string $interval, array $articles_paid, \DateTime $start, \DateTime $end, int $difference_in_days, string $locale)
    {
        $current = clone $start;
        $current_end = clone $start;
        $this->set_current_time($interval, $current, $current_end);
        $xAxis = [];
        $purchase_prices = [];
        $selling_prices = [];
        $profit = [];
        $purchase_price = 0;
        $selling_price = 0;
        for($i = 0; $i < sizeof($articles_paid); ++$i) {
            $article_paid = $articles_paid[$i];
            $article_created = $article_paid['created'];
            if($start > $article_created) {
                continue;
            }
            if($end < $article_created) {
                break;
            }
            if($current_end < $article_created) {
                $xAxis[] = $this->get_xaxis($interval, $current, $locale);
                $purchase_prices[] = round($purchase_price, 2);
                $selling_prices[] = round($selling_price, 2);
                $profit[] = round($selling_price - $purchase_price, 2);
                $purchase_price = 0;
                $selling_price = 0;
                $this->modify_current_time($interval, $current, $current_end);
                --$i;
                continue;
            }
            $purchase_price += $article_paid['purchase_price'];
            $selling_price += $article_paid['selling_price'];
        }
        if($current < $end) {
            $xAxis[] = $this->get_xaxis($interval, $current, $locale);
            $purchase_prices[] = round($purchase_price, 2);
            $selling_prices[] = round($selling_price, 2);
            $profit[] = round($selling_price - $purchase_price, 2);
            $this->modify_current_time($interval, $current, $current_end);
        }
        while($current < $end) {
            $xAxis[] = $this->get_xaxis($interval, $current, $locale);
            $purchase_prices[] = round(0, 2);
            $selling_prices[] = round(0, 2);
            $profit[] = round(0, 2);
            $this->modify_current_time($interval, $current, $current_end);
        }
        return ['xAxis' => $xAxis, 'purchase_prices' => $purchase_prices, 'selling_prices' => $selling_prices, 'profit' => $profit];
    }

    protected function modify_current_time(string $interval, \DateTime $current, \DateTime $current_end)
    {
        switch($interval) {
            case '+1 day':
                $current->modify('+1 day');
                $current_end->modify('+1 day');
                break;
            case '+1 week':
                $current->modify('+1 week');
                $current_end->modify('+1 week');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 month':
                $current->modify('first day of next month');
                $current_end->modify('last day of next month');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 year':
                $current->modify('first day of January next year');
                $current_end->modify('last day of December next year');
                $current_end->setTime(23, 59, 59);
                break;
            default:
                throw new \Exception(_('Unknown formatting'));
        }
    }

    protected function set_current_time(string $interval, \DateTime $current, \DateTime $current_end)
    {
        switch($interval) {
            case '+1 day':
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 week':
                $current_end->modify('+6 days');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 month':
                $current_end->modify('last day of this month');
                $current_end->setTime(23, 59, 59);
                break;
            case '+1 year':
                $current_end->modify('last day of December this year');
                $current_end->setTime(23, 59, 59);
                break;
            default:
                throw new \Exception(_('Unknown formatting'));
        }
    }

    protected function get_xaxis(string $interval, \DateTime $current, string $locale) : string
    {
        switch($interval) {
            case '+1 day':
                $day = $current->format('d');
                $month = DateHelper::get_month_with_translation(intval($current->format('m')) - 1, $locale);
                return $day . '-' . $month;
            case '+1 week':
                $week = $current->format('W');
                $year = $current->format('Y');
                return $week . '-' . $year;
            case '+1 month':
                $month = DateHelper::get_month_with_translation(intval($current->format('m')) - 1, $locale);
                $year = $current->format('Y');
                return $month . '-' . $year;
            case '+1 year':
                $year = $current->format('Y');
                return $year;
            default:
                throw new \Exception(_('Unknown formatting'));
        }
    }

    protected function mock_data()
    {
        $ret = [];
        $now = new \DateTime('now');
        $current = new \DateTime('now');
        $days = 3*366;
        $current->modify('-' . $days . ' day');
        for($i = 0; $i < 3*$days + 40; ++$i) {
            if($i % 3 == 0) {
                $current->modify('+1 day');
            }
            $ret[] = ['created' => (clone $current), 'purchase_price' => 1, 'selling_price' => 1+1.*1/10. ];
        }
        return $ret;
    }

    public function get_categories_paid(\DateTime $start, \DateTime $end, $locale)
    {
        $query = "SELECT cat.name as category_name, scat.name as sub_category_name, SUM(ap.purchase_price) as purchase_price, SUM(ap.selling_price) as selling_price FROM " . $this->getClassName() . " ap JOIN ap.category cat JOIN ap.sub_category scat WHERE ap.created >= '" . $start->format('Y-m-d H:i:s') . "' AND ap.created <= '" . $end->format('Y-m-d H:i:s') . "' GROUP BY ap.category, ap.sub_category";
        $categories_paid = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        for($i = 0; $i < sizeof($categories_paid); ++$i) {
            $categories_paid[$i]['category_name'] = ArticleCategoryTranslationEntity::get_translation($categories_paid[$i]['category_name'], $locale);
            $categories_paid[$i]['sub_category_name'] = ArticleSubCategoryTranslationEntity::get_translation($categories_paid[$i]['sub_category_name'], $locale);
        }
        $categories_paid = $this->sort_categories_paid($categories_paid);
        return ['start' => $start->format('Y-m-d'), 'end' => $end->format('Y-m-d'), 'data' => $categories_paid];
    }

    protected function sort_categories_paid(array $categories_paid) : array
    {
        for($i = 0; $i < sizeof($categories_paid); ++$i) {
            if($categories_paid[$i]['purchase_price'] != 0) {
                $categories_paid[$i]['profit'] = round(($categories_paid[$i]['selling_price']/$categories_paid[$i]['purchase_price'] -1.)*100., 2);
            } else {
                $categories_paid[$i]['profit'] = 0.;
            }
        }
        usort($categories_paid, function($a, $b) { return $a['selling_price'] < $b['selling_price']; });
        return $categories_paid;
    }
}
