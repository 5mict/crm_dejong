<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/8/2017
 * Time: 1:06 PM
 */

namespace AppBundle\Repository\User\Articles;

use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryTranslationEntity;
use AppBundle\Repository\AppEntityRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\BrowserKit\Request;

class ArticleRepository extends AppEntityRepository
{
    /**
     * @return int
     */
    public function getDefaultPageNumber() : int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function getDefaultPageSize(): int
    {
        return 12;
    }

    /**
     * @param int $page_number
     * @param int $page_size
     * @param string|null $sort_by
     * @param string|null $sort_direction
     * @return array
     */
    public function get_articles(int $page_number, int $page_size, string $sort_by = null, string $sort_direction = null) : array
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getPrimaryKeyColumnName();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $articles = $this->findBy([], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$articles, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction] ;
    }

    /**
     * @return array
     */
    public function get_articlesData()
    {
       return $this->findAll();
    }

    /**
     * @param string $locale
     * @return array
     */
    public function get_article_number_name(string $locale)
    {
        $query = '';
        switch($locale) {
            case 'en':
                $query = "SELECT a.article_name as article_name, a.article_number, a.picture_url as article_image, a.description as article_description FROM " . $this->getClassName() . " a ORDER BY a.article_number ASC";
                break;
            case 'nl':
                $query = "SELECT a.article_name_dutch as article_name, a.article_number, a.picture_url as article_image, a.description as article_description FROM " . $this->getClassName() . " a ORDER BY a.article_number ASC";
                break;
            default:
                $query = "SELECT a.article_name as article_name, a.article_number, a.picture_url as article_image, a.description as article_description FROM " . $this->getClassName() . " a ORDER BY a.article_number ASC";
                break;
        }
        $article_number_names = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $article_number_names;
    }

    /**
     * @return array
     */
    public function get_article_category_prices(string $locale)
    {
        $query = "SELECT a FROM "  . $this->getClassName() . " a WHERE a.profit_percents > 0 ORDER BY a.profit_percents DESC";
        $articles = $this->getEntityManager()->createQuery($query)
            ->getResult();
        $data = [];
        foreach($articles as $article) {
            if(isset($data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)])){
                $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['total_selling'] =
                    $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['total_selling'] + $article->getTotalSellingPrice();
                $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['total_purchase'] =
                    $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['total_purchase'] + $article->getTotalPurchasePrice();
            } else {
                $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['total_selling'] = $article->getTotalSellingPrice();
                $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['total_purchase'] = $article->getTotalPurchasePrice();
                $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['category'] =
                    ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale);
                $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['sub_category'] =
                    ArticleSubCategoryTranslationEntity::get_translation($article->getArticleSubCategory()->getName(), $locale);
                $data[ArticleCategoryTranslationEntity::get_translation($article->getArticleCategory()->getName(), $locale)]['selling_price'] =
                    $article->getTotalSellingPrice();
            }
        }

        return $data;
    }
}
