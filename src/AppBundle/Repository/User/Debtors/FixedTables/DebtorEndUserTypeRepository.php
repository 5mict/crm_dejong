<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 1:14 PM
 */

namespace AppBundle\Repository\User\Debtors\FixedTables;

use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class DebtorEndUserTypeRepository extends AppFixedTableEntityRepository
{
    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT e.name as name, et.name as trans FROM \AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeTranslationEntity et JOIN et.debtor_end_user_type e WHERE et.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT e.name FROM " . $this->getClassName() . " e";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }
    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return DebtorEndUserTypeEntity
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : DebtorEndUserTypeEntity
    {
        $debtor_end_user_type_entity = $em->getRepository(DebtorEndUserTypeEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($debtor_end_user_type_entity)) {
            $debtor_end_user_type_entity = new DebtorEndUserTypeEntity();
            $debtor_end_user_type_entity->setName($data['name']);
        }
        return $debtor_end_user_type_entity;
    }
}