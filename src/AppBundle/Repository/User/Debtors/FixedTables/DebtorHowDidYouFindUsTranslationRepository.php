<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 11:54 AM
 */

namespace AppBundle\Repository\User\Debtors\FixedTables;

use AppBundle\Controller\User\Debtors\Exceptions\HowDidYouFindUsNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class DebtorHowDidYouFindUsTranslationRepository extends AppFixedTableEntityRepository
{
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT ht.name, l.language FROM " . $this->getClassName() . " ht JOIN ht.locale l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return DebtorHowDidYouFindUsTranslationEntity
     * @throws HowDidYouFindUsNotFoundException
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : DebtorHowDidYouFindUsTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $debtor_how_did_you_find_us_entity = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($debtor_how_did_you_find_us_entity)) {
            throw new HowDidYouFindUsNotFoundException(_('How did you us not found'));
        }
        $debtor_how_did_you_find_us_translation_entity = $em->getRepository(DebtorHowDidYouFindUsTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($debtor_how_did_you_find_us_translation_entity)) {
            $debtor_how_did_you_find_us_translation_entity = new DebtorHowDidYouFindUsTranslationEntity();
            $debtor_how_did_you_find_us_translation_entity->setName($data['name']);
            $debtor_how_did_you_find_us_translation_entity->setLocale($locale_entity);
        }
        $debtor_how_did_you_find_us_translation_entity->setHowDidYouFindUs($debtor_how_did_you_find_us_entity);
        return $debtor_how_did_you_find_us_translation_entity;
    }
}