<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 11:33 AM
 */

namespace AppBundle\Repository\User\Debtors\FixedTables;

use AppBundle\Controller\User\Debtors\Exceptions\EndUserTypeNotFoundException;
use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeTranslationEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class DebtorEndUserTypeTranslationRepository extends AppFixedTableEntityRepository
{
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT et.name, l.language FROM " . $this->getClassName() . " et JOIN et.locale l";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return DebtorEndUserTypeTranslationEntity
     * @throws EndUserTypeNotFoundException
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : DebtorEndUserTypeTranslationEntity
    {
        $locale_entity = $em->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $data['language']]);
        if(empty($locale_entity)) {
            throw new LocaleEntityNotFoundException(_('Locale entity not found'));
        }
        $debtor_end_user_type_entity = $em->getRepository(DebtorEndUserTypeEntity::class)
            ->findOneBy(['name' => $data['reference']]);
        if(empty($debtor_end_user_type_entity)) {
            throw new EndUserTypeNotFoundException(_('End user type not found'));
        }
        $debtor_end_user_type_translation_entity = $em->getRepository(DebtorEndUserTypeTranslationEntity::class)
            ->findOneBy(['name' => $data['name'], 'locale' => $locale_entity]);
        if(empty($debtor_end_user_type_translation_entity)) {
            $debtor_end_user_type_translation_entity = new DebtorEndUserTypeTranslationEntity();
            $debtor_end_user_type_translation_entity->setName($data['name']);
            $debtor_end_user_type_translation_entity->setLocale($locale_entity);
        }
        $debtor_end_user_type_translation_entity->setDebtorEndUserType($debtor_end_user_type_entity);
        return $debtor_end_user_type_translation_entity;
    }
}