<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 1:15 PM
 */

namespace AppBundle\Repository\User\Debtors\FixedTables;

use AppBundle\Entity\FixedTables\Exceptions\LocaleEntityNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Repository\AppFixedTableEntityRepository;
use Doctrine\ORM\EntityManagerInterface;

class DebtorHowDidYouFindUsRepository extends AppFixedTableEntityRepository
{
    /**
     * @param string $locale
     * @return array
     */
    public function get_all_with_translation(string $locale) : array
    {
        $locale_entity = $this->getEntityManager()->getRepository(LocaleEntity::class)
            ->findOneBy(['language' => $locale]);
        $query = "SELECT h.name as name, ht.name as trans FROM \AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsTranslationEntity ht JOIN ht.how_did_you_find_us h WHERE ht.locale = '" . $locale_entity->getId() . "'";
        $result = $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
        return $result;
    }
    /**
     * @return array
     */
    public function get_table_rows_without_id() : array
    {
        $query = "SELECT h.name FROM " . $this->getClassName() . " h";
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }

    /**
     * @param array $data
     * @param EntityManagerInterface $em
     * @return DebtorHowDidYouFindUsEntity
     * @throws LocaleEntityNotFoundException
     */
    public static function from_model(array $data, EntityManagerInterface $em) : DebtorHowDidYouFindUsEntity
    {
        $debtor_how_did_you_find_us_entity = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
            ->findOneBy(['name' => $data['name']]);
        if(empty($debtor_how_did_you_find_us_entity)) {
            $debtor_how_did_you_find_us_entity = new DebtorHowDidYouFindUsEntity();
            $debtor_how_did_you_find_us_entity->setName($data['name']);
        }
        return $debtor_how_did_you_find_us_entity;
    }
}