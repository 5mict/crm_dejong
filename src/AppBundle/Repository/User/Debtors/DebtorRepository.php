<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/24/2017
 * Time: 4:54 PM
 */

namespace AppBundle\Repository\User\Debtors;

use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Repository\AppEntityRepository;

class DebtorRepository extends AppEntityRepository
{
    /**
     * @return int
     */
    public function getDefaultPageNumber() : int
    {
        return 1;
    }

    /**
     * @param int $page_number
     * @param int $page_size
     * @param string|null $sort_by
     * @param string|null $sort_direction
     * @return array
     */
    public function get_debtors(int $page_number, int $page_size, string $sort_by = null, string $sort_direction = null) : array
    {
        if($page_number == 0) {
            $page_number = $this->getDefaultPageNumber();
        }
        if($page_size == 0) {
            $page_size = $this->getDefaultPageSize();
        }
        if(empty($sort_by)) {
            $sort_by = $this->getPrimaryKeyColumnName();
        }
        if(empty($sort_direction)) {
            $sort_direction = $this->getDefaultSortDirection();
        }
        $this->check_ordering_consistency($sort_by, $sort_direction);
        $total_number_of_pages = $this->getTotalNumberOfPages($page_size);
        $debtors = $this->findBy([], [$sort_by => $sort_direction], $page_size, ($page_number - 1) * $page_size);
        return [$debtors, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction] ;
    }

    /**
     * @return array
     */
    public function get_debtorsData()
    {
        return $this->findAll();
    }

    /**
     * @return array
     */
    public function get_how_did_you_us_data() :  array
    {
        $total = 0.0;
        $data = [];
        foreach(DebtorHowDidYouFindUsEntity::WAYS_OF_FINDING as $way_of_finding) {
            $data[$way_of_finding . '-total'] = $this->found_by($way_of_finding);
            $total += $data[$way_of_finding . '-total'];
        }
        foreach(DebtorHowDidYouFindUsEntity::WAYS_OF_FINDING as $way_of_finding) {
            $data[$way_of_finding] = $data[$way_of_finding . '-total']/$total;
        }
        return $data;
    }

    /**
     * @return array
     */
    public function get_how_did_you_find_use_purching_data() : array
    {
        $total = 0.0;
        $data = [];
        foreach(DebtorHowDidYouFindUsEntity::WAYS_OF_FINDING as $way_of_finding) {
            $debotors_total_price = $this->get_debtors_total_selling_by_how_did_they_find_us($way_of_finding);
            foreach($debotors_total_price as $item){
                $total += $item['total_selling_price'];
            }
            $data[$way_of_finding] = $total;
            $total = 0.0;
        }
        return $data;
    }

    /**
     * @return array
     */
    public function get_countries_data()
    {
        $data = [];
        $debtor_data = [];
        $query = "SELECT DISTINCT d.country FROM " . $this->getClassName() . " d";
        $debtor_unique_countries = array_column($this->getEntityManager()->createQuery($query)
            ->getArrayResult(), 'country');
        foreach($debtor_unique_countries as $debtor_country ){
            $total_profit = 0.0;
            $total_purchase = 0.0;
            $total_selling = 0.0;
            $debtor_countries = $this->getEntityManager()->getRepository(DebtorEntity::class)
                ->findBy(['country' => $debtor_country]);
            foreach($debtor_countries as $item){
                $total_selling += $item->getTotalSellingPrice();
                $total_purchase += $item->getTotalPurchasePrice();
                $total_profit += $item->getProfitPercents();
            }
            $debtor_data["name"] = $debtor_country;
            $debtor_data["total_selling_price"] = $total_selling;
            $debtor_data["total_purchase_price"] = $total_purchase;
            $debtor_data["profit_percents"] = $total_profit;
            $data[$debtor_country] = $debtor_data;
        }
        return $data;
    }

    /**
     * @param string $way_of_finding
     * @return mixed
     */
    protected function found_by(string $way_of_finding)
    {
        $hdyfu_entity = $this->getEntityManager()->getRepository(DebtorHowDidYouFindUsEntity::class)
            ->findOneBy(['name' => $way_of_finding]);
        $query = "SELECT COUNT(1) FROM " . $this->getClassName() . " d WHERE d.how_did_you_find_us = " . $hdyfu_entity->getId();
        return $this->getEntityManager()->createQuery($query)
            ->getSingleScalarResult();
    }

    /**
     * @return array
     */
    public function get_debtor_names()
    {
        $query = "SELECT d.name FROM " . $this->getClassName() . " d";
        $debtor_names = array_column($this->getEntityManager()->createQuery($query)
            ->getArrayResult(), 'name');
        return $debtor_names;
    }

    /**
     * @return array
     */
    public function get_debtors_by_total_selling_price()
    {
        $query = "SELECT d FROM " . $this->getClassName() . " d ORDER BY d.total_selling_price DESC";
        $debtors = $this->getEntityManager()->createQuery($query)->setMaxResults(50)
            ->getArrayResult();
        return $debtors;
    }

    /**
     * @return array
     */
    public function get_debtors_by_total_selling_price_in_interval($start, $end, $locale) : array
    {
        $query = "SELECT d.name AS debtor_name, sum( o.total_selling_amount ) AS total_selling_price, sum( o.total_purchase_amount ) AS total_purchase_price FROM " .
            $this->getClassName() .
            " d JOIN \AppBundle\Entity\User\Orders\OrderEntity o WITH o.debtor = d.id WHERE o.created >= '" .
            $start .
            "' AND o.created <= '" .
            $end .
            "' AND ( o.order_status_name = '" .
            OrderStatusEntity::FINISHED .
            "' OR o.order_status_name ='" .
            OrderStatusEntity::POST_CALCULATION_READY .
            "') AND ( o.total_selling_amount > 0 OR o.total_purchase_amount > 0 ) GROUP BY d.id ORDER BY total_selling_price DESC";
        $debtors = $this->getEntityManager()->createQuery($query)
            ->getResult();
        return $debtors;
    }

    /**
     * @return array
     */
    public function get_countries_data_in_interval($start, $end, $locale) : array
    {
        $query = "SELECT d.country AS country_name, sum( o.total_purchase_amount ) AS country_total_purchase_price, sum( o.total_selling_amount ) AS country_total_selling_price FROM " .
            $this->getClassName() .
            " d JOIN \AppBundle\Entity\User\Orders\OrderEntity o WITH o.debtor = d.id WHERE o.created >= '" .
            $start .
            "' AND o.created <= '" .
            $end .
            "' AND ( o.order_status_name = '" .
            OrderStatusEntity::FINISHED .
            "' OR o.order_status_name ='" .
            OrderStatusEntity::POST_CALCULATION_READY .
            "') AND ( o.total_selling_amount > 0 OR o.total_purchase_amount > 0 ) GROUP BY d.country ORDER BY d.country";
        $countries = $this->getEntityManager()->createQuery($query)->getResult();
        error_log( json_encode($countries ) );
        return $countries;
    }

    /**
     * @param string $way_of_finding
     * @return array
     */
    public function get_debtors_total_selling_by_how_did_they_find_us(string $way_of_finding){
        $hdyfu_entity = $this->getEntityManager()->getRepository(DebtorHowDidYouFindUsEntity::class)
            ->findOneBy(['name' => $way_of_finding]);
        $query = "SELECT d.total_selling_price FROM " . $this->getClassName() . " d WHERE d.how_did_you_find_us = " . $hdyfu_entity->getId();
        return $this->getEntityManager()->createQuery($query)
            ->getArrayResult();
    }
}