<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 9/17/2017
 * Time: 8:10 PM
 */
namespace AppBundle\Repository\Login;

use AppBundle\Entity\Login\RoleEntity;
use AppBundle\Entity\Login\UserEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserRepository extends EntityRepository implements  UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        $user = $this->findOneBy(['username' => $username]);
        if($user == null)
            throw new UsernameNotFoundException();
        $_roles = $this->getEntityManager()
            ->getRepository(RoleEntity::class)
            ->findBy(['user_id' => $user->getId()]);
        $roles = new ArrayCollection();
        foreach($_roles as $role) {
            $roles->add($role->getRole());
        }
        $user->setRoles($roles);
        return $user;
    }

    public function get_all_users() : array
    {
        $users = $this->findAll();
        $_users = [];
        foreach($users as $user) {
            $roles = $this->get_roles($user->getId());
            if(!$roles->contains('ROLE_SUPER_ADMIN')) {
                $user->setRoles($roles);
                $_users[] = $user;
            }
        }
        return $_users;
    }

    public function get_all_users_username()
    {
        $users = $this->findAll();
        $_users = [];
        foreach($users as $user){
            array_push($_users, $user->getUsername());
        }
        return $_users;
    }

    public function get_user(int $user_id) : UserEntity
    {
        $user = $this->findOneBy(['id' => $user_id]);
        if(empty($user)) {
            throw new \Exception('User does not exist.');
        }
        $user->setRoles($this->get_roles($user_id));
        return $user;
    }


    public function update_user(int $user_id)
    {

    }

    public function delete_user(int $user_id)
    {
        $user = $this->findOneBy(['id' => $user_id]);
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }

    public function get_roles(int $user_id)
    {
        $_roles = $this->getEntityManager()
            ->getRepository(RoleEntity::class)
            ->findBy(['user_id' => $user_id]);
        $roles = new ArrayCollection();
        foreach($_roles as $role) {
            $roles->add($role->getRole());
        }
        return $roles;
    }

    /**
     * @param string $username
     * @param string $encoded_passwd
     * @return int
     */
    public function update_password(
        string $username,
        string $encoded_passwd
    ) : int
    {
        $user = $this->findOneBy(['username' => $username]);
        $user->setPassword($encoded_passwd);
        $user->setUpdated(new \DateTime('now'));
        $this->getEntityManager()->persist($user);
        $this->getEntityManager()->flush();
        return $user->getId();
    }
}