<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/13/2017
 * Time: 3:18 PM
 */
namespace AppBundle\Repository\Login;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class RoleRepository extends EntityRepository
{
    public function delete_roles(int $user_id, EntityManagerInterface $em)
    {
        $roles = $this->findBy(['user_id' => $user_id]);
        foreach($roles as $role) {
            $em->remove($role);
        }
        $em->flush();
    }
}