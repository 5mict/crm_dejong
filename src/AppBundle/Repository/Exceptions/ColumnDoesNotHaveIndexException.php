<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/3/2017
 * Time: 11:43 AM
 */

namespace AppBundle\Repository\Exceptions;

class ColumnDoesNotHaveIndexException extends \Exception
{

}