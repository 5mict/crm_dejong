<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/29/17
 * Time: 10:34 PM
 */

namespace AppBundle\ViewModels\User;

use AppBundle\Entity\Login\UserEntity;

class UpdateUserEntity
{
    public $username;
    public $first_name;
    public $last_name;
    public $phonenumber;
    public $email;
    public $is_active;
    public $roles;
    public $id;

    public function __construct(UserEntity $user_entity)
    {
        $this->username = $user_entity->getUsername();
        $this->first_name = $user_entity->getFirstName();
        $this->last_name = $user_entity->getLastName();
        $this->phonenumber = $user_entity->getPhonenumber();
        $this->email = $user_entity->getEmail();
        $this->is_active = $user_entity->getIsActive();
        $roles = $user_entity->getRoles();
        $this->roles = [
            'user' => in_array('ROLE_USER', $roles),
            'admin' => in_array('ROLE_ADMIN', $roles)
        ];
        $this->id = $user_entity->getId();
    }
}