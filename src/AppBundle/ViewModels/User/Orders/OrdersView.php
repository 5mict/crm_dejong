<?php
/**
 * Created by PhpStorm.
 * User: Strale
 * Date: 25.12.2017.
 * Time: 11.19
 */

namespace AppBundle\ViewModels\User\Orders;

use AppBundle\Entity\User\Orders\OrderEntity;

class OrdersView
{
    public $id;
    public $order_number;
    public $order_datum;
    public $debtor_name;
    public $order_supervisor;
    public $order_status;

    /**
     * OrdersView constructor.
     * @param OrderEntity $order
     */
    public function __construct(OrderEntity $order)
    {
        $this->id = $order->getId();
        $this->order_number = $order->getOrderNumber();
        $this->order_datum = $order->getDeliveryTimeString();
        $this->debtor_name = $order->getDebtorName();
        $this->order_supervisor = $order->getOrderSupervisor();
        $this->order_status = $order->getOrderStatusString();
    }
}