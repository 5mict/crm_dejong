<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/26/17
 * Time: 11:20 PM
 */

namespace AppBundle\ViewModels\User\Orders;

use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;

class OrderArticlesViewSuppliers
{
    public $supplier_name;

    public function __construct(SupplierEntity $supplier_entity) {
        $this->supplier_name = $supplier_entity->getName();
    }
}