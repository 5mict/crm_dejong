<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/26/17
 * Time: 11:20 PM
 */

namespace AppBundle\ViewModels\User\Orders;

use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;

class OrderArticlesViewArticles
{
    public $article_number;
    public $article_name;

    public function __construct(ArticleEntity $article_entity) {
        $this->article_number = $article_entity->getArticleName();
        $this->article_name = $article_entity->getArticleName();
    }
}