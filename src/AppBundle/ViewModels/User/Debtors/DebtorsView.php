<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/25/2017
 * Time: 2:21 PM
 */

namespace AppBundle\ViewModels\User\Debtors;

use AppBundle\Entity\User\Debtors\DebtorEntity;

class DebtorsView
{
    public $id;
    public $name;
    public $email;
    public $city;

    public function __construct(DebtorEntity $debtor)
    {
        $this->id = $debtor->getId();
        $this->name = $debtor->getName();
        $this->email = $debtor->getEmail();
        $this->city = $debtor->getCity();
    }
}