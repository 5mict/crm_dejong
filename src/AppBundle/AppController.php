<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/28/17
 * Time: 4:23 PM
 */
namespace AppBundle;

use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\Login\UserEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use \Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


/**
 * Class AppController
 * @package AppBundle
 */
class AppController extends Controller
{
    /**
     * @param string $grant
     * @param EntityManagerInterface $em
     * @return bool
     */
    public function is_granted(string $grant, EntityManagerInterface $em)
    {
        $grant = $em->getRepository(GrantEntity::class)
            ->findOneBy(['_grant' => $grant]);
        if(!empty($grant) && $grant->getEnabled()) {
            return true;
        }
        return false;
    }

    /**
     * @param $object
     * @return string
     */
    protected function json_encode($object) :string
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize($object, 'json');
    }
    /**
     * @param string $code
     * @param string $message
     * @return Response
     */
    public function handle_html_error(string $code, string $message) : Response
    {
        return $this->render(
            'error/error.html.twig',
            [
                'error' => ['code' => $code, 'message' => $message],
                'company' => $this->get_company(),
                'user' => $this->getUser()
            ]
        );
    }

    /**
     * @param string $code
     * @param string $message
     * @return Response
     */
    public function handle_json_error(string $code, string $message) : Response
    {
        if(strpos($message, 'Integrity constraint violation:') != false) {
            $message = explode('Integrity constraint violation:', $message);
            if (sizeof($message) > 1) {
                $message = explode('for key', $message[1]);
                if (sizeof($message) > 1) {
                    $message = $message[0];
                    $message = str_replace('1062 Duplicate entry', 'Given entry ', $message);
                    $message = $message . 'already exist. Please enter new one.';
                }
            }
            else {
                $message = $message[0];
            }
        }
        if(strpos($message, 'a foreign key constraint fails')) {
            $message = "1451 There is object referencing this object, so it can not be deleted.";
        }
        $response = new Response();
        $response->setStatusCode($code);
        $response->setContent(json_encode(['error_message' => $message]));
        return $response;
    }

    /**
     * @param $picture_tmp_name
     * @param $picture_name
     * @param $path
     * @return bool
     * @throws \Exception
     * @internal param $action
     */
    public function upload_validate(string $picture_tmp_name, string $picture_name, string $path) {

        $location_upload = $this->get_company() . $path . "/" . basename($picture_name);
        $imageType = pathinfo($location_upload, PATHINFO_EXTENSION);
        if (empty($picture_name)){
            return false;
        }
        if($imageType != "jpg" && $imageType != "png" &&
            $imageType != "jpeg" && $imageType != "JPG" &&
            $imageType != "PNG" && $imageType != "JPEG" &&
            $imageType != "PDF" && $imageType != "pdf"){
            throw new \Exception('Sorry, only PDF, JPG, JPEG & PNG are allowed.');
        }
        if (file_exists($location_upload)) {
            return true;
        }
        if(!(move_uploaded_file($picture_tmp_name, $location_upload))) {
            throw new \Exception('Sorry, there was an error uploading your file.');
        }
        return true;
    }

    /**
     * @param $picture_tmp_name
     * @param $picture_name
     * @param $path
     * @return bool
     * @throws \Exception
     * @internal param $action
     */
    public function upload_validate_file(string $picture_tmp_name, string $picture_name, string $path) {

        $location_upload = $this->get_company() . $path . "/" . basename($picture_name);
        $imageType = pathinfo($location_upload, PATHINFO_EXTENSION);
        if (empty($picture_name)){
            return false;
        }
        if (file_exists($location_upload)) {
            return true;
        }
        if(!(move_uploaded_file($picture_tmp_name, $location_upload))) {
            throw new \Exception('Sorry, there was an error uploading your file.');
        }
        return true;
    }


    /**
     * @param string $path
     * @param string $file_name
     * @throws \Exception
     */
    public function delete_validate(string $path, string $file_name){
        $location_delete = $this->get_company() . $path . "/" . basename($file_name);
        if(!unlink($location_delete)){
            throw new \Exception('Sorry, there was an error deleting your file.');
        }
    }

    /**
     * @return string
     */
    protected function get_company() : string
    {
        return 'Company/DejongInterieur/';
    }

    /**
     * @param int $length
     * @return string
     */
    protected function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * @return mixed
     */
    protected function get_debtors_by_delivery_time()
    {
        $em = $this->getDoctrine()->getManager();
        $debtors_by_delivery_time = $em->getRepository(OrderEntity::class)
            ->get_debtors_by_delivery_time();
        return $debtors_by_delivery_time;
    }

    /**
     * @return mixed
     */
    protected function get_suppliers_by_delivery_time()
    {
        $em = $this->getDoctrine()->getManager();
        $suppliers_by_delivery_time = $em->getRepository(OrderArticleEntity::class)
            ->get_suppliers_by_delivery_time();
        return $suppliers_by_delivery_time;
    }

    /**
     * @param $picture_name
     * @return string
     */
    protected function picture_encode($picture_name){
        $path = $this->get_company() . 'images/' . $picture_name;
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $image_data = file_get_contents($path);
        $picture_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
        return $picture_base64;
    }
}