<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 2/12/2018
 * Time: 4:21 PM
 */

namespace AppBundle\Controller\TestConfig;

use AppBundle\AppController;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Orders\InvoiceNumbers;
use Doctrine\ORM\Cache\EntityCacheEntry;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestInvoiceNumbering extends AppController
{
    /**
     * @Route("/test-numbering", name="test-numbering")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function test_numbering(Request $request, EntityManagerInterface $em)
    {
        set_time_limit(0);
        try {
            $invoice_number_repository = $em->getRepository(InvoiceNumbers::class);
            for ($i = 0; $i < 1000; ++$i) {
                echo $invoice_number_repository->getInvoiceNumber(DebtorEndUserTypeEntity::COMPANY) . '<br/>';
            }
            $response = new Response();
            $response->setStatusCode(400);
            return $response;
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
}