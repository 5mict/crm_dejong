<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/8/2017
 * Time: 1:44 PM
 */

namespace AppBundle\Controller\TestConfig;

use AppBundle\AppController;
use AppBundle\Entity\User\Articles\ArticleEntity;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class CreateArticlesController extends AppController
{
    /**
     * @Route("/create-even-articles", name="create-even-articles")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function create_even_articles(Request $request, EntityManagerInterface $em)
    {
        try {
            set_time_limit(0);
            for($i = 200; $i < 200+1000; $i = $i + 2) {
                $article_entity = new ArticleEntity();
                $article_entity->setArticleNumber('even-' . $i);
                $article_entity->setArticleName($this->generateRandomString(10));
                $article_entity->setSize('1 x 2 x 3');
                $article_entity->setPurchasePrice(160.0);
                $article_entity->setSellingPrice(180.0);
                $article_entity->setSellingPriceWithPdv(200.0);
                $article_entity->setDescription('description');
                $article_entity->setUsername($this->getUser()->getUsername());
                $article_entity->setCreated(new \DateTime('now'));
                $article_entity->setUpdated(new \DateTime('now'));
                $article_entity->setPictureUrl('');
                $em->persist($article_entity);
                $em->flush();
            }
            $response = new Response();
            $response->setContent(json_encode(['all even articles has been created']));
            return $response;
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @Route("/create-odd-articles", name="create-odd-articles")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function create_odd_articles(Request $request, EntityManagerInterface $em)
    {
        try {
            set_time_limit(0);
            for($i = 201; $i < 201+1000; $i = $i + 2) {
                $article_entity = new ArticleEntity();
                $article_entity->setArticleNumber('odd-' . $i);
                $article_entity->setArticleName($this->generateRandomString(10));
                $article_entity->setSize('1 x 2 x 3');
                $article_entity->setPurchasePrice(160.0);
                $article_entity->setSellingPrice(180.0);
                $article_entity->setSellingPriceWithPdv(200.0);
                $article_entity->setDescription('description');
                $article_entity->setUsername($this->getUser()->getUsername());
                $article_entity->setCreated(new \DateTime('now'));
                $article_entity->setUpdated(new \DateTime('now'));
                $article_entity->setPictureUrl('');
                $em->persist($article_entity);
                $em->flush();
            }
            $response = new Response();
            $response->setContent(json_encode(['all odd has been created']));
            return $response;
        } catch(\Exception $e) {
            echo $e->getMessage();
        }
    }
}