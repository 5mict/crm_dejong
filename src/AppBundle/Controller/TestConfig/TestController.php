<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/21/2017
 * Time: 9:31 PM
 */

namespace AppBundle\Controller\TestConfig;

use AppBundle\AppController;
use PHPMailer\PHPMailer\PHPMailer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestController extends AppController
{
    /**
     * @Route("/test-datetime", name="test-datetime")
     * @Method({"GET"})
     * @param Request $request
     * @return Response
     */
    public function test_datetime(Request $request)
    {
        $datetime_format = new \DateTimeZone('America/New_York');
        $datetime = \DateTime::createFromFormat('Y-m-d', '2017-05-06', $datetime_format);
        echo json_encode($datetime);
        echo '<br/>';
        $datetime->modify('first day of January this year');
        echo json_encode($datetime);
        echo '<br/>';
        $datetime->modify('last day of December this year');
        echo json_encode($datetime);
        echo '<br/>';
        $datetime->modify('first day of January next year');
        echo json_encode($datetime);
        echo '<br/>';
        $datetime->modify('last day of December next year');
        echo json_encode($datetime);
        echo '<br/>';
        echo json_encode(new \DateTime('now'));
        echo '<br/>';
        $response = new Response();
        return $response;
    }
}