<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 9/13/2017
 * Time: 7:42 PM
 */
namespace AppBundle\Controller\Admin\FurnitureDatabase;

use AppBundle\AppController;
use AppBundle\Controller\Admin\FurnitureDatabase\Exceptions\PasswordsDoNotMatchException;
use AppBundle\Controller\Admin\FurnitureDatabase\Exceptions\PasswordTooWeakException;
use AppBundle\Entity\Login\RoleEntity;
use AppBundle\Entity\Login\UserEntity;
use AppBundle\ViewModels\User\UpdateUserEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class FurnitureDatabaseController
 */
class FurnitureDatabaseController extends AppController
{
    /**
     * FurnitureDatabaseController constructor.
     */
    public function __construct()
    {

    }
    /**
     * @Route("/admin/users", name="users", schemes={"https"})
     * @param Request $request
     * @return Response
     */
    public function users(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository(UserEntity::class)
            ->get_all_users();
        return $this->render(
            ':admin/FurnitureDatabase:index.html.twig',
            [
                'users' => $users,
                'company' => $this->get_company(),
                'user' => $this->getUser(),
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]
        );
    }
    /**
     * @Route("/admin/get-user", name="get-user.json", schemes={"https"})
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function get_user(Request $request)
    {
        try {
            $encoders = array(new XmlEncoder(), new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());
            $serializer = new Serializer($normalizers, $encoders);
            $user_id = intval($request->get('id'));
            $user = $this->getDoctrine()
                ->getRepository(UserEntity::class)
                ->get_user($user_id);
            $view_user = new UpdateUserEntity($user);
            $json_user = $serializer->serialize($view_user, 'json');
            $response = new Response();
            $response->setContent($json_user);
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/admin/add-user", name="post-add-user.json", schemes={"https"})
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function create_user(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder
    ) {
        try {
            $user_entity = new UserEntity();
            $user_entity->setUsername($request->get('username'));
            $user_entity->setFirstName($request->get('first_name'));
            $user_entity->setLastName($request->get('last_name'));
            $user_entity->setPhonenumber($request->get('phonenumber'));
            $passwd = $request->get('password');
            $confirm_passwd = $request->get('confirm_password');
            if ($passwd != $confirm_passwd)
                throw new PasswordsDoNotMatchException(_('Entered passwords do not match.'));
            if($passwd !== '') {
                $this->check_password($passwd);
                $encoded_passwd = $encoder->encodePassword(
                    $user_entity,
                    $passwd
                );
                $user_entity->setPassword($encoded_passwd);
            }
            $user_entity->setEmail($request->get('email'));
            $user_entity->setCreated(new \DateTime('now'));
            $user_entity->setUpdated(new \DateTime('now'));
            $em->persist($user_entity);
            $roles = $request->get('roles');
            foreach ($roles as $role) {
                $role_entity = new RoleEntity();
                $role_entity->setUser($user_entity);
                if ($role == 'user')
                    $role_entity->setRole('ROLE_USER');
                if ($role == 'admin')
                    $role_entity->setRole('ROLE_ADMIN');
                $em->persist($role_entity);
            }
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['created' => $user_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/admin/delete-user", name="delete-user.json", schemes={"https"})
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function delete_user(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder
    ) {
        try {
            $user_id = intval($request->get('id'));
            $this->getDoctrine()
                ->getManager()
                ->getRepository(UserEntity::class)
                ->delete_user($user_id);
            $response = new Response();
            $response->setContent(json_encode(['deleted' => $user_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/admin/update-user", name="update-user.json", schemes={"https"})
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function update_user(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder
    ) {
        try {
            $user_id = intval($request->get('id'));
            $em->getRepository(RoleEntity::class)
                ->delete_roles($user_id, $em);
            $user = $em->getRepository(UserEntity::class)
                ->get_user($user_id);
            $user->setFirstName($request->get('first_name'));
            $user->setLastName($request->get('last_name'));
            $user->setPhonenumber($request->get('phonenumber'));
            $user->setEmail($request->get('email'));
            $is_active = $request->get('is_active');
            $user->setIsActive(json_decode($is_active));
            $user->setUpdated(new \DateTime('now'));
            $passwd = $request->get('password');
            $confirm_passwd = $request->get('confirm_password');
            if($passwd !== $confirm_passwd)
                throw new PasswordsDoNotMatchException(_('Passwords do not match.'));
            if($passwd !== '') {
                $this->check_password($passwd);
                $encoded_passwd = $encoder->encodePassword(
                    $user,
                    $passwd
                );
                $user->setPassword($encoded_passwd);
            }
            $roles = $request->get('roles');
            foreach($roles as $role) {
                $role_entity = new RoleEntity();
                if($role == 'user')
                    $role_entity->setRole('ROLE_USER');
                if($role == 'admin')
                    $role_entity->setRole('ROLE_ADMIN');
                $role_entity->setUser($user);
                $em->persist($role_entity);
            }
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['updated', $user_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @param string $pass
     * @return bool
     * @throws PasswordTooWeakException
     */
    protected function check_password(string $pass) : bool
    {
        if(!preg_match('/[A-Z]/', $pass)) {
            throw new PasswordTooWeakException(_('Passsword do not contain at least one upper case letter'));
        }
        if(!preg_match('/[a-z]/', $pass)) {
            throw new PasswordTooWeakException(_('Passsword do not contain at least one lower case letter'));
        }
        if(!preg_match('/[0-9]/', $pass)) {
            throw new PasswordTooWeakException(_('Passsword do not contain at least one numeric value'));
        }
        if(strlen($pass) < 8) {
            throw new PasswordTooWeakException(_('Password must be at least 8 characters long'));
        }
        return true;
    }
}