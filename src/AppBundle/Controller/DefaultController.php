<?php

namespace AppBundle\Controller;

use AppBundle\AppController;
use AppBundle\Controller\User\Exceptions\LanguageNotSupportedException;
use AppBundle\Entity\Login\UserEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Repository\User\LocaleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends AppController
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param UserInterface $user_entity
     * @return Response
     */
    public function indexAction(Request $request, UserInterface $user_entity)
    {
        $roles = $this->getDoctrine()
            ->getRepository(UserEntity::class)
            ->get_roles($user_entity->getId());
        if($roles->contains('ROLE_SUPER_ADMIN')) {
            return $this->redirect('/super-admin');
        }
        if($roles->contains('ROLE_ADMIN')) {
            return $this->redirect('/admin/users');
        }
        if($roles->contains('ROLE_USER')) {
            return $this->redirect('/user');
        }
        return $this->redirect('/login');
    }

    /**
     * @Route("/change/language/to/{_locale}", name="change_language")
     * @Method({"PUT"})
     * @param string $_locale
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function change_language_to_locale(string $_locale, Request $request, EntityManagerInterface $em)
    {
        try {
            $locale = $request->getLocale();
            $languages = $em->getRepository(LocaleEntity::class)
                ->get_supported_languages();
            if(!in_array($_locale, $languages)) {
                throw new LanguageNotSupportedException(_('Language is not supported'));
            }
            $response = new Response();
            $response->setContent(json_encode($locale));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(500, $e->getMessage());
        }
    }

    /**
     * @Route("/run-script", name="script1")
     * @Method({"GET"})
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function script1(EntityManagerInterface $em)
    {
        $data = $em->getRepository(OrderArticleEntity::class)
            ->run_script();

        return new Response(json_encode($data));
    }

    /**
     * @Route("/run-script2", name="script2")
     * @Method({"GET"})
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function script2(EntityManagerInterface $em)
    {
        $data = $em->getRepository(OrderArticleEntity::class)
            ->run_script2();

        return new Response(json_encode($data));
    }
}
