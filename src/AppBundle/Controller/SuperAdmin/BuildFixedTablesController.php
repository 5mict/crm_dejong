<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/17/2017
 * Time: 2:31 PM
 */

namespace AppBundle\Controller\SuperAdmin;

use AppBundle\AppController;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryTranslationEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeTranslationEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusTranslationEntity;
use AppBundle\Entity\User\Orders\InvoiceNumbers;
use AppBundle\Repository\User\Orders\InvoiceNumberRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class BuildFixedTablesController extends AppController
{
    /**
     * @Route("/super-admin/build-fixed-tables", name="super-admin-build-fixed-tables", schemes={"https"})
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function build_fixed_tables(Request $request, EntityManagerInterface $em)
    {
        try {
            $table_names = [];
            $table_names[] = $em->getRepository(LocaleEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderDeliveryStatusEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderDeliveryStatusTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderStatusEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderStatusTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(DebtorHowDidYouFindUsTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(DebtorEndUserTypeTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(ArticleCategoryEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(ArticleCategoryTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(ArticleSubCategoryEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(ArticleSubCategoryTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderMailStatusEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderMailStatusTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderPaymentMethodEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderPaymentMethodTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderPaymentStatusEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderPaymentStatusTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderToSupplierStatusEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(OrderToSupplierStatusTranslationEntity::class)
                ->create_table();
            $table_names[] = $em->getRepository(InvoiceNumbers::class)
                ->create_table();
            $response = new Response();
            $response->setContent(json_encode([$table_names]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage() . "\n" . $e->getTraceAsString());
        }
    }
}