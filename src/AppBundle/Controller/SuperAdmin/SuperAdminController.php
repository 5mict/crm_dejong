<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/30/2017
 * Time: 11:20 AM
 */

namespace AppBundle\Controller\SuperAdmin;

use AppBundle\AppController;
use AppBundle\Entity\Grant\GrantEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SuperAdminController extends AppController
{
    /**
     * @Route("/super-admin", name="super-admin-grants", schemes={"https"})
     * @param Request $request
     * @return Response
     */
    public function get_grants(Request $request)
    {
        $grants = $this->getDoctrine()->getRepository(GrantEntity::class)
            ->findAll();
        return $this->render(
            'superAdmin/index.html.twig',
            [
                'grants' => $grants,
                'user' => $this->getUser()
            ]
        );
    }
    /**
     * @Route("/super-admin/add-grant", name="super-admin-add-grant", schemes={"https"})
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_grant(Request $request, EntityManagerInterface $em)
    {
        try {
            $grant = new GrantEntity();
            $grant->set_grant($request->get('_grant'));
            $grant->setEnabled(boolval($request->get('enabled')));
            $grant->setDescription($request->get('description'));
            $grant->setCreated(new \DateTime('now'));
            $grant->setUpdated(new \DateTime('now'));
            $em->persist($grant);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $grant->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/super-admin/update-grant", name="super-admin-update-grant", schemes={"https"})
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_grant(Request $request, EntityManagerInterface $em)
    {
        try {
            $grant_id = $request->get('id');
            $grant = $this->getDoctrine()
                ->getRepository(GrantEntity::class)
                ->findOneBy(['id' => intval($grant_id)]);
            if(empty($grant_id))
                throw new \Exception('Grant not found');
            $grant->set_grant($request->get('_grant'));
            $grant->setEnabled(json_decode($request->get('enabled')));
            $grant->setDescription($request->get('description'));
            $grant->setUpdated(new \DateTime('now'));
            $em->persist($grant);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $grant->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/super-admin/toggle", name="super-admin-grant-toggle", schemes={"https"})
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function toggle_grant(Request $request, EntityManagerInterface $em)
    {
        try {
            $grant_id = intval($request->get('id'));
            $grant_entity = $em->getRepository(GrantEntity::class)
                ->findOneBy(['id' => $grant_id]);
            if(empty($grant_entity))
                throw new \Exception('Grant not found');
            $grant_entity->toggle();
            $grant_entity->setUpdated(new \DateTime('now'));
            $em->persist($grant_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $grant_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/super-admin/delete-grant", name="super-admin-grant-delete", schemes={"https"})
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_grant(Request $request, EntityManagerInterface $em)
    {
        try {
            $grant_id = intval($request->get('id'));
            $grant_entity = $em->getRepository(GrantEntity::class)
                ->findOneBy(['id' => $grant_id]);
            if(empty($grant_entity)) {
                throw new \Exception('Grant not found');
            }
            $em->remove($grant_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $grant_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}