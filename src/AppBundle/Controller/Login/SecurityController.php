<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 9/17/2017
 * Time: 6:26 PM
 */
namespace AppBundle\Controller\Login;

use AppBundle\AppController;
use AppBundle\Controller\Admin\FurnitureDatabase\Exceptions\PasswordTooWeakException;
use AppBundle\Entity\Login\UserEntity;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AppController
{
    /**
     * @Route("/login", name="login", schemes={"https"})
     * @param Request $request
     * @param AuthenticationUtils $authUtils
     * @return Response
     */
    public function loginAction(Request $request, AuthenticationUtils $authUtils)
    {
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();
        $request->setLocale('nl');
        $locale = $request->getLocale();
        $request->getSession()->set('_locale', $locale);
        return $this->render('login/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
            'company' => $this->get_company()
        ));
    }
    /**
     * @Route("/user-password", name="userpage-password.json", schemes={"https"})
     * @Method("PUT")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @return Response
     */
    public function update_password(
        Request $request,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder
    ) {
        try {
            $username = $request->get('username');
            $current_passwd = $request->get('current_password');
            if(!$encoder->isPasswordValid($this->getUser(), $current_passwd)) {
                throw new \Exception('Current password is not valid.');
            }
            $passwd = $request->get('password');
            $confirm_passwd = $request->get('confirm_password');
            if($passwd !== $confirm_passwd) {
                throw new \Exception('Entered passwords do not match.');
            }
            $this->check_password($passwd);
            $user_id = $em->getRepository(UserEntity::class)
                ->update_password($username, $encoder->encodePassword($this->getUser(), $passwd));
            $response = new Response();
            $response->setContent(json_encode(['updated' => $user_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @param string $pass
     * @return bool
     * @throws PasswordTooWeakException
     */
    protected function check_password(string $pass) : bool
    {
        if(!preg_match('/[A-Z]/', $pass)) {
            throw new PasswordTooWeakException(_('Passsword do not contain at least one upper case letter'));
        }
        if(!preg_match('/[a-z]/', $pass)) {
            throw new PasswordTooWeakException(_('Passsword do not contain at least one lower case letter'));
        }
        if(!preg_match('/[0-9]/', $pass)) {
            throw new PasswordTooWeakException(_('Passsword do not contain at least one numeric value'));
        }
        if(strlen($pass) < 8) {
            throw new PasswordTooWeakException(_('Password must be at least 8 characters long'));
        }
        return true;
    }
}