<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/29/17
 * Time: 2:57 PM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Controller\User\Debtors\Exceptions\ContactPersonNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\DebtorNotFoundException;
use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactPersonController extends AppController
{
    /**
     * @Route("/debtors/add-contact-person", name="user-debtors-add-contact-person.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_contact_person(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = $request->get('debtor_id');
            $name = $request->get('name');
            $email = $request->get('email');
            $phone_number = $request->get('phone_number');
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            $contact_person_entity = new ContactPersonEntity();
            $contact_person_entity->setName($name);
            $contact_person_entity->setEmail($email);
            $contact_person_entity->setPhoneNumber($phone_number);
            $contact_person_entity->setDebtor($debtor);
            $contact_person_entity->setCreated(new \DateTime('now'));
            $contact_person_entity->setUpdated(new \DateTime('now'));
            $contact_person_entity->setUsername($this->getUser()->getUsername());
            $em->persist($contact_person_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $contact_person_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/delete-contact-person", name="user-debtors-delete-contact-person.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_contact_person(Request $request, EntityManagerInterface $em)
    {
        try {
            $contact_person_id = $request->get('id');
            $contact_person_entity = $em->getRepository(ContactPersonEntity::class)
                ->findOneBy(['id' => $contact_person_id]);
            if(empty($contact_person_entity)) {
                throw new ContactPersonNotFoundException(_('Contact person not found'));
            }
            $em->remove($contact_person_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $contact_person_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/update-contact-person", name="user-debtors-update-contact-person.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_contact_person(Request $request, EntityManagerInterface $em)
    {
        try {
            $contact_person_id = $request->get('id');
            $name = $request->get('name');
            $email = $request->get('email');
            $phone_number = $request->get('phone_number');
            $contact_person_entity = $em->getRepository(ContactPersonEntity::class)
                ->findOneBy(['id' => $contact_person_id]);
            if(empty($contact_person_entity)) {
                throw new ContactPersonNotFoundException(_('Contact person not found'));
            }
            $contact_person_entity->setName($name);
            $contact_person_entity->setEmail($email);
            $contact_person_entity->setPhoneNumber($phone_number);
            $contact_person_entity->setUpdated(new \DateTime('now'));
            $contact_person_entity->setUsername($this->getUser()->getUsername());
            $em->persist($contact_person_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $contact_person_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}