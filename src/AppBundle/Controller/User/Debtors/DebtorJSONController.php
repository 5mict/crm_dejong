<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 4:56 PM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Controller\User\Debtors\Exceptions\DebtorNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\EndUserTypeNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\HowDidYouFindUsNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Articles\ArticlePaidEntity;
use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\DebtorFileEntity;
use AppBundle\Entity\User\Debtors\DeliveryAddressEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeTranslationEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Orders\OrderFileEntity;
use AppBundle\Entity\User\Orders\OrderMailAttachmentFileEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;
use AppBundle\Entity\User\Orders\OrderPaidEntity;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DebtorJSONController extends AppController
{
    /**
     * @Route("/debtors", name="user-debtors-get.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtors  = $em->getRepository(DebtorEntity::class)
                ->get_debtorsData();
            return $this->render(':user/debtors:index.html.twig', [
                'debtors_view' => $debtors,
                'company' => $this->get_company(),
                'locale' => $request->getLocale(),
                'user' => $this->getUser(),
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]);
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/type-contact-persons", name="user-debtors-get-type-contact-persons.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_type_contact_persons(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_name = $request->get('debtor_name');
            $debtor_entity = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['name' => $debtor_name]);
            if(empty($debtor_entity)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            $contact_persons = $debtor_entity->getContactPersons();
            $contact_person_delivery_address = [];
            foreach($contact_persons as $contact_person) {
                $delivery_addresses = $debtor_entity->getDeliveryAddresses();
                $del_add = [];
                foreach($delivery_addresses as $address){
                    $del_add [] = [
                        'id'     => $address->getId(),
                        'street' => $address->getStreet(),
                        'postcode' => $address->getPostcode(),
                        'city' => $address->getCity(),
                        'country' => $address->getCountry()
                    ];
                }
                $contact_person_delivery_address[$contact_person->getName()] = $del_add;
                $contact_persons_names[] = [
                    'name' => $contact_person->getName(),
                    'phone' => $contact_person->getPhoneNumber(),
                ];
            }
            $response = new Response();
            $response->setContent(
                json_encode(
                    [
                        'type' => DebtorEndUserTypeTranslationEntity::get_translation($debtor_entity->getEndUserType()->getName(), $request->getLocale()),
                        'contact_persons' => $contact_persons_names,
                        'delivery_address' => $contact_person_delivery_address
                    ]
                )
            );
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/debtors/add-debtor", name="user-debtors-add-debtor.json")
     * @Method({"POST"})
     * @param Request $request\
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor = new DebtorEntity();
            $debtor->setCreated(new \DateTime('now'));
            $debtor->setUpdated(new \DateTime('now'));
            $debtor->setName($request->get('name'));
            $debtor->setEmail($request->get('email'));
            $debtor->setStreet($request->get('street'));
            $debtor->setNumber($request->get('house_number'));
            $debtor->setNumberExtension($request->get('number_extension'));
            $debtor->setPostCode($request->get('post_code'));
            $debtor->setBankingAccountNumber($request->get('banking_account_number'));
            $debtor->setBTW($request->get('btw'));
            $debtor->setCity($request->get('city'));
            $debtor->setCountry($request->get('country'));
            $debtor->setWebSite($request->get('website'));
            $debtor->setUsername($this->getUser()->getUsername());
            $debtor->setCommunicationLanguage($request->get('communication_language'));
            $debtor->setTotalSellingPrice(0);
            $debtor->setTotalPurchasePrice(0);
            $debtor->setProfitPercents(0);
            $end_user_type_entity = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->findOneBy(['name' => $request->get('end_user_type')]);
            if(empty($end_user_type_entity)) {
                throw new EndUserTypeNotFoundException(_('End user type not found'));
            }
            $debtor->setEndUserType($end_user_type_entity);
            $how_did_you_find_us_entity = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->findOneBy(['name' => $request->get('how_did_you_find_us')]);
            if(empty($how_did_you_find_us_entity)) {
                throw new HowDidYouFindUsNotFoundException(_('How did you fid us not found'));
            }
            $debtor->setHowDidYouFindUs($how_did_you_find_us_entity);
            $debtor->setUsername($this->getUser()->getUsername());
            $em->persist($debtor);
            $contact_person = new ContactPersonEntity();
            $contact_person->setDebtor($debtor);
            $contact_person->setEmail($debtor->getEmail());
            $contact_person->setPhoneNumber($request->get('phone_number'));
            $contact_person->setName($debtor->getName());
            $contact_person->setCreated(new \DateTime('now'));
            $contact_person->setUpdated(new \DateTime('now'));
            $contact_person->setUsername($debtor->getUsername());
            $em->persist($contact_person);
            $delivery_address = new DeliveryAddressEntity();
            $delivery_address->setDebtor($debtor);
            $delivery_address->setStreet($debtor->getStreet());
            $delivery_address->setNumber($debtor->getNumber());
            $delivery_address->setNumberExtension($debtor->getNumberExtension());
            $delivery_address->setCity($debtor->getCity());
            $delivery_address->setPostcode($debtor->getPostCode());
            $delivery_address->setCountry($debtor->getCountry());
            $delivery_address->setCreated(new \DateTime('now'));
            $delivery_address->setUpdated(new \DateTime('now'));
            $delivery_address->setUsername($debtor->getUsername());
            if($request->getLocale() == 'en') {
                $delivery_address->setDescription('Default delivery address');
            } else {
                $delivery_address->setDescription('Standaard afleveradres');
            }
            $em->persist($delivery_address);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $debtor->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/delete-debtor", name="user-debtors-delete-debtor.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @return Response
     */
    public function delete_debtor(Request $request)
    {
        try {
            $debtor_id = intval($request->get('id'));
            $em = $this->getDoctrine()
                ->getManager();
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            $debtor_files = $em->getRepository(DebtorFileEntity::class)
                ->findBy(['debtor' => $debtor]);
            $orders = $em->getRepository(OrderEntity::class)
                ->findBy(['debtor_name' => $debtor->getName()]);
            if(!empty($debtor_files)){
                foreach ($debtor_files as $file){
                    $em->remove($file);
                }
            }
            if(!empty($orders)){
                foreach($orders as $order){
                    $orders_paid = $em->getRepository(OrderPaidEntity::class)
                        ->findBy(['order' => $order]);
                    $order_articles =  $em->getRepository(OrderArticleEntity::class)
                        ->findBy(['order' => $order]);
                    $order_mails = $em->getRepository(OrderMailEntity::class)
                        ->findBy(['order' => $order]);
                    $order_files = $em->getRepository(OrderFileEntity::class)
                        ->findBy(['order' => $order]);
                    if(!empty($order_files)){
                        foreach ($order_files as $file){
                            $em->remove($file);
                        }
                    }
                    if(!empty($order_mails)){
                        foreach($order_mails as $order_mail){
                            $order_mail_attachments = $em->getRepository(OrderMailAttachmentFileEntity::class)
                                ->findBy(['order_mail' => $order_mails]);
                            foreach($order_mail_attachments as $attachment){
                                $em->remove($attachment);
                            }
                            $em->remove($order_mail);
                        }
                    }
                    if(!empty($order_articles)){
                        foreach($order_articles as $order_article){
                            $order_mails = $em->getRepository(OrderMailEntity::class)
                                ->findBy(['order_article' => $order_article]);
                            foreach($order_mails as $order_mail){
                                $em->remove($order_mail);
                            }
                            $articles_paid = $em->getRepository(ArticlePaidEntity::class)
                                ->findBy(['order_article' => $order_article]);
                            foreach($articles_paid as $article_paid){
                                $em->remove($article_paid);
                            }
                            $em->remove($order_article);
                        }
                    }
                    if(!empty($orders_paid)){
                        foreach($orders_paid as $order_paid){
                            $em->remove($order_paid);
                        }
                    }
                    $em->remove($order);
                }
            }
            $em->remove($debtor);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $debtor_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/update-debtor", name="user-debtors-update-debtor.json")
     * @Method({"PUT"})
     * @param Request $request\
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = $request->get('id');
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new \Exception('Unknown debtor');
            }
            $debtor->setUpdated(new \DateTime('now'));
            $debtor->setName($request->get('name'));
            $debtor->setEmail($request->get('email'));
            $debtor->setStreet($request->get('street'));
            $debtor->setNumber($request->get('house_number'));
            $debtor->setNumberExtension($request->get('number_extension'));
            $debtor->setPostCode($request->get('post_code'));
            $debtor->setBankingAccountNumber($request->get('banking_account_number'));
            $debtor->setBTW($request->get('btw'));
            $debtor->setCity($request->get('city'));
            $debtor->setCountry($request->get('country'));
            $debtor->setWebSite($request->get('website'));
            $end_user_type_entity = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->findOneBy(['name' => $request->get('end_user_type')]);
            if(empty($end_user_type_entity)) {
                throw new \Exception('Unknown end user type');
            }
            $debtor->setEndUserType($end_user_type_entity);
            $how_did_you_find_us_entity = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->findOneBy(['name' => $request->get('how_did_you_find_us')]);
            if(empty($how_did_you_find_us_entity)) {
                throw new \Exception('Unknown value how did you find us');
            }
            $debtor->setHowDidYouFindUs($how_did_you_find_us_entity);
            $debtor->setCommunicationLanguage($request->get('communication_language'));
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $debtor->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/search-debtors", name="user-debtors-get-search-debtor.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_search_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $filter = $request->get('filter');
            $debtor = $em->getRepository(DebtorEntity::class)
                ->createQueryBuilder('d')
                ->where('d.name LIKE :name')
                ->setParameter('name', '%'.$filter.'%')
                ->getQuery()
                ->getArrayResult();

            $response = new Response();
            $response->setContent($this->json_encode($debtor));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/get-debtors-statistics-data", name="get-debtors=statistics-data.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_debtors_statistics_data( Request $request, EntityManagerInterface $em )
    {
        try {
            $debtors = $em->getRepository(DebtorEntity::class)
                ->get_debtors_by_total_selling_price_in_interval($request->get('start'), $request->get('end'), $request->get('locale'));
            $countries = $em->getRepository(DebtorEntity::class)
                ->get_countries_data_in_interval($request->get('start'), $request->get('end'), $request->get('locale'));
            $response = new Response();
            $response->setContent( $this->json_encode( ['debtors' => $debtors, 'countries' => $countries ] ) );
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
}