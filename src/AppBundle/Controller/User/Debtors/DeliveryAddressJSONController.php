<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/16/2017
 * Time: 7:41 PM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\DeliveryAddress\PhoneNumberEntity;
use AppBundle\Entity\User\Debtors\DeliveryAddressEntity;
use AppBundle\Repository\User\Debtors\DeliveryAddressRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DeliveryAddressJSONController extends AppController
{
    /**
     * @Route("/debtors/delivery-address/contact-persons", name="user-debtors-delivery-address-contact-persons.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_contact_persons_by_delivery_address(Request $request, EntityManagerInterface $em)
    {
        try {
            $delivery_address_id = $request->get('delivery_address_id');
            $debtor_id = $em->getRepository(DeliveryAddressEntity::class)
                ->findOneBy(['id' => $delivery_address_id])
                ->getDebtorId();
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new \Exception('Debtor does not exist');
            }
            $contact_persons = $em->getRepository(ContactPersonEntity::class)
                ->findBy(['debtor' => $debtor_id]);
            $response = new Response();
            $response->setContent(json_encode(['contact_persons' => $contact_persons]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}