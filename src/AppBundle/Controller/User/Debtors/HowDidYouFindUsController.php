<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/6/2017
 * Time: 4:05 PM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use ClassesWithParents\E;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class HowDidYouFindUsController extends AppController
{
    /**
     * @Route("/debtors/how-did-you-find-us", name="user-debtors-how-did-you-find-us")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_how_did_you_find_us(Request $request, EntityManagerInterface $em)
    {
        $graph_data = $em->getRepository(DebtorEntity::class)
            ->get_how_did_you_us_data();
        $total_selling_data = $em->getRepository(DebtorEntity::class)
            ->get_how_did_you_find_use_purching_data();
        $path = $this->get_company() . 'images/logo.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $image_data = file_get_contents($path);
        $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
        return $this->render(
            'user/debtors/pdf/how_did_you_find_us.html.twig',
            [
                'company' => $this->get_company(),
                'layout' => 'tabs',
                'Action' => 'update',
                'user' => $this->getUser(),
                'graph_data' => $graph_data,
                'total_selling_data' => $total_selling_data,
                'logo' => $logo_base64,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]
        );
    }
}