<?php
namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\DebtorFileEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class FileController extends AppController
{
    /**
     * @Route("/debtor/upload_debtor_file", name="user-upload-debtor-file.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function upload_order_file(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = $request->get('id');
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            foreach($_FILES['file']['name'] as $key => $original){
                $imageType = pathinfo($original, PATHINFO_EXTENSION);
                $file_name = $this->generateRandomString(30) . '.' . $imageType;
                $file_tmp_name = $_FILES["file"]["tmp_name"][$key];
                $this->upload_validate($file_tmp_name, $file_name, "debtor_files");
                $order_file_entity = new DebtorFileEntity();
                $order_file_entity->setDebtor($debtor);
                $order_file_entity->setFileName($file_name);
                $order_file_entity->setName($original);
                $order_file_entity->setCreated(new \DateTime('now'));
                $em->persist($order_file_entity);
            }
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['file uploaded']));
            return $response;

        } catch (\Exception $e){
            return $this->handle_json_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/debtors/get_uploaded_file/{id}", name="post-debtor-uploaded-file")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_view_order_to_supplier_pdf(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(DebtorFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'debtor_files/' . $file->getFileName();
            $debtor = $file->getDebtor();
            return $this->render(
                ':user/debtors/pdf:view_uploaded_file.html.twig',
                [
                    'company' => $this->get_company(),
                    'ActionId' => $debtor->getId(),
                    'Action' => 'DebtorFiles',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/get_uploaded_file/{id}", name="get-debtor-uploaded-file")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_view_order_to_supplier_pdf(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(DebtorFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'debtor_files/' . $file->getFileName();
            $debtor = $file->getDebtor();
            return $this->render(
                ':user/debtors/pdf:view_uploaded_file.html.twig',
                [
                    'company' => $this->get_company(),
                    'ActionId' => $debtor->getId(),
                    'Action' => 'DebtorFiles',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/delete_uploaded_file", name="delete-debtor-uploaded-file.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_uploaded_file(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(DebtorFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $this->delete_validate("debtor_files", $file->getFileName());
            $em->remove($file);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['file deleted']));
            return $response;
        }
        catch (\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}