<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/3/2017
 * Time: 12:18 PM
 */

namespace AppBundle\Controller\User\Debtors\Exceptions;

class DebtorNotFoundException extends \Exception
{

}