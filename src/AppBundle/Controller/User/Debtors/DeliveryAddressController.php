<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/29/17
 * Time: 10:39 AM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\DeliveryAddress\PhoneNumberEntity;
use AppBundle\Entity\User\Debtors\DeliveryAddressEntity;
use AppBundle\Repository\User\Debtors\DeliveryAddressRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DeliveryAddressController extends AppController
{
    /**
     * @Route("/debtors/add-delivery-address", name="user-debtors-add-delivery-address.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_delivery_address(Request $request, EntityManagerInterface $em)
    {
        $logger = $this->get('logger');
        try {
            $debtor_id = $request->get('debtor_id');
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new \Exception('Debtor does not exist');
            }
            $delivery_address_entity = new DeliveryAddressEntity();
            $delivery_address_entity->setStreet($request->get('street'));
            $delivery_address_entity->setNumber($request->get('number'));
            $delivery_address_entity->setNumberExtension($request->get('number_extension'));
            $delivery_address_entity->setPostcode($request->get('postcode'));
            $delivery_address_entity->setCity($request->get('city'));
            $delivery_address_entity->setCountry($request->get('country'));
            $delivery_address_entity->setDescription($request->get('description'));
            $delivery_address_entity->setDebtor($debtor);
            $delivery_address_entity->setCreated(new \DateTime('now'));
            $delivery_address_entity->setUpdated(new \DateTime('now'));
            $delivery_address_entity->setUsername($this->getUser()->getUsername());
            $em->persist($delivery_address_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $delivery_address_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/delete-delivery-address", name="user-debtors-delete-delivery-address.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_delivery_address(Request $request, EntityManagerInterface $em)
    {
        try {
            $delivery_address_id = $request->get('id');
            $delivery_address_entity = $em->getRepository(DeliveryAddressEntity::class)
                ->findOneBy(['id' => $delivery_address_id]);
            $em->remove($delivery_address_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $delivery_address_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/update-delivery-address", name="user-debtors-update-delivery-address.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_delivery_address(Request $request, EntityManagerInterface $em)
    {
        try {
            $delivery_address_id = $request->get('id');
            $delivery_address_entity = $em->getRepository(DeliveryAddressEntity::class)
                ->findOneBy(['id' => $delivery_address_id]);
            $delivery_address_entity->setStreet($request->get('street'));
            $delivery_address_entity->setNumber($request->get('number'));
            $delivery_address_entity->setNumberExtension($request->get('number_extension'));
            $delivery_address_entity->setPostcode($request->get('postcode'));
            $delivery_address_entity->setCity($request->get('city'));
            $delivery_address_entity->setCountry($request->get('country'));
            $delivery_address_entity->setDescription($request->get('description'));
            $delivery_address_entity->setUpdated(new \DateTime('now'));
            $delivery_address_entity->setUsername($this->getUser()->getUsername());
            $em->persist($delivery_address_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $delivery_address_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}