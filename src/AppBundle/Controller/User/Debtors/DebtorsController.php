<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/24/2017
 * Time: 4:45 PM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Controller\User\Debtors\Exceptions\DebtorNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\DebtorFileEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Entity\User\Debtors\NoteEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DebtorsController extends AppController
{
    public function __construct(EntityManagerInterface $em)
    {
        if(!$this->is_granted(GrantEntity::DEBTOR_GRANT, $em)) {
            throw $this->createNotFoundException();
        }
    }
    /**
     * @Route("/debtors", name="user-debtors")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_debtors(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtors  = $em->getRepository(DebtorEntity::class)
                ->get_debtorsData();
            return $this->render(':user/debtors:index.html.twig', [
                'debtors_view' => $debtors,
                'company' => $this->get_company(),
                'locale' => $request->getLocale(),
                'user' => $this->getUser(),
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]);
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/{id}/update-debtor", name="user-post-debtors-update")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_update_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = intval($request->get('id'));
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(NoteEntity::class)
                ->get_notes($debtor_id);
            $how_did_you_find_us = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->get_all_with_translation($request->getLocale());
            $end_user_type = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->get_all_with_translation($request->getLocale());
            $debtor_files = $em->getRepository(DebtorFileEntity::class)
                ->findBy(['debtor'=> $debtor]);
            return $this->render(
                ':user/debtors:basic.html.twig',
                [
                    'debtor' => $debtor,
                    'company' => $this->get_company(),
                    'layout' => 'tabs',
                    'debtor_files' => $debtor_files,
                    'Action' => 'update',
                    'user' => $this->getUser(),
                    'how_did_you_find_us' => $how_did_you_find_us,
                    'end_user_type' => $end_user_type,
                    'notes' => $notes,
                    'debtor_notes_page_number' => $page_number,
                    'debtor_notes_page_size' => $page_size,
                    'debtor_notes_total_number_of_pages' => $total_number_of_pages,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/{id}/update-debtor", name="user-get-debtors-update")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_update_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = intval($request->get('id'));
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(NoteEntity::class)
                    ->get_notes($debtor_id);
            $how_did_you_find_us = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->get_all_with_translation($request->getLocale());
            $end_user_type = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->get_all_with_translation($request->getLocale());
            $debtor_files = $em->getRepository(DebtorFileEntity::class)
                ->findBy(['debtor'=> $debtor]);
            return $this->render(
                ':user/debtors:basic.html.twig',
                [
                    'debtor' => $debtor,
                    'company' => $this->get_company(),
                    'layout' => 'tabs',
                    'debtor_files' => $debtor_files,
                    'Action' => 'update',
                    'user' => $this->getUser(),
                    'how_did_you_find_us' => $how_did_you_find_us,
                    'end_user_type' => $end_user_type,
                    'notes' => $notes,
                    'debtor_notes_page_number' => $page_number,
                    'debtor_notes_page_size' => $page_size,
                    'debtor_notes_total_number_of_pages' => $total_number_of_pages,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/{id}/pdf-view-debtor", name="user-debtors-post-pdf-view")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_pdf_view_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = intval($request->get('id'));
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            $how_did_you_find_us = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->get_all_with_translation($request->getLocale());
            $end_user_type = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->get_all_with_translation($request->getLocale());
            $path = $this->get_company() . 'images/logo.png';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $image_data = file_get_contents($path);
            $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
            if(empty($debtor)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            return $this->render(
                ':user/debtors/pdf:view.html.twig',
                [
                    'debtor' => $debtor,
                    'layout' => 'tabs',
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'user' => $this->getUser(),
                    'how_did_you_find_us' => $how_did_you_find_us,
                    'end_user_type' => $end_user_type,
                    'logo' => $logo_base64,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/{id}/pdf-view-debtor", name="user-debtors-get-pdf-view")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_view_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = intval($request->get('id'));
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            $how_did_you_find_us = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->get_all_with_translation($request->getLocale());
            $end_user_type = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->get_all_with_translation($request->getLocale());
            $path = $this->get_company() . 'images/logo.png';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $image_data = file_get_contents($path);
            $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
            if(empty($debtor)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            return $this->render(
                ':user/debtors/pdf:view.html.twig',
                [
                    'debtor' => $debtor,
                    'layout' => 'tabs',
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'user' => $this->getUser(),
                    'how_did_you_find_us' => $how_did_you_find_us,
                    'end_user_type' => $end_user_type,
                    'logo' => $logo_base64,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/pdf-order-debtors", name="user-debtors-get-pdf-order-debtors")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_order_debtors(Request $request, EntityManagerInterface $em)
    {
        try {
            $path = $this->get_company() . 'images/pdf_logo.jpg';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $image_data = file_get_contents($path);
            $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
            return $this->render(
                ':user/debtors/pdf:sorted-debtors.html.twig',
                [
                    'layout' => 'tabs',
                    'company' => $this->get_company(),
                    'user' => $this->getUser(),
                    'logo' => $logo_base64,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/debtors/add-debtor", name="user-debtors-get-add")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_add_debtor(Request $request, EntityManagerInterface $em)
    {
        try {
            $how_did_you_find_us = $em->getRepository(DebtorHowDidYouFindUsEntity::class)
                ->get_all_with_translation($request->getLocale());
            $end_user_type = $em->getRepository(DebtorEndUserTypeEntity::class)
                ->get_all_with_translation($request->getLocale());
            return $this->render(
                'user/debtors/basic.html.twig',
                [
                    'company' => $this->get_company(),
                    'Action' => 'add',
                    'layout' => 'tabs',
                    'user' => $this->getUser(),
                    'how_did_you_find_us' => $how_did_you_find_us,
                    'end_user_type' => $end_user_type,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/debtors-chart", name="user-debtors-articles-chart")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_debtor_chart(Request $request, EntityManagerInterface $em)
    {
        $debtor_id = $request->get('id');
        return $this->render(
            'user/debtors/charts/debtors-chart.html.twig',
            [
                'company' => $this->get_company(),
                'layout' => 'tabs',
                'locale' => $request->getLocale(),
                'Action' => 'update',
                'user' => $this->getUser(),
                'debtor_id' => $debtor_id,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]
        );
    }
}