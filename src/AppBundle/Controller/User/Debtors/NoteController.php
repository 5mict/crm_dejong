<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/2/2017
 * Time: 12:03 PM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Controller\User\Debtors\Exceptions\DebtorNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\NoteNotFoundException;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\NoteEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class NoteController extends AppController
{
    /**
     * @Route("/debtors/get-notes", name="user-debtors-get-notes.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_notes(Request $request, EntityManagerInterface $em)
    {
        try {
            $page_number = intval($request->get('page_number'));
            $page_size = intval($request->get('page_size'));
            $debtor_id = intval($request->get('parent_id'));
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(NoteEntity::class)
                    ->get_notes($debtor_id, $page_number, $page_size);
            $notes_array = [];
            foreach($notes as $note) {
                $notes_array[] = [
                    'id' => $note->getId(),
                    'username' => $note->getUsername(),
                    'updatedString' => $note->getUpdatedString(),
                    'note' => $note->getNote()
                ];
            }
            $response = new Response();
            $response->setContent(json_encode(['page_number' => $page_number, 'notes' => $notes_array]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/debtor_notes", name="debtor-notes.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_notes_by_debtor_name(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_name = $request->get('debtor_name');
            $debtor_entity = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['name' => $debtor_name]);
            if (empty($debtor_entity)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }

            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(NoteEntity::class)
                ->get_notes($debtor_entity->getId(), 0, 0);
            $notes_array = [];
            foreach($notes as $note) {
                $notes_array[] = [
                    'id' => $note->getId(),
                    'username' => $note->getUsername(),
                    'updatedString' => $note->getUpdatedString(),
                    'note' => $note->getNote()
                ];
            }
            $response = new Response();
            $response->setContent(json_encode(['notes' => $notes_array]));
            return $response;
        }catch(\Exception $e)
        {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/add-note", name="user-debtors-add-note.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = intval($request->get('debtor_id'));
            $note = $request->get('note');
            $debtor = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            $note_entity = new NoteEntity();
            $note_entity->setNote($note);
            $note_entity->setCreated(new \DateTime('now'));
            $note_entity->setUpdated(new \DateTime('now'));
            $note_entity->setDebtor($debtor);
            $note_entity->setUsername($this->getUser()->getUsername());
            $em->persist($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/delete-note", name="user-debtors-delete-note.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $note_id = intval($request->get('id'));
            $note_entity = $em->getRepository(NoteEntity::class)
                ->findOneBy(['id' => $note_id]);
            if(empty($note_entity)) {
                throw new NoteNotFoundException(_('Note not found'));
            }
            $em->remove($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/debtors/update-note", name="user-debtors-update-note.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $note_id = intval($request->get('id'));
            $note_entity = $em->getRepository(NoteEntity::class)
                ->findOneBy(['id' => $note_id]);
            if(empty($note_entity)) {
                throw new NoteNotFoundException(_('Note not found'));
            }
            $note = $request->get('note');
            $note_entity->setNote($note);
            $note_entity->setUpdated(new \DateTime('now'));
            $note_entity->setUsername($this->getUser()->getUsername());
            $em->persist($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}