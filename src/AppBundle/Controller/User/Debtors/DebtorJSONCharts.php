<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/6/2018
 * Time: 6:43 PM
 */

namespace AppBundle\Controller\User\Debtors;

use AppBundle\AppController;
use AppBundle\Controller\User\Debtors\Exceptions\DebtorNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\EndUserTypeNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\HowDidYouFindUsNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Entity\User\Orders\OrderPaidEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DebtorJSONCharts extends AppController
{
    /**
     * @Route("/debtors/charts", name="user-debtors-get-charts-data.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_chart_data(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_id = $request->get('id');
            $debtor_entity = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['id' => $debtor_id]);
            if(empty($debtor_entity)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            $start = new \DateTime($request->get('start'));
            $start->setTime(0, 0, 0);
            $end = new \DateTime($request->get('end'));
            $end->setTime(23, 59, 59);
            $orders_paid = $em->getRepository(OrderPaidEntity::class)
                ->get_orders_paid($debtor_entity, $start, $end, $request->getLocale());
            $response = new Response();
            $response->setContent(json_encode(['debtor_name' => $debtor_entity->getName(), 'data' => $orders_paid]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}