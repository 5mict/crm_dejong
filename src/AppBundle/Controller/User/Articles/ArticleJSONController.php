<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 7:54 PM
 */

namespace AppBundle\Controller\User\Articles;

use AppBundle\AppController;
use AppBundle\Controller\User\Articles\Exceptions\ArticleCategoryNotFoundException;
use AppBundle\Controller\User\Articles\Exceptions\ArticleCategorySubCategoryInconsistencyException;
use AppBundle\Controller\User\Articles\Exceptions\ArticleNotFoundException;
use AppBundle\Controller\User\Articles\Exceptions\ArticleSubCategoryNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Articles\ArticlePaidEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use AppBundle\Repository\User\Orders\OrderRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ArticleJSONController extends AppController
{
    /**
     * @Route("/articles", name="user-articles-get-article.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_id = $request->get('id');
            $article = $em->getRepository(ArticleEntity::class)
                ->findOneBy(['id' => $article_id]);
            if(empty($article)) {
                throw new ArticleNotFoundException(_('Article not found'));
            }
            $response = new Response();
            $response->setContent($this->json_encode($article));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/articles/add-article", name="user-articles-add-article.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_category = $em->getRepository(ArticleCategoryEntity::class)
                ->findOneBy(['name' => $request->get('article_category')]);
            if(empty($article_category)) {
                throw new ArticleCategoryNotFoundException(_('Article category not found'));
            }
            $article_sub_category = $em->getRepository(ArticleSubCategoryEntity::class)
                ->findOneBy(['name' => $request->get('article_sub_category')]);
            if (empty($article_sub_category)) {
                throw new ArticleSubCategoryNotFoundException(_('Article sub category not found'));
            }
            if ($article_category->getName() != $article_sub_category->getArticleCategory()->getName()) {
                throw new ArticleCategorySubCategoryInconsistencyException(_('Article category and sub category are inconsistent'));
            }
            $article_entity = new ArticleEntity();
            $article_entity->setArticleNumber($request->get('article_number'));
            $article_entity->setArticleName($request->get('article_name_english'));
            $article_entity->setArticleNameDutch($request->get('article_name_dutch'));
            $article_entity->setArticleNameGerman($request->get('article_name_german'));
            $article_entity->setSize($request->get('size'));
            $article_entity->setPurchasePrice(round(floatval($request->get('purchase_price')), 2));
            $article_entity->setSellingPrice(round(floatval($request->get('selling_price')), 2));
            $article_entity->setSellingPriceWithPdv(round(floatval($request->get('selling_price_with_pdv')), 2));
            $article_entity->setDescription($request->get('description'));
            $article_entity->setUsername($this->getUser()->getUsername());
            $article_entity->setCreated(new \DateTime('now'));
            $article_entity->setUpdated(new \DateTime('now'));
            $article_entity->setArticleCategory($article_category);
            $article_entity->setTotalSellingPrice(0);
            $article_entity->setTotalPurchasePrice(0);
            $article_entity->setProfitPercents(0);
            $article_entity->setArticleSubCategory($article_sub_category);
            if($request->get('type') == 'custom'){
                $picture_name = $_FILES["add_custom-file-upload"]["name"];
                $picture_tmp_name = $_FILES["add_custom-file-upload"]["tmp_name"];
            } else {
                $picture_name = $_FILES["add-file-upload"]["name"];
                $picture_tmp_name = $_FILES["add-file-upload"]["tmp_name"];
            }
            $this->upload_validate($picture_tmp_name, $picture_name, "images");
            $article_entity->setPictureUrl($picture_name);
            $em->persist($article_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $article_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/articles/delete-article", name="user-articles-delete-article.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_entity = $em->getRepository(ArticleEntity::class)
                ->findOneBy(['article_number' => $request->get('article_number')]);
            if(empty($article_entity)) {
                throw new ArticleNotFoundException(_('Article not found'));
            }
            $id = $article_entity->getId();
            $order_articles =  $em->getRepository(OrderArticleEntity::class)
                ->findBy(['article' => $article_entity]);
            if(!empty($order_articles)){
                foreach($order_articles as $order_article){
                    $order_mails = $em->getRepository(OrderMailEntity::class)
                        ->findBy(['order_article' => $order_article]);
                    foreach($order_mails as $order_mail){
                        $em->remove($order_mail);
                    }
                    $articles_paid = $em->getRepository(ArticlePaidEntity::class)
                        ->findBy(['order_article' => $order_article]);
                    foreach($articles_paid as $article_paid){
                        $em->remove($article_paid);
                    }
                    $em->remove($order_article);
                }
            }
            $em->remove($article_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/articles/update-article", name="user-articles-update-article.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_category = $em->getRepository(ArticleCategoryEntity::class)
                ->findOneBy(['name' => $request->get('article_category')]);
            if (empty($article_category)) {
                throw new ArticleCategoryNotFoundException(_('Article category not found'));
            }
            $article_sub_category = $em->getRepository(ArticleSubCategoryEntity::class)
                ->findOneBy(['name' => $request->get('article_sub_category')]);
            if (empty($article_sub_category)) {
                throw new ArticleSubCategoryNotFoundException(_('Article sub category not found'));
            }
            if ($article_category->getName() != $article_sub_category->getArticleCategory()->getName()) {
                throw new ArticleCategorySubCategoryInconsistencyException(_('Article category and sub category are inconsistent'));
            }
            $article_entity = $em->getRepository(ArticleEntity::class)
                ->findOneBy(['id' => $request->get('article_id')]);
            if(empty($article_entity)) {
                throw new \Exception('Article not found');
            }
            $article_entity->setArticleNumber($request->get('article_number'));
            $article_entity->setArticleName($request->get('article_name_english'));
            $article_entity->setArticleNameDutch($request->get('article_name_dutch'));
            $article_entity->setArticleNameGerman($request->get('article_name_german'));
            $article_entity->setSize($request->get('size'));
            $article_entity->setPurchasePrice(round(floatval($request->get('purchase_price')), 2));
            $article_entity->setSellingPrice(round(floatval($request->get('selling_price')), 2));
            $article_entity->setSellingPriceWithPdv(round(floatval($request->get('selling_price_with_pdv')), 2));
            $article_entity->setDescription($request->get('description'));
            $article_entity->setArticleCategory($article_category);
            $article_entity->setArticleSubCategory($article_sub_category);
            $article_entity->setUsername($this->getUser()->getUsername());
            $article_entity->setUpdated(new \DateTime('now'));
            $picture_name = $_FILES["update-file-upload"]["name"];
            $picture_tmp_name = $_FILES["update-file-upload"]["tmp_name"];
            $upload = $this->upload_validate($picture_tmp_name, $picture_name, "images");
            if($picture_name == ""){
                $picture_name = "default";
            }
            if($upload) {
                $article_entity->setPictureUrl($picture_name);
            }
            $em->persist($article_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $article_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/articles/get-description-size", name="user-articles-get-description-size.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_description_size(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_number = $request->get('article_number');
            $locale = $request->getLocale();
            $article = $em->getRepository(ArticleEntity::class)
                ->findOneBy(['article_number' => $article_number]);
            $article_name = $locale == 'de' ? $article->getArticleNameGerman() : $locale == 'nl' ? $article->getArticleNameDutch() : $article->getArticleName();
            if(empty($article)) {
                throw new ArticleNotFoundException(_('Article not found'));
            }
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode([
                'description' => $article_name,
                'size' => $article->getSize(),
                'picture_url' => $article->getPictureUrl()
            ]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/articles/article-sub-categories", name="user-articles-get-article-sub-categories.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_article_sub_categories(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_category_name = $request->get('article_category');
            $article_category = $em->getRepository(ArticleCategoryEntity::class)
                ->findOneBy(['name' => $article_category_name]);
            if(empty($article_category)) {
                throw new ArticleCategoryNotFoundException(_('Article category not found'));
            }
            $article_sub_categories = $em->getRepository(ArticleSubCategoryEntity::class)
                ->get_with_translation($article_category, $request->getLocale());
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode($article_sub_categories));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/articles/article-number-name", name="user-articles-get-article-number-name.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_article_number_name(Request $request, EntityManagerInterface $em)
    {
        try {
            $locale = $request->getLocale();
            $article_number_name = $em->getRepository(ArticleEntity::class)
                ->get_article_number_name($locale);
            $response = new Response();
	  
            $data = [];
            foreach($article_number_name as $item) {
            	$data['result'][] = [
            		'id' => $item['article_number'],
            		'text' => $item['article_number'],
		            'search' => $item['article_number'] . " " . $item['article_name'] . " " . strtolower($item['article_number']). " " .
			            preg_replace('/[^A-Za-z0-9\-]/', ' ', strtolower($item['article_name'])),
		            'image' => $item['article_image'],
		            'description' => $item['article_name']
	            ];
            }
        
            $response->setContent(json_encode($data));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/articles/search-articles", name="user-articles-get-search-articles.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_search__articles(Request $request, EntityManagerInterface $em)
    {
        try {
            $locale = $request->getLocale();
            $filter = $request->get('filter');
            $article_filter = $request->get('order_statuses');
            if($article_filter == "article-name"){
                $nameQuery = $locale == 'de' ? 'article_name_german' : $locale == 'nl' ? 'article_name_dutch' : 'article_name';
            }else if($article_filter == "article-number"){
                $nameQuery = 'article_number';
            }
            $response = new Response();
            if(is_null($article_filter))
            {
                $response->setContent("default");
                return $response;
            }
            else
            {
                $articles = $em->getRepository(ArticleEntity::class)
                ->createQueryBuilder('a')
                ->where('a.'.$nameQuery.' LIKE :name')
                ->setParameter('name', '%'.$filter.'%')
                ->getQuery()
                ->getArrayResult();
            }
            $response->setContent($this->json_encode($articles));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/articles/get-article-statistics-data", name="user-articles-get-article-statistic-data.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_article_statistics(Request $request, EntityManagerInterface $em )
    {
        try {
            $locale = $request->getLocale();
            $arts = $em->getRepository(OrderArticleEntity::class)
                ->get_article_statistics($request->get('start'), $request->get('end'), $locale);
            $response = new Response();
            $response->setContent($this->json_encode($arts));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/articles/get-article-category-statistics-data", name="user-article-get-category-statistic-data.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_article_category_statistics(Request $request, EntityManagerInterface $em )
    {
        try {
            $locale = $request->getLocale();
            $arts = $em->getRepository(OrderArticleEntity::class)
                ->get_article_category_statistics($request->get('start'), $request->get('end'), $locale);
            $response = new Response();
            $response->setContent($this->json_encode($arts));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

}