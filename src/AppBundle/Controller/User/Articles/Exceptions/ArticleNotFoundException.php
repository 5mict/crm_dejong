<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/21/2017
 * Time: 8:36 PM
 */

namespace AppBundle\Controller\User\Articles\Exceptions;

class ArticleNotFoundException extends \Exception
{

}