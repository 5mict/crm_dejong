<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/19/2017
 * Time: 8:33 PM
 */

namespace AppBundle\Controller\User\Articles\Exceptions;

class ArticleCategoryNotFoundException extends \Exception
{

}