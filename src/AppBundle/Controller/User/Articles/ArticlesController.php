<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/8/2017
 * Time: 1:01 PM
 */

namespace AppBundle\Controller\User\Articles;

use AppBundle\AppController;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ArticlesController extends AppController
{
    public function __construct(EntityManagerInterface $em)
    {
        if(!$this->is_granted(GrantEntity::DEBTOR_GRANT, $em)) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/articles", name="user-articles")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_articles(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_categories = $em->getRepository(ArticleCategoryEntity::class)
                ->get_all_with_translation($request->getLocale());
            $articles = $em->getRepository(ArticleEntity::class)
                ->get_articlesData();
            return $this->render(':user/articles:index.html.twig', [
                'articles' => $articles,
                'categories' => $article_categories,
                'company' => $this->get_company(),
                'locale' => $request->getLocale(),
                'user' => $this->getUser(),
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]);
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/articles/articles-chart", name="user-articles-articles-chart")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_articles_chart(Request $request, EntityManagerInterface $em)
    {
        $article_categories = $em->getRepository(ArticleCategoryEntity::class)
            ->get_all_with_translation($request->getLocale());
        return $this->render(
            ':user/articles/charts:articles-chart.html.twig',
            [
                'company' => $this->get_company(),
                'layout' => 'tabs',
                'Action' => 'update',
                'locale' => $request->getLocale(),
                'user' => $this->getUser(),
                'categories' => $article_categories,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]
        );
    }

    /**
     * @Route("/articles/pdf-categories", name="user-articles-pdf-categories")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_categories(Request $request, EntityManagerInterface $em)
    {
        $path = $this->get_company() . 'images/logo.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $image_data = file_get_contents($path);
        $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);

        return $this->render(
            ':user/articles/pdf:category-view.html.twig',
            [
                'company' => $this->get_company(),
                'layout' => 'tabs',
                'Action' => 'update',
                'locale' => $request->getLocale(),
                'user' => $this->getUser(),
                'logo' => $logo_base64,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]
        );
    }

    /**
     * @Route("/articles/pdf-articles", name="user-articles-pdf-articles")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_articles(Request $request, EntityManagerInterface $em)
    {
        $path = $this->get_company() . 'images/logo.png';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $image_data = file_get_contents($path);
        $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
        return $this->render(
            ':user/articles/pdf:article-view.html.twig',
            [
                'company' => $this->get_company(),
                'layout' => 'tabs',
                'Action' => 'update',
                'locale' => $request->getLocale(),
                'user' => $this->getUser(),
                'logo' => $logo_base64,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]
        );
    }
}