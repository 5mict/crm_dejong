<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/6/2018
 * Time: 6:43 PM
 */

namespace AppBundle\Controller\User\Articles;

use AppBundle\AppController;
use AppBundle\Controller\User\Articles\Exceptions\ArticleCategoryNotFoundException;
use AppBundle\Controller\User\Articles\Exceptions\ArticleSubCategoryNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\DebtorNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\EndUserTypeNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\HowDidYouFindUsNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Articles\ArticlePaidEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use AppBundle\Entity\User\Orders\OrderPaidEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticleJSONChartController extends AppController
{
    /**
     * @Route("/articles/charts", name="user-articles-get-charts-data.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_chart_data(Request $request, EntityManagerInterface $em)
    {
        try {
            $category_name = $request->get('category_name');
            $category_entity = $em->getRepository(ArticleCategoryEntity::class)
                ->findOneBy(['name' => $category_name]);
            if(empty($category_entity)) {
                throw new ArticleCategoryNotFoundException(_('Article category not found'));
            }
            $sub_category_name = $request->get('sub_category_name');
            $sub_category_entity = $em->getRepository(ArticleSubCategoryEntity::class)
                ->findOneby(['name' => $sub_category_name]);
            if (empty($sub_category_entity)) {
                throw new ArticleSubCategoryNotFoundException(_('Article sub category not found'));
            }
            $start = new \DateTime($request->get('start'));
            $start->setTime(0, 0, 0);
            $end = new \DateTime($request->get('end'));
            $end->setTime(23, 59, 59);
            $articles_paid = $em->getRepository(ArticlePaidEntity::class)
                ->get_articles_paid($category_entity, $sub_category_entity, $start, $end, $request->getLocale());
            $response = new Response();
            $response->setContent(json_encode($articles_paid));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}