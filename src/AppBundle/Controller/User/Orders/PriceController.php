<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/6/2017
 * Time: 10:17 AM
 */

namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Orders\Exceptions\OrderArticleEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PriceController extends AppController
{
    /**
     * @Route("/orders/prices", name="user-orders-prices.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_prices(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = $request->get('order_article_id');
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            if (empty($order_article)) {
                throw new OrderArticleEntityNotFoundException(_('Order article not found'));
            }
            $response = new Response();
            $response->setContent(json_encode([
                'order_article_purchase_price' => $order_article->getOrderArticlePurchasePrice(),
                'order_article_price' => $order_article->getOrderArticlePrice(),
                'price_type' => $request->get('price_type'),
                'purchase_price' => $order_article->getArticle()->getPurchasePrice(),
                'selling_price' => $order_article->getArticle()->getSellingPrice(),
                'selling_price_with_pdv' => $order_article->getArticle()->getSellingPriceWithPdv(),
                'debtor_type' => $order_article->getOrder()->getDebtor()->getEndUserType()->getName()
            ]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/prices", name="user-orders-update-prices.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function set_price(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = $request->get('order_article_id');
            $order_article_price_type = $request->get('price_type');
            $order_article_price = round(floatval($request->get('order_article_price')), 2);
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            if (empty($order_article)) {
                throw new OrderArticleEntityNotFoundException(_('Order article not found'));
            }
            $order_entity = $order_article->getOrder();
            if(empty($order_entity)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            switch($order_article_price_type) {
                case 'purchase':
                    $order_article->setOrderArticlePurchasePrice($order_article_price);
                    break;
                case 'selling':
                    $order_article->setOrderArticlePrice($order_article_price);
                    break;
                default:

            }
            $em->flush();
            $em->getRepository(OrderEntity::class)
                ->update_total_amounts($order_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $order_article_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}