<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 2/21/2018
 * Time: 1:39 AM
 */

namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Entity\User\Orders\OrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PackingAndTransportController extends AppController
{
    /**
     * @Route("/orders/packing-and-transport", name="user-orders-get-packing-and-transport.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_packing_and_transport(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('order_id');
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if (empty($order)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $response = new Response();
            $response->setContent(json_encode(['packing_and_transport' => $order->getPackingAndTransport()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/orders/packing-and-transport", name="user-orders-set-packing-and-transport.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function set_packing_and_transport(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('order_id');
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if (empty($order)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $order->setPackingAndTransport(floatval($request->get('packing_and_transport')));
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $order_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}