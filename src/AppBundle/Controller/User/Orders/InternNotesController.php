<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/20/2017
 * Time: 7:44 AM
 */

namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Orders\Exceptions\OrderNoteNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\InternNoteEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class InternNotesController extends AppController
{
    /**
     * @Route("/orders/get-intern-notes", name="user-orders-get-intern-notes.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_notes(Request $request, EntityManagerInterface $em)
    {
        try {
            $page_number = intval($request->get('page_number'));
            $page_size = intval($request->get('page_size'));
            $order_id = intval($request->get('parent_id'));
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if(empty($order)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(InternNoteEntity::class)
                    ->get_notes($order_id, $page_number, $page_size);
            $notes_array = [];
            foreach($notes as $note) {
                $notes_array[] = [
                    'id' => $note->getId(),
                    'username' => $note->getUsername(),
                    'updatedString' => $note->getUpdatedString(),
                    'note' => $note->getNote()
                ];
            }
            $response = new Response();
            $response->setContent(json_encode(['page_number' => $page_number, 'notes' => $notes_array]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/orders/add-intern-note", name="user-orders-add-intern-note.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('order_id');
            $note = $request->get('note');
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if(empty($order)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $note_entity = new InternNoteEntity();
            $note_entity->setNote($note);
            $note_entity->setCreated(new \DateTime('now'));
            $note_entity->setUpdated(new \DateTime('now'));
            $note_entity->setOrder($order);
            $note_entity->setUsername($this->getUser()->getUsername());
            $em->persist($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/delete-intern-note", name="user-orders-delete-intern-note.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $note_id = $request->get('id');
            $note_entity = $em->getRepository(InternNoteEntity::class)
                ->findOneBy(['id' => $note_id]);
            if(empty($note_entity)) {
                throw new OrderNoteNotFoundException(_('Order note not found'));
            }
            $em->remove($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/update-intern-note", name="user-orders-update-intern-note.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $note_id = $request->get('id');
            $note_entity = $em->getRepository(InternNoteEntity::class)
                ->findOneBy(['id' => $note_id]);
            if(empty($note_entity)) {
                throw new OrderNoteNotFoundException(_('Order note not found'));
            }
            $note = $request->get('note');
            $note_entity->setNote($note);
            $note_entity->setUpdated(new \DateTime('now'));
            $note_entity->setUsername($this->getUser()->getUsername());
            $em->persist($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}