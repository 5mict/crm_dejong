<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/12/2017
 * Time: 7:37 PM
 */

namespace AppBundle\Controller\User\Orders;

use AppBundle\Controller\User\Articles\Exceptions\ArticleCategoryNotFoundException;
use AppBundle\Controller\User\Articles\Exceptions\ArticleSubCategoryNotFoundException;
use AppBundle\Controller\User\Debtors\Exceptions\DebtorNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderDeliveryStatusEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderPaymentMethodEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderPaymentStatusEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderStatusNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\PaymentMethodNotFoundException;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\ArticlePaidEntity;
use AppBundle\Entity\User\Articles\ArticleSubCategoriesPaidEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\DeliveryAddressEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity;
use AppBundle\Entity\User\Orders\InvoiceNumbers;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Orders\OrderFileEntity;
use AppBundle\Entity\User\Orders\OrderMailAttachmentFileEntity;
use AppBundle\Entity\User\Orders\OrderPaidEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;
use AppBundle\Repository\User\Orders\InvoiceNumberRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\AppController;

class  OrdersJSONController extends AppController
{
    /**
     * @Route("/orders/add-order", name="user-orders-add-order.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_order(Request $request, EntityManagerInterface $em)
    {
        try {
            $debtor_name = $request->get('debtor_name');
            $debtor_entity = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['name' => $debtor_name]);
            if (empty($debtor_entity)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            $delivery_address_entity = $em->getRepository(DeliveryAddressEntity::class)
                ->findOneBy(['id' => $request->get('delivery_address_id')]);
            if (empty($delivery_address_entity)) {
                throw new \Exception('Delivery address does not exist');
            }
            $debtor_id = $debtor_entity->getId();
            $contact_person_name = $request->get('contact_person_name');
            $contact_person_entity = $em->getRepository(ContactPersonEntity::class)
                ->findOneBy(['name' => $contact_person_name, 'debtor' => $debtor_id]);
            if(empty($contact_person_entity)) {
                throw new \Exception('Contact person does not exist');
            }
            $order_delivery_status = $request->get('order_delivery_status');
            $order_delivery_status_entity = $em->getRepository(OrderDeliveryStatusEntity::class)
                ->findOneBy(['name' => $order_delivery_status]);
            if(empty($order_delivery_status_entity)) {
                throw new OrderDeliveryStatusEntityNotFoundException(_('Delivery status not supported'));
            }
            $order_status = $request->get('order_status');
            $order_status_entity = $em->getRepository(OrderStatusEntity::class)
                ->findOneBy(['name' => $order_status]);
            if(empty($order_status_entity)) {
                throw new OrderStatusNotFoundException(_('Status not supported'));
            }
            $payment_method = $request->get('payment_method');
            $payment_method_entity = $em->getRepository(OrderPaymentMethodEntity::class)
                ->findOneBy(['name' => $payment_method]);
            if(empty($payment_method_entity)) {
                throw new OrderPaymentMethodEntityNotFoundException(_('Payment method not found'));
            }
            $not_paid = $em->getRepository(OrderPaymentStatusEntity::class)
                ->findOneBy(['name' => OrderPaymentStatusEntity::NOT_PAID]);
            $order_entity = new OrderEntity();
            $order_entity->setDebtor($debtor_entity);
            $order_entity->setContactPerson($contact_person_entity);
            $order_entity->setDeliveryStatus($order_delivery_status_entity);
            $order_entity->setOrderStatus($order_status_entity);
            $order_entity->setOrderDatum(new \DateTime('now'));
            $order_entity->setCreated(new \DateTime('now'));
            $order_entity->setUpdated(new \DateTime('now'));
            $order_entity->setDeliveryAddress($delivery_address_entity);
            $order_entity->setInvoiceNumber('');
            $order_entity->setOrderInvoiceNumber(0);
            $order_entity->setOrderSupervisor($request->get('supervisor'));
            $order_entity->setDiscount(0);
            $etu = $debtor_entity->getEndUserType()->getName();
            $order_entity->setOrderNumber($em->getRepository(InvoiceNumbers::class)
                ->get_order_number());
            //$order_entity->setOrderNumber($this->generateRandomString(10));
            $order_entity->setDeliveryTime(new \DateTime($request->get('delivery_time')));
            $order_entity->setTotalPurchaseAmount(0);
            $order_entity->setTotalSellingAmount(0);
            $order_entity->setPaymentMethod($payment_method_entity);
            $order_entity->setPaymentStatus($not_paid);
            $order_entity->setPackingAndTransport(0);
            $em->persist($order_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $order_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/delete-order", name="user-orders-delete-order.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_order(Request $request, EntityManagerInterface $em)
    {
        try{
            $order_id = $request->get('id');
            $order_entity = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            $order_files = $em->getRepository(OrderFileEntity::class)
                ->findBy(['order' => $order_entity]);
            $orders_paid = $em->getRepository(OrderPaidEntity::class)
                ->findBy(['order' => $order_entity]);
            $order_articles =  $em->getRepository(OrderArticleEntity::class)
                ->findBy(['order' => $order_entity]);
            $order_mails = $em->getRepository(OrderMailEntity::class)
                ->findBy(['order' => $order_entity]);
            if(!empty($order_files)){
                foreach ($order_files as $file){
                    $em->remove($file);
                }
            }
            if(!empty($order_mails)){
                foreach($order_mails as $order_mail){
                    $order_mail_attachments = $em->getRepository(OrderMailAttachmentFileEntity::class)
                        ->findBy(['order_mail' => $order_mails]);
                    foreach($order_mail_attachments as $attachment){
                        $em->remove($attachment);
                    }
                    $em->remove($order_mail);
                }
            }
            if(!empty($order_articles)){
                foreach($order_articles as $order_article){
                    $order_mails = $em->getRepository(OrderMailEntity::class)
                        ->findBy(['order_article' => $order_article]);
                    foreach($order_mails as $order_mail){
                        $em->remove($order_mail);
                    }
                    $articles_paid = $em->getRepository(ArticlePaidEntity::class)
                        ->findBy(['order_article' => $order_article]);
                    foreach($articles_paid as $article_paid){
                        $em->remove($article_paid);
                    }
                    $em->remove($order_article);
                }
            }
            if(!empty($orders_paid)){
                foreach($orders_paid as $order_paid){
                    $em->remove($order_paid);
                }
            }
            $em->remove($order_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $order_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/update-order", name="user-orders-update-order.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_order(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_number = $request->get('order_number');
            $order_entity = $em->getRepository(OrderEntity::class)
                ->findOneBy(['order_number' => $order_number]);
            if(empty($order_entity)) {
                throw new \Exception('Order does not exist');
            }
            $debtor_name = $request->get('debtor_name');
            $debtor_entity = $em->getRepository(DebtorEntity::class)
                ->findOneBy(['name' => $debtor_name]);
            if (empty($debtor_entity)) {
                throw new \Exception('Debtor does not exist');
            }
            $delivery_address_entity = $em->getRepository(DeliveryAddressEntity::class)
                ->findOneBy(['id' => $request->get('delivery_address_id')]);
            if (empty($delivery_address_entity)) {
                throw new \Exception('Delivery address does not exist');
            }
            $debtor_id = $debtor_entity->getId();
            $contact_person_name = $request->get('contact_person_name');
            $contact_person_entity = $em->getRepository(ContactPersonEntity::class)
                ->findOneBy(['name' => $contact_person_name, 'debtor' => $debtor_id]);
            if(empty($contact_person_entity)) {
                throw new \Exception(_('Contact person does not exist'));
            }
            $order_delivery_status = $request->get('order_delivery_status');
            $order_delivery_status_entity = $em->getRepository(OrderDeliveryStatusEntity::class)
                ->findOneBy(['name' => $order_delivery_status]);
            if(empty($order_delivery_status_entity)) {
                throw new \Exception(_('Delivery status not supported'));
            }
            $order_status = $request->get('order_status');
            $order_status_entity = $em->getRepository(OrderStatusEntity::class)
                ->findOneBy(['name' => $order_status]);
            if(empty($order_status)) {
                throw new \Exception(_('Status not supported'));
            }
            $payment_method_entity = $em->getRepository(OrderPaymentMethodEntity::class)
                ->findOneBy(['name' => $request->get('payment_method')]);
            if(empty($order_status)) {
                throw new \Exception(_('Payment method not supported'));
            }
            $order_status_name = $order_status_entity->getName();
            $order_articles = $em->getRepository(OrderArticleEntity::class)
                ->findBy(['order' => $order_entity]);
            list($total_purchase_price, $total_selling_price) =
                $this->get_total_prices($order_articles);
            if($order_status_name == OrderStatusEntity::POST_CALCULATION_READY || $order_status_name == OrderStatusEntity::FINISHED){
                $order_supplier_articles = $em->getRepository(OrderArticleEntity::class)
                    ->get_order_articles_for_supplierDataOrder($order_entity->getId());
                $to_supplier_status = $em->getRepository(OrderToSupplierStatusEntity::class)
                    ->findOneBy(['name' => OrderToSupplierStatusEntity::FINISHED]);
                $this->create_order_paid_entity(
                    $em,
                    $order_entity,
                    $debtor_entity,
                    $total_purchase_price,
                    $total_selling_price
                );
                $this->increase_debtor_total_prices(
                    $debtor_entity,
                    $total_purchase_price,
                    $total_selling_price
                );
                foreach ($order_articles as $order_article) {
                    $article_entity = $order_article->getArticle();
                    $this->create_article_paid_entity($em, $article_entity, $order_article);
                    $this->increase_article_total_prices($article_entity, $order_article);
                }
               foreach ($order_supplier_articles as $order_supplier_article){
                   $order_supplier_article->setToSupplierStatus($to_supplier_status);
                   $em->persist($order_status_entity);
               }
                $payment_status_entity = $em->getRepository(OrderPaymentStatusEntity::class)
                    ->findOneBy(['name' => OrderPaymentStatusEntity::PAID]);
                $order_entity->setPaymentStatus($payment_status_entity);
            }
            $order_entity->setDebtor($debtor_entity);
            $order_entity->setContactPerson($contact_person_entity);
            $order_entity->setDeliveryStatus($order_delivery_status_entity);
            $order_entity->setOrderStatus($order_status_entity);
            $order_entity->setDeliveryAddress($delivery_address_entity);
            $order_entity->setOrderDatum(new \DateTime('now'));
            $order_entity->setUpdated(new \DateTime('now'));
            $order_entity->setOrderSupervisor($request->get('supervisor'));
            $order_entity->setDiscount(0);
            if(!empty($request->get('delivery_time'))){
            $order_entity->setDeliveryTime(new \DateTime($request->get('delivery_time')));}
            $order_entity->setPaymentMethod($payment_method_entity);
            $em->persist($order_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $order_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/invoice-paid", name="user-orders-invoice-paid.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function invoice_paid(Request $request, EntityManagerInterface $em)
    {
        try {
            $response = new Response();
            $payment_status = $request->get('payment_status');
            $payment_status_entity = $em->getRepository(OrderPaymentStatusEntity::class)
                ->findOneBy(['name' => $payment_status]);
            if(empty($payment_status_entity)) {
                throw new OrderPaymentStatusEntityNotFoundException(_('Payment status not found'));
            }
            $order_id = $request->get('id');
            $order_entity = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if(empty($order_entity)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            if($order_entity->getPaymentStatus()->getName() === $payment_status_entity->getName()) {
                $response->setContent(json_encode(['nothing to update']));
                return $response;
            }
            $debtor_entity = $order_entity->getDebtor();
            if(empty($debtor_entity)) {
                throw new DebtorNotFoundException(_('Debtor not found'));
            }
            $order_articles = $em->getRepository(OrderArticleEntity::class)
                ->findBy(['order' => $order_entity]);
            list($total_purchase_price, $total_selling_price) =
                $this->get_total_prices($order_articles);
            switch($payment_status) {
                case OrderPaymentStatusEntity::PAID:
                    $this->create_order_paid_entity(
                        $em,
                        $order_entity,
                        $debtor_entity,
                        $total_purchase_price,
                        $total_selling_price
                    );
                    $this->increase_debtor_total_prices(
                        $debtor_entity,
                        $total_purchase_price,
                        $total_selling_price
                    );
                    foreach ($order_articles as $order_article) {
                        $article_entity = $order_article->getArticle();
                        $this->create_article_paid_entity($em, $article_entity, $order_article);
                        $this->increase_article_total_prices($article_entity, $order_article);
                    }
                    break;
                case OrderPaymentStatusEntity::NOT_PAID:
                    $this->remove_order_paid_entity($em, $order_entity);
                    $this->decrease_debtor_total_prices(
                        $debtor_entity,
                        $total_purchase_price,
                        $total_selling_price
                    );
                    foreach($order_articles as $order_article) {
                        $this->remove_article_paid_entity($em, $order_article);
                        $this->decrease_article_total_prices($order_article);
                    }
                    break;
                default:
            }
            $order_entity->setPaymentStatus($payment_status_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['payment status set to' . $payment_status]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    protected function increase_article_total_prices(
        ArticleEntity $article_entity,
        OrderArticleEntity $order_article
    )
    {
        $total_selling_price = $order_article->getTotalSellingPrice();
        $total_purchase_price = $order_article->getTotalPurchasePrice();
        $article_total_purchase_price = $article_entity->getTotalPurchasePrice();
        $article_total_purchase_price += $total_purchase_price;
        $article_entity->setTotalPurchasePrice($article_total_purchase_price);
        $article_total_selling_price = $article_entity->getTotalSellingPrice();
        $article_total_selling_price += $total_selling_price;
        $article_entity->setTotalSellingPrice($article_total_selling_price);
        if ($article_total_purchase_price == 0) {
            $article_entity->setProfitPercents(0);
        }
        else {
            $article_entity->setProfitPercents(round(($article_total_selling_price / $article_total_purchase_price - 1.) * 100., 2));
        }
    }

    protected function decrease_article_total_prices(
        OrderArticleEntity $order_article
    ) {
        $article_entity = $order_article->getArticle();
        $total_selling_price = $order_article->getTotalSellingPrice();
        $total_purchase_price = $order_article->getTotalPurchasePrice();
        $article_total_purchase_price = $article_entity->getTotalPurchasePrice();
        $article_total_purchase_price -= $total_purchase_price;
        $article_entity->setTotalPurchasePrice($article_total_purchase_price);
        $article_total_selling_price = $article_entity->getTotalSellingPrice();
        $article_total_selling_price -= $total_selling_price;
        $article_entity->setTotalSellingPrice($article_total_selling_price);
        if ($article_total_purchase_price == 0) {
            $article_entity->setProfitPercents(0);
        }
        else {
            $article_entity->setProfitPercents(round(($article_total_selling_price / $article_total_purchase_price - 1.) * 100., 2));
        }
    }

    protected function create_article_paid_entity(
        EntityManagerInterface $em,
        ArticleEntity $article_entity,
        OrderArticleEntity $order_article
    ) {
        $article_paid_entity = $em->getRepository(ArticlePaidEntity::class)
            ->findOneBy(['order_article' => $order_article]);
        if (empty($article_paid_entity)) {
            $article_paid_entity = new ArticlePaidEntity();
            $em->persist($article_paid_entity);
        }
        $article_paid_entity->setArticle($article_entity);
        $article_paid_entity->setCategory($article_entity->getArticleCategory());
        $article_paid_entity->setSubCategory($article_entity->getArticleSubCategory());
        $article_paid_entity->setOrderArticle($order_article);
        $total_selling_price = $order_article->getTotalSellingPrice();
        $total_purchase_price = $order_article->getTotalPurchasePrice();
        $article_paid_entity->setSellingPrice($total_selling_price);
        $article_paid_entity->setPurchasePrice($total_purchase_price);
        $article_paid_entity->setNumberOfArticles($order_article->getQuantity());
        if ($total_purchase_price == 0) {
            $article_paid_entity->setProfitPercents(0);
        }
        else {
            $article_paid_entity->setProfitPercents(round(($total_selling_price / $total_purchase_price - 1.) * 100., 2));
        }
        $article_paid_entity->setCreated(new \DateTime('now'));
        return $article_paid_entity;
    }

    protected function remove_article_paid_entity(
        EntityManagerInterface $em,
        OrderArticleEntity $order_article
    ) {
        $article_paid_entity = $em->getRepository(ArticlePaidEntity::class)
            ->findOneBy(['order_article' => $order_article]);
        if(!empty($article_paid_entity)) {
            $em->remove($article_paid_entity);
        }
    }

    protected function decrease_debtor_total_prices(
        DebtorEntity $debtor_entity,
        float $total_purchase_price,
        float $total_selling_price
    ) {
        $debtor_total_purchase_price = $debtor_entity->getTotalPurchasePrice();
        $debtor_total_purchase_price -= $total_purchase_price;
        $debtor_entity->setTotalPurchasePrice($debtor_total_purchase_price);
        $debtor_total_selling_price = $debtor_entity->getTotalSellingPrice();
        $debtor_total_selling_price -= $total_selling_price;
        $debtor_entity->setTotalSellingPrice($debtor_total_selling_price);
        if ($debtor_total_purchase_price == 0) {
            $debtor_entity->setProfitPercents(0);
        }
        else {
            $debtor_entity->setProfitPercents(round(($debtor_total_selling_price / $debtor_total_purchase_price - 1.) * 100., 2));
        }
    }

    protected function increase_debtor_total_prices(
        DebtorEntity $debtor_entity,
        float $total_purchase_price,
        float $total_selling_price
    ) {
        $debtor_total_purchase_price = $debtor_entity->getTotalPurchasePrice();
        $debtor_total_purchase_price += $total_purchase_price;
        $debtor_entity->setTotalPurchasePrice($debtor_total_purchase_price);
        $debtor_total_selling_price = $debtor_entity->getTotalSellingPrice();
        $debtor_total_selling_price += $total_selling_price;
        $debtor_entity->setTotalSellingPrice($debtor_total_selling_price);
        if($debtor_total_purchase_price == 0) {
            $debtor_entity->setProfitPercents(0);
        } else {
            $debtor_entity->setProfitPercents(round(($debtor_total_selling_price / $debtor_total_purchase_price - 1.) * 100., 2));
        }
    }

    protected function create_order_paid_entity(
        EntityManagerInterface $em,
        OrderEntity $order_entity,
        DebtorEntity $debtor_entity,
        float $total_purchase_price,
        float $total_selling_price
    ) : OrderPaidEntity
    {
        $order_paid_entity = $em->getRepository(OrderPaidEntity::class)
            ->findOneBy(['order' => $order_entity]);
        if (empty($order_paid_entity)) {
            $order_paid_entity = new OrderPaidEntity();
            $em->persist($order_paid_entity);
        }
        $order_paid_entity->setDebtor($debtor_entity);
        $order_paid_entity->setOrder($order_entity);
        $order_paid_entity->setPurchasePrice($total_purchase_price);
        $order_paid_entity->setSellingPrice($total_selling_price);
        if ($total_purchase_price == 0) {
            $order_paid_entity->setProfitPercent(0);
        }
        else {
            $order_paid_entity->setProfitPercent(round(($total_selling_price / $total_purchase_price - 1.) * 100., 2));
        }
        $order_paid_entity->setCreated(new \DateTime('now'));
        return $order_paid_entity;
    }

    protected function remove_order_paid_entity(EntityManagerInterface $em, OrderEntity $order_entity)
    {
        $order_paid_entity = $em->getRepository(OrderPaidEntity::class)
            ->findOneBy(['order' => $order_entity]);
        if (!empty($order_paid_entity)) {
            $em->remove($order_paid_entity);
        }
    }

    protected function get_total_prices($order_articles)
    {
        $total_purchase_price = 0.;
        $total_selling_price = 0.;
        foreach($order_articles as $order_article) {
            $total_purchase_price += $order_article->getTotalPurchasePrice();
            $total_selling_price += $order_article->getTotalSellingPrice();
        }
        return [$total_purchase_price, $total_selling_price];
    }
}