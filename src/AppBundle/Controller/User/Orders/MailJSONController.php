<?php
namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Orders\Exceptions\OrderArticleEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderMailNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderMailNotSentException;
use AppBundle\Controller\User\Orders\Exceptions\OrderMailStatusEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderStatusNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Orders\OrderMailAttachmentFileEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MailJSONController extends AppController
{
    /**
     * @Route("/orders/send-mail", name="user-orders-send-mail.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function send_mail(Request $request, EntityManagerInterface $em)
    {
        $failed_recipients = [];
        $locations = [];
        try {
            $message_type = $request->get('message_type');
            $locale = $request->getLocale();
            $document_name = $this->get_document_name($message_type, $locale);
            $order_entity = $em->getRepository(OrderEntity::class)
                ->findOneBy(['order_number' => $request->get('order_number')]);
            if(empty($order_entity)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $order_mail_status = $em->getRepository(OrderMailStatusEntity::class)
                ->findOneBy(['name' => $request->get('mail_type')]);
            if(empty($order_mail_status)) {
                throw new OrderMailStatusEntityNotFoundException(_('Order mail status not found'));
            }
            $message_string = str_replace("\n", "<br/>", $request->get('mail_body'));
            $order_mail_entity = new OrderMailEntity();
            $order_mail_entity->setOrder($order_entity);
            $order_mail_entity->setOrderMailStatus($order_mail_status);
            $order_mail_entity->setSendTo($request->get('mail_address'));
            $order_mail_entity->setOrderDetails($request->get('details'));
            $order_mail_entity->setMailSubject($request->get('mail_subject'));
            $signature = $this->get_signature($request->getLocale());
            $order_mail_entity->setMailBody($request->get('mail_body'));
            $order_mail_entity->setCreated(new \DateTime('now'));
            $file_name = $this->generateRandomString(30) . '.pdf';
            $location_upload = $this->get_company() . "mail_attachments/" . $file_name;
            $file_tmp_name = $_FILES["pdf"]["tmp_name"];
            $this->upload_validate($file_tmp_name, $file_name, "mail_attachments");
            $order_mail_entity->setAttachmentFileName($file_name);
            if($order_mail_status->getName() == OrderMailStatusEntity::ORDER_FROM_SUPPLIER) {
                $order_article_entity = $em->getRepository(OrderArticleEntity::class)
                    ->findOneBy(['id' => intval($request->get('order_article_id'))]);
                if(empty($order_article_entity)) {
                    throw new OrderArticleEntityNotFoundException(_('Order article not found'));
                }
                $order_article_entity->setDeliveryTime(new \DateTime($request->get('delivery_date')));
                $order_article_entity->setCommission($request->get('commission'));
                $order_mail_entity->setOrderArticle($order_article_entity);
            } else {
                $order_status = $em->getRepository(OrderStatusEntity::class)
                    ->findOneBy(['name' => $order_mail_status->getName()]);
                if(empty($order_status)) {
                    throw new OrderStatusNotFoundException(_('Order status not found'));
                }
                $order_entity->setOrderStatus($order_status);
                $em->persist($order_entity);
            }
            if(isset($_FILES["file"])){
                foreach($_FILES['file']['name'] as $key => $name){

                    $tmp_name = $_FILES['file']['tmp_name'][$key];
                    $imageType = pathinfo($name, PATHINFO_EXTENSION);
                    $attached_file_name = $this->generateRandomString(30) . '.' . $imageType;
                    $this->upload_validate($tmp_name, $attached_file_name, "mail_attachments");
                    $locations[$key] = $attached_file_name;
                    $order_mail_attachment = new OrderMailAttachmentFileEntity();
                    $order_mail_attachment->setFileName($attached_file_name);
                    $order_mail_attachment->setOriginalName($name);
                    $order_mail_attachment->setOrderMail($order_mail_entity);
                    $order_mail_attachment->setCreated(new \DateTime('now'));
                    $em->persist($order_mail_attachment);
                }
            }
            $order_mail_attachment = new OrderMailAttachmentFileEntity();
            $order_mail_attachment->setFileName($file_name);
            $order_mail_attachment->setOriginalName($document_name);
            $order_mail_attachment->setOrderMail($order_mail_entity);
            $order_mail_attachment->setCreated(new \DateTime('now'));
            $em->persist($order_mail_attachment);
            $user = $this->getUser();
            $transport = (new \Swift_SmtpTransport('smtp.office365.com', 587, 'tls'))
                ->setUsername($user->getEmail())
                ->setPassword('ACrb&w604:H');
            $swift_mailer = new \Swift_Mailer($transport);
            $message = (new \Swift_Message())
                ->setSubject($request->get('mail_subject'))
                ->setFrom($user->getEmail())
                ->setTo($request->get('mail_address'))
                ->setBody('' . $message_string . $signature, "text/html")
                ->attach(\Swift_Attachment::fromPath($location_upload)->setFilename($document_name));
            if(isset($_FILES["file"])) {
                foreach ($_FILES["file"]["name"] as $key => $file) {
                    $location_upload = $this->get_company() . "mail_attachments/" . $locations[ $key ];
                    $message->attach(\Swift_Attachment::fromPath($location_upload)->setFilename($file));
                }
            }

            $result = $swift_mailer->send($message, $failed_recipients);
            if($result == 0) {
                throw new OrderMailNotSentException(_('Order mail not sent for recipient ' . $failed_recipients[0]));
            }
            $em->persist($order_mail_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['mail sent']));
            return $response;
        } catch(\Exception $e) {
            return  $this->handle_json_error(400, $e->getMessage());
        }
    }

    protected function get_signature(string $locale)
    {
        if($this->getUser()->getPhonenumber() != "" && $this->getUser()->getFirstName() != 'Nanne'){
            $phone_number = '     <span style="float: left">
                <span style="color: #CEB795"> Mobile:</span> '. $this->getUser()->getPhonenumber() .'
            </span>';
        }else{
            $phone_number = '';
        }
        if($this->getUser()->getFirstName() == "Stijn"){
            $title = ' <b>De Jong</b><b> Interieur / De Jachtkamer</b>';
            $website = '<a class="links" style="color: #CEB795">www.dejonginterieur.com / www.dejachtkamer.nl</a>';
        }else{
            $title = ' <b>De Jong</b><b> Interieur</b>';
            $website = '<a class="links" style="color: #CEB795">www.dejonginterieur.com</a>';
        }
        $signature = "<br/><br/>" .
                    '<div id="signature">
            <span style="float: left">  Met vriendelijke groet - With kind regards – Mit freundlichen Grüßen </span>
            <br/><br/>
            <span style="float: left">
               ' . $this->getUser()->getFirstName() . " " . $this->getUser()->getLastName() . '
            </span>
            <br/>
            <span style="float: left">
                '. $title .'
            </span> <br/><br/>
            <span style="float: left">  De Blecourtstraat 28a | 5652GB | Eindhoven | Holland</span>
            <br/><br/>
            '. $phone_number .'
            <br/><br/>
            <span style="float: left">
                <span style="color: #CEB795">Email:</span>
                <a class="links" style="color: #CEB795">info@dejonginterieur.com</a>
            </span>
            <br/><br/>
            <span style="float: left">
                <span style="color: #CEB795">Website:</span>
                 '. $website .'
            </span>
            <br/><br/>
              <img style="float: left" src="http://185.135.10.98/Company/DejongInterieur/images/environment.gif" />
        </div>';
        return $signature;
    }
    /**
     * @Route("/orders/get-mail", name="user-orders-get-mail.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_mail(Request $request, EntityManagerInterface $em)
    {
        try {
            $mail_id = intval($request->get('id'));
            $mail_entity = $em->getRepository(OrderMailEntity::class)
                ->findOneBy(['id' => $mail_id]);
            $mail_attachment_entity = $em->getRepository(OrderMailAttachmentFileEntity::class)
                ->findBy(['order_mail' => $mail_entity]);
            if (empty($mail_entity)) {
                throw new OrderMailNotFoundException(_('Order mail not found'));
            }
            $files = [];
            foreach($mail_attachment_entity as $key => $attachment){
                $files[$key] = $attachment->getFileName();
            }
            $data = [];
            $data['mail_subject'] = $mail_entity->getMailSubject();
            $data['send_to'] = $mail_entity->getSendTo();
            $data['mail_body'] = $mail_entity->getMailBody();
            $data['file'] = '/' . $this->get_company() . 'mail_attachments/' . $mail_entity->getAttachmentFileName();
            $data['created'] = $mail_entity->getCreatedString();
            $data['mail_status'] = $mail_entity->getOrderMailStatus();
            $data['files'] = $files;
            $response = new Response();
            $response->setContent(json_encode($data));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/orders/get-mail-to-supplier", name="user-orders-get-mail-to-supplier.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_mail_to_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = intval($request->get('id'));
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            $mail_status = $em->getRepository(OrderMailStatusEntity::class)
                ->findOneBy(['name' => 'order-from-supplier']);
            $mail_entities = $em->getRepository(OrderMailEntity::class)
                ->findBy(['order_article' => $order_article, 'order_mail_status' => $mail_status], ['created' => 'DESC']);
            $mail_entity = null;
            if(sizeof($mail_entities)  > 0)
                $mail_entity = $mail_entities[0];
            else
                throw new OrderMailNotFoundException(_('Order mail not found'));
            $data = [];
            $data['mail_subject'] = $mail_entity->getMailSubject();
            $data['send_to'] = $mail_entity->getSendTo();
            $data['mail_body'] = $mail_entity->getMailBody();
            $data['file'] = '/' . $this->get_company() . 'mail_attachments/' . $mail_entity->getAttachmentFileName();
            $data['created'] = $mail_entity->getCreatedString();
            $data['mail_status'] = $mail_entity->getOrderMailStatus();
            $data['files'] = "";
            $response = new Response();
            $response->setContent(json_encode($data));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @param string $message_type
     * @param string $locale
     * @return string
     */
    public function get_document_name(string $message_type, string $locale)
    {
        switch ($message_type){
            case 'offer':
                if ($locale == "en"){
                    $document_name = "Offer-de-Jong-Interieur.pdf";
                }else{
                    $document_name = "Offerte-de-Jong-Interieur.pdf";
                }
                break;
            case 'invoice':
                if ($locale == "en"){
                    $document_name = "Invoice-de-Jong-Interieur.pdf";
                }else{
                    $document_name = "Factuur-de-Jong-Interieur.pdf";
                }
                break;
            case 'order_from_supplier':
                if ($locale == "en"){
                    $document_name = "Order-from-supplier-de-Jong-Interieur.pdf";
                }else{
                    $document_name = "Bestelling-naar-leverancier-de-Jong-Interieur.pdf";
                }
                break;
            default:
                if ($locale == "en"){
                    $document_name = "Order-Confirmation-de-Jong-Interieur.pdf";
                }else{
                    $document_name = "Opdrachtbevestiging-de-Jong-Interieur.pdf";
                }
                break;
        }
        return $document_name;
    }
}
