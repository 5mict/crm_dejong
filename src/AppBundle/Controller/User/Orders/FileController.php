<?php
namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Orders\OrderFileEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class FileController extends AppController
{
    /**
     * @Route("/orders/upload_order_file", name="user-upload_order_file.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function upload_order_file(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_number = $request->get('order_number');
            $order_entity = $em->getRepository(OrderEntity::class)
                ->findOneBy(['order_number' => $order_number]);
            foreach($_FILES['file']['name'] as $key => $original){
                $imageType = pathinfo($original, PATHINFO_EXTENSION);
                $file_name = $this->generateRandomString(30) . '.' . $imageType;
                $file_tmp_name = $_FILES["file"]["tmp_name"][$key];
                $this->upload_validate_file($file_tmp_name, $file_name, "order_files");
                $order_file_entity = new OrderFileEntity();
                $order_file_entity->setOrder($order_entity);
                $order_file_entity->setFileName($file_name);
                $order_file_entity->setName($original);
                $order_file_entity->setCreated(new \DateTime('now'));
                $em->persist($order_file_entity);
            }
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['file uploaded']));
            return $response;

        } catch (\Exception $e){
            return $this->handle_json_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/orders/get_uploaded_file/{id}", name="post-uploaded-file")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_view_order_to_supplier_pdf(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(OrderFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'order_files/' . $file->getFileName();
            $order = $file->getOrder();
            return $this->render(
                ':user/orders/pdf:view_uploaded_file.html.twig',
                [
                    'company' => $this->get_company(),
                    'ActionId' => $order->getId(),
                    'Action' => 'OrderFiles',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/get_uploaded_file/{id}", name="get-uploaded-file")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_view_order_to_supplier_pdf(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(OrderFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'order_files/' . $file->getFileName();
            $order = $file->getOrder();
            return $this->render(
                ':user/orders/pdf:view_uploaded_file.html.twig',
                [
                    'company' => $this->get_company(),
                    'ActionId' => $order->getId(),
                    'Action' => 'OrderFiles',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/get-view-pdf-file", name="get-view-file")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_view_pdf_file(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(OrderFileEntity::class)
                ->findOneBy(['order' => $file_id]);
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'order_files/' . $file->getFileName();
            $order = $file->getOrder();
            return $this->render(
                ':user/orders/pdf:view_uploaded_file.html.twig',
                [
                    'company' => $this->get_company(),
                    'ActionId' => $order->getId(),
                    'Action' => 'OrderFiles',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/delete_uploaded_file", name="delete-order-uploaded-file.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_uploaded_file(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(OrderFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $this->delete_validate("order_files", $file->getFileName());
            $em->remove($file);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['file deleted']));
            return $response;

        }
        catch (\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}