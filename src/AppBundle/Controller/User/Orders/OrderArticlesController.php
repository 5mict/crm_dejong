<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/13/2017
 * Time: 7:48 AM
 */

namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class OrderArticlesController extends AppController
{
    public function __construct(EntityManagerInterface $em)
    {
        if (!$this->is_granted(GrantEntity::ORDER_GRANT, $em)) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/orders/order-articles", name="user-orders-order-articles")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_order_articles(Request $request, EntityManagerInterface $em)
    {
        try {
            $page_number = intval($request->get('page_number'));
            $page_size = intval($request->get('page_size'));
            $sort_by = $request->get('sort_by');
            $sort_direction = $request->get('sort_direction');
            list($order_articles, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(OrderArticleEntity::class)
                    ->get_order_articles($page_number, $page_size, $sort_by, $sort_direction);
            return $this->render(':user/orders:index.html.twig', [
                'order_articles' => $order_articles,
                'company' => $this->get_company(),
                'user' => $this->getUser(),
                'order_article_page_number' => $page_number,
                'order_article_page_size' => $page_size,
                'order_article_total_number_of_pages' => $total_number_of_pages,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]);
        } catch(\Exception $e) {
            return  $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/pdf-order-supplier", name="user-pdf-order-supplier-articles")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_order_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('order_id');
            $supplier_id = $request->get('supplier_id');
            $order_suppliers = $em->getRepository(OrderArticleEntity::class)
                ->get_order_articles_for_supplier($order_id, $supplier_id);
            $order_articles_data = [];
            $oa_data = [];
            foreach($order_suppliers as $order_article){
                $order = $order_article->getOrder();
                $supplier = $order_article->getSupplier();
                $oa_data = [
                    'order_article_id' => $order_article->getId(),
                    'supplier_id' => $supplier->getId(),
                    'supplier_name' => $supplier->getName(),
                    'supplier_email' => $supplier->getEmail(),
                    'language' => $supplier->getCommunicationLanguage(),
                    'order_number' => $order->getOrderNumber(),
                ];
                if($order_article->getPictureUrl() == "default.jpg" or empty($order_article->getPictureUrl())){
                    $show_picture = false;
                    $picture_encoded = '';
                }else{
                    $show_picture = true;
                    $picture_encoded = $this->picture_encode($order_article->getPictureUrl());
                }
                $order_articles_data[] = [
                    'picture_url' => $order_article->getPictureUrl(),
                    'encoded_picture' => $picture_encoded,
                    'show_picture' => $show_picture,
                    'delivered' => $order_article->getDelivered(),
                    'details' => str_replace(array("\r", "\n"), '\n', $order_article->getDetails())
                ];

            }
            $oa_data['order_articles'] = $order_articles_data;
            $logo = $this->picture_encode('pdf_logo.jpg');
            return $this->render(':user/orders/pdf:order_to_supplier.html.twig', [
                'company' => $this->get_company(),
                'order_supplier' => $oa_data,
                'user' => $this->getUser(),
                'locale' => $request->getLocale(),
                'order_id' => $order_id,
                'logo' => $logo,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                'mail_type' => OrderMailStatusEntity::ORDER_FROM_SUPPLIER
            ]);
        } catch(\Exception $e) {
            return  $this->handle_html_error(400, $e->getMessage());
        }
    }
}
