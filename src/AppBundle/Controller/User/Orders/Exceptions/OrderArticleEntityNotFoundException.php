<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/26/2017
 * Time: 10:11 AM
 */

namespace AppBundle\Controller\User\Orders\Exceptions;

class OrderArticleEntityNotFoundException extends \Exception
{

}