<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/11/2018
 * Time: 9:52 PM
 */

namespace AppBundle\Controller\User\Orders\Exceptions;

class OrderToSupplierStatusNotFoundException extends \Exception
{

}