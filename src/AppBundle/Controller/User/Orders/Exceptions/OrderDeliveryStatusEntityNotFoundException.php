<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 4:48 PM
 */

namespace AppBundle\Controller\User\Orders\Exceptions;

class OrderDeliveryStatusEntityNotFoundException extends \Exception
{

}