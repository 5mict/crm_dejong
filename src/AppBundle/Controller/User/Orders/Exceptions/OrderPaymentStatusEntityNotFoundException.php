<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 1:59 PM
 */

namespace AppBundle\Controller\User\Orders\Exceptions;

class OrderPaymentStatusEntityNotFoundException extends \Exception
{

}