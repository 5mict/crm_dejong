<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/13/2017
 * Time: 5:53 PM
 */

namespace AppBundle\Controller\User\Orders\Exceptions;

class OrderNotFoundException extends \Exception
{

}