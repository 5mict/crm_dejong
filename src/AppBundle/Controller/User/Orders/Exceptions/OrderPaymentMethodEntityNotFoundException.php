<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 1:49 PM
 */
namespace AppBundle\Controller\User\Orders\Exceptions;

class OrderPaymentMethodEntityNotFoundException extends \Exception
{

}