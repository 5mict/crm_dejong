<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/29/2017
 * Time: 12:26 AM
 */

namespace AppBundle\Controller\User\Orders\Exceptions;

class OrderMailNotSentException extends \Exception
{

}