<?php

namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Entity\User\Orders\OrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class DiscountController extends AppController
{
    /**
     * @Route("/orders/discount", name="user-orders-get-discount.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_discount(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('order_id');
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if (empty($order)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $response = new Response();
            $response->setContent(json_encode(['discount' => $order->getDiscount()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/orders/discount", name="user-orders-set-discount.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function set_discount(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('order_id');
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if (empty($order)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $order->setDiscount(floatval($request->get('discount')));
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $order_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}