<?php
namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Orders\Exceptions\OrderMailNotFoundException;
use AppBundle\Entity\User\Orders\OrderMailAttachmentFileEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class MailController extends AppController
{
    /**
     * @Route("/orders/get-email/{id}", name="user-orders-post-mail")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_mail(Request $request, EntityManagerInterface $em)
    {
        try {
            $mail_id = intval($request->get('id'));
            $mail_entity = $em->getRepository(OrderMailEntity::class)
                ->findOneBy(['id' => $mail_id]);
            $mail_attachment_entity =  $em->getRepository(OrderMailAttachmentFileEntity::class)
                ->findBy(['order_mail' => $mail_entity]);
            $order = $mail_entity->getOrder();
            if (empty($mail_entity)) {
                throw new OrderMailNotFoundException(_('Order mail not found'));
            }
            return $this->render(
                ':user/orders/email:email_view.html.twig',
                [
                    'layout' => 'tabs',
                    'mail' => $mail_entity,
                    'attachment_files' => $mail_attachment_entity,
                    'mail_file' => '/' . $this->get_company() . 'mail_attachments/' . $mail_entity->getAttachmentFileName(),
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'order_id' => $order->getId(),
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/get-email/{id}", name="user-orders-get-mail")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_mail(Request $request, EntityManagerInterface $em)
    {
        try {
            $mail_id = intval($request->get('id'));
            $mail_entity = $em->getRepository(OrderMailEntity::class)
                ->findOneBy(['id' => $mail_id]);
            $mail_attachment_entity =  $em->getRepository(OrderMailAttachmentFileEntity::class)
                ->findBy(['order_mail' => $mail_entity]);
            $order = $mail_entity->getOrder();
            if (empty($mail_entity)) {
                throw new OrderMailNotFoundException(_('Order mail not found'));
            }
            return $this->render(
                ':user/orders/email:email_view.html.twig',
                [
                    'layout' => 'tabs',
                    'mail' => $mail_entity,
                    'attachment_files' => $mail_attachment_entity,
                    'mail_file' => '/' . $this->get_company() . 'mail_attachments/' . $mail_entity->getAttachmentFileName(),
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'order_id' => $order->getId(),
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/get_attached_file/{id}", name="user-orders-post-attached-file")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_attached_file(Request $request, EntityManagerInterface $em)
    {
        try{
            $attached_id = intval($request->get('id'));
            $attached_file = $em->getRepository(OrderMailAttachmentFileEntity::class)
                ->findOneBy(['id' => $attached_id]);
            $file_location = '/' . $this->get_company() . 'mail_attachments/' . $attached_file->getFileName();
            $mail = $attached_file->getOrderMail();
            return $this->render(
                ':user/orders/pdf:view_uploaded_file.html.twig',
                [
                    'file' => $file_location,
                    'company' => $this->get_company(),
                    'locale' => $request->getLocale(),
                    'ActionId' => $mail->getId(),
                    'Action' => 'AttachedFiles',
                    'user' => $this->getUser(),
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        }catch (\Exception $e){
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/get_attached_file/{id}", name="user-orders-get-attached-file")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_attached_file(Request $request, EntityManagerInterface $em)
    {
        try{
            $attached_id = intval($request->get('id'));
            $attached_file = $em->getRepository(OrderMailAttachmentFileEntity::class)
                ->findOneBy(['id' => $attached_id]);
            $file_location = '/' . $this->get_company() . 'mail_attachments/' . $attached_file->getFileName();
            $mail = $attached_file->getOrderMail();
            return $this->render(
                ':user/orders/pdf:view_uploaded_file.html.twig',
                [
                    'file' => $file_location,
                    'company' => $this->get_company(),
                    'locale' => $request->getLocale(),
                    'ActionId' => $mail->getId(),
                    'Action' => 'AttachedFiles',
                    'user' => $this->getUser(),
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        }catch (\Exception $e){
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}