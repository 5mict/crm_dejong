<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/8/2017
 * Time: 4:42 PM
 */

namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderPaymentMethodEntityNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\Login\UserEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Orders\AdministrativeNoteEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\InternNoteEntity;
use AppBundle\Entity\User\Orders\InvoiceNumbers;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Orders\OrderFileEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use AppBundle\Repository\User\Orders\OrderMailRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User\Debtors\NoteEntity;

class OrdersController extends AppController
{
    /**
     * OrdersController constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        if(!$this->is_granted(GrantEntity::ORDER_GRANT, $em)) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("/orders", name="user-orders")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_orders(Request $request, EntityManagerInterface $em)
    {
        $order_statuses = $em->getRepository(OrderEntity::class)
            ->get_active_order_statuses();
        $orders = $em->getRepository(OrderEntity::class)
            ->get_activeOrdersData();
        return $this->render(':user/orders:index.html.twig', [
            'orders' => $orders,
            'company' => $this->get_company(),
            'order_statuses' => $order_statuses,
            'locale' => $request->getLocale(),
            'user' => $this->getUser(),
            'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
            'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
        ]);
    }

    /**
     * @Route("/finished-orders", name="user-finished-orders")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_finished_orders(Request $request, EntityManagerInterface $em)
    {
        $order_statuses = $em->getRepository(OrderEntity::class)
            ->get_finished_order_statuses();
        $orders = $em->getRepository(OrderEntity::class)
            ->get_finishedOrdersData();
        return $this->render(':user/orders:index.html.twig', [
            'orders' => $orders,
            'company' => $this->get_company(),
            'order_statuses' => $order_statuses,
            'locale' => $request->getLocale(),
            'user' => $this->getUser(),
            'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
            'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
        ]);
    }

    /**
     * @Route("/orders/add-order", name="user-orders-add-order")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_order(Request $request, EntityManagerInterface $em)
    {
        $debtor_names = $em->getRepository(DebtorEntity::class)
            ->get_debtor_names();
        $order_statuses = $em->getRepository(OrderStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $users = $em->getRepository(UserEntity::class)
            ->get_all_users_username();
        $order_delivery_statuses = $em->getRepository(OrderDeliveryStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_payment_methods = $em->getRepository(OrderPaymentMethodEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_payment_statuses = $em->getRepository(OrderPaymentStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_articles = [];
        return $this->render(':user/orders:base.html.twig',//add_order
            [
                'debtor_names' => $debtor_names,
                'order_statuses' => $order_statuses,
                'order_delivery_statuses' => $order_delivery_statuses,
                'order_payment_methods' => $order_payment_methods,
                'order_payment_statuses' => $order_payment_statuses,
                'order_files' => '',
                'order_administrative_notes' => '',
                'order_intern_notes' => '',
                'sent_mails' => '',
                'company' => $this->get_company(),
                'user' => $this->getUser(),
                'users' => $users,
                'order_articles' => $order_articles,
                'Action' => 'add',
                'layout' => 'tabs',
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]
        );
    }
    /**
     * @Route("/orders/{id}/update-order", name="user-post-orders-update-order")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @throws \Exception
     */
    public function post_update_order(Request $request, EntityManagerInterface $em)
    {
        $order_id = intval($request->get('id'));
        $order_entity = $em->getRepository(OrderEntity::class)
            ->findOneBy(['id' => $order_id]);
        if(empty($order_entity)) {
            throw new OrderNotFoundException(_('Order not found'));
        }
        $debtor_names = $em->getRepository(DebtorEntity::class)
            ->get_debtor_names();
        $debtor = $order_entity->getDebtor();
        $users = $em->getRepository(UserEntity::class)
            ->get_all_users_username();
        $order_statuses = $em->getRepository(OrderStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_delivery_statuses = $em->getRepository(OrderDeliveryStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_payment_methods = $em->getRepository(OrderPaymentMethodEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_payment_statuses = $em->getRepository(OrderPaymentStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_articles = $em->getRepository(OrderArticleEntity::class)
            ->findBy(['order' => $order_id]);
        $order_files = $em->getRepository(OrderFileEntity::class)
            ->findBy(['order' => $order_entity]);
        $supplier_names = $em->getRepository(SupplierEntity::class)
            ->get_supplier_names();
        $order_suppliers = $em->getRepository(OrderArticleEntity::class)
            ->get_order_suppliers($order_id);
        $sent_mails = $em->getRepository(OrderMailEntity::class)
            ->findBy(['order' => $order_id]);
        list($administrative_notes, $administrative_notes_page_number, $administrative_notes_page_size, $administrative_notes_total_number_of_pages, $administrative_notes_sort_by, $administrative_notes_sort_direction)
            = $em->getRepository(NoteEntity::class)
                ->get_notes($debtor->getId());
        list($intern_notes, $intern_notes_page_number, $intern_notes_page_size, $intern_notes_total_number_of_pages, $intern_notes_sort_by, $intern_notes_sort_direction)
            = $em->getRepository(InternNoteEntity::class)
                ->get_notes($order_id);
        return $this->render(':user/orders:base.html.twig',
            [
                'supplier_names' => $supplier_names,
                'order' => $order_entity,
                'debtor_names' => $debtor_names,
                'order_articles' => $order_articles,
                'order_files' => $order_files,
                'order_statuses' => $order_statuses,
                'order_delivery_statuses' => $order_delivery_statuses,
                'order_payment_methods' => $order_payment_methods,
                'order_payment_statuses' => $order_payment_statuses,
                'order_suppliers' => $order_suppliers,
                'users' => $users,
                'company' => $this->get_company(),
                'user' => $this->getUser(),
                'Action' => 'update',
                'layout' => 'tabs',
                'locale' => $request->getLocale(),
                'order_intern_notes' => $intern_notes,
                'order_intern_notes_page_number' => $intern_notes_page_number,
                'order_intern_notes_page_size' => $intern_notes_page_size,
                'order_intern_notes_total_number_of_pages' => $intern_notes_total_number_of_pages,
                'order_administrative_notes' => $administrative_notes,
                'order_administrative_notes_page_number' => $administrative_notes_page_number,
                'order_administrative_notes_page_size' => $administrative_notes_page_size,
                'order_administrative_notes_total_number_of_pages' => $administrative_notes_total_number_of_pages,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                'sent_mails' => $sent_mails
            ]
        );
    }
    /**
     * @Route("/orders/{id}/update-order", name="user-get-orders-update-order")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     * @throws \Exception
     */
    public function get_update_order(Request $request, EntityManagerInterface $em)
    {
        $order_id = intval($request->get('id'));
        $order_entity = $em->getRepository(OrderEntity::class)
            ->findOneBy(['id' => $order_id]);
        if(empty($order_entity)) {
            throw new OrderNotFoundException(_('Order not found'));
        }
        $debtor_names = $em->getRepository(DebtorEntity::class)
            ->get_debtor_names();
        $debtor = $order_entity->getDebtor();
        $users = $em->getRepository(UserEntity::class)
            ->get_all_users_username();
        $order_statuses = $em->getRepository(OrderStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_delivery_statuses = $em->getRepository(OrderDeliveryStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_payment_methods = $em->getRepository(OrderPaymentMethodEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_payment_statuses = $em->getRepository(OrderPaymentStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $order_articles = $em->getRepository(OrderArticleEntity::class)
            ->findBy(['order' => $order_id]);
        $order_files = $em->getRepository(OrderFileEntity::class)
            ->findBy(['order' => $order_entity]);
        $supplier_names = $em->getRepository(SupplierEntity::class)
            ->get_supplier_names();
        $order_suppliers = $em->getRepository(OrderArticleEntity::class)
            ->get_order_suppliers($order_id);
        $sent_mails = $em->getRepository(OrderMailEntity::class)
            ->findBy(['order' => $order_id]);
        list($administrative_notes, $administrative_notes_page_number, $administrative_notes_page_size, $administrative_notes_total_number_of_pages, $administrative_notes_sort_by, $administrative_notes_sort_direction)
            = $em->getRepository(NoteEntity::class)
            ->get_notes($debtor->getId());
        list($intern_notes, $intern_notes_page_number, $intern_notes_page_size, $intern_notes_total_number_of_pages, $intern_notes_sort_by, $intern_notes_sort_direction)
            = $em->getRepository(InternNoteEntity::class)
            ->get_notes($order_id);
        return $this->render(':user/orders:base.html.twig',
            [
                'supplier_names' => $supplier_names,
                'order' => $order_entity,
                'debtor_names' => $debtor_names,
                'order_articles' => $order_articles,
                'order_files' => $order_files,
                'order_statuses' => $order_statuses,
                'order_delivery_statuses' => $order_delivery_statuses,
                'order_payment_methods' => $order_payment_methods,
                'order_payment_statuses' => $order_payment_statuses,
                'order_suppliers' => $order_suppliers,
                'users' => $users,
                'company' => $this->get_company(),
                'user' => $this->getUser(),
                'Action' => 'update',
                'layout' => 'tabs',
                'locale' => $request->getLocale(),
                'order_intern_notes' => $intern_notes,
                'order_intern_notes_page_number' => $intern_notes_page_number,
                'order_intern_notes_page_size' => $intern_notes_page_size,
                'order_intern_notes_total_number_of_pages' => $intern_notes_total_number_of_pages,
                'order_administrative_notes' => $administrative_notes,
                'order_administrative_notes_page_number' => $administrative_notes_page_number,
                'order_administrative_notes_page_size' => $administrative_notes_page_size,
                'order_administrative_notes_total_number_of_pages' => $administrative_notes_total_number_of_pages,
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                'sent_mails' => $sent_mails
            ]
        );
    }
    /**
     * @Route("/orders/{id}/pdf-view-order", name="user-orders-post-pdf-view")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_pdf_view_order(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('id');
            $mail_type = $request->get('mail_type');
            $path = $this->get_company() . 'images/pdf_logo.jpg';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $image_data = file_get_contents($path);
            $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if(empty($order)) {
                throw new OrderNotFoundException(_('Order not found.'));
            }
            $debtor = $order->getDebtor();
            $mail_status = $em->getRepository(OrderMailStatusEntity::class)
                ->findOneBy(['name' => $mail_type]);
            $order_mail_details = '';

            if(!empty($mail_status)){
                $order_mail_details = $em->getRepository(OrderMailEntity::class)
                    ->get_last_email($order_id, $mail_status->getId());
            }
            $end_user_type = $debtor->getEndUserType()->getName();
            if($mail_type == OrderMailStatusEntity::INVOICE_SENT) {
                $invoice_number = $order->getInvoiceNumber();
                if (empty($invoice_number)){
                    $invoice_number = $em->getRepository(InvoiceNumbers::class)
                        ->get_invoice_number($end_user_type);

                    $order->setOrderInvoiceNumber($invoice_number[1]);
                    $order->setInvoiceNumber($invoice_number[0]);
                    $em->persist($order);
                    $em->flush();
                    $pdf_invoice_number = $invoice_number[0];
                } else {
                    $pdf_invoice_number = $invoice_number;
                }
            } else {
                $pdf_invoice_number = '';
            }
            return $this->render(
                ':user/orders/pdf:view.html.twig',
                [
                    'invoice_number' => $order->getOrderInvoiceNumber(),
                    'order' => $order,
                    'order_mail_details' => $order_mail_details,
                    'layout' => 'tabs',
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'logo' => $logo_base64,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                    'mail_type' => $mail_type
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/{id}/pdf-view-order", name="user-orders-get-pdf-view")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_view_order(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_id = $request->get('id');
            $mail_type = $request->get('mail_type');
            $path = $this->get_company() . 'images/pdf_logo.jpg';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $image_data = file_get_contents($path);
            $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if(empty($order)) {
                throw new OrderNotFoundException(_('Order not found.'));
            }
            $debtor = $order->getDebtor();
            $mail_status = $em->getRepository(OrderMailStatusEntity::class)
                ->findOneBy(['name' => $mail_type]);
            $order_mail_details = '';

            if(!empty($mail_status)){
                $order_mail_details = $em->getRepository(OrderMailEntity::class)
                    ->get_last_email($order_id, $mail_status->getId());
            }
            $end_user_type = $debtor->getEndUserType()->getName();
            if($mail_type == OrderMailStatusEntity::INVOICE_SENT) {
                $invoice_number = $order->getInvoiceNumber();
                if (empty($invoice_number)){
                    $invoice_number = $em->getRepository(InvoiceNumbers::class)
                        ->get_invoice_number($end_user_type);

                    $order->setOrderInvoiceNumber($invoice_number[1]);
                    $order->setInvoiceNumber($invoice_number[0]);
                    $em->persist($order);
                    $em->flush();
                    $pdf_invoice_number = $invoice_number[0];
                } else {
                    $pdf_invoice_number = $invoice_number;
                }
            } else {
                $pdf_invoice_number = '';
            }
            return $this->render(
                ':user/orders/pdf:view.html.twig',
                [
                    'invoice_number' => $order->getOrderInvoiceNumber(),
                    'order' => $order,
                    'order_mail_details' => $order_mail_details,
                    'layout' => 'tabs',
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'logo' => $logo_base64,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                    'mail_type' => $mail_type
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/get-invoices", name="user-orders-get-invoices")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_invoices(Request $request, EntityManagerInterface $em)
    {
            $order_payment_statuses = $em->getRepository(OrderPaymentStatusEntity::class)
                ->get_all_with_translation($request->getLocale());
            $orders = $em->getRepository(OrderEntity::class)
                   ->get_invoicesData();
            return $this->render(
                ':user/invoices:index.html.twig',
                [
                    'orders' => $orders,
                    'layout' => 'tabs',
                    'locale' => $request->getLocale(),
                    'company' => $this->get_company(),
                    'user' => $this->getUser(),
                    'order_payment_statuses' => $order_payment_statuses,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
    }

    /**
     * @Route("orders/finished-invoices", name="user-finished-invoices")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_finished_invoices(Request $request, EntityManagerInterface $em)
    {
        $order_payment_statuses = $em->getRepository(OrderPaymentStatusEntity::class)
            ->get_all_with_translation($request->getLocale());
        $orders = $em->getRepository(OrderEntity::class)
            ->get_invoiceFinishedData();
        return $this->render(':user/invoices:index.html.twig', [
            'orders' => $orders,
            'company' => $this->get_company(),
            'locale' => $request->getLocale(),
            'user' => $this->getUser(),
            'order_payment_statuses' => $order_payment_statuses,
            'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
            'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
        ]);
    }

    /**
     * @Route("orders/{id}/pdf-information", name="post-pdf-information")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_pdf_information(Request $request, EntityManagerInterface $em)
    {
        $order = $em->getRepository(OrderEntity::class)
            ->find(['id'=> $request->get('id')]);
        $delivery_address = $order->getDeliveryAddress();
        $contact_person = $order->getContactPerson();
        $debtor = $order->getDebtor();
        return $this->render(':user/orders/pdf:informations.html.twig', [
            'debtor'  => $debtor,
            'delivery_address' => $delivery_address,
            'contact_person' => $contact_person,
            'company' => $this->get_company(),
            'user' => $this->getUser(),
            'order_id' => $order->getId(),
            'order' => $order,
            'locale' => $request->getLocale(),
            'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
            'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
        ]);
    }

    /**
     * @Route("orders/{id}/pdf-information", name="get-pdf-information")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_information(Request $request, EntityManagerInterface $em)
    {
        $order = $em->getRepository(OrderEntity::class)
            ->find(['id'=> $request->get('id')]);
        $delivery_address = $order->getDeliveryAddress();
        $contact_person = $order->getContactPerson();
        $debtor = $order->getDebtor();
        return $this->render(':user/orders/pdf:informations.html.twig', [
            'debtor'  => $debtor,
            'delivery_address' => $delivery_address,
            'contact_person' => $contact_person,
            'company' => $this->get_company(),
            'user' => $this->getUser(),
            'order_id' => $order->getId(),
            'order' => $order,
            'locale' => $request->getLocale(),
            'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
            'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
        ]);
    }
}