<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/13/2017
 * Time: 7:57 AM
 */

namespace AppBundle\Controller\User\Orders;

use AppBundle\AppController;
use AppBundle\Controller\User\Articles\Exceptions\ArticleNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderArticleEntityNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderNotFoundException;
use AppBundle\Controller\User\Orders\Exceptions\OrderToSupplierStatusNotFoundException;
use AppBundle\Controller\User\Suppliers\Exceptions\SupplierNotFoundException;
use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity;
use AppBundle\Entity\User\Orders\InvoiceNumbers;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Orders\OrderMailAttachmentFileEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use AppBundle\Repository\User\Orders\OrderRepository;
use AppBundle\ViewModels\User\Orders\OrderArticlesViewArticles;
use AppBundle\ViewModels\User\Orders\OrderArticlesViewSuppliers;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class OrderArticlesJSONController extends AppController
{
    public function __construct(EntityManagerInterface $em)
    {
        if (!$this->is_granted(GrantEntity::ORDER_GRANT, $em)) {
            throw $this->createNotFoundException();
        }
    }

    /**
     * @Route("orders/get-article")
     * @Method({"get"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_order_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $locale = $request->getLocale();
            $order_article_id = $request->get('id');
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            $article = $order_article->getArticle();
            $article_name = $locale == 'de' ? $article->getArticleNameGerman() : $locale == 'nl' ? $article->getArticleNameDutch() : $article->getArticleName();
            $response = new Response();
            $response = $response->setStatusCode(200);
            $response = $response->setContent(json_encode([
                'article_number' => $order_article->getArticleNumber(),
                'description' => $order_article->getArticleDescription(),
                'size' => $order_article->getArticleSize(),
                'color' => $order_article->getColor(),
                'material' => $order_article->getMaterial(),
                'quantity' => $order_article->getQuantity(),
                'in_stock' => $order_article->getInStock(),
                'order_from_supplier' => $order_article->getOrderFromSupplier(),
                'supplier_name' => $order_article->getSupplierName(),
                'details' => $order_article->getDetails(),
                'delivered' => $order_article->getDelivered(),
                'picture_url' => $order_article->getPictureUrl(),
                'article_name' => $article_name,
                'show' => $order_article->getShowSize(),
            ]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/add-order-article", name="user-orders-add-order-article.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_order_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $article_number = $request->get('article_number');
            $article = $em->getRepository(ArticleEntity::class)
                ->findOneBy(['article_number' => $article_number]);
            if(empty($article)) {
                throw new ArticleNotFoundException(_('Article not found'));
            }
            $supplier_name = $request->get('supplier_name');
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['name' => $supplier_name]);
            if(empty($supplier)) {
                throw new SupplierNotFoundException(_('Supplier not found'));
            }
            $order_id = $request->get('order_id');
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_id]);
            if(empty($order)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $to_supplier_status = $em->getRepository(OrderToSupplierStatusEntity::class)
                ->findOneBy(['name' => OrderToSupplierStatusEntity::NEW]);
            if(empty($to_supplier_status)) {
                throw new OrderToSupplierStatusNotFoundException(_('To supplier status not found'));
            }
            $order_article_entity = new OrderArticleEntity();
            $order_article_entity->setArticle($article);
            $order_article_entity->setSupplier($supplier);
            $order_article_entity->setOrder($order);
            $order_article_entity->setMaterial($request->get('material'));
            $order_article_entity->setColor($request->get('color'));
            $order_article_entity->setDetails($request->get('details'));
            $order_article_entity->setQuantity(intval($request->get('quantity')));
            $order_article_entity->setInStock(intval($request->get('in_stock')));
            $order_article_entity->setOrderFromSupplier(intval($request->get('order_from_supplier')));
            $order_article_entity->setCreated(new \DateTime('now'));
            $order_article_entity->setUpdated(new \DateTime('now'));
            $order_article_entity->setUsername($this->getUser()->getUsername());
            $order_article_entity->setDelivered($request->get('delivered'));
            $order_article_entity->setDeliveryTime(new \DateTime('1900-01-01'));
            $order_article_entity->setShowSize(intval($request->get('show')));
            $order_article_entity->setOrderArticlePurchasePrice($article->getPurchasePrice());
            $order_article_entity->setCustomOrderArticleNumber($em->getRepository(OrderArticleEntity::class)
                ->generateCustomOrderArticleNumber($order_id));
            $debtor = $order->getDebtor();
            $end_user_type = $debtor->getEndUserType();
            if($end_user_type->getName() == DebtorEndUserTypeEntity::COMPANY) {
                $order_article_entity->setOrderArticlePrice($article->getSellingPrice());
            } else  {
                $order_article_entity->setOrderArticlePrice($article->getSellingPriceWithPdv());
            }
            $order_article_entity->setCommission('');
            $order_article_entity->setToSupplierStatus($to_supplier_status);
            $em->persist($order_article_entity);
            $em->flush();
            $em->getRepository(OrderEntity::class)
                ->update_total_amounts($order);
            $em->flush();
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['id' => $order_article_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/orders/delete-order-article", name="user-orders-delete-order-article.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_order_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = $request->get('id');
            $order_article_entity = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            $order = $em->getRepository(OrderEntity::class)
                ->findOneBy(['id' => $order_article_entity->getOrderId()]);

            if (empty($order_article_entity)) {
                throw new OrderArticleEntityNotFoundException(_('Order article not found'));
            }
            $order_entity = $order_article_entity->getOrder();
            $order_mail_entity = $em->getRepository(OrderMailEntity::class)
                ->findOneBy(['order_article' => $order_article_entity, 'order'=> $order_entity]);
            $order_mail_attachment_files = $em->getRepository(OrderMailAttachmentFileEntity::class)
                ->findOneBy(['order_mail' => $order_mail_entity]);
            if(empty($order_entity)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            if(!empty($order_mail_attachment_files)) {
                $em->remove($order_mail_attachment_files);
            }
            if(!empty($order_mail_entity)) {
                $em->remove($order_mail_entity);
            }
            $em->remove($order_article_entity);
            $em->flush();
            $em->getRepository(OrderEntity::class)
                ->update_total_amounts($order_entity);

            $orderArticles = $order->getOrderArticles();
            $i = 0;
            foreach ($orderArticles as $orderArticle) {
                $orderArticle->setCustomOrderArticleNumber($i++);
                $em->persist($orderArticle);
            }
            $em->flush();

            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['id' => $order_article_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/update-order-article", name="user-orders-update-order-article.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_order_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = $request->get('id');
            $order_article_entity = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            if(empty($order_article_entity)) {
                throw new OrderArticleEntityNotFoundException(_('Order article not found'));
            }
            $order_entity = $order_article_entity->getOrder();
            if(empty($order_entity)) {
                throw new OrderNotFoundException(_('Order not found'));
            }
            $supplier_name = $request->get('supplier_name');
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['name' => $supplier_name]);
            if(empty($supplier)) {
                throw new SupplierNotFoundException(_('Supplier not found'));
            }
            $article_number = $request->get('article_number');
            $article = $em->getRepository(ArticleEntity::class)
                ->findOneBy(['article_number' => $article_number]);
            $order_article_entity->setArticle($article);
            $order_article_entity->setSupplier($supplier);
            $order_article_entity->setMaterial($request->get('material'));
            $order_article_entity->setColor($request->get('color'));
            $order_article_entity->setDetails($request->get('details'));
            $order_article_entity->setQuantity(intval($request->get('quantity')));
            $order_article_entity->setInStock(intval($request->get('in_stock')));
            $order_article_entity->setShowSize(intval($request->get('show')));
            $order_article_entity->setOrderFromSupplier(intval($request->get('order_from_supplier')));
            $order_article_entity->setUpdated(new \DateTime('now'));
            $order_article_entity->setUsername($this->getUser()->getUsername());
            $order_article_entity->setDelivered($request->get('delivered'));
            $order_article_entity->setDeliveryTime(new \DateTime($request->get('delivered')));
            switch( $request->getLocale())
            {
                case LocaleEntity::ENGLISH:
                    $order_article_entity->setDescription($article->getArticleName());
                    break;
                case LocaleEntity::DUTCH:
                    $order_article_entity->setDescription($article->getArticleNameDutch());
                    break;
                case LocaleEntity::GERMAN:
                    $order_article_entity->setDescription($article->getArticleNameGerman());
                    break;
                default :
                    $order_article_entity->setDescription($article->getArticleName());
                    break;
            }
            $em->flush();
            $em->getRepository(OrderEntity::class)
                ->update_total_amounts($order_entity);
            $em->flush();
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['id' => $order_article_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/order-article-supplier-status", name="user-orders-order-article-get-to-supplier-status.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_to_supplier_status(Request $request, EntityManagerInterface $em)
    {
        try {
            $to_supplier_statuses = $em->getRepository(OrderToSupplierStatusEntity::class)
                ->get_all_with_translation($request->getLocale());
            $response = new Response();
            $response->setContent(json_encode($to_supplier_statuses));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/order-article-description", name="user-orders-order-article-get-description.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_order_article_description(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $request->get('order_article_id')]);
            $response = new Response();
            if( !empty( $order_article->getDescription() ) ) {
                $response->setContent(json_encode($order_article->getDescription()));
            }
            else {
                switch( $request->getLocale() ) {
                    case LocaleEntity::DUTCH:
                        $response->setContent(json_encode($order_article->getArticle()->getArticleNameDutch() ) );
                        break;
                    case LocaleEntity::GERMAN:
                        $response->setContent(json_encode($order_article->getArticle()->getArticleNameGerman() ) );
                        break;
                    case LocaleEntity::ENGLISH:
                        $response->setContent(json_encode($order_article->getArticle()->getArticleName() ) );
                        break;
                    default:
                        $response->setContent(json_encode($order_article->getArticle()->getArticleName() ) );
                }
            }
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/order-article-description", name="user-orders-order-article-set-description.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function set_order_article_description(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $request->get('order_article_id')]);
            $order_article->setDescription( $request->get('order_article_description'));
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode($request->get('order_article_id')));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/order-article-supplier-status", name="user-orders-order-article-update-to-supplier-status.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_to_supplier_status(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $request->get('id')]);
            $supplier = $order_article->getSupplier();
            if(empty($order_article)) {
                throw new OrderArticleEntityNotFoundException(_('Order article not found'));
            }
            $to_supplier_status = $em->getRepository(OrderToSupplierStatusEntity::class)
                ->findOneBy(['name' => $request->get('to_supplier_status')]);
            if(empty($to_supplier_status)) {
                throw new OrderToSupplierStatusNotFoundException(_('To supplier status not found'));
            }
            $order_article->setToSupplierStatus($to_supplier_status);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $supplier->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/up-order-article", name="user-orders-up-order-article.json")
     * @Method({"PATCH"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function up_order_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = $request->get('id');
            $order_article_entity_up = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            if (empty($order_article_entity_up)) {
                throw new OrderArticleEntityNotFoundException(_('Order article not found'));
            }

            $order_article_entity_up_number = $order_article_entity_up->getCustomOrderArticleNumber();
            $order_article_entity_down_number = $order_article_entity_up->getCustomOrderArticleNumber() - 1;

            if ($order_article_entity_up_number !== 0) {
                $order_article_entity_down = $em->getRepository(OrderArticleEntity::class)
                    ->findOneBy([
                        'order' => $order_article_entity_up->getOrderId(),
                        'custom_order_article_number' => $order_article_entity_down_number
                    ]);
                if (empty($order_article_entity_down)) {
                    throw new OrderArticleEntityNotFoundException(_('Order article not found'));
                }

                $order_article_entity_up->setCustomOrderArticleNumber($order_article_entity_down_number);
                $em->persist($order_article_entity_up);
                $order_article_entity_down->setCustomOrderArticleNumber($order_article_entity_up_number);
                $em->persist($order_article_entity_down);

                $em->flush();
            }
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['id' => $order_article_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/down-order-article", name="user-orders-down-order-article.json")
     * @Method({"PATCH"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function down_order_article(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = $request->get('id');
            $order_article_entity_down = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            if (empty($order_article_entity_down)) {
                throw new OrderArticleEntityNotFoundException(_('Order article not found'));
            }

            $order_article_entity_down_number = $order_article_entity_down->getCustomOrderArticleNumber();
            $order_article_entity_up_number = $order_article_entity_down->getCustomOrderArticleNumber() + 1;

            if ($order_article_entity_down_number < $em->getRepository(OrderArticleEntity::class)
                    ->generateCustomOrderArticleNumber($order_article_entity_down->getOrderId()) - 1) {
                $order_article_entity_up = $em->getRepository(OrderArticleEntity::class)
                    ->findOneBy([
                        'order' => $order_article_entity_down->getOrderId(),
                        'custom_order_article_number' => $order_article_entity_up_number
                    ]);
                if (empty($order_article_entity_up)) {
                    throw new OrderArticleEntityNotFoundException(_('Order article not found'));
                }

                $order_article_entity_down->setCustomOrderArticleNumber($order_article_entity_up_number);
                $em->persist($order_article_entity_down);
                $order_article_entity_up->setCustomOrderArticleNumber($order_article_entity_down_number);
                $em->persist($order_article_entity_up);

                $em->flush();
            }
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['id' => $order_article_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/orders/order_article_update_database", name="user-orders-order-article.json")
     * @Method({"GET"})
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_order_article_update_database(EntityManagerInterface $em)
    {
        $orders = $em->getRepository(OrderEntity::class)
            ->findAll();
        $response = new Response();

        foreach ($orders as $order) {
            $orderArticles = $order->getOrderArticles();
            $i = 0;
            foreach ($orderArticles as $orderArticle) {
                $orderArticle->setCustomOrderArticleNumber($i++);
                $em->persist($orderArticle);
            }
        }

        $em->flush();
        return $response;
    }
	
	/**
	 * @Route("/orders/order_invoice_numbers_update_database")
	 * @Method({"GET"})
	 * @param EntityManagerInterface $em
	 * @return Response
	 */
	public function get_order_invoice_numbers_update_database(EntityManagerInterface $em)
	{
		$invoiceNumbers = $em->getRepository(InvoiceNumbers::class)
			->find(1);
		$response = new Response();
		
		$invoiceNumbers->setOrderNumberingYear(2021);
		$invoiceNumbers->setCompanyInvoiceNumber(21000);
		$invoiceNumbers->setPrivateInvoiceNumber(21000);
		
		$em->persist($invoiceNumbers);
		
		$em->flush();
		return $response;
	}
}
