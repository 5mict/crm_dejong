<?php

namespace AppBundle\Controller\User;

use AppBundle\AppController;
use AppBundle\Entity\Login\UserEntity;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends AppController
{
    /**
     * @Route("/user", name="userpage")
     * @Method("GET");
     * @param Request $request
     * @param UserInterface $user
     * @return Response
     */
    public function userAction(Request $request, UserInterface $user)
    {
        return $this->render(':user:index.html.twig', [
            'user' => $this->getUser(),
            'company' => $this->get_company(),
            'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
            'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
        ]);
    }
    /**
     * @Route("/user", name="userpage.json", schemes={"https"})
     * @Method("PUT")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_user(
        Request $request,
        EntityManagerInterface $em
    ) {
        try {
            $user = $em->getRepository(UserEntity::class)
                ->findOneBy(['username' => $request->get('username')]);
            $user->setFirstName($request->get('first_name'));
            $user->setLastName($request->get('last_name'));
            $user->setPhonenumber($request->get('phonenumber'));
            $user->setEmail($request->get('email'));
            $user->setUpdated(new \DateTime('now'));
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['updated' => $user->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}