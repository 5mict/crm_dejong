<?php

namespace AppBundle\Controller\User\Planning;

use AppBundle\AppController;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PlanningController extends AppController
{
    public function __construct(EntityManagerInterface $em)
    {
    }
    /**
     * @Route("/orders-planning", name="user-planning-orders")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_orders(Request $request, EntityManagerInterface $em)
    {
        try {
            return $this->render(':user/planning:index.html.twig', [
                'company' => $this->get_company(),
                'user' => $this->getUser(),
                'locale' => $request->getLocale(),
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]);
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
}