<?php
/**
 * Created by PhpStorm.
 * User: Strale
 * Date: 22.8.2018.
 * Time: 12.26
 */

namespace AppBundle\Controller\User\Planning;


use AppBundle\AppController;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Orders\OrderEntity;
use AppBundle\Entity\User\Planning\PlanningEntity;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PlanningControllerJSON extends AppController
{
    /**
     * @Route("/orders-planning-dates", name="user-planning-orders-dates.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_orders_dates(Request $request, EntityManagerInterface $em)
    {
        try {
            $start = new \DateTime($request->get('start'));
            $end = new \DateTime($request->get('end'));
            $orders = $em->getRepository(OrderEntity::class)
                ->get_order_events($start, $end, $request->getLocale());
            $suppliers = $em->getRepository(OrderArticleEntity::class)
                ->get_supplier_events($start, $end, $request->getLocale());
            $plannings = $em->getRepository(PlanningEntity::class)
                ->get_title_dates($start, $end);
            $response = new Response();
            $response->setContent(json_encode(array_merge($orders, $plannings, $suppliers)));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/planning/get-title", name="user-planning-get-title.json")
     * @param Request $request
     * @Method({"POST"})
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_title(Request $request, EntityManagerInterface $em)
    {
        try{
            $planning_id = $request->get('id');
            $planning_entity = $em->getRepository(PlanningEntity::class)
                ->get_title($planning_id);
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode($planning_entity));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/planning/add-title", name="user-planning-add-title-dates.json")
     * @param Request $request
     * @Method({"POST"})
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_title(Request $request, EntityManagerInterface $em)
    {
        try{
            $date = $request->get('date');
            $title = $request->get('title');
            $planning_entity = new PlanningEntity();
            $planning_entity->setDate(new \DateTime($date));
            $planning_entity->setTitle($title);
            $em->persist($planning_entity);
            $em->flush();
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['id' => $planning_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/planning/update-title", name="user-planning-update-title.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_title(Request $request, EntityManagerInterface $em)
    {
        try{
            $date = $request->get('date');
            $title = $request->get('title');
            $planning_id = $request->get('id');
            $planning_entity = $em->getRepository(PlanningEntity::class)
                ->find(['id' => $planning_id]);
            $planning_entity->setDate(new \DateTime($date));
            $planning_entity->setTitle($title);
            $em->persist($planning_entity);
            $em->flush();
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['id' => $planning_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/planning/delete-title", name="user-planning-delete-title.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_title(Request $request, EntityManagerInterface $em)
    {
        try{
            $planning_id = $request->get('id');
            $planning_entity = $em->getRepository(PlanningEntity::class)
                ->find(['id' => $planning_id]);
            $em->remove($planning_entity);
            $em->flush();
            $response = new Response();
            $response->setStatusCode(200);
            $response->setContent(json_encode(['title deleted']));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}