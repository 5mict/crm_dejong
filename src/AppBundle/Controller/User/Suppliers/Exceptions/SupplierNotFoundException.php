<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/3/2017
 * Time: 12:24 PM
 */

namespace AppBundle\Controller\User\Suppliers\Exceptions;

class SupplierNotFoundException extends \Exception
{

}