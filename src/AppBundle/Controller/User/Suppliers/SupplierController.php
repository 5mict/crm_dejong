<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/16/2017
 * Time: 10:08 AM
 */

namespace AppBundle\Controller\User\Suppliers;

use AppBundle\AppController;
use AppBundle\Controller\User\Suppliers\Exceptions\SupplierNotFoundException;
use AppBundle\Entity\Grant\GrantEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use AppBundle\Entity\User\Suppliers\NoteEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use AppBundle\Entity\User\Suppliers\SupplierFileEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\OrderMailEntity;


class SupplierController extends AppController
{
    public function __construct(EntityManagerInterface $em)
    {
        if(!$this->is_granted(GrantEntity::SUPPLIER_GRANT, $em)) {
            throw $this->createNotFoundException();
        }
    }
    /**
     * @Route("/suppliers", name="user-suppliers")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_suppliers(Request $request, EntityManagerInterface $em)
    {
        try {
            $suppliers = $em->getRepository(SupplierEntity::class)
                           ->get_suppliersData();
            return $this->render('user/suppliers/index.html.twig', [
                'suppliers_view' => $suppliers,
                'company' => $this->get_company(),
                'locale' => $request->getLocale(),
                'user' => $this->getUser(),
                'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]);
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/suppliers/add-supplier", name="user-suppliers-get-add")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_add_supplier(Request $request, EntityManagerInterface $em)
    {
        try{
            return $this->render(
                'user/suppliers/base.html.twig',
                [
                    'company' => $this->get_company(),
                    'Action' => 'add',
                    'layout' => 'tabs',
                    'user' => $this->getUser(),
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch (\Exception $e){
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/suppliers/{id}/update-supplier", name="user-suppliers-post-update")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_update_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = intval($request->get('id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $supplier_files = $em->getRepository(SupplierFileEntity::class)
                ->findBy(['supplier'=> $supplier]);
            if(empty($supplier)) {
                throw new SupplierNotFoundException(_('Supplier not found'));
            }
            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(NoteEntity::class)
                    ->get_notes($supplier_id);
            return $this->render(
                'user/suppliers/base.html.twig',
                [
                    'supplier' => $supplier,
                    'company' => $this->get_company(),
                    'layout' => 'tabs',
                    'Action' => 'update',
                    'supplier_files' => $supplier_files,
                    'user' => $this->getUser(),
                    'notes' => $notes,
                    'supplier_notes_page_number' => $page_number,
                    'supplier_notes_page_size' => $page_size,
                    'supplier_notes_total_number_of_pages' => $total_number_of_pages,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/suppliers/{id}/update-supplier", name="user-suppliers-get-update")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_update_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = intval($request->get('id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $supplier_files = $em->getRepository(SupplierFileEntity::class)
                ->findBy(['supplier'=> $supplier]);
            if(empty($supplier)) {
                throw new SupplierNotFoundException(_('Supplier not found'));
            }
            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(NoteEntity::class)
                ->get_notes($supplier_id);
            return $this->render(
                'user/suppliers/base.html.twig',
                [
                    'supplier' => $supplier,
                    'company' => $this->get_company(),
                    'layout' => 'tabs',
                    'Action' => 'update',
                    'supplier_files' => $supplier_files,
                    'user' => $this->getUser(),
                    'notes' => $notes,
                    'supplier_notes_page_number' => $page_number,
                    'supplier_notes_page_size' => $page_size,
                    'supplier_notes_total_number_of_pages' => $total_number_of_pages,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/suppliers/{id}/pdf-view-supplier", name="user-suppliers-post-pdf-view")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_pdf_view_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = intval($request->get('id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $path = $this->get_company() . 'images/pdf_logo.jpg';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $image_data = file_get_contents($path);
            $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
            if(empty($supplier)) {
                throw new \Exception('Supplier does not exist.');
            }
            return $this->render(
                'user/suppliers/pdf/view.html.twig',
                [
                    'supplier' => $supplier,
                    'layout' => 'tabs',
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'user' => $this->getUser(),
                    'logo' => $logo_base64,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/suppliers/{id}/pdf-view-supplier", name="user-suppliers-get-pdf-view")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_pdf_view_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = intval($request->get('id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $path = $this->get_company() . 'images/pdf_logo.jpg';
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $image_data = file_get_contents($path);
            $logo_base64 = 'data:image/' . $type . ';base64,' . base64_encode($image_data);
            if(empty($supplier)) {
                throw new \Exception('Supplier does not exist.');
            }
            return $this->render(
                'user/suppliers/pdf/view.html.twig',
                [
                    'supplier' => $supplier,
                    'layout' => 'tabs',
                    'company' => $this->get_company(),
                    'Action' => 'view',
                    'user' => $this->getUser(),
                    'logo' => $logo_base64,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/suppliers/{id}/order-supplier", name="user-suppliers-order-supplier")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function view_order_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = intval($request->get('parent_id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $order_articles    = $em->getRepository(OrderArticleEntity::class)
                    ->get_order_articles_for_supplierData($supplier_id);
            return $this->render(
                ':user/suppliers:order_supplier.html.twig',
                [
                    'company' => $this->get_company(),
                    'Action' => 'add',
                    'layout' => 'tabs',
                    'locale' => $request->getLocale(),
                    'type' => 'regular',
                    'user' => $this->getUser(),
                    'order_articles' => $order_articles,
                    'supplier_id' => $supplier_id,
                    'supplier' => $supplier,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch (\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/{id}/order-supplier", name="user-suppliers-get-order-supplier")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_view_order_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = intval($request->get('id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $order_articles    = $em->getRepository(OrderArticleEntity::class)
                    ->get_order_articles_for_supplierData($supplier_id);
            return $this->render(
                ':user/suppliers:order_supplier.html.twig',
                [
                    'company' => $this->get_company(),
                    'Action' => 'add',
                    'layout' => 'tabs',
                    'locale' => $request->getLocale(),
                    'type' => 'regular',
                    'user' => $this->getUser(),
                    'order_articles' => $order_articles,
                    'supplier_id' => $supplier_id,
                    'supplier' => $supplier,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]
            );
        } catch (\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/{id}/finished-suppliers", name="user-post-suppliers-finished")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_finished_suppliers(Request $request, EntityManagerInterface $em)
    {
        try{
            $supplier_id = intval($request->get('id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $order_articles = $em->getRepository(OrderArticleEntity::class)
                ->get_finished_suppliers($supplier_id);
            return $this->render(':user/suppliers:order_supplier.html.twig',
                [
                    'company' => $this->get_company(),
                    'Action' => 'add',
                    'layout' => 'tabs',
                    'type' => 'archive',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'order_articles' => $order_articles,
                    'supplier_id' => $supplier_id,
                    'supplier' => $supplier,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
            ]);
        } catch (\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/{id}/finished-suppliers", name="user-get-suppliers-finished")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_finished_suppliers(Request $request, EntityManagerInterface $em)
    {
        try{
            $supplier_id = intval($request->get('id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $order_articles = $em->getRepository(OrderArticleEntity::class)
                ->get_finished_suppliers($supplier_id);
            return $this->render(':user/suppliers:order_supplier.html.twig',
                [
                    'company' => $this->get_company(),
                    'Action' => 'add',
                    'layout' => 'tabs',
                    'type' => 'archive',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'order_articles' => $order_articles,
                    'supplier_id' => $supplier_id,
                    'supplier' => $supplier,
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time()
                ]);
        } catch (\Exception $e) {
            return $this->handle_html_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/suppliers/get_view_order_to_supplier_pdf", name="get-view-order-to-supplier-pdf")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_view_order_to_supplier_pdf(Request $request, EntityManagerInterface $em)
    {
        try {
            $order_article_id = intval($request->get('id'));
            $order_article = $em->getRepository(OrderArticleEntity::class)
                ->findOneBy(['id' => $order_article_id]);
            $mail_status = $em->getRepository(OrderMailStatusEntity::class)
                ->findOneBy(['name' => 'order-from-supplier']);
            $mail_entities = $em->getRepository(OrderMailEntity::class)
                ->findBy(['order_article' => $order_article, 'order_mail_status' => $mail_status], ['created' => 'DESC']);
            $mail_entity = null;
            if(sizeof($mail_entities)  > 0)
                $mail_entity = $mail_entities[0];
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'mail_attachments/' . $mail_entity->getAttachmentFileName();
            return $this->render(
                ':user/suppliers/pdf:view_order_to_supplier.html.twig',
                [
                    'company' => $this->get_company(),
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}
