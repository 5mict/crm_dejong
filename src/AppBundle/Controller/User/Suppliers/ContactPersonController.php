<?php
/**
 * Created by PhpStorm.
 * User: Strale
 * Date: 17.11.2017.
 * Time: 10.06
 */

namespace AppBundle\Controller\User\Suppliers;


use AppBundle\AppController;
use AppBundle\Entity\User\Suppliers\ContactPersonEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContactPersonController extends AppController
{
    /**
     * @Route("/suppliers/add-contact-person", name="user-suppliers-add-contact-person.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_contact_person(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = $request->get('supplier_id');
            $name = $request->get('name');
            $email = $request->get('email');
            $phone_number = $request->get('phone_number');
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $contact_person_entity = new ContactPersonEntity();
            $contact_person_entity->setName($name);
            $contact_person_entity->setEmail($email);
            $contact_person_entity->setPhoneNumber($phone_number);
            $contact_person_entity->setSupplier($supplier);
            $contact_person_entity->setCreated(new \DateTime('now'));
            $contact_person_entity->setUpdated(new \DateTime('now'));
            $contact_person_entity->setUsername($this->getUser()->getUsername());
            $em->persist($contact_person_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $contact_person_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/delete-contact-person", name="user-suppliers-delete-contact-person.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_contact_person(Request $request, EntityManagerInterface $em)
    {
        try {
            $contact_person_id = $request->get('id');
            $contact_person_entity = $em->getRepository(ContactPersonEntity::class)
                ->findOneBy(['id' => $contact_person_id]);
            $em->remove($contact_person_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $contact_person_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/update-contact-person", name="user-suppliers-update-contact-person.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_contact_person(Request $request, EntityManagerInterface $em)
    {
        try {
            $contact_person_id = $request->get('id');
            $name = $request->get('name');
            $email = $request->get('email');
            $phone_number = $request->get('phone_number');
            $contact_person_entity = $em->getRepository(ContactPersonEntity::class)
                ->findOneBy(['id' => $contact_person_id]);
            $contact_person_entity->setName($name);
            $contact_person_entity->setEmail($email);
            $contact_person_entity->setPhoneNumber($phone_number);
            $contact_person_entity->setUpdated(new \DateTime('now'));
            $contact_person_entity->setUsername($this->getUser()->getUsername());
            $em->persist($contact_person_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $contact_person_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}