<?php
/**
 * Created by PhpStorm.
 * User: Strale
 * Date: 17.11.2017.
 * Time: 09.29
 */

namespace AppBundle\Controller\User\Suppliers;


use AppBundle\AppController;
use AppBundle\Entity\User\Suppliers\ContactPersonEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SupplierJSONController extends AppController
{
    /**
     * @Route("/suppliers", name="user-suppliers-get.json")
     * @Method({"POST"})
     * @param Request $request
     * @return Response
     */
    public function get_supplier(Request $request)
    {
        try {
            $supplier_id = intval($request->get('id'));
            $supplier = $this->getDoctrine()
                ->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            if(empty($supplier)) {
                throw new \Exception('Supplier does not exist');
            }
            $response = new Response();
            $response->setContent($this->json_encode($supplier));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/type-contact-persons", name="user-suppliers-get-type-contact-persons.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_type_contact_persons(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_name = $request->get('supplier_name');
            $supplier_entity = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['name' => $supplier_name]);
            if(empty($supplier_entity)) {
                throw new \Exception('Supplier does not exist');
            }
            $contact_persons = $supplier_entity->getContactPersons();
            $contact_persons_names = [];
            foreach($contact_persons as $contact_person) {
                $contact_persons_names[] = $contact_person->getName();
            }
            $response = new Response();
            $response->setContent(
                json_encode(
                    [
                        'type' => $supplier_entity->getEndUserType()->getName(),
                        'contact_persons' => $contact_persons_names
                    ]
                )
            );
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/suppliers/add-supplier", name="user-suppliers-add-supplier.json")
     * @Method({"POST"})
     * @param Request $request\
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier = new SupplierEntity();
            $supplier->setCreated(new \DateTime('now'));
            $supplier->setUpdated(new \DateTime('now'));
            $supplier->setName($request->get('name'));
            $supplier->setEmail($request->get('email'));
            $supplier->setStreet($request->get('street'));
            $supplier->setNumber($request->get('house_number'));
            $supplier->setNumberExtension($request->get('number_extension'));
            $supplier->setPostCode($request->get('post_code'));
            $supplier->setCity($request->get('city'));
            $supplier->setCountry($request->get('country'));
            $supplier->setUsername($this->getUser()->getUsername());
            $supplier->setCommunicationLanguage($request->get('communication_language'));
            $contact_person_entity = new ContactPersonEntity();
            $contact_person_entity->setSupplier($supplier);
            $contact_person_entity->setName($supplier->getName());
            $contact_person_entity->setEmail($supplier->getEmail());
            $contact_person_entity->setPhoneNumber($request->get('phone_number'));
            $contact_person_entity->setCreated(new \DateTime('now'));
            $contact_person_entity->setUpdated(new \DateTime('now'));
            $contact_person_entity->setUsername($this->getUser()->getUsername());
            $em->persist($contact_person_entity);
            $em->persist($supplier);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $supplier->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/delete-supplier", name="user-suppliers-delete-supplier.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @return Response
     */
    public function delete_supplier(Request $request)
    {
        try {
            $supplier_id = intval($request->get('id'));
            $em = $this->getDoctrine()
                ->getManager();
            $suppler = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $em->remove($suppler);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $supplier_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/update-supplier", name="user-suppliers-update-supplier.json")
     * @Method({"PUT"})
     * @param Request $request\
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_supplier(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = $request->get('id');
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            if(empty($supplier)) {
                throw new \Exception('Unknown supplier');
            }
            $supplier->setUpdated(new \DateTime('now'));
            $supplier->setName($request->get('name'));
            $supplier->setEmail($request->get('email'));
            $supplier->setStreet($request->get('street'));
            $supplier->setNumber($request->get('house_number'));
            $supplier->setNumberExtension($request->get('number_extension'));
            $supplier->setPostCode($request->get('post_code'));
            $supplier->setCity($request->get('city'));
            $supplier->setCountry($request->get('country'));
            $supplier->setCommunicationLanguage($request->get('communication_language'));
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $supplier->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}