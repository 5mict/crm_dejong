<?php
/**
 * Created by PhpStorm.
 * User: Strale
 * Date: 20.11.2018.
 * Time: 11.23
 */

namespace AppBundle\Controller\User\Suppliers;

use AppBundle\AppController;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use AppBundle\Entity\User\Suppliers\SupplierFileEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class FileController extends AppController
{
    /**
     * @Route("/suppliers/upload_supplier_file", name="user-upload-supplier-file.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function upload_supplier_file(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = $request->get('id');
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            foreach($_FILES['file']['name'] as $key => $original){
                $imageType = pathinfo($original, PATHINFO_EXTENSION);
                $file_name = $this->generateRandomString(30) . '.' . $imageType;
                $file_tmp_name = $_FILES["file"]["tmp_name"][$key];
                $this->upload_validate($file_tmp_name, $file_name, "supplier_files");
                $supplier_file_entity = new SupplierFileEntity();
                $supplier_file_entity->setSupplier($supplier);
                $supplier_file_entity->setFileName($file_name);
                $supplier_file_entity->setName($original);
                $supplier_file_entity->setCreated(new \DateTime('now'));
                $em->persist($supplier_file_entity);
            }
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['file uploaded']));
            return $response;

        } catch (\Exception $e){
            return $this->handle_json_error(400, $e->getMessage());
        }
    }


    /**
     * @Route("/suppliers/get_uploaded_file/{id}", name="post-supplier-uploaded-file")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function post_view_supplier_to_supplier_pdf(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(SupplierFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'supplier_files/' . $file->getFileName();
            $supplier = $file->getSupplier();
            return $this->render(
                ':user/suppliers/pdf:view_uploaded_file.html.twig',
                [
                    'company' => $this->get_company(),
                    'ActionId' => $supplier->getId(),
                    'Action' => 'SupplierFiles',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/get_uploaded_file/{id}", name="get-supplier-uploaded-file")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_view_supplier_to_supplier_pdf(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(SupplierFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $data = [];
            $data['file'] = '/' . $this->get_company() . 'supplier_files/' . $file->getFileName();
            $supplier = $file->getSupplier();
            return $this->render(
                ':user/suppliers/pdf:view_uploaded_file.html.twig',
                [
                    'company' => $this->get_company(),
                    'ActionId' => $supplier->getId(),
                    'Action' => 'SupplierFiles',
                    'locale' => $request->getLocale(),
                    'user' => $this->getUser(),
                    'file' => $data['file'],
                    'debtors_by_delivery_time' => $this->get_debtors_by_delivery_time(),
                    'suppliers_by_delivery_time' => $this->get_suppliers_by_delivery_time(),
                ]
            );
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/delete_uploaded_file", name="delete-supplier-uploaded-file.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_uploaded_file(Request $request, EntityManagerInterface $em)
    {
        try {
            $file_id = intval($request->get('id'));
            $file = $em->getRepository(SupplierFileEntity::class)
                ->findOneBy(['id' => $file_id]);
            $this->delete_validate("supplier_files", $file->getFileName());
            $em->remove($file);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['file deleted']));
            return $response;
        }
        catch (\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}