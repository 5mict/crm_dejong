<?php
/**
 * Created by PhpStorm.
 * User: Strale
 * Date: 17.11.2017.
 * Time: 10.07
 */

namespace AppBundle\Controller\User\Suppliers;

use AppBundle\AppController;
use AppBundle\Controller\User\Suppliers\Exceptions\SupplierNotFoundException;
use AppBundle\Entity\User\Suppliers\NoteEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class NoteController extends AppController
{
    /**
     * @Route("/suppliers/get-notes", name="user-suppliers-get-notes.json")
     * @Method({"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function get_notes(Request $request, EntityManagerInterface $em)
    {
        try {
            $page_number = intval($request->get('page_number'));
            $page_size = intval($request->get('page_size'));
            $supplier_id = intval($request->get('parent_id'));
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            if(empty($supplier)) {
                throw new SupplierNotFoundException(_('Supplier not found'));
            }
            list($notes, $page_number, $page_size, $total_number_of_pages, $sort_by, $sort_direction)
                = $em->getRepository(NoteEntity::class)
                    ->get_notes($supplier_id, $page_number, $page_size);
            $notes_array = [];
            foreach($notes as $note) {
                $notes_array[] = [
                    'id' => $note->getId(),
                    'username' => $note->getUsername(),
                    'updatedString' => $note->getUpdatedString(),
                    'note' => $note->getNote()
                ];
            }
            $response = new Response();
            $response->setContent(json_encode(['page_number' => $page_number, 'notes' => $notes_array]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
    /**
     * @Route("/suppliers/add-note", name="user-suppliers-add-note.json")
     * @Method({"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function add_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $supplier_id = $request->get('supplier_id');
            $note = $request->get('note');
            $supplier = $em->getRepository(SupplierEntity::class)
                ->findOneBy(['id' => $supplier_id]);
            $note_entity = new NoteEntity();
            $note_entity->setNote($note);
            $note_entity->setCreated(new \DateTime('now'));
            $note_entity->setUpdated(new \DateTime('now'));
            $note_entity->setSupplier($supplier);
            $note_entity->setUsername($this->getUser()->getUsername());
            $em->persist($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_entity->getId()]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/delete-note", name="user-suppliers-delete-note.json")
     * @Method({"DELETE"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $note_id = $request->get('id');
            $note_entity = $em->getRepository(NoteEntity::class)
                ->findOneBy(['id' => $note_id]);
            $em->remove($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }

    /**
     * @Route("/suppliers/update-note", name="user-suppliers-update-note.json")
     * @Method({"PUT"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function update_note(Request $request, EntityManagerInterface $em)
    {
        try {
            $note_id = $request->get('id');
            $note_entity = $em->getRepository(NoteEntity::class)
                ->findOneBy(['id' => $note_id]);
            $note = $request->get('note');
            $note_entity->setNote($note);
            $note_entity->setUpdated(new \DateTime('now'));
            $note_entity->setUsername($this->getUser()->getUsername());
            $em->persist($note_entity);
            $em->flush();
            $response = new Response();
            $response->setContent(json_encode(['id' => $note_id]));
            return $response;
        } catch(\Exception $e) {
            return $this->handle_json_error(400, $e->getMessage());
        }
    }
}