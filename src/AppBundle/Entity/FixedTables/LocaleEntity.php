<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/16/2017
 * Time: 5:25 PM
 */

namespace AppBundle\Entity\FixedTables;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="fixed_tables_locale")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FixedTables\LocaleRepository")
 */
class LocaleEntity
{
    const ENGLISH = 'en';
    const DUTCH = 'nl';
    const GERMAN = 'ge';
    const LANGUAGES = [
        LocaleEntity::ENGLISH,
        LocaleEntity::DUTCH,
        LocaleEntity::GERMAN
    ];
    const TABLE_ROWS = [
        ['language' => LocaleEntity::ENGLISH],
        ['language' => LocaleEntity::DUTCH],
        ['language' => LocaleEntity::GERMAN]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $language;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getLanguage() : string
    {
        return $this->language;
    }
    /**
     * @param string $language
     */
    public function setLanguage(string $language)
    {
        $this->language = $language;
    }
}