<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 9/17/2017
 * Time: 7:07 PM
 */

namespace AppBundle\Entity\Login;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Table(name="app_users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Login\UserRepository")
 */
class UserEntity implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $first_name;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $last_name;
    /**
     * @ORM\Column(type="string", length=64)
     */
    private $phonenumber;
    /**
     * @ORM\Column(type="string", length=60)
     */
    private $email;
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    /**
     * @ORM\OneToMany(targetEntity="RoleEntity", mappedBy="user")
     */
    private $roles;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $updated;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive)
    {
        $this->isActive = $isActive;
    }
    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }
    /**
     * @return string
     */
    public function getFirstName() : string
    {
        return $this->first_name;
    }
    /**
     * @param string $first_name
     */
    public function setFirstName(string $first_name)
    {
        $this->first_name = $first_name;
    }
    /**
     * @return string
     */
    public function getLastName() : string
    {
        return $this->last_name;
    }
    /**
     * @param string $last_name
     */
    public function setLastName(string $last_name)
    {
        $this->last_name = $last_name;
    }
    /**
     * @return mixed
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }
    /**
     * @param mixed $phonenumber
     */
    public function setPhonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    }
    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }
    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }
    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }
    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }
    /**
     * @return array
     */
    public function getRoles() : array
    {
        return $this->roles->toArray();
    }
    /**
     * @return ArrayCollection
     */
    public function getRolesCollection() : ArrayCollection
    {
        return $this->roles;
    }
    /**
     * @param ArrayCollection $roles
     */
    public function setRoles(ArrayCollection $roles)
    {
        $this->roles = $roles;
    }
    /**
     * @return string
     */
    public function getRolesString()
    {
        $_roles = [];
        foreach ($this->roles as $role) {
            if($role == 'ROLE_USER')
                $_roles[] = 'user';
            if($role == 'ROLE_ADMIN')
                $_roles[] = 'admin';
        }
        return json_encode($_roles);
    }
    /**
     *
     */
    public function eraseCredentials()
    {

    }
    /**
     * @return bool
     */
    public function isEnabled() {
        return $this->isActive;
    }
    /**
     * @return bool
     */
    public function isAccountNonExpired() : bool
    {
        return true;
    }
    /**
     * @return bool
     */
    public function isAccountNonLocked() : bool
    {
        return true;
    }
    /**
     * @return bool
     */
    public function isCredentialsNonExpired() : bool
    {
        return true;
    }
    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }
    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            // see section on salt below
            // $this->salt,
        ));
    }
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->isActive,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }
    /**
     * UserEntity constructor.
     */
    public function __construct()
    {
        $this->isActive = true;
        $this->roles = new ArrayCollection();
    }
}