<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/13/2017
 * Time: 3:16 PM
 */
namespace AppBundle\Entity\Login;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="user_roles")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Login\RoleRepository")
 */
class RoleEntity
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $user_id;
    /**
     * @ORM\ManyToOne(targetEntity="UserEntity", inversedBy="roles")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $role;

    public function __construct() {

    }

    /**
     * @return string
     */
    public function getRole() : string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role) {
        $this->role = $role;
    }

    /**
     * @return int
     */
    public function getUserId() : int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id)
    {
        $this->user_id = $user_id;
    }

    /*public function getUser() {
        return $this->user;
    }*/

    /**
     * @param UserEntity $user
     */
    public function setUser(UserEntity $user) {
        $this->user = $user;
        //$this->user_id = $this->user->getId();
    }
}
