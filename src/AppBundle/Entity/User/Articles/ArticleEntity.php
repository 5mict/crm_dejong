<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/8/2017
 * Time: 1:05 PM
 */

namespace AppBundle\Entity\User\Articles;

use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="articles",
 *     indexes={
 *          @ORM\Index(
 *              name="search_article_name",
 *              columns={"article_name"}
 *          ),
 *          @ORM\Index(
 *              name="search_article_number",
 *              columns={"article_number"}
 *          ),
 *          @ORM\Index(
 *              name="search_purchase_price",
 *              columns={"purchase_price"}
 *          ),
 *          @ORM\Index(
 *              name="search_selling_price",
 *              columns={"selling_price"}
 *          ),
 *          @ORM\Index(
 *              name="search_selling_price_with_pdv",
 *              columns={"selling_price_with_pdv"}
 *          ),
 *          @ORM\Index(
 *              name="search_total_selling_price",
 *              columns={"total_selling_price"}
 *          ),
 *          @ORM\Index(
 *              name="search_profit_percents",
 *              columns={"profit_percents"}
 *          ),
 *      },
 *     )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Articles\ArticleRepository")
 */
class ArticleEntity
{
    //ordered by article_name
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $article_number;//ART. NR.
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $article_name;//article
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $article_name_dutch;//article
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $article_name_german;//article
    /**
     * @ORM\Column(type="string", length=150)
     */
    private $size;
    /**
     * @ORM\Column(type="float")
     */
    private $purchase_price;//ik
    /**
     * @ORM\Column(type="float")
     */
    private $selling_price;//VK ex
    /**
     * @ORM\Column(type="float")
     */
    private $selling_price_with_pdv;//VK. part. Inc
    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $description;
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $picture_url;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderArticleEntity", mappedBy="article")
     */
    private $order_articles;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $updated;
    /**
     * @ORM\Column(type="string")
     */
    private $username;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity", inversedBy="articles")
     * @ORM\JoinColumn(name="article_category_id", referencedColumnName="id")
     */
    private $article_category;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity", inversedBy="articles")
     * @ORM\JoinColumn(name="article_sub_category_id", referencedColumnName="id")
     */
    private $article_sub_category;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Articles\ArticlePaidEntity", mappedBy="article")
     */
    private $articles_paid;
    /**
     * @ORM\Column(type="float")
     */
    private $total_selling_price;
    /**
     * @ORM\Column(type="float")
     */
    private $total_purchase_price;
    /**
     * @ORM\Column(type="float")
     */
    private $profit_percents;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getArticleNumber() : string
    {
        return $this->article_number;
    }
    /**
     * @param string $article_number
     */
    public function setArticleNumber(string $article_number)
    {
        $this->article_number = $article_number;
    }
    /**
     * @return string
     */
    public function getArticleName() : string
    {
        return $this->article_name;
    }
    /**
     * @param string $article_name
     */
    public function setArticleName($article_name)
    {
        $this->article_name = $article_name;
    }
    /**
     * @return string
     */
    public function getArticleNameDutch() : string
    {
        return $this->article_name_dutch;
    }
    /**
     * @param string $article_name_dutch
     */
    public function setArticleNameDutch(string $article_name_dutch)
    {
        $this->article_name_dutch = $article_name_dutch;
    }
    /**
     * @return string
     */
    public function getArticleNameGerman() : string
    {
        return $this->article_name_german;
    }
    /**
     * @param string $article_name_german
     */
    public function setArticleNameGerman(string $article_name_german)
    {
        $this->article_name_german = $article_name_german;
    }
    /**
     * @return string
     */
    public function getSize() : string
    {
        return $this->size;
    }
    /**
     * @param string $size
     */
    public function setSize(string $size)
    {
        $this->size = $size;
    }
    /**
     * @return float
     */
    public function getPurchasePrice() : float
    {
        return $this->purchase_price;
    }
    /**
     * @param float $purchase_price
     */
    public function setPurchasePrice(float $purchase_price)
    {
        $this->purchase_price = $purchase_price;
    }
    /**
     * @return float
     */
    public function getSellingPrice() : float
    {
        return $this->selling_price;
    }
    /**
     * @param float $selling_price
     */
    public function setSellingPrice(float $selling_price)
    {
        $this->selling_price = $selling_price;
    }
    /**
     * @return float
     */
    public function getSellingPriceWithPdv() : float
    {
        return $this->selling_price_with_pdv;
    }
    /**
     * @param float $selling_price_with_pdv
     */
    public function setSellingPriceWithPdv(float $selling_price_with_pdv)
    {
        $this->selling_price_with_pdv = $selling_price_with_pdv;
    }
    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }
    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
    /**
     * @return string
     */
    public function getPictureUrl() : string
    {
        return $this->picture_url;
    }
    /**
     * @param string $picture_url
     */
    public function setPictureUrl(string $picture_url)
    {
        $this->picture_url = $picture_url;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }
    /**
     * @return \DateTime
     */
    public function getUpdated() :  \DateTime
    {
        return $this->updated;
    }
    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }
    /**
     * @param mixed $order_articles
     */
    public function setOrderArticles($order_articles)
    {
        $this->order_articles = $order_articles;
    }
    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }
    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }
    /**
     * @return ArticleCategoryEntity
     */
    public function getArticleCategory() : ArticleCategoryEntity
    {
        return $this->article_category;
    }
    /**
     * @param ArticleCategoryEntity $article_category
     */
    public function setArticleCategory(ArticleCategoryEntity $article_category)
    {
        $this->article_category = $article_category;
    }
    /**
     * @return mixed
     */
    public function getArticleSubCategory()
    {
        return $this->article_sub_category;
    }
    /**
     * @param ArticleSubCategoryEntity $article_sub_category
     */
    public function setArticleSubCategory(ArticleSubCategoryEntity $article_sub_category)
    {
        $this->article_sub_category = $article_sub_category;
    }
    /**
     * @param ArrayCollection $articles_paid
     */
    public function setArticlesPaid(ArrayCollection $articles_paid)
    {
        $this->articles_paid = $articles_paid;
    }
    /**
     * @return float
     */
    public function getTotalSellingPrice() : float
    {
        return $this->total_selling_price;
    }
    /**
     * @param float $total_selling_price
     */
    public function setTotalSellingPrice(float $total_selling_price)
    {
        $this->total_selling_price = $total_selling_price;
    }
    /**
     * @return float
     */
    public function getTotalPurchasePrice() : float
    {
        return $this->total_purchase_price;
    }
    /**
     * @param float $total_purchase_prise
     */
    public function setTotalPurchasePrice(float $total_purchase_prise)
    {
        $this->total_purchase_price = $total_purchase_prise;
    }
    /**
     * @return float
     */
    public function getProfitPercents() : float
    {
        return $this->profit_percents;
    }
    /**
     * @param float $profit_percents
     */
    public function setProfitPercents(float $profit_percents)
    {
        $this->profit_percents = $profit_percents;
    }

    /**
     * ArticleEntity constructor.
     */
    public function __construct()
    {
        $this->order_articles = new ArrayCollection();
        $this->articles_paid = new ArrayCollection();
    }
}