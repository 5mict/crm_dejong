<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/16/2017
 * Time: 5:06 PM
 */

namespace AppBundle\Entity\User\Articles\FixedTables;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation\Article;

/**
 * @ORM\Table(name="articles_fixed_tables_sub_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Articles\FixedTables\ArticleSubCategoryRepository")
 */
class ArticleSubCategoryEntity
{
    const DINING_CHAIR = "dining chair";
    const COUCHE = "couche";
    const ARMCHAIR = "Armchair";
    const POUF = "pouf";
    const STOOL_BENCH = "stool bench";
    const BAR_STOOL = "bar stool";
    const SEATING_FURNITURE = [
        ArticleSubCategoryEntity::DINING_CHAIR,
        ArticleSubCategoryEntity::COUCHE,
        ArticleSubCategoryEntity::ARMCHAIR,
        ArticleSubCategoryEntity::POUF,
        ArticleSubCategoryEntity::STOOL_BENCH,
        ArticleSubCategoryEntity::BAR_STOOL
    ];
    const SIDEBOARD = "sideboard";
    const CABINET = "cabinet";
    const BEDROOM_DRESSER = "bedroom dresser";
    const SIDE_TABLE = "side table";
    const CABINETS = [
        ArticleSubCategoryEntity::SIDEBOARD,
        ArticleSubCategoryEntity::CABINET,
        ArticleSubCategoryEntity::BEDROOM_DRESSER,
        ArticleSubCategoryEntity::SIDE_TABLE,
    ];
    const COFFEE_TABLES = "coffee tables";
    const DINING_TABLES = "dining tables";
    const TABLES = [
        ArticleSubCategoryEntity::COFFEE_TABLES,
        ArticleSubCategoryEntity::DINING_TABLES,
    ];
    const ANTLER_PENDANT_LIGHT = "antler pendant light";
    const ANTLER_WALL_LAMP = "antler wall lamp";
    const ANTLER_STANDING_LAMP = "antler standing lamp";
    const ANTLER_CEILING_LAMP = "antler ceiling lamp";
    const ANTLER_CHAMPAGNE_COOLER = "antler champagne cooler";
    const ANTLER_BOWL = "antler bowl";
    const ANTLER_CANDLE_STICK = "antler candle stick";
    const ANTLER_SEATING_FURNITURE = "antler seating furniture";
    const ANTLER_TABLE = "antler table";
    const ANTLER_COAT_RACK = "antler coat rack";
    const ANTLER_DECORATION = "antler decoration";
    const ANTLER_MIRROR = "antler mirror";
    const ANTLER_TABLE_LAMP = "antler table lamp";
    const ANTLER_ARTICLES = [
        ArticleSubCategoryEntity::ANTLER_PENDANT_LIGHT,
        ArticleSubCategoryEntity::ANTLER_WALL_LAMP,
        ArticleSubCategoryEntity::ANTLER_STANDING_LAMP,
        ArticleSubCategoryEntity::ANTLER_CEILING_LAMP,
        ArticleSubCategoryEntity::ANTLER_CHAMPAGNE_COOLER,
        ArticleSubCategoryEntity::ANTLER_BOWL,
        ArticleSubCategoryEntity::ANTLER_CANDLE_STICK,
        ArticleSubCategoryEntity::ANTLER_SEATING_FURNITURE,
        ArticleSubCategoryEntity::ANTLER_TABLE,
        ArticleSubCategoryEntity::ANTLER_COAT_RACK,
        ArticleSubCategoryEntity::ANTLER_DECORATION,
        ArticleSubCategoryEntity::ANTLER_MIRROR,
        ArticleSubCategoryEntity::ANTLER_TABLE_LAMP,
    ];
    const TABLE_LAMP = "table lamp";
    const WALL_LAMP = "wall lamp";
    const PENDANT_LIGHT = "pendant light";
    const STANDING_LAMP = "standing lamp";
    const LAMP_SHADE = "lamp shade";
    const LIGHTING = [
        ArticleSubCategoryEntity::TABLE_LAMP,
        ArticleSubCategoryEntity::WALL_LAMP,
        ArticleSubCategoryEntity::PENDANT_LIGHT,
        ArticleSubCategoryEntity::STANDING_LAMP,
        ArticleSubCategoryEntity::LAMP_SHADE,
    ];
    const FULL_MOUNTS = "full mounts";
    const SHOULDER_MOUNTS = "shoulder mounts";
    const BIRDS = "birds";
    const INSECTS = "insects";
    const ANTLER_AND_SKULLS = "antler and skulls";
    const ANIMAL_HIDES = "animal hides";
    const VARIOUS_ITEMS = "various items";
    const BUTTERFLIES_AND_INSECTS = 'butterflies & Insects';
    const REPLICAS = "replicas";
    const TAXIDERMY = [
        ArticleSubCategoryEntity::FULL_MOUNTS,
        ArticleSubCategoryEntity::SHOULDER_MOUNTS,
        ArticleSubCategoryEntity::BIRDS,
        ArticleSubCategoryEntity::INSECTS,
        ArticleSubCategoryEntity::ANTLER_AND_SKULLS,
        ArticleSubCategoryEntity::ANIMAL_HIDES,
        ArticleSubCategoryEntity::VARIOUS_ITEMS,
        ArticleSubCategoryEntity::BUTTERFLIES_AND_INSECTS,
        ArticleSubCategoryEntity::REPLICAS,
    ];
    const SUITCASE = "suitcase";
    const SUITCASES = [
        ArticleSubCategoryEntity::SUITCASE,
    ];
    const MIRROR_WOOD = "mirror_wood";
    const MIRROR_UPHOLSTERED = "mirror upholstered";
    const MIRRORS = [
        ArticleSubCategoryEntity::MIRROR_WOOD,
        ArticleSubCategoryEntity::MIRROR_UPHOLSTERED,
    ];
    const DECORATION_ARTICLE = "decoration article";
    const DECORATION_ARTICLES = [
        ArticleSubCategoryEntity::DECORATION_ARTICLE,
    ];
    const SLEEPING_ROOM = "sleeping room";
    const Commission = "commission";
    const Rental = "rental";
    const DIBS = "1st dibs";
    const PAYPAL_PAYMENT = "paypal payment";
    const COMPLAIN = "complain";
    const SERVICE_PROVIDED = "service_provided";

    const TABLE_ROWS = [
        ['name' => ArticleSubCategoryEntity::DINING_CHAIR, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE],
        ['name' => ArticleSubCategoryEntity::COUCHE, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE],
        ['name' => ArticleSubCategoryEntity::ARMCHAIR, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE],
        ['name' => ArticleSubCategoryEntity::POUF, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE],
        ['name' => ArticleSubCategoryEntity::STOOL_BENCH, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE],
        ['name' => ArticleSubCategoryEntity::BAR_STOOL, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE],
        ['name' => ArticleSubCategoryEntity::SIDEBOARD, 'reference' => ArticleCategoryEntity::CABINETS],
        ['name' => ArticleSubCategoryEntity::CABINET, 'reference' => ArticleCategoryEntity::CABINETS],
        ['name' => ArticleSubCategoryEntity::BEDROOM_DRESSER, 'reference' => ArticleCategoryEntity::CABINETS],
        ['name' => ArticleSubCategoryEntity::SIDE_TABLE, 'reference' => ArticleCategoryEntity::CABINETS],
        ['name' => ArticleSubCategoryEntity::COFFEE_TABLES, 'reference' => ArticleCategoryEntity::TABLES],
        ['name' => ArticleSubCategoryEntity::DINING_TABLES, 'reference' => ArticleCategoryEntity::TABLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_PENDANT_LIGHT, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_WALL_LAMP, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_STANDING_LAMP, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_CEILING_LAMP, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_CHAMPAGNE_COOLER, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_BOWL, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_CANDLE_STICK, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_SEATING_FURNITURE, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_TABLE, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_COAT_RACK, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_DECORATION, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::ANTLER_TABLE_LAMP, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleSubCategoryEntity::TABLE_LAMP, 'reference' => ArticleCategoryEntity::LIGHTING],
        ['name' => ArticleSubCategoryEntity::WALL_LAMP, 'reference' => ArticleCategoryEntity::LIGHTING],
        ['name' => ArticleSubCategoryEntity::PENDANT_LIGHT, 'reference' => ArticleCategoryEntity::LIGHTING],
        ['name' => ArticleSubCategoryEntity::STANDING_LAMP, 'reference' => ArticleCategoryEntity::LIGHTING],
        ['name' => ArticleSubCategoryEntity::FULL_MOUNTS, 'reference' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleSubCategoryEntity::SHOULDER_MOUNTS, 'reference' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleSubCategoryEntity::BIRDS, 'reference' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleSubCategoryEntity::INSECTS, 'reference' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleSubCategoryEntity::ANTLER_AND_SKULLS, 'reference' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleSubCategoryEntity::ANIMAL_HIDES, 'reference' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleSubCategoryEntity::VARIOUS_ITEMS, 'reference' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleSubCategoryEntity::SUITCASE, 'reference' => ArticleCategoryEntity::SUITCASES],
        ['name' => ArticleSubCategoryEntity::MIRROR_WOOD, 'reference' => ArticleCategoryEntity::MIRRORS],
        ['name' => ArticleSubCategoryEntity::MIRROR_UPHOLSTERED, 'reference' => ArticleCategoryEntity::MIRRORS],
        ['name' => ArticleSubCategoryEntity::DECORATION_ARTICLE, 'reference' => ArticleCategoryEntity::DECORATION_ARTICLES],
        ['name' => ArticleSubCategoryEntity::SLEEPING_ROOM, 'reference' => ArticleCategoryEntity::BEDROOM],
        ['name' => ArticleSubCategoryEntity::Commission, 'reference' => ArticleCategoryEntity::Commission],
        ['name' => ArticleSubCategoryEntity::Rental, 'reference' => ArticleCategoryEntity::Rental],
        ['name' => ArticleSubCategoryEntity::Rental, 'reference' => ArticleCategoryEntity::ADMINISTRATION],
        ['name' => ArticleSubCategoryEntity::DIBS, 'reference' => ArticleCategoryEntity::ADMINISTRATION],
        ['name' => ArticleSubCategoryEntity::PAYPAL_PAYMENT, 'reference' => ArticleCategoryEntity::ADMINISTRATION],
        ['name' => ArticleSubCategoryEntity::COMPLAIN, 'reference' => ArticleCategoryEntity::ADMINISTRATION],
        ['name' => ArticleSubCategoryEntity::SERVICE_PROVIDED, 'reference' => ArticleCategoryEntity::ADMINISTRATION],
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=30, unique=true)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity", inversedBy="article_sub_categories")
     * @ORM\JoinColumn(name="article_category_id", referencedColumnName="id")
     */
    private $article_category;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryTranslationEntity", mappedBy="article_sub_category")
     */
    private $article_sub_category_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Articles\ArticleEntity", mappedBy="article_sub_category")
     */
    private $articles;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return ArticleCategoryEntity
     */
    public function getArticleCategory() : ArticleCategoryEntity
    {
        return $this->article_category;
    }
    /**
     * @param ArticleCategoryEntity $article_category
     */
    public function setArticleCategory(ArticleCategoryEntity $article_category)
    {
        $this->article_category = $article_category;
    }
    /**
     * @param mixed $article_sub_category_translations
     */
    public function setArticleSubCategoryTranslations($article_sub_category_translations)
    {
        $this->article_sub_category_translations = $article_sub_category_translations;
    }
    /**
     * @param mixed $articles
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;
    }
    /**
     * ArticleSubCategoryEntity constructor.
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->article_sub_category_translations = new ArrayCollection();
    }
}