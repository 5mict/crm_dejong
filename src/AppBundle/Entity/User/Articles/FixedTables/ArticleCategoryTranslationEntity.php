<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 2:07 PM
 */

namespace AppBundle\Entity\User\Articles\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="articles_fixed_tables_category_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 *  )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Articles\FixedTables\ArticleCategoryTranslationRepository")
 */
class ArticleCategoryTranslationEntity
{
    const SEATING_FURNITURE_EN = "Seating furniture";
    const SEATING_FURNITURE_NL = "Zitmeubels";
    const SEATING_FURNITURE_GE = "mobel";
    const CABINETS_EN = "Cabinets";
    const CABINETS_NL = "Kasten";
    const CABINETS_GE = "Sitzmobel";
    const TABLES_EN = "Tables";
    const TABLES_NL = "Tafels";
    const TABLES_GE = "Tabellen";
    const ANTLER_ARTICLES_EN = "Antler articles";
    const ANTLER_ARTICLES_NL = "Gewei artikelen";
    const ANTLER_ARTICLES_GE = "Geweih Artikel";
    const LIGHTING_EN = "Lighting";
    const LIGHTING_NL = "Verlinchting";
    const LIGHTING_GE = "Beleuchtung";
    const TAXIDERMY_EN = "Taxidermy";
    const TAXIDERMY_NL = "Taxidermy";
    const TAXIDERMY_GE = "Präparatoren";
    const SUITCASES_EN = "Suitcases";
    const SUITCASES_NL = "Koffers";
    const SUITCASES_GE = "Koffers";
    const MIRRORS_EN = "Mirrors";
    const MIRRORS_NL = "Spiegels";
    const MIRRORS_GE = "Spiegels";
    const DECORATION_ARTICLES_EN = "Decoration articles";
    const DECORATION_ARTICLES_NL = "Decoratie artikelen";
    const DECORATION_ARTICLES_GE = "Dekorationsartikel";
    const BEDROOM_EN = "Bedroom";
    const BEDROOM_NL = "Slaapkamer";
    const Commission_EN = "Commission";
    const Commission_NL = "Commissie ";
    const RENTAL_EN = "Rental";
    const RENTAL_NL = "Verhuur";
    const ADMINISTRATION_EN = "Administration";
    const ADMINISTRATION_NL = "Administration";
    const TABLE_ROWS = [
        ['name' => ArticleCategoryTranslationEntity::SEATING_FURNITURE_EN, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::SEATING_FURNITURE_NL, 'reference' => ArticleCategoryEntity::SEATING_FURNITURE, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::CABINETS_EN, 'reference' => ArticleCategoryEntity::CABINETS, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::CABINETS_NL, 'reference' => ArticleCategoryEntity::CABINETS, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::TABLES_EN, 'reference' => ArticleCategoryEntity::TABLES, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::TABLES_NL, 'reference' => ArticleCategoryEntity::TABLES, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::ANTLER_ARTICLES_EN, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::ANTLER_ARTICLES_NL, 'reference' => ArticleCategoryEntity::ANTLER_ARTICLES, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::LIGHTING_EN, 'reference' => ArticleCategoryEntity::LIGHTING, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::LIGHTING_NL, 'reference' => ArticleCategoryEntity::LIGHTING, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::TAXIDERMY_EN, 'reference' => ArticleCategoryEntity::TAXIDERMY, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::TAXIDERMY_NL, 'reference' => ArticleCategoryEntity::TAXIDERMY, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::SUITCASES_EN, 'reference' => ArticleCategoryEntity::SUITCASES, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::SUITCASES_NL, 'reference' => ArticleCategoryEntity::SUITCASES, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::MIRRORS_EN, 'reference' => ArticleCategoryEntity::MIRRORS, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::MIRRORS_NL, 'reference' => ArticleCategoryEntity::MIRRORS, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::DECORATION_ARTICLES_EN, 'reference' => ArticleCategoryEntity::DECORATION_ARTICLES, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::DECORATION_ARTICLES_NL, 'reference' => ArticleCategoryEntity::DECORATION_ARTICLES, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::BEDROOM_EN, 'reference' => ArticleCategoryEntity::BEDROOM, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::BEDROOM_NL, 'reference' => ArticleCategoryEntity::BEDROOM, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::Commission_EN, 'reference' => ArticleCategoryEntity::Commission, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::Commission_NL, 'reference' => ArticleCategoryEntity::Commission, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::RENTAL_EN, 'reference' => ArticleCategoryEntity::Rental, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::RENTAL_NL, 'reference' => ArticleCategoryEntity::Rental, 'language' => 'nl'],
        ['name' => ArticleCategoryTranslationEntity::ADMINISTRATION_EN, 'reference' => ArticleCategoryEntity::ADMINISTRATION, 'language' => 'en'],
        ['name' => ArticleCategoryTranslationEntity::ADMINISTRATION_NL, 'reference' => ArticleCategoryEntity::ADMINISTRATION, 'language' => 'nl'],
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            ArticleCategoryEntity::SEATING_FURNITURE => ArticleCategoryTranslationEntity::SEATING_FURNITURE_EN,
            ArticleCategoryEntity::CABINETS => ArticleCategoryTranslationEntity::CABINETS_EN,
            ArticleCategoryEntity::TABLES => ArticleCategoryTranslationEntity::TABLES_EN,
            ArticleCategoryEntity::ANTLER_ARTICLES => ArticleCategoryTranslationEntity::ANTLER_ARTICLES_EN,
            ArticleCategoryEntity::LIGHTING => ArticleCategoryTranslationEntity::LIGHTING_EN,
            ArticleCategoryEntity::TAXIDERMY => ArticleCategoryTranslationEntity::TAXIDERMY_EN,
            ArticleCategoryEntity::SUITCASES => ArticleCategoryTranslationEntity::SUITCASES_EN,
            ArticleCategoryEntity::MIRRORS => ArticleCategoryTranslationEntity::MIRRORS_EN,
            ArticleCategoryEntity::DECORATION_ARTICLES => ArticleCategoryTranslationEntity::DECORATION_ARTICLES_EN,
            ArticleCategoryEntity::BEDROOM => ArticleCategoryTranslationEntity::BEDROOM_EN,
            ArticleCategoryEntity::Commission => ArticleCategoryTranslationEntity::Commission_EN,
            ArticleCategoryEntity::Rental => ArticleCategoryTranslationEntity::RENTAL_EN,
            ArticleCategoryEntity::ADMINISTRATION => ArticleCategoryTranslationEntity::ADMINISTRATION_EN,
        ],
        LocaleEntity::DUTCH => [
            ArticleCategoryEntity::SEATING_FURNITURE => ArticleCategoryTranslationEntity::SEATING_FURNITURE_NL,
            ArticleCategoryEntity::CABINETS => ArticleCategoryTranslationEntity::CABINETS_NL,
            ArticleCategoryEntity::TABLES => ArticleCategoryTranslationEntity::TABLES_NL,
            ArticleCategoryEntity::ANTLER_ARTICLES => ArticleCategoryTranslationEntity::ANTLER_ARTICLES_NL,
            ArticleCategoryEntity::LIGHTING => ArticleCategoryTranslationEntity::LIGHTING_NL,
            ArticleCategoryEntity::TAXIDERMY => ArticleCategoryTranslationEntity::TAXIDERMY_NL,
            ArticleCategoryEntity::SUITCASES => ArticleCategoryTranslationEntity::SUITCASES_NL,
            ArticleCategoryEntity::MIRRORS => ArticleCategoryTranslationEntity::MIRRORS_NL,
            ArticleCategoryEntity::DECORATION_ARTICLES => ArticleCategoryTranslationEntity::DECORATION_ARTICLES_NL,
            ArticleCategoryEntity::BEDROOM => ArticleCategoryTranslationEntity::BEDROOM_NL,
            ArticleCategoryEntity::Commission => ArticleCategoryTranslationEntity::Commission_NL,
            ArticleCategoryEntity::Rental => ArticleCategoryTranslationEntity::RENTAL_NL,
            ArticleCategoryEntity::ADMINISTRATION => ArticleCategoryTranslationEntity::ADMINISTRATION_NL,
        ],
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity", inversedBy="article_category_translations")
     * @ORM\JoinColumn(name="article_category_id", referencedColumnName="id")
     */
    private $article_category;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return ArticleCategoryEntity
     */
    public function getArticleCategory() : ArticleCategoryEntity
    {
        return $this->article_category;
    }
    /**
     * @param ArticleCategoryEntity $article_category
     */
    public function setArticleCategory(ArticleCategoryEntity $article_category)
    {
        $this->article_category = $article_category;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return ArticleCategoryTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * ArticleCategoryEntity constructor.
     */
    public function __construct()
    {

    }
}