<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 2:30 PM
 */

namespace AppBundle\Entity\User\Articles\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="articles_fixed_tables_sub_category_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Articles\FixedTables\ArticleSubCategoryTranslationRepository")
 */
class ArticleSubCategoryTranslationEntity
{
    const DINING_CHAIR_EN = "Dining chair";
    const DINING_CHAIR_NL = "Eetkamerstoel";
    const DINING_CHAIR_GE = "Essensstuhl";
    const COUCHE_EN = "Couche";
    const COUCHE_NL = "Zitbank";
    const COUCHE_GE = "Schicht";
    const ARMCHAIR_EN = "armchair";
    const ARMCHAIR_NL = "Fauteuil";
    const ARMCHAIR_GE = "Sessel";
    const POUF_EN = "Pouf";
    const POUF_NL = "Poef";
    const STOOL_BENCH_EN = "Stool/Bench";
    const STOOL_BENCH_NL = "Krukjes/Banken";
    const BAR_STOOL_EN = "Bar stool";
    const BAR_STOOL_NL = "Barkruk";

    const SIDEBOARD_EN = "Sideboard";
    const SIDEBOARD_NL = "Dressoir";
    const CABINET_EN = "Cabinet";
    const CABINET_NL = "Kast";
    const BEDROOM_DRESSER_EN = "Bedroom dresser";
    const BEDROOM_DRESSER_NL = "Nachtkastje";
    const SIDE_TABLE_EN = "Side table";
    const SIDE_TABLE_NL = "Wandtafel";

    const COFFEE_TABLES_EN = "Coffee tables";
    const COFFEE_TABLES_NL = "Salontafel";
    const DINING_TABLES_EN = "Dining tables";
    const DINING_TABLES_NL = "Eetkamertafel";

    const ANTLER_PENDANT_LIGHT_EN = "Antler Pendant light";
    const ANTLER_PENDANT_LIGHT_NL = "Hanglamp gewei";
    const ANTLER_WALL_LAMP_EN = "Antler wall lamp";
    const ANTLER_WALL_LAMP_NL = "Wandlamp gewei";
    const ANTLER_TABLE_LAMP_EN = "Antler table lamp";
    const ANTLER_TABLE_LAMP_NL = "Schemerlamp gewei";
    const ANTLER_STANDING_LAMP_EN = "Antler standing lamp";
    const ANTLER_STANDING_LAMP_NL = "Vloerlamp gewei";
    const ANTLER_CEILING_LAMP_EN = "Antler ceiling lamp";
    const ANTLER_CEILING_LAMP_NL = "Plafondlamp gewei";
    const ANTLER_CHAMPAGNE_COOLER_EN = "Antler champagne cooler";
    const ANTLER_CHAMPAGNE_COOLER_NL = "Champagne koeler gewei";
    const ANTLER_BOWL_EN = "Antler bowl";
    const ANTLER_BOWL_NL = "Schaal gewei";
    const ANTLER_CANDLE_STICK_EN = "Antler candle stick";
    const ANTLER_CANDLE_STICK_NL = "Kandelaar gewei";
    const ANTLER_SEATING_FURNITURE_EN = "Antler seating furniture";
    const ANTLER_SEATING_FURNITURE_NL = "Zitmeubel gewei";
    const ANTLER_TABLE_EN = "Antler table";
    const ANTLER_TABLE_NL = "Tafel gewei";
    const ANTLER_COAT_RACK_EN = "Antler coat rack";
    const ANTLER_COAT_RACK_NL = "Gewei kapstok";
    const ANTLER_DECORATION_EN = "Antler decoration";
    const ANTLER_DECORATION_NL = "Gewei decoratie";

    const TABLE_LAMP_EN = "Table lamp";
    const TABLE_LAMP_NL = "Tafellamp";
    const WALL_LAMP_EN = "Wall lamp";
    const WALL_LAMP_NL = "Wandlamp";
    const PENDANT_LIGHT_EN = "Pendant light";
    const PENDANT_LIGHT_NL = "Hanglamp";
    const STANDING_LAMP_EN = "Standing lamp";
    const STANDING_LAMP_NL = "Vloerlamp";

    const FULL_MOUNTS_EN = "Fullmounts";
    const FULL_MOUNTS_NL = "Fullmounts";
    const SHOULDER_MOUNTS_EN = "Shoulder mounts";
    const SHOULDER_MOUNTS_NL = "Shoulder mounts";
    const BIRDS_EN = "Birds";
    const BIRDS_NL = "Vogels";
    const INSECTS_EN = "Insects";
    const INSECTS_NL = "Insecten";
    const ANTLER_AND_SKULLS_EN = "Antler & Skulls";
    const ANTLER_AND_SKULLS_NL = "Gewei & Schedels";
    const ANIMAL_HIDES_EN = "Animal hides";
    const ANIMAL_HIDES_NL = "Dierhuiden";
    const VARIOUS_ITEMS_EN = "Various items";
    const VARIOUS_ITEMS_NL = "Diverse naturalia";

    const SUITCASE_EN = "Suitcase";
    const SUITCASE_NL = "Koffer";

    const MIRROR_WOOD_EN = "Mirror wood";
    const MIRROR_WOOD_NL = "Spiegel hout";
    const MIRROR_UPHOLSTERED_EN = "Mirror upholstered";
    const MIRROR_UPHOLSTERED_NL = "Spiegel bekleed";

    const DECORATION_ARTICLE_EN = "Decoration article";
    const DECORATION_ARTICLE_NL = "Decoratie artikel";

    const BUTTERFLIES_AND_INSECTS_EN = "Butterflies & Insects";
    const BUTTERFLIES_AND_INSECTS_NL = "Vlinders & Insecten";
    const ANTLER_MIRROR_EN = "Antler Mirror";
    const ANTLER_MIRROR_NL = "Spiegel gewei";
    const LAMP_SHADE_EN = "Lamp Shade";
    const LAMP_SHADE_NL = "Lampenkap";
    const REPLICAS_EN = "Replica's";
    const REPLICAS_NL = "Replica's";

    const SLEEPING_ROOM_EN = "Sleepingroom";
    const SLEEPING_ROM_NL = "Slaapkamer";

    const Commission_EN = "Commission";
    const Commission_NL = "Commissie";

    const RENTAL_EN = "Rental";
    const RENTAL_NL = "Verhuur";
    const DIBS_EN = "1st dibs";
    const DIBS_NL = "1st dibs";
    const PAYPAL_PAYMENT_EN = "Paypal payment";
    const PAYPAL_PAYMENT_NL = "Paypal betaling";
    const COMPLAIN_EN = "Complain";
    const COMPLAIN_NL = "Klacht";
    const SERVICE_PROVIDED_EN = "Service provided";
    const SERVICE_PROVIDED_NL = "Geleverde diensten";

    const TABLE_ROWS = [
        ['name' => ArticleSubCategoryTranslationEntity::DINING_CHAIR_EN, 'reference' => ArticleSubCategoryEntity::DINING_CHAIR, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::DINING_CHAIR_NL, 'reference' => ArticleSubCategoryEntity::DINING_CHAIR, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::COUCHE_EN, 'reference' => ArticleSubCategoryEntity::COUCHE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::COUCHE_NL, 'reference' => ArticleSubCategoryEntity::COUCHE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ARMCHAIR_EN, 'reference' => ArticleSubCategoryEntity::ARMCHAIR, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ARMCHAIR_NL, 'reference' => ArticleSubCategoryEntity::ARMCHAIR, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::POUF_EN, 'reference' => ArticleSubCategoryEntity::POUF, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::POUF_NL, 'reference' => ArticleSubCategoryEntity::POUF, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::STOOL_BENCH_EN, 'reference' => ArticleSubCategoryEntity::STOOL_BENCH, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::STOOL_BENCH_NL, 'reference' => ArticleSubCategoryEntity::STOOL_BENCH, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::BAR_STOOL_EN, 'reference' => ArticleSubCategoryEntity::BAR_STOOL, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::BAR_STOOL_NL, 'reference' => ArticleSubCategoryEntity::BAR_STOOL, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::SIDEBOARD_EN, 'reference' => ArticleSubCategoryEntity::SIDEBOARD, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::SIDEBOARD_NL, 'reference' => ArticleSubCategoryEntity::SIDEBOARD, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::CABINET_EN, 'reference' => ArticleSubCategoryEntity::CABINET, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::CABINET_NL, 'reference' => ArticleSubCategoryEntity::CABINET, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_EN, 'reference' => ArticleSubCategoryEntity::BEDROOM_DRESSER, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_NL, 'reference' => ArticleSubCategoryEntity::BEDROOM_DRESSER, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::SIDE_TABLE_EN, 'reference' => ArticleSubCategoryEntity::SIDE_TABLE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::SIDE_TABLE_NL, 'reference' => ArticleSubCategoryEntity::SIDE_TABLE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::COFFEE_TABLES_EN, 'reference' => ArticleSubCategoryEntity::COFFEE_TABLES, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::COFFEE_TABLES_NL, 'reference' => ArticleSubCategoryEntity::COFFEE_TABLES, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::DINING_TABLES_EN, 'reference' => ArticleSubCategoryEntity::DINING_TABLES, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::DINING_TABLES_NL, 'reference' => ArticleSubCategoryEntity::DINING_TABLES, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_PENDANT_LIGHT_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_PENDANT_LIGHT, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_PENDANT_LIGHT_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_PENDANT_LIGHT, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_WALL_LAMP_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_WALL_LAMP, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_WALL_LAMP_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_WALL_LAMP, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_STANDING_LAMP_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_STANDING_LAMP, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_STANDING_LAMP_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_STANDING_LAMP, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_CEILING_LAMP_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_CEILING_LAMP, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_CEILING_LAMP_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_CEILING_LAMP, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_CHAMPAGNE_COOLER_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_CHAMPAGNE_COOLER, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_CHAMPAGNE_COOLER_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_CHAMPAGNE_COOLER, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_BOWL_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_BOWL, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_BOWL_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_BOWL, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_CANDLE_STICK_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_CANDLE_STICK, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_CANDLE_STICK_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_CANDLE_STICK, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_SEATING_FURNITURE_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_SEATING_FURNITURE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_SEATING_FURNITURE_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_SEATING_FURNITURE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_TABLE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_TABLE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_COAT_RACK_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_COAT_RACK, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_COAT_RACK_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_COAT_RACK, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_DECORATION_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_DECORATION, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_DECORATION_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_DECORATION, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_LAMP_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_TABLE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_LAMP_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_TABLE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::TABLE_LAMP_EN, 'reference' => ArticleSubCategoryEntity::TABLE_LAMP, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::TABLE_LAMP_NL, 'reference' => ArticleSubCategoryEntity::TABLE_LAMP, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::WALL_LAMP_EN, 'reference' => ArticleSubCategoryEntity::WALL_LAMP, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::WALL_LAMP_NL, 'reference' => ArticleSubCategoryEntity::WALL_LAMP, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::PENDANT_LIGHT_EN, 'reference' => ArticleSubCategoryEntity::PENDANT_LIGHT, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::PENDANT_LIGHT_NL, 'reference' => ArticleSubCategoryEntity::PENDANT_LIGHT, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::STANDING_LAMP_EN, 'reference' => ArticleSubCategoryEntity::STANDING_LAMP, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::STANDING_LAMP_NL, 'reference' => ArticleSubCategoryEntity::STANDING_LAMP, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::FULL_MOUNTS_EN, 'reference' => ArticleSubCategoryEntity::FULL_MOUNTS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::FULL_MOUNTS_NL, 'reference' => ArticleSubCategoryEntity::FULL_MOUNTS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::SHOULDER_MOUNTS_EN, 'reference' => ArticleSubCategoryEntity::SHOULDER_MOUNTS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::SHOULDER_MOUNTS_NL, 'reference' => ArticleSubCategoryEntity::SHOULDER_MOUNTS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::BIRDS_EN, 'reference' => ArticleSubCategoryEntity::BIRDS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::BIRDS_NL, 'reference' => ArticleSubCategoryEntity::BIRDS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::INSECTS_EN, 'reference' => ArticleSubCategoryEntity::INSECTS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::INSECTS_NL, 'reference' => ArticleSubCategoryEntity::INSECTS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_AND_SKULLS_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_AND_SKULLS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_AND_SKULLS_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_AND_SKULLS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANIMAL_HIDES_EN, 'reference' => ArticleSubCategoryEntity::ANIMAL_HIDES, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANIMAL_HIDES_NL, 'reference' => ArticleSubCategoryEntity::ANIMAL_HIDES, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::VARIOUS_ITEMS_EN, 'reference' => ArticleSubCategoryEntity::VARIOUS_ITEMS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::VARIOUS_ITEMS_NL, 'reference' => ArticleSubCategoryEntity::VARIOUS_ITEMS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::SUITCASE_EN, 'reference' => ArticleSubCategoryEntity::SUITCASE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::SUITCASE_NL, 'reference' => ArticleSubCategoryEntity::SUITCASE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::MIRROR_WOOD_EN, 'reference' => ArticleSubCategoryEntity::MIRROR_WOOD, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::MIRROR_WOOD_NL, 'reference' => ArticleSubCategoryEntity::MIRROR_WOOD, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::MIRROR_UPHOLSTERED_EN, 'reference' => ArticleSubCategoryEntity::MIRROR_UPHOLSTERED, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::MIRROR_UPHOLSTERED_NL, 'reference' => ArticleSubCategoryEntity::MIRROR_UPHOLSTERED, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::DECORATION_ARTICLE_EN, 'reference' => ArticleSubCategoryEntity::DECORATION_ARTICLE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::DECORATION_ARTICLE_NL, 'reference' => ArticleSubCategoryEntity::DECORATION_ARTICLE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::BUTTERFLIES_AND_INSECTS_EN, 'reference' => ArticleSubCategoryEntity::BUTTERFLIES_AND_INSECTS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::BUTTERFLIES_AND_INSECTS_NL, 'reference' => ArticleSubCategoryEntity::BUTTERFLIES_AND_INSECTS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_MIRROR_EN, 'reference' => ArticleSubCategoryEntity::ANTLER_MIRROR, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::ANTLER_MIRROR_NL, 'reference' => ArticleSubCategoryEntity::ANTLER_MIRROR, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::LAMP_SHADE_EN, 'reference' => ArticleSubCategoryEntity::LAMP_SHADE, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::LAMP_SHADE_NL, 'reference' => ArticleSubCategoryEntity::LAMP_SHADE, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::REPLICAS_EN, 'reference' => ArticleSubCategoryEntity::REPLICAS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::REPLICAS_NL, 'reference' => ArticleSubCategoryEntity::REPLICAS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_EN, 'reference' => ArticleSubCategoryEntity::BEDROOM_DRESSER, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_NL, 'reference' => ArticleSubCategoryEntity::BEDROOM_DRESSER, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::Commission_EN, 'reference' => ArticleSubCategoryEntity::Commission, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::Commission_NL, 'reference' => ArticleSubCategoryEntity::Commission, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::RENTAL_EN, 'reference' => ArticleSubCategoryEntity::Rental, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::RENTAL_NL, 'reference' => ArticleSubCategoryEntity::Rental, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::DIBS_EN, 'reference' => ArticleSubCategoryEntity::DIBS, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::DIBS_NL, 'reference' => ArticleSubCategoryEntity::DIBS, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::PAYPAL_PAYMENT_EN, 'reference' => ArticleSubCategoryEntity::PAYPAL_PAYMENT, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::PAYPAL_PAYMENT_NL, 'reference' => ArticleSubCategoryEntity::PAYPAL_PAYMENT, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::COMPLAIN_EN, 'reference' => ArticleSubCategoryEntity::COMPLAIN, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::COMPLAIN_NL, 'reference' => ArticleSubCategoryEntity::COMPLAIN, 'language' => 'nl'],
        ['name' => ArticleSubCategoryTranslationEntity::SERVICE_PROVIDED_EN, 'reference' => ArticleSubCategoryEntity::SERVICE_PROVIDED, 'language' => 'en'],
        ['name' => ArticleSubCategoryTranslationEntity::SERVICE_PROVIDED_NL, 'reference' => ArticleSubCategoryEntity::SERVICE_PROVIDED, 'language' => 'nl'],

    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            ArticleSubCategoryEntity::DINING_CHAIR => ArticleSubCategoryTranslationEntity::DINING_CHAIR_EN,
            ArticleSubCategoryEntity::COUCHE => ArticleSubCategoryTranslationEntity::COUCHE_EN,
            ArticleSubCategoryEntity::ARMCHAIR => ArticleSubCategoryTranslationEntity::ARMCHAIR_EN,
            ArticleSubCategoryEntity::POUF => ArticleSubCategoryTranslationEntity::POUF_EN,
            ArticleSubCategoryEntity::STOOL_BENCH => ArticleSubCategoryTranslationEntity::STOOL_BENCH_EN,
            ArticleSubCategoryEntity::BAR_STOOL => ArticleSubCategoryTranslationEntity::BAR_STOOL_EN,
            ArticleSubCategoryEntity::SIDEBOARD => ArticleSubCategoryTranslationEntity::SIDEBOARD_EN,
            ArticleSubCategoryEntity::CABINET => ArticleSubCategoryTranslationEntity::CABINET_EN,
            ArticleSubCategoryEntity::BEDROOM_DRESSER => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_EN,
            ArticleSubCategoryEntity::SIDE_TABLE => ArticleSubCategoryTranslationEntity::SIDE_TABLE_EN,
            ArticleSubCategoryEntity::COFFEE_TABLES => ArticleSubCategoryTranslationEntity::COFFEE_TABLES_EN,
            ArticleSubCategoryEntity::DINING_TABLES => ArticleSubCategoryTranslationEntity::DINING_TABLES_EN,
            ArticleSubCategoryEntity::ANTLER_PENDANT_LIGHT => ArticleSubCategoryTranslationEntity::ANTLER_PENDANT_LIGHT_EN,
            ArticleSubCategoryEntity::ANTLER_WALL_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_WALL_LAMP_EN,
            ArticleSubCategoryEntity::ANTLER_STANDING_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_STANDING_LAMP_EN,
            ArticleSubCategoryEntity::ANTLER_CEILING_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_CEILING_LAMP_EN,
            ArticleSubCategoryEntity::ANTLER_CHAMPAGNE_COOLER => ArticleSubCategoryTranslationEntity::ANTLER_CHAMPAGNE_COOLER_EN,
            ArticleSubCategoryEntity::ANTLER_BOWL => ArticleSubCategoryTranslationEntity::ANTLER_BOWL_EN,
            ArticleSubCategoryEntity::ANTLER_CANDLE_STICK => ArticleSubCategoryTranslationEntity::ANTLER_CANDLE_STICK_EN,
            ArticleSubCategoryEntity::ANTLER_SEATING_FURNITURE => ArticleSubCategoryTranslationEntity::ANTLER_SEATING_FURNITURE_EN,
            ArticleSubCategoryEntity::ANTLER_TABLE => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_EN,
            ArticleSubCategoryEntity::ANTLER_COAT_RACK => ArticleSubCategoryTranslationEntity::ANTLER_COAT_RACK_EN,
            ArticleSubCategoryEntity::ANTLER_DECORATION => ArticleSubCategoryTranslationEntity::ANTLER_DECORATION_EN,
            ArticleSubCategoryEntity::ANTLER_TABLE_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_LAMP_EN,
            ArticleSubCategoryEntity::TABLE_LAMP => ArticleSubCategoryTranslationEntity::TABLE_LAMP_EN,
            ArticleSubCategoryEntity::WALL_LAMP => ArticleSubCategoryTranslationEntity::WALL_LAMP_EN,
            ArticleSubCategoryEntity::PENDANT_LIGHT => ArticleSubCategoryTranslationEntity::PENDANT_LIGHT_EN,
            ArticleSubCategoryEntity::STANDING_LAMP => ArticleSubCategoryTranslationEntity::STANDING_LAMP_EN,
            ArticleSubCategoryEntity::FULL_MOUNTS => ArticleSubCategoryTranslationEntity::FULL_MOUNTS_EN,
            ArticleSubCategoryEntity::SHOULDER_MOUNTS => ArticleSubCategoryTranslationEntity::SHOULDER_MOUNTS_EN,
            ArticleSubCategoryEntity::BIRDS => ArticleSubCategoryTranslationEntity::BIRDS_EN,
            ArticleSubCategoryEntity::INSECTS => ArticleSubCategoryTranslationEntity::INSECTS_EN,
            ArticleSubCategoryEntity::ANTLER_AND_SKULLS => ArticleSubCategoryTranslationEntity::ANTLER_AND_SKULLS_EN,
            ArticleSubCategoryEntity::ANIMAL_HIDES => ArticleSubCategoryTranslationEntity::ANIMAL_HIDES_EN,
            ArticleSubCategoryEntity::VARIOUS_ITEMS => ArticleSubCategoryTranslationEntity::VARIOUS_ITEMS_EN,
            ArticleSubCategoryEntity::SUITCASE => ArticleSubCategoryTranslationEntity::SUITCASE_EN,
            ArticleSubCategoryEntity::MIRROR_WOOD => ArticleSubCategoryTranslationEntity::MIRROR_WOOD_EN,
            ArticleSubCategoryEntity::MIRROR_UPHOLSTERED => ArticleSubCategoryTranslationEntity::MIRROR_UPHOLSTERED_EN,
            ArticleSubCategoryEntity::DECORATION_ARTICLE => ArticleSubCategoryTranslationEntity::DECORATION_ARTICLE_EN,
            ArticleSubCategoryEntity::BUTTERFLIES_AND_INSECTS => ArticleSubCategoryTranslationEntity::BUTTERFLIES_AND_INSECTS_EN,
            ArticleSubCategoryEntity::ANTLER_MIRROR => ArticleSubCategoryTranslationEntity::ANTLER_MIRROR_EN,
            ArticleSubCategoryEntity::LAMP_SHADE => ArticleSubCategoryTranslationEntity::LAMP_SHADE_EN,
            ArticleSubCategoryEntity::REPLICAS => ArticleSubCategoryTranslationEntity::REPLICAS_EN,
            ArticleSubCategoryEntity::BEDROOM_DRESSER => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_EN,
            ArticleSubCategoryEntity::Commission => ArticleSubCategoryTranslationEntity::Commission_EN,
            ArticleSubCategoryEntity::Rental => ArticleSubCategoryTranslationEntity::RENTAL_EN,
            ArticleSubCategoryEntity::DIBS => ArticleSubCategoryTranslationEntity::DIBS_EN,
            ArticleSubCategoryEntity::PAYPAL_PAYMENT => ArticleSubCategoryTranslationEntity::PAYPAL_PAYMENT_EN,
            ArticleSubCategoryEntity::COMPLAIN => ArticleSubCategoryTranslationEntity::COMPLAIN_EN,
            ArticleSubCategoryEntity::SERVICE_PROVIDED => ArticleSubCategoryTranslationEntity::SERVICE_PROVIDED_EN,
        ],
        LocaleEntity::DUTCH => [
            ArticleSubCategoryEntity::DINING_CHAIR => ArticleSubCategoryTranslationEntity::DINING_CHAIR_NL,
            ArticleSubCategoryEntity::COUCHE => ArticleSubCategoryTranslationEntity::COUCHE_NL,
            ArticleSubCategoryEntity::ARMCHAIR => ArticleSubCategoryTranslationEntity::ARMCHAIR_NL,
            ArticleSubCategoryEntity::POUF => ArticleSubCategoryTranslationEntity::POUF_NL,
            ArticleSubCategoryEntity::STOOL_BENCH => ArticleSubCategoryTranslationEntity::STOOL_BENCH_NL,
            ArticleSubCategoryEntity::BAR_STOOL => ArticleSubCategoryTranslationEntity::BAR_STOOL_NL,
            ArticleSubCategoryEntity::SIDEBOARD => ArticleSubCategoryTranslationEntity::SIDEBOARD_NL,
            ArticleSubCategoryEntity::CABINET => ArticleSubCategoryTranslationEntity::CABINET_NL,
            ArticleSubCategoryEntity::BEDROOM_DRESSER => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_NL,
            ArticleSubCategoryEntity::SIDE_TABLE => ArticleSubCategoryTranslationEntity::SIDE_TABLE_NL,
            ArticleSubCategoryEntity::COFFEE_TABLES => ArticleSubCategoryTranslationEntity::COFFEE_TABLES_NL,
            ArticleSubCategoryEntity::DINING_TABLES => ArticleSubCategoryTranslationEntity::DINING_TABLES_NL,
            ArticleSubCategoryEntity::ANTLER_PENDANT_LIGHT => ArticleSubCategoryTranslationEntity::ANTLER_PENDANT_LIGHT_NL,
            ArticleSubCategoryEntity::ANTLER_WALL_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_WALL_LAMP_NL,
            ArticleSubCategoryEntity::ANTLER_STANDING_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_STANDING_LAMP_NL,
            ArticleSubCategoryEntity::ANTLER_CEILING_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_CEILING_LAMP_NL,
            ArticleSubCategoryEntity::ANTLER_CHAMPAGNE_COOLER => ArticleSubCategoryTranslationEntity::ANTLER_CHAMPAGNE_COOLER_NL,
            ArticleSubCategoryEntity::ANTLER_BOWL => ArticleSubCategoryTranslationEntity::ANTLER_BOWL_NL,
            ArticleSubCategoryEntity::ANTLER_CANDLE_STICK => ArticleSubCategoryTranslationEntity::ANTLER_CANDLE_STICK_NL,
            ArticleSubCategoryEntity::ANTLER_SEATING_FURNITURE => ArticleSubCategoryTranslationEntity::ANTLER_SEATING_FURNITURE_NL,
            ArticleSubCategoryEntity::ANTLER_TABLE => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_NL,
            ArticleSubCategoryEntity::ANTLER_COAT_RACK => ArticleSubCategoryTranslationEntity::ANTLER_COAT_RACK_NL,
            ArticleSubCategoryEntity::ANTLER_DECORATION => ArticleSubCategoryTranslationEntity::ANTLER_DECORATION_NL,
            ArticleSubCategoryEntity::ANTLER_TABLE_LAMP => ArticleSubCategoryTranslationEntity::ANTLER_TABLE_LAMP_NL,
            ArticleSubCategoryEntity::TABLE_LAMP => ArticleSubCategoryTranslationEntity::TABLE_LAMP_NL,
            ArticleSubCategoryEntity::WALL_LAMP => ArticleSubCategoryTranslationEntity::WALL_LAMP_NL,
            ArticleSubCategoryEntity::PENDANT_LIGHT => ArticleSubCategoryTranslationEntity::PENDANT_LIGHT_NL,
            ArticleSubCategoryEntity::STANDING_LAMP => ArticleSubCategoryTranslationEntity::STANDING_LAMP_NL,
            ArticleSubCategoryEntity::FULL_MOUNTS => ArticleSubCategoryTranslationEntity::FULL_MOUNTS_NL,
            ArticleSubCategoryEntity::SHOULDER_MOUNTS => ArticleSubCategoryTranslationEntity::SHOULDER_MOUNTS_NL,
            ArticleSubCategoryEntity::BIRDS => ArticleSubCategoryTranslationEntity::BIRDS_NL,
            ArticleSubCategoryEntity::INSECTS => ArticleSubCategoryTranslationEntity::INSECTS_NL,
            ArticleSubCategoryEntity::ANTLER_AND_SKULLS => ArticleSubCategoryTranslationEntity::ANTLER_AND_SKULLS_NL,
            ArticleSubCategoryEntity::ANIMAL_HIDES => ArticleSubCategoryTranslationEntity::ANIMAL_HIDES_NL,
            ArticleSubCategoryEntity::VARIOUS_ITEMS => ArticleSubCategoryTranslationEntity::VARIOUS_ITEMS_NL,
            ArticleSubCategoryEntity::SUITCASE => ArticleSubCategoryTranslationEntity::SUITCASE_NL,
            ArticleSubCategoryEntity::MIRROR_WOOD => ArticleSubCategoryTranslationEntity::MIRROR_WOOD_NL,
            ArticleSubCategoryEntity::MIRROR_UPHOLSTERED => ArticleSubCategoryTranslationEntity::MIRROR_UPHOLSTERED_NL,
            ArticleSubCategoryEntity::DECORATION_ARTICLE => ArticleSubCategoryTranslationEntity::DECORATION_ARTICLE_NL,
            ArticleSubCategoryEntity::BUTTERFLIES_AND_INSECTS => ArticleSubCategoryTranslationEntity::BUTTERFLIES_AND_INSECTS_NL,
            ArticleSubCategoryEntity::ANTLER_MIRROR => ArticleSubCategoryTranslationEntity::ANTLER_MIRROR_NL,
            ArticleSubCategoryEntity::LAMP_SHADE => ArticleSubCategoryTranslationEntity::LAMP_SHADE_NL,
            ArticleSubCategoryEntity::REPLICAS => ArticleSubCategoryTranslationEntity::REPLICAS_NL,
            ArticleSubCategoryEntity::BEDROOM_DRESSER => ArticleSubCategoryTranslationEntity::BEDROOM_DRESSER_NL,
            ArticleSubCategoryEntity::Commission => ArticleSubCategoryTranslationEntity::Commission_NL,
            ArticleSubCategoryEntity::Rental => ArticleSubCategoryTranslationEntity::RENTAL_NL,
            ArticleSubCategoryEntity::DIBS => ArticleSubCategoryTranslationEntity::DIBS_NL,
            ArticleSubCategoryEntity::PAYPAL_PAYMENT => ArticleSubCategoryTranslationEntity::PAYPAL_PAYMENT_NL,
            ArticleSubCategoryEntity::COMPLAIN => ArticleSubCategoryTranslationEntity::Commission_NL,
            ArticleSubCategoryEntity::SERVICE_PROVIDED => ArticleSubCategoryTranslationEntity::SERVICE_PROVIDED_NL,
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity", inversedBy="article_sub_category_translations")
     * @ORM\JoinColumn(name="article_sub_category_id", referencedColumnName="id")
     */
    private $article_sub_category;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return ArticleSubCategoryEntity
     */
    public function getArticleSubCategory() : ArticleSubCategoryEntity
    {
        return $this->article_sub_category;
    }
    /**
     * @param ArticleSubCategoryEntity $article_sub_category
     */
    public function setArticleSubCategory(ArticleSubCategoryEntity $article_sub_category)
    {
        $this->article_sub_category = $article_sub_category;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @param mixed $articles
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return ArticleSubCategoryTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * ArticleSubCategoryEntity constructor.
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
    }
}