<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 12:10 PM
 */

namespace AppBundle\Entity\User\Articles\FixedTables;

use AppBundle\Entity\User\Articles\ArticleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="articles_fixed_tables_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Articles\FixedTables\ArticleCategoryRepository")
 */
class ArticleCategoryEntity
{
    const SEATING_FURNITURE = "seating furniture";
    const CABINETS = "cabinets";
    const TABLES = "tables";
    const ANTLER_ARTICLES = "antler articles";
    const LIGHTING = "lighting";
    const TAXIDERMY = "taxidermy";
    const SUITCASES = "suitcases";
    const MIRRORS = "mirrors";
    const DECORATION_ARTICLES = "decoration articles";
    const BEDROOM = "bedroom";
    const Commission = "commission";
    const Rental = "rental";
    const ADMINISTRATION = "administration";

    const CATEGORIES = [
        ArticleCategoryEntity::SEATING_FURNITURE,
        ArticleCategoryEntity::CABINETS,
        ArticleCategoryEntity::TABLES,
        ArticleCategoryEntity::ANTLER_ARTICLES,
        ArticleCategoryEntity::LIGHTING,
        ArticleCategoryEntity::TAXIDERMY,
        ArticleCategoryEntity::SUITCASES,
        ArticleCategoryEntity::MIRRORS,
        ArticleCategoryEntity::DECORATION_ARTICLES,
        ArticleCategoryEntity::BEDROOM,
        ArticleCategoryEntity::Commission,
        ArticleCategoryEntity::Rental,
        ArticleCategoryEntity::ADMINISTRATION
    ];
    const TABLE_ROWS = [
        ['name' => ArticleCategoryEntity::SEATING_FURNITURE],
        ['name' => ArticleCategoryEntity::CABINETS],
        ['name' => ArticleCategoryEntity::TABLES],
        ['name' => ArticleCategoryEntity::ANTLER_ARTICLES],
        ['name' => ArticleCategoryEntity::LIGHTING],
        ['name' => ArticleCategoryEntity::TAXIDERMY],
        ['name' => ArticleCategoryEntity::SUITCASES],
        ['name' => ArticleCategoryEntity::MIRRORS],
        ['name' => ArticleCategoryEntity::DECORATION_ARTICLES],
        ['name' => ArticleCategoryEntity::BEDROOM],
        ['name' => ArticleCategoryEntity::Commission],
        ['name' => ArticleCategoryEntity::Rental],
        ['name' => ArticleCategoryEntity::ADMINISTRATION],
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=30, unique=true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity", mappedBy="article_category")
     */
    private $article_sub_categories;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryTranslationEntity", mappedBy="article_category")
     */
    private $article_category_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Articles\ArticleEntity", mappedBy="article_category")
     */
    private $articles;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @param mixed $article_sub_categories
     */
    public function setArticleSubCategories($article_sub_categories)
    {
        $this->article_sub_categories = $article_sub_categories;
    }
    /**
     * @param mixed $article_category_translations
     */
    public function setArticleCategoryTranslations($article_category_translations)
    {
        $this->article_category_translations = $article_category_translations;
    }
    /**
     * @param mixed $articles
     */
    public function setArticles($articles)
    {
        $this->articles = $articles;
    }
    /**
     * ArticleCategoryEntity constructor.
     */
    public function __construct()
    {
        $this->articles = new ArrayCollection();
        $this->article_subcategories = new ArrayCollection();
        $this->article_category_translations = new ArrayCollection();
    }
}