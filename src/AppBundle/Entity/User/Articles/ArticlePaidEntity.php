<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/4/2018
 * Time: 5:01 PM
 */

namespace AppBundle\Entity\User\Articles;

use AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity;
use AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity;
use AppBundle\Entity\User\Orders\OrderArticleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="articles_paid",
 *     indexes={
 *          @ORM\Index(
 *              name="search_created",
 *              columns={"created"}
 *          ),
 *          @ORM\Index(
 *              name="search_category_sub_category_created",
 *              columns={"category_id", "sub_category_id", "created"}
 *          ),
 *      },
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Articles\ArticlePaidRepository")
 */
class ArticlePaidEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="float")
     */
    private $purchase_price;
    /**
     * @ORM\Column(type="float")
     */
    private $selling_price;
    /**
     * @ORM\Column(type="float")
     */
    private $profit_percents;
    /**
     * @ORM\Column(type="integer")
     */
    private $number_of_articles;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\ArticleEntity", inversedBy="articles_paid")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleCategoryEntity")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\FixedTables\ArticleSubCategoryEntity")
     * @ORM\JoinColumn(name="sub_category_id", referencedColumnName="id")
     */
    private $sub_category;
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User\Orders\OrderArticleEntity")
     * @ORM\JoinColumn(name="order_article_id", referencedColumnName="id")
     */
    private $order_article;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return float
     */
    public function getPurchasePrice() : float
    {
        return $this->purchase_price;
    }
    /**
     * @param float $purchase_price
     */
    public function setPurchasePrice(float $purchase_price)
    {
        $this->purchase_price = $purchase_price;
    }
    /**
     * @return float
     */
    public function getSellingPrice() : float
    {
        return $this->selling_price;
    }

    /**
     * @param float $selling_price
     */
    public function setSellingPrice(float $selling_price)
    {
        $this->selling_price = $selling_price;
    }
    /**
     * @return float
     */
    public function getProfitPercents() : float
    {
        return $this->profit_percents;
    }
    /**
     * @param float $profit_percents
     */
    public function setProfitPercents(float $profit_percents)
    {
        $this->profit_percents = $profit_percents;
    }
    /**
     * @return int
     */
    public function getNumberOfArticles() : int
    {
        return $this->number_of_articles;
    }
    /**
     * @param int $number_of_articles
     */
    public function setNumberOfArticles(int $number_of_articles)
    {
        $this->number_of_articles = $number_of_articles;
    }
    /**
     * @return ArticleEntity
     */
    public function getArticle() : ArticleEntity
    {
        return $this->article;
    }
    /**
     * @param ArticleEntity $article
     */
    public function setArticle(ArticleEntity $article)
    {
        $this->article = $article;
    }
    /**
     * @return ArticleCategoryEntity
     */
    public function getCategory() : ArticleCategoryEntity
    {
        return $this->category;
    }
    /**
     * @param ArticleCategoryEntity $category
     */
    public function setCategory(ArticleCategoryEntity $category)
    {
        $this->category = $category;
    }
    /**
     * @return mixed
     */
    public function getSubCategory()
    {
        return $this->sub_category;
    }
    /**
     * @param mixed $sub_category
     */
    public function setSubCategory($sub_category)
    {
        $this->sub_category = $sub_category;
    }
    /**
     * @return OrderArticleEntity
     */
    public function getOrderArticle() : OrderArticleEntity
    {
        return $this->order_article;
    }
    /**
     * @param OrderArticleEntity $order_article
     */
    public function setOrderArticle(OrderArticleEntity $order_article)
    {
        $this->order_article = $order_article;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }


}