<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 2/12/2018
 * Time: 2:53 PM
 */

namespace AppBundle\Entity\User\Orders;

use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\ArticlePaidEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="order_invoice_numbers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\InvoiceNumberRepository")
 */
class InvoiceNumbers
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $private_invoice_number;
    /**
     * @ORM\Column(type="integer")
     */
    private $company_invoice_number;
    /**
     * @ORM\Column(type="integer")
     */
    private $order_number;
    /**
     * @ORM\Column(type="string", length=8)
     */
    private $order_numbering_year;
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return int
     */
    public function getPrivateInvoiceNumber() : int
    {
        return $this->private_invoice_number;
    }
    /**
     * @param int $private_invoice_number
     */
    public function setPrivateInvoiceNumber(int $private_invoice_number)
    {
        $this->private_invoice_number = $private_invoice_number;
    }
    /**
     * @return int
     */
    public function getCompanyInvoiceNumber() : int
    {
        return $this->company_invoice_number;
    }
    /**
     * @param int $company_invoice_number
     */
    public function setCompanyInvoiceNumber(int $company_invoice_number)
    {
        $this->company_invoice_number = $company_invoice_number;
    }
    /**
     * @return int
     */
    public function getOrderNumber() : int
    {
        return $this->order_number;
    }
    /**
     * @param int $order_number
     */
    public function setOrderNumber(int $order_number)
    {
        $this->order_number = $order_number;
    }
    /**
     * @return string
     */
    public function getOrderNumberingYear() : string
    {
        return $this->order_numbering_year;
    }
    /**
     * @param string $order_numbering_year
     */
    public function setOrderNumberingYear(string $order_numbering_year)
    {
        $this->order_numbering_year = $order_numbering_year;
    }
}