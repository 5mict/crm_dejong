<?php
namespace AppBundle\Entity\User\Orders;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="order_mail_attachment_files")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\OrderMailAttachmentFileRepository")
 */
class OrderMailAttachmentFileEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $original_name;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $file_name;
    /**
     * @ORM\ManyToOne(targetEntity="OrderMailEntity", inversedBy="order_attachment_file")
     * @ORM\JoinColumn(name="order_mail_id", referencedColumnName="id")
     */
    private $order_mail;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->file_name;
    }

    /**
     * @param mixed $file_name
     */
    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @return OrderMailEntity
     */
    public function getOrderMail() : OrderMailEntity
    {
        return $this->order_mail;
    }

    /**
     * @param OrderMailEntity $order_mail
     */
    public function setOrderMail(OrderMailEntity $order_mail)
    {
        $this->order_mail = $order_mail;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getCreatedString() : string
    {
        return $this->created->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getOriginalName()
    {
        return $this->original_name;
    }

    /**
     * @param mixed $original_name
     */
    public function setOriginalName($original_name)
    {
        $this->original_name = $original_name;
    }
}