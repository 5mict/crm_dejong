<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/25/2017
 * Time: 2:59 PM
 */

namespace AppBundle\Entity\User\Orders;

use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_mail",
 *     indexes = {
 *          @ORM\Index(
 *              name="search_order_article_mail_status",
 *              columns={"order_article_id", "order_mail_status_id"}
 *          ),
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\OrderMailRepository")
 */
class OrderMailEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $send_to;
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $mail_subject;
    /**
     * @ORM\Column(type="string", length=20000)
     */
    private $mail_body;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $attachment_file_name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity")
     * @ORM\JoinColumn(name="order_mail_status_id", referencedColumnName="id")
     */
    private $order_mail_status;
    /**
     * @ORM\ManyToOne(targetEntity="OrderEntity", inversedBy="order_mails")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;
    /**
     * @ORM\OneToMany(targetEntity="OrderMailAttachmentFileEntity", mappedBy="order_mail")
     */
    private $order_attachment_file;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\OrderArticleEntity", inversedBy="order_article_mails")
     * @ORM\JoinColumn(name="order_article_id", referencedColumnName="id")
     */
    private $order_article;
    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $order_details;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getSendTo()
    {
        return $this->send_to;
    }
    /**
     * @param mixed $send_to
     */
    public function setSendTo($send_to)
    {
        $this->send_to = $send_to;
    }
    /**
     * @return string
     */
    public function getMailSubject()  : string
    {
        return $this->mail_subject;
    }
    /**
     * @param string $mail_subject
     */
    public function setMailSubject(string $mail_subject)
    {
        $this->mail_subject = $mail_subject;
    }
    /**
     * @return string
     */
    public function getMailBody()  : string
    {
        return $this->mail_body;
    }
    /**
     * @param string $mail_body
     */
    public function setMailBody(string $mail_body)
    {
        $this->mail_body = $mail_body;
    }
    /**
     * @return string
     */
    public function getAttachmentFileName() : string
    {
        return $this->attachment_file_name;
    }
    /**
     * @param string $attachment_file_name
     */
    public function setAttachmentFileName(string $attachment_file_name)
    {
        $this->attachment_file_name = $attachment_file_name;
    }
    /**
     * @return OrderMailStatusEntity
     */
    public function getOrderMailStatus() : OrderMailStatusEntity
    {
        return $this->order_mail_status;
    }
    /**
     * @param OrderMailStatusEntity $order_mail_status
     */
    public function setOrderMailStatus(OrderMailStatusEntity $order_mail_status)
    {
        $this->order_mail_status = $order_mail_status;
    }
    /**
     * @return OrderEntity
     */
    public function getOrder() : OrderEntity
    {
        return $this->order;
    }
    /**
     * @param OrderEntity $order
     */
    public function setOrder(OrderEntity $order)
    {
        $this->order = $order;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @return string
     */
    public function getCreatedString() : string
    {
        return $this->created->format('Y-m-d H:i:s');
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return OrderArticleEntity
     */
    public function getOrderArticle() : OrderArticleEntity
    {
        return $this->order_article;
    }

    /**
     * @param OrderArticleEntity $order_article
     */
    public function setOrderArticle(OrderArticleEntity $order_article)
    {
        $this->order_article = $order_article;
    }

    /**
     * @return mixed
     */
    public function getOrderDetails()
    {
        return $this->order_details;
    }

    /**
     * @param mixed $order_details
     */
    public function setOrderDetails($order_details)
    {
        $this->order_details = $order_details;
    }

    /**
     * @return OrderMailAttachmentFileEntity
     */
    public function getOrderAttachmentFile() :OrderMailAttachmentFileEntity
    {
        return $this->order_attachment_file;
    }

    /**
     * @param mixed $order_attachment_file
     */
    public function setOrderAttachmentFile($order_attachment_file)
    {
        $this->order_attachment_file = $order_attachment_file;
    }
    /**
     * OrderMailEntity constructor.
     */
    public function __construct()
    {
        $this->order_details = "";
    }
}