<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/10/2017
 * Time: 4:37 PM
 */

namespace AppBundle\Entity\User\Orders;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_intern_note",
 *     indexes={
 *          @ORM\Index(
 *              name="search_updated",
 *              columns={"updated"}
 *          )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\InternNoteRepository")
 */
class InternNoteEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="OrderEntity", inversedBy="intern_notes")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $order;

    /**
     * @ORM\Column(type="string", length=9000)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;

    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $updated;

    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }

    /**
     * @return string
     */
    public function getUpdatedString() : string
    {
        return $this->updated->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param OrderEntity $order
     */
    public function setOrder(OrderEntity $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote(string $note)
    {
        $this->note = $note;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

}