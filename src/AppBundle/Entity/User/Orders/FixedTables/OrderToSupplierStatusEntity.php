<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/11/2018
 * Time: 9:03 PM
 */

namespace AppBundle\Entity\User\Orders\FixedTables;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_to_supplier_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderToSupplierStatusRepository")
 */
class OrderToSupplierStatusEntity
{
    // collected
    // sent
    // deliver
    // finished
    const NEW = "new";
    const COLLECTED = "collected";
    const SENT = "sent";
    const DELIVER = "deliver";
    const FINISHED = "finished";
    const TABLE_ROWS = [
        ['name' => OrderToSupplierStatusEntity::NEW],
        ['name' => OrderToSupplierStatusEntity::COLLECTED],
        ['name' => OrderToSupplierStatusEntity::SENT],
        ['name' => OrderToSupplierStatusEntity::DELIVER],
        ['name' => OrderToSupplierStatusEntity::FINISHED],
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique = true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusTranslationEntity", mappedBy="order_to_supplier_status")
     */
    private $order_to_supplier_status_translations;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $order_to_supplier_status_translations
     */
    public function setOrderToSupplierStatusTranslations($order_to_supplier_status_translations)
    {
        $this->order_to_supplier_status_translations = $order_to_supplier_status_translations;
    }

    /**
     * OrderMailStatusEntity constructor.
     */
    public function __construct()
    {
        $this->order_mail_status_translations = new ArrayCollection();
    }
}