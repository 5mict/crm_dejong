<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 11:29 AM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_payment_method")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderPaymentMethodRepository")
 */
class OrderPaymentMethodEntity
{
    const CREDIT_CARD = 'credit card';
    const PIN = "pin";
    const BANK = "bank";
    const CASH = "cash";
    const CASH_ON_DELIVERY = "cash on delivery";
    const TABLE_ROWS = [
        ['name' => OrderPaymentMethodEntity::CREDIT_CARD],
        ['name' => OrderPaymentMethodEntity::PIN],
        ['name' => OrderPaymentMethodEntity::BANK],
        ['name' => OrderPaymentMethodEntity::CASH],
        ['name' => OrderPaymentMethodEntity::CASH_ON_DELIVERY]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique = true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodTranslationEntity", mappedBy="payment_method")
     */
    private $payment_method_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderEntity", mappedBy="payment_method")
     */
    private $orders;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @param ArrayCollection $payment_method_translations
     */
    public function setPaymentMethodTranslations(ArrayCollection $payment_method_translations)
    {
        $this->payment_method_translations = $payment_method_translations;
    }
    /**
     * @param ArrayCollection $orders
     */
    public function setOrders(ArrayCollection $orders)
    {
        $this->orders = $orders;
    }
    /**
     * OrderPaymentMethodEntity constructor.
     */
    public function __construct()
    {
        $this->payment_method_translations = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }
}