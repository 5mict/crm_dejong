<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 12:11 PM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_payment_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderPaymentStatusRepository")
 */
class OrderPaymentStatusEntity
{
    const PAID = 'paid';
    const NOT_PAID = "not paid";
    const TABLE_ROWS = [
        ['name' => OrderPaymentStatusEntity::PAID],
        ['name' => OrderPaymentStatusEntity::NOT_PAID]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique = true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusTranslationEntity", mappedBy="order_payment_status")
     */
    private $order_payment_status_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderEntity", mappedBy="payment_status")
     */
    private $orders;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    /**
     * @param ArrayCollection $order_payment_status_translations
     */
    public function setOrderPaymentStatusTranslations(ArrayCollection $order_payment_status_translations)
    {
        $this->order_payment_status_translations = $order_payment_status_translations;
    }
    /**
     * @param ArrayCollection $orders
     */
    public function setOrders(ArrayCollection $orders)
    {
        $this->orders = $orders;
    }
    /**
     * OrderPaymentStatusEntity constructor.
     */
    public function __construct()
    {
        $this->order_payment_status_translations = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }
}