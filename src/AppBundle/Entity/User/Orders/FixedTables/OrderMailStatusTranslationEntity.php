<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/25/2017
 * Time: 3:04 PM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_mail_status_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderMailStatusTranslationRepository")
 */
class OrderMailStatusTranslationEntity
{
    const ORDER_FROM_SUPPLIER_EN = 'order to supplier';
    const ORDER_FROM_SUPPLIER_NL = 'bestelling naar leverancier';
    const ORDER_FROM_SUPPLIER_GE = 'Bestellung an den Lieferanten';
    const OFFER_EMAILED_EN = "offer emailed";
    const OFFER_EMAILED_NL = "bieden gemaild";
    const OFFER_EMAILED_GE = "angebot per e-mail";
    const ORDER_CONFIRMATION_EMAILED_EN = "order confirmation emailed";
    const ORDER_CONFIRMATION_EMAILED_NL = "orderbevestiging gemaild";
    const ORDER_CONFIRMATION_EMAILED_GE = "auftragsbestätigung per e-mail";
    const INVOICE_SENT_EN = "invoice sent";
    const INVOICE_SENT_NL = "factuur verzonden";
    const INVOICE_SENT_GE = "rechnung versandt";
    const TABLE_ROWS = [
        ['name' => OrderMailStatusTranslationEntity::ORDER_FROM_SUPPLIER_EN, 'reference' => OrderMailStatusEntity::ORDER_FROM_SUPPLIER, 'language' => 'en'],
        ['name' => OrderMailStatusTranslationEntity::ORDER_FROM_SUPPLIER_NL, 'reference' => OrderMailStatusEntity::ORDER_FROM_SUPPLIER, 'language' => 'nl'],
        ['name' => OrderMailStatusTranslationEntity::ORDER_FROM_SUPPLIER_GE, 'reference' => OrderMailStatusEntity::ORDER_FROM_SUPPLIER, 'language' => 'ge'],
        ['name' => OrderMailStatusTranslationEntity::OFFER_EMAILED_EN, 'reference' => OrderMailStatusEntity::OFFER_EMAILED, 'language' => 'en'],
        ['name' => OrderMailStatusTranslationEntity::OFFER_EMAILED_NL, 'reference' => OrderMailStatusEntity::OFFER_EMAILED, 'language' => 'nl'],
        ['name' => OrderMailStatusTranslationEntity::OFFER_EMAILED_GE, 'reference' => OrderMailStatusEntity::OFFER_EMAILED, 'language' => 'ge'],
        ['name' => OrderMailStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_EN, 'reference' => OrderMailStatusEntity::ORDER_CONFIRMATION_EMAILED, 'language' => 'en'],
        ['name' => OrderMailStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_NL, 'reference' => OrderMailStatusEntity::ORDER_CONFIRMATION_EMAILED, 'language' => 'nl'],
        ['name' => OrderMailStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_GE, 'reference' => OrderMailStatusEntity::ORDER_CONFIRMATION_EMAILED, 'language' => 'ge'],
        ['name' => OrderMailStatusTranslationEntity::INVOICE_SENT_EN, 'reference' => OrderMailStatusEntity::INVOICE_SENT, 'language' => 'en'],
        ['name' => OrderMailStatusTranslationEntity::INVOICE_SENT_NL, 'reference' => OrderMailStatusEntity::INVOICE_SENT, 'language' => 'nl'],
        ['name' => OrderMailStatusTranslationEntity::INVOICE_SENT_GE, 'reference' => OrderMailStatusEntity::INVOICE_SENT, 'language' => 'ge'],
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            OrderMailStatusEntity::ORDER_FROM_SUPPLIER => OrderMailStatusTranslationEntity::ORDER_FROM_SUPPLIER_EN,
            OrderMailStatusEntity::OFFER_EMAILED => OrderMailStatusTranslationEntity::OFFER_EMAILED_EN,
            OrderMailStatusEntity::ORDER_CONFIRMATION_EMAILED => OrderMailStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_EN,
            OrderMailStatusEntity::INVOICE_SENT => OrderMailStatusTranslationEntity::INVOICE_SENT_EN
        ],
        LocaleEntity::DUTCH => [
            OrderMailStatusEntity::ORDER_FROM_SUPPLIER => OrderMailStatusTranslationEntity::ORDER_FROM_SUPPLIER_NL,
            OrderMailStatusEntity::OFFER_EMAILED => OrderMailStatusTranslationEntity::OFFER_EMAILED_NL,
            OrderMailStatusEntity::ORDER_CONFIRMATION_EMAILED => OrderMailStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_NL,
            OrderMailStatusEntity::INVOICE_SENT => OrderMailStatusTranslationEntity::INVOICE_SENT_NL
        ],
        LocaleEntity::GERMAN => [
            OrderMailStatusEntity::ORDER_FROM_SUPPLIER => OrderMailStatusTranslationEntity::ORDER_FROM_SUPPLIER_GE,
            OrderMailStatusEntity::OFFER_EMAILED => OrderMailStatusTranslationEntity::OFFER_EMAILED_GE,
            OrderMailStatusEntity::ORDER_CONFIRMATION_EMAILED => OrderMailStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_GE,
            OrderMailStatusEntity::INVOICE_SENT => OrderMailStatusTranslationEntity::INVOICE_SENT_GE
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity", inversedBy="order_mail_status_translations")
     * @ORM\JoinColumn(name="order_mail_status_id", referencedColumnName="id")
     */
    private $order_mail_status;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @return OrderMailStatusEntity
     */
    public function getOrderMailStatus() : OrderMailStatusEntity
    {
        return $this->order_mail_status;
    }
    /**
     * @param OrderMailStatusEntity $order_mail_status
     */
    public function setOrderMailStatus(OrderMailStatusEntity $order_mail_status)
    {
        $this->order_mail_status = $order_mail_status;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return OrderMailStatusTranslationEntity::TRANSLATIONS[$locale][$name];
    }
}