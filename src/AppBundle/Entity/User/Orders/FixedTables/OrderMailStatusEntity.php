<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/25/2017
 * Time: 3:04 PM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_mail_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderMailStatusRepository")
 */
class OrderMailStatusEntity
{
    const ORDER_FROM_SUPPLIER = 'order-from-supplier';
    const OFFER_EMAILED = "offer-emailed";
    const ORDER_CONFIRMATION_EMAILED = "order-confirmation-emailed";
    const INVOICE_SENT = "invoice-emailed";
    const TABLE_ROWS = [
        ['name' => OrderMailStatusEntity::OFFER_EMAILED],
        ['name' => OrderMailStatusEntity::ORDER_CONFIRMATION_EMAILED],
        ['name' => OrderMailStatusEntity::INVOICE_SENT],
        ['name' => OrderMailStatusEntity::ORDER_FROM_SUPPLIER]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique = true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusTranslationEntity", mappedBy="order_mail_status")
     */
    private $order_mail_status_translations;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @param mixed $order_mail_status_translations
     */
    public function setOrderMailStatusTranslations($order_mail_status_translations)
    {
        $this->order_mail_status_translations = $order_mail_status_translations;
    }
    /**
     * OrderMailStatusEntity constructor.
     */
    public function __construct()
    {
        $this->order_mail_status_translations = new ArrayCollection();
    }
}
