<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 12:16 PM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_payment_status_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderPaymentStatusTranslationRepository")
 */
class OrderPaymentStatusTranslationEntity
{
    const PAID_EN = 'paid';
    const PAID_NL = 'betaald';
    const PAID_GE = 'bezahlt';
    const NOT_PAID_EN = "not paid";
    const NOT_PAID_NL = "niet betaald";
    const NOT_PAID_GE = "nicht bezahlt";
    const TABLE_ROWS = [
        ['name' => OrderPaymentStatusTranslationEntity::PAID_EN, 'reference' => OrderPaymentStatusEntity::PAID, 'language' => 'en'],
        ['name' => OrderPaymentStatusTranslationEntity::PAID_NL, 'reference' => OrderPaymentStatusEntity::PAID, 'language' => 'nl'],
        ['name' => OrderPaymentStatusTranslationEntity::PAID_GE, 'reference' => OrderPaymentStatusEntity::PAID, 'language' => 'ge'],
        ['name' => OrderPaymentStatusTranslationEntity::NOT_PAID_EN, 'reference' => OrderPaymentStatusEntity::NOT_PAID, 'language' => 'en'],
        ['name' => OrderPaymentStatusTranslationEntity::NOT_PAID_NL, 'reference' => OrderPaymentStatusEntity::NOT_PAID, 'language' => 'nl'],
        ['name' => OrderPaymentStatusTranslationEntity::NOT_PAID_GE, 'reference' => OrderPaymentStatusEntity::NOT_PAID, 'language' => 'ge'],
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            OrderPaymentStatusEntity::PAID => OrderPaymentStatusTranslationEntity::PAID_EN,
            OrderPaymentStatusEntity::NOT_PAID => OrderPaymentStatusTranslationEntity::NOT_PAID_EN
        ],
        LocaleEntity::DUTCH => [
            OrderPaymentStatusEntity::PAID => OrderPaymentStatusTranslationEntity::PAID_NL,
            OrderPaymentStatusEntity::NOT_PAID => OrderPaymentStatusTranslationEntity::NOT_PAID_NL
        ],
        LocaleEntity::GERMAN => [
            OrderPaymentStatusEntity::PAID => OrderPaymentStatusTranslationEntity::PAID_GE,
            OrderPaymentStatusEntity::NOT_PAID => OrderPaymentStatusTranslationEntity::NOT_PAID_GE
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity", inversedBy="order_payment_status_translations")
     * @ORM\JoinColumn(name="order_payment_status_id", referencedColumnName="id")
     */
    private $order_payment_status;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @return OrderPaymentStatusEntity
     */
    public function getOrderPaymentStatus() : OrderPaymentStatusEntity
    {
        return $this->order_payment_status;
    }
    /**
     * @param OrderPaymentStatusEntity $order_payment_status
     */
    public function setPaymentStatus(OrderPaymentStatusEntity $order_payment_status)
    {
        $this->order_payment_status = $order_payment_status;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return OrderPaymentStatusTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * OrderPaymentStatusTranslationEntity constructor.
     */
    public function __construct()
    {

    }
}