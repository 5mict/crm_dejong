<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 1:34 PM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_status_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 *  )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderStatusTranslationRepository")
 */
class OrderStatusTranslationEntity
{
    // new order
    //  offer -
    //  offer-emailed
    //  order-confirmation
    //  order-confirmation-emailed
    //  canceled
    //  invoice-sent
    const NEW_ORDER_EN = "new order";
    const NEW_ORDER_NL = "nieuwe bestelling";
    const NEW_ORDER_GE = "neue bestellung";
    const OFFER_EN = "offer";
    const OFFER_NL = "aanbod";
    const OFFER_GE = "angebot";
    const OFFER_EMAILED_EN = "offer emailed";
    const OFFER_EMAILED_NL = "bieden gemaild";
    const OFFER_EMAILED_GE = "angebot per e-mail";
    const ORDER_CONFIRMATION_EN = "order confirmation";
    const ORDER_CONFIRMATION_NL = "order bevestiging";
    const ORDER_CONFIRMATION_GE = "bestellbestätigung";
    const ORDER_CONFIRMATION_EMAILED_EN = "order confirmation emailed";
    const ORDER_CONFIRMATION_EMAILED_NL = "orderbevestiging gemaild";
    const ORDER_CONFIRMATION_EMAILED_GE = "auftragsbestätigung per e-mail";
    const CANCELED_EN = "canceled";
    const CANCELED_NL = "geannuleerd";
    const CANCELED_GE = "abgebrochen";
    const ORDER_IN_PROCESS_EN = "order in process";
    const ORDER_IN_PROCESS_NL = "opdracht loopt";
    const ORDER_IN_PROCESS_DEPOSIT_RECEIVED_EN = "order in process,deposit received";
    const ORDER_IN_PROCESS_DEPOSIT_RECEIVED_NL = "opdracht loopt, aanbetaling ontvangen";
    const ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER_EN = "order in process,ordered by supplier";
    const ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER_NL = "opdracht loopt, besteld bij leverancier";
    const INVOICE_SENT_EN = "invoice sent";
    const INVOICE_SENT_NL = "factuur verzonden";
    const INVOICE_SENT_GE = "rechnung versandt";
    const FINISHED_EN = "finished";
    const FINISHED_NL = "afgewerkt";
    const POST_CALCULATION_READY_EN = "post calculation ready";
    const POST_CALCULATION_READY_NL = "nacalculatie gereed";
    const WAIT_FOR_DOWN_PAYMENT_EN = "wait for down payment";
    const WAIT_FOR_DOWN_PAYMENT_NL = "wacht op aanbetaling";
    const TABLE_ROWS = [
        ['name' => OrderStatusTranslationEntity::NEW_ORDER_EN, 'reference' => OrderStatusEntity::NEW_ORDER, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::NEW_ORDER_NL, 'reference' => OrderStatusEntity::NEW_ORDER, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::NEW_ORDER_GE, 'reference' => OrderStatusEntity::NEW_ORDER, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::OFFER_EN, 'reference' => OrderStatusEntity::OFFER, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::OFFER_NL, 'reference' => OrderStatusEntity::OFFER, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::OFFER_GE, 'reference' => OrderStatusEntity::OFFER, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::OFFER_EMAILED_EN, 'reference' => OrderStatusEntity::OFFER_EMAILED, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::OFFER_EMAILED_NL, 'reference' => OrderStatusEntity::OFFER_EMAILED, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::OFFER_EMAILED_GE, 'reference' => OrderStatusEntity::OFFER_EMAILED, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EN, 'reference' => OrderStatusEntity::ORDER_CONFIRMATION, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::ORDER_CONFIRMATION_NL, 'reference' => OrderStatusEntity::ORDER_CONFIRMATION, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::ORDER_CONFIRMATION_GE, 'reference' => OrderStatusEntity::ORDER_CONFIRMATION, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_EN, 'reference' => OrderStatusEntity::ORDER_CONFIRMATION_EMAILED, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_NL, 'reference' => OrderStatusEntity::ORDER_CONFIRMATION_EMAILED, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_GE, 'reference' => OrderStatusEntity::ORDER_CONFIRMATION_EMAILED, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::CANCELED_EN, 'reference' => OrderStatusEntity::CANCELED, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::CANCELED_NL, 'reference' => OrderStatusEntity::CANCELED, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::CANCELED_GE, 'reference' => OrderStatusEntity::CANCELED, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::ORDER_IN_PROCESS_EN, 'reference' => OrderStatusEntity::ORDER_IN_PROCESS, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::ORDER_IN_PROCESS_NL, 'reference' => OrderStatusEntity::ORDER_IN_PROCESS, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::INVOICE_SENT_EN, 'reference' => OrderStatusEntity::INVOICE_SENT, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::INVOICE_SENT_NL, 'reference' => OrderStatusEntity::INVOICE_SENT, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::INVOICE_SENT_GE, 'reference' => OrderStatusEntity::INVOICE_SENT, 'language' => 'ge'],
        ['name' => OrderStatusTranslationEntity::FINISHED_EN, 'reference' => OrderStatusEntity::FINISHED, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::FINISHED_NL, 'reference' => OrderStatusEntity::FINISHED, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::POST_CALCULATION_READY_EN, 'reference' => OrderStatusEntity::POST_CALCULATION_READY, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::POST_CALCULATION_READY_NL, 'reference' => OrderStatusEntity::POST_CALCULATION_READY, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER_EN, 'reference' => OrderStatusEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER_NL, 'reference' => OrderStatusEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER, 'language' => 'nl'],
        ['name' => OrderStatusTranslationEntity::WAIT_FOR_DOWN_PAYMENT_EN, 'reference' => OrderStatusEntity::WAIT_FOR_DOWN_PAYMENT, 'language' => 'en'],
        ['name' => OrderStatusTranslationEntity::WAIT_FOR_DOWN_PAYMENT_NL, 'reference' => OrderStatusEntity::WAIT_FOR_DOWN_PAYMENT, 'language' => 'nl'],
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            OrderStatusEntity::NEW_ORDER => OrderStatusTranslationEntity::NEW_ORDER_EN,
            OrderStatusEntity::OFFER => OrderStatusTranslationEntity::OFFER_EN,
            OrderStatusEntity::OFFER_EMAILED => OrderStatusTranslationEntity::OFFER_EMAILED_EN,
            OrderStatusEntity::ORDER_CONFIRMATION => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EN,
            OrderStatusEntity::ORDER_CONFIRMATION_EMAILED => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_EN,
            OrderStatusEntity::CANCELED => OrderStatusTranslationEntity::CANCELED_EN,
            OrderStatusEntity::ORDER_IN_PROCESS => OrderStatusTranslationEntity::ORDER_IN_PROCESS_EN,
            OrderStatusEntity::INVOICE_SENT => OrderStatusTranslationEntity::INVOICE_SENT_EN,
            OrderStatusEntity::FINISHED => OrderStatusTranslationEntity::FINISHED_EN,
            OrderStatusEntity::POST_CALCULATION_READY => OrderStatusTranslationEntity::POST_CALCULATION_READY_EN,
            OrderStatusEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER => OrderStatusTranslationEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER_EN,
            OrderStatusEntity::ORDER_IN_PROCESS_DEPOSIT_RECEIVED => OrderStatusTranslationEntity::ORDER_IN_PROCESS_DEPOSIT_RECEIVED_EN,
            OrderStatusEntity::WAIT_FOR_DOWN_PAYMENT => OrderStatusTranslationEntity::WAIT_FOR_DOWN_PAYMENT_EN,

        ],
        LocaleEntity::DUTCH => [
            OrderStatusEntity::NEW_ORDER => OrderStatusTranslationEntity::NEW_ORDER_NL,
            OrderStatusEntity::OFFER => OrderStatusTranslationEntity::OFFER_NL,
            OrderStatusEntity::OFFER_EMAILED => OrderStatusTranslationEntity::OFFER_EMAILED_NL,
            OrderStatusEntity::ORDER_CONFIRMATION => OrderStatusTranslationEntity::ORDER_CONFIRMATION_NL,
            OrderStatusEntity::ORDER_CONFIRMATION_EMAILED => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_NL,
            OrderStatusEntity::CANCELED => OrderStatusTranslationEntity::CANCELED_NL,
            OrderStatusEntity::ORDER_IN_PROCESS => OrderStatusTranslationEntity::ORDER_IN_PROCESS_NL,
            OrderStatusEntity::INVOICE_SENT => OrderStatusTranslationEntity::INVOICE_SENT_NL,
            OrderStatusEntity::FINISHED => OrderStatusTranslationEntity::FINISHED_NL,
            OrderStatusEntity::POST_CALCULATION_READY => OrderStatusTranslationEntity::POST_CALCULATION_READY_NL,
            OrderStatusEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER => OrderStatusTranslationEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER_NL,
            OrderStatusEntity::ORDER_IN_PROCESS_DEPOSIT_RECEIVED => OrderStatusTranslationEntity::ORDER_IN_PROCESS_DEPOSIT_RECEIVED_NL,
            OrderStatusEntity::WAIT_FOR_DOWN_PAYMENT => OrderStatusTranslationEntity::WAIT_FOR_DOWN_PAYMENT_NL
        ],
        LocaleEntity::GERMAN => [
            OrderStatusEntity::NEW_ORDER => OrderStatusTranslationEntity::NEW_ORDER_GE,
            OrderStatusEntity::OFFER => OrderStatusTranslationEntity::OFFER_GE,
            OrderStatusEntity::OFFER_EMAILED => OrderStatusTranslationEntity::OFFER_EMAILED_GE,
            OrderStatusEntity::ORDER_CONFIRMATION => OrderStatusTranslationEntity::ORDER_CONFIRMATION_GE,
            OrderStatusEntity::ORDER_CONFIRMATION_EMAILED => OrderStatusTranslationEntity::ORDER_CONFIRMATION_EMAILED_GE,
            OrderStatusEntity::CANCELED => OrderStatusTranslationEntity::CANCELED_GE,
            OrderStatusEntity::INVOICE_SENT => OrderStatusTranslationEntity::INVOICE_SENT_GE
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity", inversedBy="order_status_translations")
     * @ORM\JoinColumn(name="order_status_id", referencedColumnName="id")
     */
    private $order_status;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @return OrderStatusEntity
     */
    public function getOrderStatus() : OrderStatusEntity
    {
        return $this->order_status;
    }
    /**
     * @param OrderStatusEntity $order_status
     */
    public function setOrderStatus(OrderStatusEntity $order_status)
    {
        $this->order_status = $order_status;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return OrderStatusTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * OrderStatusEntity constructor.
     */
    public function __construct()
    {

    }
}
