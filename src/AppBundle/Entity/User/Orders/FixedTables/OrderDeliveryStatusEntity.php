<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 11:58 AM
 */

namespace AppBundle\Entity\User\Orders\FixedTables;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_delivery_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderDeliveryStatusRepository")
 */
class OrderDeliveryStatusEntity
{
    //  collected
    //  sent
    //  deliver
    const PICK_UP = "pick-up";
    const SEND = "send";
    const CASH_ON_DELIVERY = "cash_on_delivery";
    const DELIVER = "deliver";
    const TABLE_ROWS = [
        ['name' => OrderDeliveryStatusEntity::PICK_UP],
        ['name' => OrderDeliveryStatusEntity::SEND],
        ['name' => OrderDeliveryStatusEntity::CASH_ON_DELIVERY],
        ['name' => OrderDeliveryStatusEntity::DELIVER],
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique = true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusTranslationEntity", mappedBy="delivery_status")
     */
    private $delivery_status_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderEntity", mappedBy="delivery_status")
     */
    private $orders;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @param mixed $delivery_status_translations
     */
    public function setDeliveryStatusTranslations($delivery_status_translations)
    {
        $this->delivery_status_translations = $delivery_status_translations;
    }
    /**
     * @param mixed $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    }
    /**
     * DeliveryStatusEntity constructor.
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->delivery_status_translations = new ArrayCollection();
    }
}

