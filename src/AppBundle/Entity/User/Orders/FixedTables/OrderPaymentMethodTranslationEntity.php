<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/3/2018
 * Time: 11:50 AM
 */

namespace AppBundle\Entity\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_payment_method_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderPaymentMethodTranslationRepository")
 */
class OrderPaymentMethodTranslationEntity
{
    const CREDIT_CARD_EN = 'credit card';
    const CREDIT_CARD_NL = 'kredietkaart';
    const CREDIT_CARD_GE = 'Kreditkarte';
    const PIN_EN = "PIN";
    const PIN_NL = "PIN";
    const PIN_GE = "PIN";
    const BANK_EN = "bank";
    const BANK_NL = "bank";
    const BANK_GE = "Bank";
    const CASH_EN = "cash";
    const CASH_NL = "contant geld";
    const CASH_GE = "Kasse";
    const CASH_ON_DELIVERY_EN = "cash on delivery";
    const CASH_ON_DELIVERY_NL = "rembours";
    const TABLE_ROWS = [
        ['name' => OrderPaymentMethodTranslationEntity::CREDIT_CARD_EN, 'reference' => OrderPaymentMethodEntity::CREDIT_CARD, 'language' => 'en'],
        ['name' => OrderPaymentMethodTranslationEntity::CREDIT_CARD_NL, 'reference' => OrderPaymentMethodEntity::CREDIT_CARD, 'language' => 'nl'],
        ['name' => OrderPaymentMethodTranslationEntity::CREDIT_CARD_GE, 'reference' => OrderPaymentMethodEntity::CREDIT_CARD, 'language' => 'ge'],
        ['name' => OrderPaymentMethodTranslationEntity::PIN_EN, 'reference' => OrderPaymentMethodEntity::PIN, 'language' => 'en'],
        ['name' => OrderPaymentMethodTranslationEntity::PIN_NL, 'reference' => OrderPaymentMethodEntity::PIN, 'language' => 'nl'],
        ['name' => OrderPaymentMethodTranslationEntity::PIN_GE, 'reference' => OrderPaymentMethodEntity::PIN, 'language' => 'ge'],
        ['name' => OrderPaymentMethodTranslationEntity::BANK_EN, 'reference' => OrderPaymentMethodEntity::BANK, 'language' => 'en'],
        ['name' => OrderPaymentMethodTranslationEntity::BANK_NL, 'reference' => OrderPaymentMethodEntity::BANK, 'language' => 'nl'],
        ['name' => OrderPaymentMethodTranslationEntity::BANK_GE, 'reference' => OrderPaymentMethodEntity::BANK, 'language' => 'ge'],
        ['name' => OrderPaymentMethodTranslationEntity::CASH_EN, 'reference' => OrderPaymentMethodEntity::CASH, 'language' => 'en'],
        ['name' => OrderPaymentMethodTranslationEntity::CASH_NL, 'reference' => OrderPaymentMethodEntity::CASH, 'language' => 'nl'],
        ['name' => OrderPaymentMethodTranslationEntity::CASH_GE, 'reference' => OrderPaymentMethodEntity::CASH, 'language' => 'ge'],
        ['name' => OrderPaymentMethodTranslationEntity::CASH_ON_DELIVERY_EN, 'reference' => OrderPaymentMethodEntity::CASH_ON_DELIVERY, 'language' => 'en'],
        ['name' => OrderPaymentMethodTranslationEntity::CASH_ON_DELIVERY_NL, 'reference' => OrderPaymentMethodEntity::CASH_ON_DELIVERY, 'language' => 'nl'],
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            OrderPaymentMethodEntity::CREDIT_CARD => OrderPaymentMethodTranslationEntity::CREDIT_CARD_EN,
            OrderPaymentMethodEntity::PIN => OrderPaymentMethodTranslationEntity::PIN_EN,
            OrderPaymentMethodEntity::BANK => OrderPaymentMethodTranslationEntity::BANK_EN,
            OrderPaymentMethodEntity::CASH => OrderPaymentMethodTranslationEntity::CASH_EN,
            OrderPaymentMethodEntity::CASH_ON_DELIVERY => OrderPaymentMethodTranslationEntity::CASH_ON_DELIVERY_EN
        ],
        LocaleEntity::DUTCH => [
            OrderPaymentMethodEntity::CREDIT_CARD => OrderPaymentMethodTranslationEntity::CREDIT_CARD_NL,
            OrderPaymentMethodEntity::PIN => OrderPaymentMethodTranslationEntity::PIN_NL,
            OrderPaymentMethodEntity::BANK => OrderPaymentMethodTranslationEntity::BANK_NL,
            OrderPaymentMethodEntity::CASH => OrderPaymentMethodTranslationEntity::CASH_NL,
            OrderPaymentMethodEntity::CASH_ON_DELIVERY => OrderPaymentMethodTranslationEntity::CASH_ON_DELIVERY_NL
        ],
        LocaleEntity::GERMAN => [
            OrderPaymentMethodEntity::CREDIT_CARD => OrderPaymentMethodTranslationEntity::CREDIT_CARD_GE,
            OrderPaymentMethodEntity::PIN => OrderPaymentMethodTranslationEntity::PIN_GE,
            OrderPaymentMethodEntity::BANK => OrderPaymentMethodTranslationEntity::BANK_GE,
            OrderPaymentMethodEntity::CASH => OrderPaymentMethodTranslationEntity::CASH_GE
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity", inversedBy="payment_method_translations")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id")
     */
    private $payment_method;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @return OrderPaymentMethodEntity
     */
    public function getPaymentMethod() : OrderPaymentMethodEntity
    {
        return $this->payment_method;
    }
    /**
     * @param OrderPaymentMethodEntity $payment_method
     */
    public function setPaymentMethod(OrderPaymentMethodEntity $payment_method)
    {
        $this->payment_method = $payment_method;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return OrderPaymentMethodTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * OrderPaymentMethodTranslationEntity constructor.
     */
    public function __construct()
    {

    }
}