<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 12:07 PM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Table(name="orders_fixed_tables_delivery_status_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 *  )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderDeliveryStatusTranslationRepository")
 */
class OrderDeliveryStatusTranslationEntity
{
    //  collected
    //  sent
    //  deliver
    const PICK_UP_EN = "pick-up";
    const PICK_UP_NL = "ophalen";
    const CASH_ON_DELIVERY_EN = "cash_on_delivery";
    const CASH_ON_DELIVERY_NL = "rembours";
    const SEND_EN = "send";
    const SEND_NL = "versturen";
    const DELIVER_EN = "deliver";
    const DELIVER_NL = "afleveren";
    const TABLE_ROWS = [
        ['name' => OrderDeliveryStatusTranslationEntity::PICK_UP_EN, 'reference' => OrderDeliveryStatusEntity::PICK_UP, 'language' => 'en'],
        ['name' => OrderDeliveryStatusTranslationEntity::PICK_UP_NL, 'reference' => OrderDeliveryStatusEntity::PICK_UP, 'language' => 'nl'],
        ['name' => OrderDeliveryStatusTranslationEntity::CASH_ON_DELIVERY_EN, 'reference' => OrderDeliveryStatusEntity::CASH_ON_DELIVERY, 'language' => 'en'],
        ['name' => OrderDeliveryStatusTranslationEntity::CASH_ON_DELIVERY_NL, 'reference' => OrderDeliveryStatusEntity::CASH_ON_DELIVERY, 'language' => 'nl'],
        ['name' => OrderDeliveryStatusTranslationEntity::SEND_EN, 'reference' => OrderDeliveryStatusEntity::SEND, 'language' => 'en'],
        ['name' => OrderDeliveryStatusTranslationEntity::SEND_NL, 'reference' => OrderDeliveryStatusEntity::SEND, 'language' => 'nl'],
        ['name' => OrderDeliveryStatusTranslationEntity::DELIVER_EN, 'reference' => OrderDeliveryStatusEntity::DELIVER, 'language' => 'en'],
        ['name' => OrderDeliveryStatusTranslationEntity::DELIVER_NL, 'reference' => OrderDeliveryStatusEntity::DELIVER, 'language' => 'nl'],
    ];
    const TRANSLATIONS =[
        LocaleEntity::ENGLISH => [
            OrderDeliveryStatusEntity::PICK_UP => OrderDeliveryStatusTranslationEntity::PICK_UP_EN,
            OrderDeliveryStatusEntity::CASH_ON_DELIVERY => OrderDeliveryStatusTranslationEntity::CASH_ON_DELIVERY_EN,
            OrderDeliveryStatusEntity::SEND => OrderDeliveryStatusTranslationEntity::SEND_EN,
            OrderDeliveryStatusEntity::DELIVER => OrderDeliveryStatusTranslationEntity::DELIVER_EN
        ],
        LocaleEntity::DUTCH => [
            OrderDeliveryStatusEntity::PICK_UP => OrderDeliveryStatusTranslationEntity::PICK_UP_NL,
            OrderDeliveryStatusEntity::CASH_ON_DELIVERY => OrderDeliveryStatusTranslationEntity::CASH_ON_DELIVERY_NL,
            OrderDeliveryStatusEntity::SEND => OrderDeliveryStatusTranslationEntity::SEND_NL,
            OrderDeliveryStatusEntity::DELIVER => OrderDeliveryStatusTranslationEntity::DELIVER_NL
        ],
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity", inversedBy="delivery_status_translations")
     * @ORM\JoinColumn(name="delivery_status_id", referencedColumnName="id")
     */
    private $delivery_status;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @return OrderDeliveryStatusEntity
     */
    public function getDeliveryStatus() : OrderDeliveryStatusEntity
    {
        return $this->delivery_status;
    }
    /**
     * @param OrderDeliveryStatusEntity $delivery_status
     */
    public function setDeliveryStatus(OrderDeliveryStatusEntity $delivery_status)
    {
        $this->delivery_status = $delivery_status;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return OrderDeliveryStatusTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * DeliveryStatusEntity constructor.
     */
    public function __construct()
    {

    }
}

