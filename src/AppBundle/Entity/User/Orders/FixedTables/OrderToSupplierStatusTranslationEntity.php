<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/11/2018
 * Time: 9:07 PM
 */
namespace AppBundle\Entity\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_to_supplier_status_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 *  )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderToSupplierStatusTranslationRepository")
 */
class OrderToSupplierStatusTranslationEntity
{
    // collected
    // sent
    // deliver
    // finished
    const NEW_EN = "new";
    const NEW_NL = "nieuwe";
    const NEW_GE = "neu";
    const COLLECTED_EN = "collected";
    const COLLECTED_NL = "opgehaald";
    const COLLECTED_GE = "gesammelt";
    const SENT_EN = "sent";
    const SENT_NL = "verzonden";
    const SENT_GE = "geschickt";
    const DELIVER_EN = "deliver";
    const DELIVER_NL = "geleverd";
    const DELIVER_GE = "liefern";
    const FINISHED_EN = "finished";
    const FINISHED_NL = "afgewerkt";
    const FINISHED_GE = "fertig";
    const TABLE_ROWS = [
        ['name' => OrderToSupplierStatusTranslationEntity::NEW_EN, 'reference' => OrderToSupplierStatusEntity::NEW, 'language' => 'en'],
        ['name' => OrderToSupplierStatusTranslationEntity::NEW_NL, 'reference' => OrderToSupplierStatusEntity::NEW, 'language' => 'nl'],
        ['name' => OrderToSupplierStatusTranslationEntity::NEW_GE, 'reference' => OrderToSupplierStatusEntity::NEW, 'language' => 'ge'],
        ['name' => OrderToSupplierStatusTranslationEntity::COLLECTED_EN, 'reference' => OrderToSupplierStatusEntity::COLLECTED, 'language' => 'en'],
        ['name' => OrderToSupplierStatusTranslationEntity::COLLECTED_NL, 'reference' => OrderToSupplierStatusEntity::COLLECTED, 'language' => 'nl'],
        ['name' => OrderToSupplierStatusTranslationEntity::COLLECTED_GE, 'reference' => OrderToSupplierStatusEntity::COLLECTED, 'language' => 'ge'],
        ['name' => OrderToSupplierStatusTranslationEntity::SENT_EN, 'reference' => OrderToSupplierStatusEntity::SENT, 'language' => 'en'],
        ['name' => OrderToSupplierStatusTranslationEntity::SENT_NL, 'reference' => OrderToSupplierStatusEntity::SENT, 'language' => 'nl'],
        ['name' => OrderToSupplierStatusTranslationEntity::SENT_GE, 'reference' => OrderToSupplierStatusEntity::SENT, 'language' => 'ge'],
        ['name' => OrderToSupplierStatusTranslationEntity::DELIVER_EN, 'reference' => OrderToSupplierStatusEntity::DELIVER, 'language' => 'en'],
        ['name' => OrderToSupplierStatusTranslationEntity::DELIVER_NL, 'reference' => OrderToSupplierStatusEntity::DELIVER, 'language' => 'nl'],
        ['name' => OrderToSupplierStatusTranslationEntity::DELIVER_GE, 'reference' => OrderToSupplierStatusEntity::DELIVER, 'language' => 'ge'],
        ['name' => OrderToSupplierStatusTranslationEntity::FINISHED_EN, 'reference' => OrderToSupplierStatusEntity::FINISHED, 'language' => 'en'],
        ['name' => OrderToSupplierStatusTranslationEntity::FINISHED_NL, 'reference' => OrderToSupplierStatusEntity::FINISHED, 'language' => 'nl'],
        ['name' => OrderToSupplierStatusTranslationEntity::FINISHED_GE, 'reference' => OrderToSupplierStatusEntity::FINISHED, 'language' => 'ge'],
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            OrderToSupplierStatusEntity::NEW => OrderToSupplierStatusTranslationEntity::NEW_EN,
            OrderToSupplierStatusEntity::COLLECTED => OrderToSupplierStatusTranslationEntity::COLLECTED_EN,
            OrderToSupplierStatusEntity::SENT => OrderToSupplierStatusTranslationEntity::SENT_EN,
            OrderToSupplierStatusEntity::DELIVER => OrderToSupplierStatusTranslationEntity::DELIVER_EN,
            OrderToSupplierStatusEntity::FINISHED => OrderToSupplierStatusTranslationEntity::FINISHED_EN
        ],
        LocaleEntity::DUTCH => [
            OrderToSupplierStatusEntity::NEW => OrderToSupplierStatusTranslationEntity::NEW_NL,
            OrderToSupplierStatusEntity::COLLECTED => OrderToSupplierStatusTranslationEntity::COLLECTED_NL,
            OrderToSupplierStatusEntity::SENT => OrderToSupplierStatusTranslationEntity::SENT_NL,
            OrderToSupplierStatusEntity::DELIVER => OrderToSupplierStatusTranslationEntity::DELIVER_NL,
            OrderToSupplierStatusEntity::FINISHED => OrderToSupplierStatusTranslationEntity::FINISHED_NL
        ],
        LocaleEntity::GERMAN => [
            OrderToSupplierStatusEntity::NEW => OrderToSupplierStatusTranslationEntity::NEW_GE,
            OrderToSupplierStatusEntity::COLLECTED => OrderToSupplierStatusTranslationEntity::COLLECTED_GE,
            OrderToSupplierStatusEntity::SENT => OrderToSupplierStatusTranslationEntity::SENT_GE,
            OrderToSupplierStatusEntity::DELIVER => OrderToSupplierStatusTranslationEntity::DELIVER_GE,
            OrderToSupplierStatusEntity::FINISHED => OrderToSupplierStatusTranslationEntity::FINISHED_GE
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity", inversedBy="order_to_supplier_status_translations")
     * @ORM\JoinColumn(name="order_to_supplier_status_id", referencedColumnName="id")
     */
    private $order_to_supplier_status;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @return OrderToSupplierStatusEntity
     */
    public function getOrderToSupplierStatus() : OrderToSupplierStatusEntity
    {
        return $this->order_to_supplier_status;
    }
    /**
     * @param OrderToSupplierStatusEntity $order_to_supplier_status
     */
    public function setOrderStatus(OrderToSupplierStatusEntity $order_to_supplier_status)
    {
        $this->order_to_supplier_status = $order_to_supplier_status;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return OrderToSupplierStatusTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * OrderToSupplierStatusTranslationEntity constructor.
     */
    public function __construct()
    {

    }
}