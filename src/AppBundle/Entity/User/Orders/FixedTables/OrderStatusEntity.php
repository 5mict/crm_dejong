<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 11:48 AM
 */

namespace AppBundle\Entity\User\Orders\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_fixed_tables_status")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\FixedTables\OrderStatusRepository")
 */
class OrderStatusEntity
{
    // new order
    //  offer -
    //  offer-emailed
    //  order-confirmation
    //  order-confirmation-emailed
    //  canceled
    //  invoice-sent
    const NEW_ORDER = "new order";
    const OFFER = "new offer";
    const OFFER_EMAILED = "offer-emailed";
    const ORDER_CONFIRMATION = "order-confirmation";
    const ORDER_CONFIRMATION_EMAILED = "order-confirmation-emailed";
    const CANCELED = "canceled";
    const ORDER_IN_PROCESS = "order in process";
    const ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER = "order in process,ordered by supplier";
    const ORDER_IN_PROCESS_DEPOSIT_RECEIVED = "order in process,deposit received";
    const INVOICE_SENT = "invoice-emailed";
    const FINISHED = "finished";
    const POST_CALCULATION_READY = "post calculation ready";
    const WAIT_FOR_DOWN_PAYMENT = "wait for down payment";
    const TABLE_ROWS = [
        ['name' => OrderStatusEntity::NEW_ORDER],
        ['name' => OrderStatusEntity::OFFER],
        ['name' => OrderStatusEntity::OFFER_EMAILED],
        ['name' => OrderStatusEntity::ORDER_CONFIRMATION],
        ['name' => OrderStatusEntity::ORDER_CONFIRMATION_EMAILED],
        ['name' => OrderStatusEntity::CANCELED],
        ['name' => OrderStatusEntity::ORDER_IN_PROCESS],
        ['name' => OrderStatusEntity::ORDER_IN_PROCESS_DEPOSIT_RECEIVED],
        ['name' => OrderStatusEntity::ORDER_IN_PROCESS_ORDERED_BY_SUPPLIER],
        ['name' => OrderStatusEntity::INVOICE_SENT],
        ['name' => OrderStatusEntity::FINISHED],
        ['name' => OrderStatusEntity::POST_CALCULATION_READY],
        ['name' => OrderStatusEntity::WAIT_FOR_DOWN_PAYMENT],
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique = true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderStatusTranslationEntity", mappedBy="order_status")
     */
    private $order_status_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderEntity", mappedBy="order_status")
     */
    private $orders;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @param mixed $order_status_translations
     */
    public function setOrdersStatusTranslations($order_status_translations)
    {
        $this->order_status_translations = $order_status_translations;
    }
    /**
     * @param mixed $orders
     */
    public function setOrders($orders)
    {
        $this->orders = $orders;
    }
    /**
     * OrderStatusEntity constructor.
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->order_status_translations = new ArrayCollection();
    }
}
