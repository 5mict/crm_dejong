<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/25/2017
 * Time: 2:59 PM
 */

namespace AppBundle\Entity\User\Orders;

use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="order_files")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\OrderFileRepository")
 */
class OrderFileEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $file_name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\OrderEntity", inversedBy="order_files")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getFileName() : string
    {
        return $this->file_name;
    }
    /**
     * @param string $file_name
     */
    public function setFileName(string $file_name)
    {
        $this->file_name = $file_name;
    }
    /**
     * @return OrderEntity
     */
    public function getOrder() : OrderEntity
    {
        return $this->order;
    }
    /**
     * @param OrderEntity $order
     */
    public function setOrder(OrderEntity $order)
    {
        $this->order = $order;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @return string
     */
    public function getCreatedString() : string
    {
        return $this->created->format('Y-m-d H:i:s');
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}