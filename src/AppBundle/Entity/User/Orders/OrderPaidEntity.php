<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/4/2018
 * Time: 5:00 PM
 */

namespace AppBundle\Entity\User\Orders;

use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_paid",
 *     indexes={
 *          @ORM\Index(
 *              name="search_created",
 *              columns={"created"}
 *          ),
 *          @ORM\Index(
 *              name="search_debtor_created",
 *              columns={"debtor_id", "created"}
 *          )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\OrderPaidRepository")
 */
class OrderPaidEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User\Orders\OrderEntity")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\DebtorEntity", inversedBy="orders_paid")
     * @ORM\JoinColumn(name="debtor_id", referencedColumnName="id")
     */
    private $debtor;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;
    /**
     * @ORM\Column(type="float")
     */
    private $purchase_price;
    /**
     * @ORM\Column(type="float")
     */
    private $selling_price;
    /**
     * @ORM\Column(type="float")
     */
    private $profit_percent;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return OrderEntity
     */
    public function getOrder() : OrderEntity
    {
        return $this->order;
    }
    /**
     * @param OrderEntity $order
     */
    public function setOrder(OrderEntity $order)
    {
        $this->order = $order;
    }
    /**
     * @return DebtorEntity
     */
    public function getDebtor() : DebtorEntity
    {
        return $this->debtor;
    }
    /**
     * @param DebtorEntity $debtor
     */
    public function setDebtor(DebtorEntity $debtor)
    {
        $this->debtor = $debtor;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }
    /**
     * @return float
     */
    public function getPurchasePrice() : float
    {
        return $this->purchase_price;
    }
    /**
     * @param float $purchase_price
     */
    public function setPurchasePrice(float $purchase_price)
    {
        $this->purchase_price = $purchase_price;
    }
    /**
     * @return float
     */
    public function getSellingPrice() : float
    {
        return $this->selling_price;
    }
    /**
     * @param float $selling_price
     */
    public function setSellingPrice(float $selling_price)
    {
        $this->selling_price = $selling_price;
    }
    /**
     * @return float
     */
    public function getProfitPercent() : float
    {
        return $this->profit_percent;
    }
    /**
     * @param float $profit_percent
     */
    public function setProfitPercent(float $profit_percent)
    {
        $this->profit_percent = $profit_percent;
    }
}