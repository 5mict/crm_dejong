<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/10/2017
 * Time: 4:45 PM
 */

namespace AppBundle\Entity\User\Orders;

use AppBundle\Entity\FixedTables\LocaleEntity;
use AppBundle\Entity\User\Articles\ArticleEntity;
use AppBundle\Entity\User\Articles\ArticlePaidEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity;
use AppBundle\Entity\User\Suppliers\SupplierEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders_article",
 *     indexes = {
 *          @ORM\Index(
 *              name="search_delivery_time",
 *              columns={"delivery_time"}
 *          ),
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\OrderArticleRepository")
 */
class OrderArticleEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var ArticleEntity
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Articles\ArticleEntity", inversedBy="order_articles")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $article;
    /**
     * @var OrderEntity
     * @ORM\ManyToOne(targetEntity="OrderEntity", inversedBy="order_articles")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $order;
    /**
     * @var SupplierEntity
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Suppliers\SupplierEntity", inversedBy="order_articles")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     */
    private $supplier;
    /**
     * @var OrderToSupplierStatusEntity
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderToSupplierStatusEntity")
     * @ORM\JoinColumn(name="to_supplier_status_id", referencedColumnName="id")
     */
    private $to_supplier_status;
    /**
     * @ORM\OneToMany(targetEntity="OrderMailEntity", mappedBy="order_article")
     */
    private $order_article_mails;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $color;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $material;
    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $quantity;
    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $in_stock;
    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $order_from_supplier;
    /**
     * @ORM\Column(type="float")
     */
    private $order_article_purchase_price;
    /**
     * @ORM\Column(type="float")
     */
    private $order_article_price;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $updated;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;
    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $details;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $delivered;
    /**
     * @ORM\Column(type="datetime")
     */
    private $delivery_time;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $commission;
    /**
     * @ORM\Column(type="boolean")
     */
    private $show_size;
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $description = null;
    /**
     * @ORM\Column(type="integer")
     */
    private $custom_order_article_number;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return ArticleEntity
     */
    public function getArticle() : ArticleEntity
    {
        return $this->article;
    }
    /**
     * @return string
     */
    public function getArticleNumber() : string
    {
        return $this->article->getArticleNumber();
    }
    /**
     * @return string
     */
    public function getArticleSize() : string
    {
        return $this->article->getSize();
    }
    /**
     * @return string
     */
    public function getArticleDescription() : string
    {
        return $this->article->getDescription();
    }
    /**
     * @return string
     */
    public function getDescription()
    {
        return preg_replace( "/\r|\n/", "", $this->description );
    }

    /**
     * @param $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
    /**
     * @return string
     */
    public function getArticleName() : string
    {
        return $this->article->getArticleName();
    }
    /**
     * @return string
     */
    public function getArticleNameInRows() : string
    {
        $articleName = $this->article->getArticleName();
        if( strlen( $articleName ) > 40 )
        {
            $articleNames = explode( $articleName, " ", 2);
            return $articleNames[0] . "\n" . $articleNames[1];
        }
        return $articleName;
    }
    /**
     * @return string
     */
    public function getArticleNameInRowsDutch() : string
    {
        $articleName = $this->article->getArticleName();
        if( strlen( $articleName ) > 40 )
        {
            $articleNames = explode( $articleName, " ", 2);
            return $articleNames[0] . "\n" . $articleNames[1];
        }
        return $articleName;
    }
    /**
     * @return string
     */
    public function getPictureUrl() : string
    {
        return $this->article->getPictureUrl();
    }
    /**
     * @param ArticleEntity $article
     */
    public function setArticle(ArticleEntity $article)
    {
        $this->article = $article;
    }
    /**
     * @return string
     */
    public function getMaterial() : string
    {
        return $this->material;
    }
    /**
     * @param string $material
     */
    public function setMaterial(string $material)
    {
        $this->material = $material;
    }
    /**
     * @return string
     */
    public function getColor() : string
    {
        return $this->color;
    }
    /**
     * @param string $color
     */
    public function setColor(string $color)
    {
        $this->color = $color;
    }
    /**
     * @return string
     */
    public function getDetails() : string
    {
        return $this->details;
    }
    /**
     * @param string $details
     */
    public function setDetails(string $details)
    {
        $this->details = $details;
    }
    /**
     * @return OrderEntity
     */
    public function getOrder() : OrderEntity
    {
        return $this->order;
    }
    /**
     * @return int
     */
    public function getOrderId() : int
    {
        return $this->order->getId();
    }
    /**
     * @param OrderEntity $order
     */
    public function setOrder(OrderEntity $order)
    {
        $this->order = $order;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime$created)
    {
        $this->created = $created;
    }
    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }
    /**
     * @return string
     */
    public function getUpdatedString() : string
    {
        return $this->updated->format('Y-m-d');
    }
    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }
    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }
    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }
    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }
    /**
     * @return int
     */
    public function getInStock(): int
    {
        return $this->in_stock;
    }
    /**
     * @param int $in_stock
     */
    public function setInStock(int $in_stock)
    {
        $this->in_stock = $in_stock;
    }
    /**
     * @return float
     */
    public function getOrderArticlePurchasePrice(): float
    {
        return $this->order_article_purchase_price;
    }
    /**
     * @param float $order_article_purchase_price
     */
    public function setOrderArticlePurchasePrice(float $order_article_purchase_price)
    {
        $this->order_article_purchase_price = $order_article_purchase_price;
    }
    /**
     * @return float
     */
    public function getOrderArticlePrice(): float
    {
        return $this->order_article_price;
    }
    /**
     * @param float $order_article_price
     */
    public function setOrderArticlePrice(float $order_article_price)
    {
        $this->order_article_price = $order_article_price;
    }
    /**
     * @return int
     */
    public function getOrderFromSupplier()
    {
        return $this->order_from_supplier;
    }
    /**
     * @param int $order_from_supplier
     */
    public function setOrderFromSupplier(int $order_from_supplier)
    {
        $this->order_from_supplier = $order_from_supplier;
    }
    /**
     * @return SupplierEntity
     */
    public function getSupplier() : SupplierEntity
    {
        return $this->supplier;
    }
    /**
     * @return string
     */
    public function getSupplierName() : string
    {
        return $this->supplier->getName();
    }
    /**
     * @param SupplierEntity $supplier
     */
    public function setSupplier(SupplierEntity $supplier)
    {
        $this->supplier = $supplier;
    }
    /**
     * @return float
     */
    public function getSellingPrice()
    {
        return $this->order_article_price;
    }
    /**
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->order_article_purchase_price;
    }
    /**
     * @return float
     */
    public function getTotalSellingPrice() : float
    {
        return ($this->quantity * $this->order_article_price);
    }
    /**
     * @return float
     */
    public function getTotalPurchasePrice() : float
    {
        return ($this->quantity * $this->order_article_purchase_price);
    }
    /**
     * @return mixed
     */
    public function getDelivered()
    {
        return $this->delivered;
    }
    /**
     * @param mixed $delivered
     */
    public function setDelivered($delivered)
    {
        $this->delivered = $delivered;
    }
    /**
     * @return \DateTime
     */
    public function getDeliveryTime() : \DateTime
    {
        return $this->delivery_time;
    }
    /**
     * @param \DateTime $delivery_time
     */
    public function setDeliveryTime(\DateTime $delivery_time)
    {
        $this->delivery_time = $delivery_time;
    }
    /**
     * @return string
     */
    public function getDeliveryTimeString() :  string
    {
        return $this->delivery_time->format('Y-m-d');
    }
    /**
     * @return string
     */
    public function getOrderStatusString() : string
    {
        return $this->order->getOrderStatusString();
    }
    /**
     * @return string
     */
    public function getContactPerson() : string
    {
        return $this->order->getContactPerson()->getName();
    }
    /**
     * @param $order_article_mails
     */
    public function setOrderArticleMails($order_article_mails)
    {
        $this->order_article_mails = $order_article_mails;
    }
    /**
     * @return OrderToSupplierStatusEntity
     */
    public function getToSupplierStatus(): OrderToSupplierStatusEntity
    {
        return $this->to_supplier_status;
    }
    /**
     * @param OrderToSupplierStatusEntity $to_supplier_status
     */
    public function setToSupplierStatus(OrderToSupplierStatusEntity $to_supplier_status)
    {
        $this->to_supplier_status = $to_supplier_status;
    }
    /**
     * @return string
     */
    public function getCommission() : string
    {
        return $this->commission;
    }
    /**
     * @param string $commission
     */
    public function setCommission(string $commission)
    {
        $this->commission = $commission;
    }
    /**
     * OrderArticleEntity constructor.
     */
    public function __construct()
    {
        $this->order_article_mails = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getShowSize()
    {
        return $this->show_size;
    }

    /**
     * @param mixed $show_size
     */
    public function setShowSize($show_size)
    {
        $this->show_size = $show_size;
    }

    /**
     * @return int
     */
    public function getCustomOrderArticleNumber()
    {
        return $this->custom_order_article_number;
    }

    /**
     * @param $custom_order_article_number
     */
    public function setCustomOrderArticleNumber($custom_order_article_number)
    {
        $this->custom_order_article_number = $custom_order_article_number;
    }
}

