<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/8/2017
 * Time: 8:19 PM
 */

namespace AppBundle\Entity\User\Orders;

use AppBundle\Entity\User\Debtors\ContactPersonEntity;
use AppBundle\Entity\User\Debtors\DebtorEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderMailStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity;
use AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="orders",
 *     indexes={
 *          @ORM\Index(
 *              name="search_order_supervisor",
 *              columns={"order_supervisor"}
 *          ),
 *          @ORM\Index(
 *              name="search_order_datum",
 *              columns={"order_datum"}
 *          ),
 *          @ORM\Index(
 *              name="search_debtor_name",
 *              columns={"debtor_name"}
 *          ),
 *          @ORM\Index(
 *              name="search_order_datum",
 *              columns={"order_datum"}
 *          ),
 *          @ORM\Index(
 *              name="search_order_status_name",
 *              columns={"order_status_name"}
 *          ),
 *          @ORM\Index(
 *              name="search_delivery_time",
 *              columns={"delivery_time"}
 *          ),
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Orders\OrderRepository")
 */
class OrderEntity
{
    //filtering by the order_datum
    //filtering by order_status
    //ordered by order_datum, debtor_name
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $order_number;
    /**
     * @ORM\Column(type="string", length=500)
     */
    private $invoice_number;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\DeliveryAddressEntity")
     * @ORM\JoinColumn(name="delivery_address_id", referencedColumnName="id")
     */
    private $delivery_address;

    /**
     * @ORM\Column(type="datetime")
     */
    private $order_datum;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $order_supervisor;
    /**
     * @var DebtorEntity
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\DebtorEntity", inversedBy="orders")
     * @ORM\JoinColumn(name="debtor_id", referencedColumnName="id")
     */
    private $debtor;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $debtor_name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\ContactPersonEntity", inversedBy="orders")
     * @ORM\JoinColumn(name="contact_person_id", referencedColumnName="id")
     */
    private $contact_person;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderDeliveryStatusEntity", inversedBy="orders")
     * @ORM\JoinColumn(name="delivery_status_id", referencedColumnName="id")
     */
    private $delivery_status;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderPaymentStatusEntity", inversedBy="orders")
     * @ORM\JoinColumn(name="payment_status_id", referencedColumnName="id")
     */
    private $payment_status;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderPaymentMethodEntity", inversedBy="orders")
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id")
     */
    private $payment_method;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Orders\FixedTables\OrderStatusEntity", inversedBy="orders")
     * @ORM\JoinColumn(name="order_status_id", referencedColumnName="id")
     */
    private $order_status;
    /**
     * @ORM\OneToMany(targetEntity="OrderMailEntity", mappedBy="order")
     */
    private $order_mails;
    /**
     * @ORM\OneToMany(targetEntity="OrderFileEntity", mappedBy="order")
     */
    private $order_files;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $order_status_name;
    /**
     * @ORM\Column(type="datetime")
     */
    private $delivery_time;//levtd
    /**
     * @ORM\Column(type="float")
     */
    private $discount;
    /**
     * @ORM\OneToMany(targetEntity="OrderArticleEntity", mappedBy="order")
     * @ORM\OrderBy({"custom_order_article_number" = "ASC"})
     */
    private $order_articles;
    /**
     * @ORM\OneToMany(targetEntity="InternNoteEntity", mappedBy="order")
     * @ORM\OrderBy({"updated" = "DESC"})
     */
    private $intern_notes;
    /**
     * @ORM\OneToMany(targetEntity="AdministrativeNoteEntity", mappedBy="order")
     * @ORM\OrderBy({"updated" = "DESC"})
     */
    private $administrative_notes;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $updated;
    /**
     * @ORM\Column(type="float")
     */
    private $total_purchase_amount;
    /**
     * @ORM\Column(type="float")
     */
    private $total_selling_amount;
    /**
     * @ORM\Column(type="float")
     */
    private $packing_and_transport;

    /**
     * @ORM\Column(type="integer")
     */
    private $order_invoice_number;
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getOrderNumber() : string
    {
        return $this->order_number;
    }
    /**
     * @param string $order_number
     */
    public function setOrderNumber(string $order_number)
    {
        $this->order_number = $order_number;
    }
    /**
     * @return \DateTime
     */
    public function getOrderDatum() : \DateTime
    {
        return $this->order_datum;
    }
    /**
     * @return string
     */
    public function getOrderDatumString() : string
    {
        return $this->order_datum->format('Y-m-d');
    }
    /**
     * @param \DateTime $order_datum
     */
    public function setOrderDatum(\DateTime $order_datum)
    {
        $this->order_datum = $order_datum;
    }
    /**
     * @return string
     */
    public function getOrderSupervisor() : string
    {
        return $this->order_supervisor;
    }
    /**
     * @param string $order_supervisor
     */
    public function setOrderSupervisor(string $order_supervisor)
    {
        $this->order_supervisor = $order_supervisor;
    }
    /**
     * @return DebtorEntity
     */
    public function getDebtor() : DebtorEntity
    {
        return $this->debtor;
    }
    /**
     * @return string
     */
    public function getDebtorName() : string
    {
        return $this->debtor->getName();
    }
    /**
     * @param DebtorEntity $debtor
     */
    public function setDebtor(DebtorEntity $debtor)
    {
        $this->debtor = $debtor;
        $this->debtor_name = $debtor->getName();
    }
    /**
     * @return OrderDeliveryStatusEntity
     */
    public function getDeliveryStatus() : OrderDeliveryStatusEntity
    {
        return $this->delivery_status;
    }
    /**
     * @return string
     */
    public function getDeliveryStatusString() : string
    {
        return $this->delivery_status->getName();
    }
    /**
     * @param OrderDeliveryStatusEntity $delivery_status
     */
    public function setDeliveryStatus(OrderDeliveryStatusEntity $delivery_status)
    {
        $this->delivery_status = $delivery_status;
    }
    /**
     * @return mixed
     */
    public function getOrderFiles()
    {
        return $this->order_files;
    }

    /**
     * @param mixed $order_files
     */
    public function setOrderFiles($order_files)
    {
        $this->order_files = $order_files;
    }
    /**
     * @return OrderStatusEntity
     */
    public function getOrderStatus() : OrderStatusEntity
    {
        return $this->order_status;
    }
    /**
     * @return string
     */
    public function getOrderStatusString() : string
    {
        return $this->order_status_name;
    }
    /**
     * @param OrderStatusEntity $order_status
     */
    public function setOrderStatus(OrderStatusEntity $order_status)
    {
        $this->order_status = $order_status;
        $this->order_status_name = $order_status->getName();
    }
    /**
     * @return \DateTime
     */
    public function getDeliveryTime() : \DateTime
    {
        return $this->delivery_time;
    }
    /**
     * @return string
     */
    public function getDeliveryTimeString() : string
    {
        return $this->delivery_time->format('Y-m-d');
    }
    /**
     * @param \DateTime $delivery_time
     */
    public function setDeliveryTime(\DateTime $delivery_time)
    {
        $this->delivery_time = $delivery_time;
    }
    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }
    /**
     * @param float $discount
     */
    public function setDiscount(float $discount)
    {
        $this->discount = $discount;
    }
    /**
     * @param mixed $order_articles
     */
    public function setOrderArticles($order_articles)
    {
        $this->order_articles = $order_articles;
    }
    /**
     * @return ArrayCollection
     */
    public function getOrderArticles()
    {
        return $this->order_articles;
    }
    /**
     * @return ContactPersonEntity
     */
    public function getContactPerson() : ContactPersonEntity
    {
        return $this->contact_person;
    }
    /**
     * @param ContactPersonEntity $contact_person
     */
    public function setContactPerson(ContactPersonEntity $contact_person)
    {
        $this->contact_person = $contact_person;
    }
    /**
     * @return ArrayCollection
     */
    public function getInternNotes()
    {
        return $this->intern_notes;
    }
    /**
     * @param mixed $intern_notes
     */
    public function setInternNotes($intern_notes)
    {
        $this->intern_notes = $intern_notes;
    }
    /**
     * @return ArrayCollection
     */
    public function getAdministrativeNotes()
    {
        return $this->administrative_notes;
    }
    /**
     * @param mixed $administrative_notes
     */
    public function setAdministrativeNotes($administrative_notes)
    {
        $this->administrative_notes = $administrative_notes;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    public function  getCreatedString() : string
    {
        return $this->created->format('Y-m-d');
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }
    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }
    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @param mixed $order_mails
     */
    public function setOrderMails($order_mails)
    {
        $this->order_mails = $order_mails;
    }
    /**
     * @return OrderPaymentStatusEntity
     */
    public function getPaymentStatus() : OrderPaymentStatusEntity
    {
        return $this->payment_status;
    }
    /**
     * @param OrderPaymentStatusEntity $payment_status
     */
    public function setPaymentStatus(OrderPaymentStatusEntity $payment_status)
    {
        $this->payment_status = $payment_status;
    }
    /**
     * @return OrderPaymentMethodEntity
     */
    public function getPaymentMethod() : OrderPaymentMethodEntity
    {
        return $this->payment_method;
    }
    /**
     * @param OrderPaymentMethodEntity $payment_method
     */
    public function setPaymentMethod(OrderPaymentMethodEntity $payment_method)
    {
        $this->payment_method = $payment_method;
    }
    /**
     * @return float
     */
    public function getTotalPurchaseAmount() : float
    {
        return $this->total_purchase_amount;
    }
    /**
     * @param float $total_amount
     */
    public function setTotalPurchaseAmount(float $total_amount)
    {
        $this->total_purchase_amount = $total_amount;
    }
    /**
     * @return float
     */
    public function getTotalSellingAmount() : float
    {
        return $this->total_selling_amount;
    }
    /**
     * @return string
     */
    public function getTotalSellingAmountWith2DP() : string
    {
        switch($this->debtor->getEndUserType()->getName()) {
            case DebtorEndUserTypeEntity::COMPANY:
                return number_format(round($this->total_selling_amount + $this->packing_and_transport + $this->discount, 2), 2);
                break;
            case DebtorEndUserTypeEntity::PRIVATE:
                return number_format(round(($this->total_selling_amount + $this->packing_and_transport + $this->discount)/ 1.21, 2), 2);
                break;
            default:
        }
    }
    /**
     * @param float $total_amount
     */
    public function setTotalSellingAmount(float $total_amount)
    {
        $this->total_selling_amount = $total_amount;
    }
    /**
     * @return float
     */
    public function getPackingAndTransport() : float
    {
        return $this->packing_and_transport;
    }
    /**
     * @param float $packing_and_transport
     */
    public function setPackingAndTransport(float $packing_and_transport)
    {
        $this->packing_and_transport = $packing_and_transport;
    }
    /**
     * @return float
     */
    public function getPdv() : float
    {
        return round(.21*$this->total_selling_amount);
    }
    /**
     * @return string
     */
    public function getPdvWith2DP() : string
    {
        switch($this->debtor->getEndUserType()->getName()) {
            case DebtorEndUserTypeEntity::COMPANY:
                return number_format(round(.21 * ($this->total_selling_amount + $this->packing_and_transport + $this->discount), 2), 2);
                break;
            case DebtorEndUserTypeEntity::PRIVATE:
                return number_format(round(.21 * ($this->total_selling_amount + $this->packing_and_transport + $this->discount)/(1 + .21), 2), 2);
                break;
            default:
        }
    }
    /**
     * @return float
     */
    public function getTotalSellingAmountWithPdv() : float
    {
        switch($this->debtor->getEndUserType()->getName()) {
            case DebtorEndUserTypeEntity::COMPANY:
                return $this->total_selling_amount + round(.21 * $this->total_selling_amount);
                break;
            case DebtorEndUserTypeEntity::PRIVATE:
                return $this->total_selling_amount;
                break;
            default:
        }
    }
    /**
     * @return string
     */
    public function getTotalSellingAmountWithPdvWith2DP() : string
    {
        switch($this->debtor->getEndUserType()->getName()) {
            case DebtorEndUserTypeEntity::COMPANY:
                if($this->debtor->getCountry() != 'Switzerland') {
                    return number_format(round($this->total_selling_amount + $this->packing_and_transport + $this->discount, 2) + round(.21 * ($this->total_selling_amount + $this->packing_and_transport + $this->discount), 2), 2);
                } else {
                    return number_format(round($this->total_selling_amount + $this->packing_and_transport + $this->discount, 2), 2);
                }
                break;
            case DebtorEndUserTypeEntity::PRIVATE:
                return number_format(round($this->total_selling_amount + $this->packing_and_transport + $this->discount, 2), 2);
                break;
            default:
        }
    }
    /**
     * @return bool
     */
    public function isBTWType() : bool
    {
        $debtor_country = $this->debtor->getCountry();
        $end_user_type = $this->debtor->getEndUserType()->getName();
        if($debtor_country == "Switzerland") {
            return false;
        }
        if($debtor_country == "Netherlands") {
            return true;
        }
        if($end_user_type == DebtorEndUserTypeEntity::COMPANY) {
            return false;
        }
        return true;
    }
    /**
     * @return bool
     */
    public function isSwiss() : bool
    {
        $debtor_country = $this->debtor->getCountry();
        if($debtor_country == "Switzerland") {
            return true;
        }
        return false;
    }
    /**
     * OrderEntity constructor.
     */
    public function __construct()
    {
        $this->order_articles = new ArrayCollection();
        $this->administrative_notes = new ArrayCollection();
        $this->intern_notes = new ArrayCollection();
        $this->order_mails = new ArrayCollection();
        $this->order_files = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getInvoiceNumber()
    {
        return $this->invoice_number;
    }

    /**
     * @param mixed $invoice_number
     */
    public function setInvoiceNumber($invoice_number)
    {
        $this->invoice_number = $invoice_number;
    }

    /**
     * @return mixed
     */
    public function getOrderInvoiceNumber()
    {
        return $this->order_invoice_number;
    }

    /**
     * @param mixed $order_invoice_number
     */
    public function setOrderInvoiceNumber($order_invoice_number)
    {
        $this->order_invoice_number = $order_invoice_number;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress()
    {
        return $this->delivery_address;
    }

    /**
     * @param mixed $delivery_address
     */
    public function setDeliveryAddress($delivery_address)
    {
        $this->delivery_address = $delivery_address;
    }
}