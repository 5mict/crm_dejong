<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/13/2017
 * Time: 10:21 PM
 */

namespace AppBundle\Entity\User\Suppliers;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="suppliers_notes",
 *     indexes={
 *          @ORM\Index(
 *              name="search_updated",
 *              columns={"updated"}
 *          )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Suppliers\NoteRepository")
 */
class NoteEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SupplierEntity", inversedBy="notes")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $supplier;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;

    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $updated;

    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }

    /**
     * @return string
     */
    public function getUpdatedString() : string
    {
        return $this->updated->format('Y-m-d H:i:s');
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param SupplierEntity $supplier
     */
    public function setSupplier(SupplierEntity $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     */
    public function setNote(string $note)
    {
        $this->note = $note;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

}