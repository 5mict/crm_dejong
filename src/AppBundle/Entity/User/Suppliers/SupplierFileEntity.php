<?php
namespace AppBundle\Entity\User\Suppliers;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="supplier_files")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Suppliers\SupplierFileRepository")
 */
class SupplierFileEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $file_name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Suppliers\SupplierEntity", inversedBy="supplier_files")
     * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     */
    private $supplier;
    /**
     * @ORM\Column(type="datetime", options={"default":0})
     */
    private $created;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getFileName() : string
    {
        return $this->file_name;
    }
    /**
     * @param string $file_name
     */
    public function setFileName(string $file_name)
    {
        $this->file_name = $file_name;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @return string
     */
    public function getCreatedString() : string
    {
        return $this->created->format('Y-m-d H:i:s');
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param mixed $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }
}