<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/13/2017
 * Time: 9:46 PM
 */

namespace AppBundle\Entity\User\Suppliers;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="suppliers",
 *      indexes={
 *          @ORM\Index(
 *              name="search_name",
 *              columns={"name"}
 *          ),
 *          @ORM\Index(
 *              name="search_email",
 *              columns={"email"}
 *          ),
 *          @ORM\Index(
 *              name="search_post_code",
 *              columns={"post_code"}
 *          ),
 *          @ORM\Index(
 *              name="search_city",
 *              columns={"city"}
 *          ),
 *          @ORM\Index(
 *              name="search_country",
 *              columns={"country"}
 *          ),
 *      },
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="address_contraint",
 *              columns={"street", "number", "number_extension", "post_code", "city", "country"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Suppliers\SupplierRepository")
 */
class SupplierEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email;
    /**
     * @ORM\Column(type="string", length=60)
     */
    private $street;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $number;
    /**
     * @ORM\Column(type="string", length=5, options={"default" = ""})
     */
    private $number_extension;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $post_code;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $country;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderArticleEntity", mappedBy="supplier")
     */
    private $order_articles;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $updated;
    /**
     * @ORM\OneToMany(targetEntity="NoteEntity", mappedBy="supplier")
     * @ORM\OrderBy({"updated" = "DESC"})
     */
    private $notes;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;
    /**
     * @ORM\OneToMany(targetEntity="ContactPersonEntity", mappedBy="supplier")
     */
    private $contact_persons;
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $communication_language;
    /**
     * @ORM\OneToMany(targetEntity="SupplierFileEntity", mappedBy="supplier")
     */
    private $supplier_files;
    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }
    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }
    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }
    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }
    /**
     * @return mixed
     */
    public function getNumberExtension()
    {
        return $this->number_extension;
    }
    /**
     * @param mixed $number_extension
     */
    public function setNumberExtension($number_extension)
    {
        $this->number_extension = $number_extension;
    }
    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->post_code;
    }
    /**
     * @param mixed $post_code
     */
    public function setPostCode($post_code)
    {
        $this->post_code = $post_code;
    }
    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }
    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }
    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return ArrayCollection
     */
    public function getContactPersons()
    {
        return $this->contact_persons;
    }
    /**
     * @param $contact_persons
     */
    public function setContactPersons($contact_persons)
    {
        $this->contact_persons = $contact_persons;
    }
    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }
    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }
    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }
    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }
    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }
    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }
    /**
     * @param mixed $order_articles
     */
    public function setOrderArticles($order_articles)
    {
        $this->order_articles = $order_articles;
    }
    /**
     * @return string
     */
    public function getCommunicationLanguage() : string
    {
        return $this->communication_language;
    }
    /**
     * @param string $communication_language
     */
    public function setCommunicationLanguage(string $communication_language)
    {
        $this->communication_language = $communication_language;
    }
    /**
     * SupplierEntity constructor.
     */
    public function __construct()
    {
        $this->contact_persons = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->order_articles = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getSupplierFiles()
    {
        return $this->supplier_files;
    }

    /**
     * @param mixed $supplier_files
     */
    public function setSupplierFiles($supplier_files)
    {
        $this->supplier_files = $supplier_files;
    }
}