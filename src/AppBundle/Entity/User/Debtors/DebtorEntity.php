<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/24/2017
 * Time: 4:52 PM
 */

namespace AppBundle\Entity\User\Debtors;

use AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity;
use AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors",
 *     indexes={
 *          @ORM\Index(
 *              name="search_email",
 *              columns={"email"}
 *          ),
 *          @ORM\Index(
 *              name="search_city",
 *              columns={"city"}
 *          ),
 *          @ORM\Index(
 *              name="search_name",
 *              columns={"name"}
 *          ),
 *          @ORM\Index(
 *              name="search_selling_price",
 *              columns={"total_selling_price"}
 *          ),
 *          @ORM\Index(
 *              name="search_purchase_price",
 *              columns={"total_purchase_price"}
 *          ),
 *          @ORM\Index(
 *              name="search_profit_percents",
 *              columns={"profit_percents"}
 *          )
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="address_contraint",
 *              columns={"street", "number", "number_extension", "post_code", "city", "country"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\DebtorRepository")
 */
class DebtorEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email;
    /**
     * @ORM\Column(type="string", length=60)
     */
    private $street;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $number;
    /**
     * @ORM\Column(type="string", length=5, options={"default" = ""})
     */
    private $number_extension;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $post_code;
    /**
     * @ORM\Column(type="string", length=60)
     */
    private $city;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $country;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $BTW;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity", inversedBy="debtors")
     * @ORM\JoinColumn(name="end_user_type_id", referencedColumnName="id")
     */
    private $end_user_type;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity", inversedBy="debtors")
     * @ORM\JoinColumn(name="how_did_you_find_us_id", referencedColumnName="id")
     */
    private $how_did_you_find_us;
    /**
     * @ORM\OneToMany(targetEntity="DebtorFileEntity", mappedBy="debtor")
     */
    private $debtor_files;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $web_site;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $banking_account_number;
    /**
     * @ORM\OneToMany(targetEntity="ContactPersonEntity", mappedBy="debtor")
     */
    private $contact_persons;
    /**
     * @ORM\OneToMany(targetEntity="NoteEntity", mappedBy="debtor")
     * @ORM\OrderBy({"updated" = "DESC"})
     */
    private $notes;
    /**
     * @ORM\OneToMany(targetEntity="DeliveryAddressEntity", mappedBy="debtor")
     */
    private $delivery_addresses;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $updated;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderEntity", mappedBy="debtor")
     */
    private $orders;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderPaidEntity", mappedBy="debtor")
     */
    private $orders_paid;
    /**
     * @ORM\Column(type="float")
     */
    private $total_purchase_price;
    /**
     * @ORM\Column(type="float")
     */
    private $total_selling_price;
    /**
     * @ORM\Column(type="float")
     */
    private $profit_percents;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $communication_language;
    /**
     * @return string
     */
    public function getStreet() : string
    {
        return $this->street;
    }
    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }
    /**
     * @return string
     */
    public function getNumber() : string
    {
        return $this->number;
    }
    /**
     * @param string $number
     */
    public function setNumber(string $number)
    {
        $this->number = $number;
    }
    /**
     * @return string
     */
    public function getNumberExtension() : string
    {
        return $this->number_extension;
    }
    /**
     * @param string $number_extension
     */
    public function setNumberExtension(string $number_extension)
    {
        $this->number_extension = $number_extension;
    }
    /**
     * @return string
     */
    public function getPostCode() : string
    {
        return $this->post_code;
    }
    /**
     * @param string $post_code
     */
    public function setPostCode(string $post_code)
    {
        $this->post_code = $post_code;
    }
    /**
     * @return string
     */
    public function getCity() : string
    {
        return $this->city;
    }
    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }
    /**
     * @return string
     */
    public function getCountry() : string
    {
        return $this->country;
    }
    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function getSpllitedName(): array
    {
        if( strlen( $this->name ) > 20 )
        {
            return array_map( 'trim', explode(" ", $this->name, 2 ) );
        }
        return array( $this->name );
    }
    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return mixed
     */
    public function getContactPersons()
    {
        return $this->contact_persons;
    }
    /**
     * @param mixed $contact_persons
     */
    public function setContactPersons($contact_persons)
    {
        $this->contact_persons = $contact_persons;
    }
    /**
     * @return mixed
     */
    public function getDeliveryAddresses()
    {
        return $this->delivery_addresses;
    }
    /**
     * @param mixed $delivery_addresses
     */
    public function setDeliveryAddresses($delivery_addresses)
    {
        $this->delivery_addresses = $delivery_addresses;
    }
    /**
     * @return string
     */
    public function getBTW() : string
    {
        return $this->BTW;
    }
    /**
     * @param string $BTW
     */
    public function setBTW(string $BTW)
    {
        $this->BTW = $BTW;
    }
    /**
     * @return DebtorEndUserTypeEntity
     */
    public function getEndUserType() : DebtorEndUserTypeEntity
    {
        return $this->end_user_type;
    }
    /**
     * @return string
     */
    public function getEndUserTypeName() : string
    {
        return $this->end_user_type->getName();
    }
    /**
     * @param DebtorEndUserTypeEntity $end_user_type
     */
    public function setEndUserType(DebtorEndUserTypeEntity $end_user_type)
    {
        $this->end_user_type = $end_user_type;
    }
    /**
     * @return DebtorHowDidYouFindUsEntity
     */
    public function getHowDidYouFindUs() : DebtorHowDidYouFindUsEntity
    {
        return $this->how_did_you_find_us;
    }
    /**
     * @return string
     */
    public function getHowDidYouFindUsName() : string
    {
        return $this->how_did_you_find_us->getName();
    }
    /**
     * @param DebtorHowDidYouFindUsEntity $how_did_you_find_us
     */
    public function setHowDidYouFindUs(DebtorHowDidYouFindUsEntity $how_did_you_find_us)
    {
        $this->how_did_you_find_us = $how_did_you_find_us;
    }
    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }
    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }
    /**
     * @return string
     */
    public function getWebSite() : string
    {
        return $this->web_site;
    }
    /**
     * @param string $web_site
     */
    public function setWebSite(string $web_site)
    {
        $this->web_site = $web_site;
    }
    /**
     * @return string
     */
    public function getBankingAccountNumber() : string
    {
        return $this->banking_account_number;
    }
    /**
     * @param string $banking_account_number
     */
    public function setBankingAccountNumber(string $banking_account_number)
    {
        $this->banking_account_number = $banking_account_number;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }
    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }
    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }
    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }
    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }
    /**
     * @param ArrayCollection $orders
     */
    public function setOrders(ArrayCollection $orders)
    {
        $this->orders = $orders;
    }
    /**
     * @param ArrayCollection $orders_paid
     */
    public function setOrdersPaid(ArrayCollection $orders_paid)
    {
        $this->orders_paid = $orders_paid;
    }
    /**
     * @return float
     */
    public function getTotalPurchasePrice() : float
    {
        return $this->total_purchase_price;
    }
    /**
     * @param float $total_purchase_price
     */
    public function setTotalPurchasePrice(float $total_purchase_price)
    {
        $this->total_purchase_price = $total_purchase_price;
    }
    /**
     * @return float
     */
    public function getTotalSellingPrice() : float
    {
        return $this->total_selling_price;
    }
    /**
     * @param float $total_selling_price
     */
    public function setTotalSellingPrice(float $total_selling_price)
    {
        $this->total_selling_price = $total_selling_price;
    }
    /**
     * @return float
     */
    public function getProfitPercents() : float
    {
        return $this->profit_percents;
    }
    /**
     * @param float $profit_percents
     */
    public function setProfitPercents(float $profit_percents)
    {
        $this->profit_percents = $profit_percents;
    }
    /**
     * @return string
     */
    public function getCommunicationLanguage() : string
    {
        return $this->communication_language;
    }
    /**
     * @param string $communication_language
     */
    public function setCommunicationLanguage(string $communication_language)
    {
        $this->communication_language = $communication_language;
    }
    /**
     * DebtorEntity constructor.
     */
    public function __construct()
    {
        $this->contact_persons = new ArrayCollection();
        $this->delivery_addresses = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->orders_paid = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getDebtorFiles()
    {
        return $this->debtor_files;
    }

    /**
     * @param mixed $debtor_files
     */
    public function setDebtorFiles($debtor_files)
    {
        $this->debtor_files = $debtor_files;
    }
}