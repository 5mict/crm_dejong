<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/10/2017
 * Time: 6:58 AM
 */

namespace AppBundle\Entity\User\Debtors\DeliveryAddress;

use AppBundle\Entity\User\Debtors\DeliveryAddressEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors_delivery_address_phone_number")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\DeliveryAddress\PhoneNumberRepository")
 */

class PhoneNumberEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\DeliveryAddressEntity", inversedBy="phone_numbers")
     * @ORM\JoinColumn(name="delivery_address_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $delivery_address;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $phone_number;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $delivery_address
     */
    public function setDeliveryAddress($delivery_address)
    {
        $this->delivery_address = $delivery_address;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param string $phone_number
     */
    public function setPhoneNumber(string $phone_number)
    {
        $this->phone_number = $phone_number;
    }
}