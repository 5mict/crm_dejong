<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/24/2017
 * Time: 5:08 PM
 */

namespace AppBundle\Entity\User\Debtors;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors_contact_person",
 *     indexes={
 *          @ORM\Index(
 *              name="name_debtor_indx",
 *              columns={"name", "debtor_id"}
 *          )
 *     }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\ContactPersonRepository")
 */
class ContactPersonEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="DebtorEntity", inversedBy="contact_persons")
     * @ORM\JoinColumn(name="debtor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $debtor;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Orders\OrderEntity", mappedBy="contact_person")
     */
    private $orders;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $email;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $phone_number;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $updated;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;
    /**
     * @param mixed $debtor
     */
    public function setDebtor($debtor)
    {
        $this->debtor = $debtor;
    }
    /**
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }
    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
    /**
     * @return string
     */
    public function getPhoneNumber() : string
    {
        return $this->phone_number;
    }
    /**
     * @param string $phone_number
     */
    public function setPhoneNumber(string $phone_number)
    {
        $this->phone_number = $phone_number;
    }
    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }
    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }
    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }
    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }
    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }
    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }
    /**
     * @param ArrayCollection $orders
     */
    public function setOrders(ArrayCollection $orders)
    {
        $this->orders = $orders;
    }
    /**
     * ContactPersonEntity constructor.
     */
    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress()
    {
        return $this->delivery_address;
    }

}