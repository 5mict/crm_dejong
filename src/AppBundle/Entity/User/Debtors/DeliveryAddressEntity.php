<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/25/2017
 * Time: 12:35 PM
 */

namespace AppBundle\Entity\User\Debtors;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors_delivery_address")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\DeliveryAddressRepository")
 */
class DeliveryAddressEntity
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="DebtorEntity", inversedBy="delivery_addresses")
     * @ORM\JoinColumn(name="debtor_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $debtor;
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $number_extension;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", options={"default": 0})
     */
    private $updated;
    /**
     * @ORM\Column(type="string", length=25)
     */
    private $username;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $debtor
     */
    public function setDebtor($debtor)
    {
        $this->debtor = $debtor;
    }

    /**
     * @return int
     */
    public function getDebtorId() : int
    {
        return $this->debtor->getId();
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getNumberExtension()
    {
        return $this->number_extension;
    }

    /**
     * @param mixed $number_extension
     */
    public function setNumberExtension($number_extension)
    {
        $this->number_extension = $number_extension;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getDeliveryAddressString(): string
    {
        return $this->street . " " . $this->postcode . " " . $this->city . " " . $this->country;
    }
}