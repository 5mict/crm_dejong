<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 11:44 AM
 */

namespace AppBundle\Entity\User\Debtors\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors_fixed_tables_how_did_you_find_us_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\FixedTables\DebtorHowDidYouFindUsTranslationRepository")
 */
class DebtorHowDidYouFindUsTranslationEntity
{
    const GOOGLE_EN = "Google";
    const GOOGLE_NL = "Google";
    const GOOGLE_GE = "Google";
    const WORLDS_FAIR_EN = "Fair";
    const WORLDS_FAIR_NL = "Beurs";
    const WORLDS_FAIR_GE = "Weltausstellung";
    const SOCIAL_NETWORKS_EN = "Social Networks";
    const SOCIAL_NETWORKS_NL = "Sociale media";
    const SOCIAL_NETWORKS_GE = "Soziale Netzwerke";
    const MOUTH_TO_MOUTH_EN = "Mouth to mouth";
    const MOUTH_TO_MOUTH_NL = "Mond op mond";
    const MOUTH_TO_MOUTH_GE = "Mund zu Mund";
    const ACQUISITION_EN = "Acquisition";
    const ACQUISITION_NL = "Acquisitie";
    const TABLE_ROWS = [
        ['name' => DebtorHowDidYouFindUsTranslationEntity::GOOGLE_EN, 'reference' => DebtorHowDidYouFindUsEntity::GOOGLE, 'language' => 'en'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::GOOGLE_NL, 'reference' => DebtorHowDidYouFindUsEntity::GOOGLE, 'language' => 'nl'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::GOOGLE_GE, 'reference' => DebtorHowDidYouFindUsEntity::GOOGLE, 'language' => 'ge'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::WORLDS_FAIR_EN, 'reference' => DebtorHowDidYouFindUsEntity::WORLDS_FAIR, 'language' => 'en'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::WORLDS_FAIR_NL, 'reference' => DebtorHowDidYouFindUsEntity::WORLDS_FAIR, 'language' => 'nl'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::WORLDS_FAIR_GE, 'reference' => DebtorHowDidYouFindUsEntity::WORLDS_FAIR, 'language' => 'ge'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::SOCIAL_NETWORKS_EN, 'reference' => DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS, 'language' => 'en'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::SOCIAL_NETWORKS_NL, 'reference' => DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS, 'language' => 'nl'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::SOCIAL_NETWORKS_GE, 'reference' => DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS, 'language' => 'ge'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::MOUTH_TO_MOUTH_EN, 'reference' => DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH, 'language' => 'en'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::MOUTH_TO_MOUTH_NL, 'reference' => DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH, 'language' => 'nl'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::MOUTH_TO_MOUTH_GE, 'reference' => DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH, 'language' => 'ge'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::ACQUISITION_EN, 'reference' => DebtorHowDidYouFindUsEntity::ACQUISITION, 'language' => 'en'],
        ['name' => DebtorHowDidYouFindUsTranslationEntity::ACQUISITION_NL, 'reference' => DebtorHowDidYouFindUsEntity::ACQUISITION, 'language' => 'nl']
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            DebtorHowDidYouFindUsEntity::GOOGLE => DebtorHowDidYouFindUsTranslationEntity::GOOGLE_EN,
            DebtorHowDidYouFindUsEntity::WORLDS_FAIR => DebtorHowDidYouFindUsTranslationEntity::WORLDS_FAIR_EN,
            DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS => DebtorHowDidYouFindUsTranslationEntity::SOCIAL_NETWORKS_EN,
            DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH => DebtorHowDidYouFindUsTranslationEntity::MOUTH_TO_MOUTH_EN,
            DebtorHowDidYouFindUsEntity::ACQUISITION => DebtorHowDidYouFindUsTranslationEntity::ACQUISITION_EN
        ],
        LocaleEntity::DUTCH => [
            DebtorHowDidYouFindUsEntity::GOOGLE => DebtorHowDidYouFindUsTranslationEntity::GOOGLE_NL,
            DebtorHowDidYouFindUsEntity::WORLDS_FAIR => DebtorHowDidYouFindUsTranslationEntity::WORLDS_FAIR_NL,
            DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS => DebtorHowDidYouFindUsTranslationEntity::SOCIAL_NETWORKS_NL,
            DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH => DebtorHowDidYouFindUsTranslationEntity::MOUTH_TO_MOUTH_NL,
            DebtorHowDidYouFindUsEntity::ACQUISITION => DebtorHowDidYouFindUsTranslationEntity::ACQUISITION_NL
        ],
        LocaleEntity::GERMAN => [
            DebtorHowDidYouFindUsEntity::GOOGLE => DebtorHowDidYouFindUsTranslationEntity::GOOGLE_GE,
            DebtorHowDidYouFindUsEntity::WORLDS_FAIR => DebtorHowDidYouFindUsTranslationEntity::WORLDS_FAIR_GE,
            DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS => DebtorHowDidYouFindUsTranslationEntity::SOCIAL_NETWORKS_GE,
            DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH => DebtorHowDidYouFindUsTranslationEntity::MOUTH_TO_MOUTH_GE
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsEntity", inversedBy="how_did_you_find_us_translations")
     * @ORM\JoinColumn(name="how_did_you_find_us_id", referencedColumnName="id")
     */
    private $how_did_you_find_us;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @return DebtorHowDidYouFindUsEntity
     */
    public function getHowDidYouFindUs() : DebtorHowDidYouFindUsEntity
    {
        return $this->how_did_you_find_us;
    }
    /**
     * @param DebtorHowDidYouFindUsEntity $how_did_you_find_us
     */
    public function setHowDidYouFindUs(DebtorHowDidYouFindUsEntity $how_did_you_find_us)
    {
        $this->how_did_you_find_us = $how_did_you_find_us;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return DebtorHowDidYouFindUsTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * DebtorHowDidYouFindUsEntity constructor.
     */
    public function __construct()
    {

    }
}
