<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 12:19 PM
 */

namespace AppBundle\Entity\User\Debtors\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors_fixed_tables_how_did_you_find_us")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\FixedTables\DebtorHowDidYouFindUsRepository")
 */
class DebtorHowDidYouFindUsEntity
{
    const GOOGLE = "Google";
    const WORLDS_FAIR = "Fair";
    const SOCIAL_NETWORKS = "Social Networks";
    const MOUTH_TO_MOUTH = "Mouth to mouth";
    const ACQUISITION = "Acquisition";
    const WAYS_OF_FINDING = [
        DebtorHowDidYouFindUsEntity::GOOGLE,
        DebtorHowDidYouFindUsEntity::WORLDS_FAIR,
        DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH,
        DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS,
        DebtorHowDidYouFindUsEntity::ACQUISITION,
    ];
    const TABLE_ROWS = [
        ['name' => DebtorHowDidYouFindUsEntity::GOOGLE],
        ['name' => DebtorHowDidYouFindUsEntity::WORLDS_FAIR],
        ['name' => DebtorHowDidYouFindUsEntity::SOCIAL_NETWORKS],
        ['name' => DebtorHowDidYouFindUsEntity::MOUTH_TO_MOUTH],
        ['name' => DebtorHowDidYouFindUsEntity::ACQUISITION]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Debtors\FixedTables\DebtorHowDidYouFindUsTranslationEntity", mappedBy="how_did_you_find_us")
     */
    private $how_did_you_find_us_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Debtors\DebtorEntity", mappedBy="how_did_you_find_us")
     */
    private $debtors;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @param mixed $debtors_how_did_you_find_us_translations
     */
    public function setHowDidYouFindUsTranslations($debtors_how_did_you_find_us_translations)
    {
        $this->how_did_you_find_us_translations = $debtors_how_did_you_find_us_translations;
    }
    /**
     * @param mixed $debtors
     */
    public function setDebtors($debtors)
    {
        $this->debtors = $debtors;
    }
    /**
     * DebtorHowDidYouFindUsEntity constructor.
     */
    public function __construct()
    {
        $this->debtors = new ArrayCollection();
        $this->how_did_you_find_us_translations = new ArrayCollection();
    }
}
