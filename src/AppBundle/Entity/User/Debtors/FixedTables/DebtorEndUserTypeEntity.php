<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 11/11/2017
 * Time: 12:26 PM
 */

namespace AppBundle\Entity\User\Debtors\FixedTables;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors_fixed_tables_end_user_type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\FixedTables\DebtorEndUserTypeRepository")
 */
class DebtorEndUserTypeEntity
{
    const PRIVATE = "Private";
    //const RESALE = "Resale";
    const COMPANY = "Company";
    const END_USER_TYPES = [
        DebtorEndUserTypeEntity::PRIVATE,
        //DebtorEndUserTypeEntity::RESALE,
        DebtorEndUserTypeEntity::COMPANY
    ];
    const TABLE_ROWS = [
        ['name' => DebtorEndUserTypeEntity::PRIVATE],
        //'name' => DebtorEndUserTypeEntity::RESALE],
        ['name' => DebtorEndUserTypeEntity::COMPANY],

    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20, unique=true)
     */
    private $name;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeTranslationEntity", mappedBy="debtor_end_user_type")
     */
    private $debtor_end_user_type_translations;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\User\Debtors\DebtorEntity", mappedBy="end_user_type")
     */
    private $debtors;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @param mixed $debtor_end_user_type_translations
     */
    public function setDebtorEndUserTypeTranslations($debtor_end_user_type_translations)
    {
        $this->debtor_end_user_type_translations = $debtor_end_user_type_translations;
    }
    /**
     * @param mixed $debtors
     */
    public function setDebtors($debtors)
    {
        $this->debtors = $debtors;
    }
    /**
     * DebtorEndUserTypeEntity constructor.
     */
    public function __construct()
    {
        $this->debtors = new ArrayCollection();
        $this->debtor_end_user_type_translations = new ArrayCollection();
    }
}