<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 12/20/2017
 * Time: 11:20 AM
 */

namespace AppBundle\Entity\User\Debtors\FixedTables;

use AppBundle\Entity\FixedTables\LocaleEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="debtors_fixed_tables_end_user_type_translations",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              name="name_locale_constraint",
 *              columns={"name", "locale_id"}
 *          )
 *      }
 *  )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\User\Debtors\FixedTables\DebtorEndUserTypeTranslationRepository")
 */
class DebtorEndUserTypeTranslationEntity
{
    const PRIVATE_EN = "Private";
    const PRIVATE_NL = "Privé";
    const PRIVATE_GE = "Privatgelände";
    //const RESALE_EN = "Resale";
    //const RESALE_NL = "Wederverkoop";
    //const RESALE_GE = "Wiederverkauf";
    const COMPANY_EN = "Company";
    const COMPANY_NL = "Bedrijf";
    const COMPANY_GE = "Unternehmen";
    const TABLE_ROWS = [
        ['name' => DebtorEndUserTypeTranslationEntity::PRIVATE_EN, 'reference' => DebtorEndUserTypeEntity::PRIVATE, 'language' => 'en'],
        ['name' => DebtorEndUserTypeTranslationEntity::PRIVATE_NL, 'reference' => DebtorEndUserTypeEntity::PRIVATE,'language' => 'nl'],
        ['name' => DebtorEndUserTypeTranslationEntity::PRIVATE_GE, 'reference' => DebtorEndUserTypeEntity::PRIVATE, 'language' => 'ge'],
        //['name' => DebtorEndUserTypeTranslationEntity::RESALE_EN, 'reference' => DebtorEndUserTypeEntity::RESALE, 'language' => 'en'],
        //['name' => DebtorEndUserTypeTranslationEntity::RESALE_NL, 'reference' => DebtorEndUserTypeEntity::RESALE, 'language' => 'nl'],
        //['name' => DebtorEndUserTypeTranslationEntity::RESALE_GE, 'reference' => DebtorEndUserTypeEntity::RESALE, 'language' => 'ge'],
        ['name' => DebtorEndUserTypeTranslationEntity::COMPANY_EN, 'reference' => DebtorEndUserTypeEntity::COMPANY, 'language' => 'en'],
        ['name' => DebtorEndUserTypeTranslationEntity::COMPANY_NL, 'reference' => DebtorEndUserTypeEntity::COMPANY, 'language' => 'nl'],
        ['name' => DebtorEndUserTypeTranslationEntity::COMPANY_GE, 'reference' => DebtorEndUserTypeEntity::COMPANY, 'language' => 'ge']
    ];
    const TRANSLATIONS = [
        LocaleEntity::ENGLISH => [
            DebtorEndUserTypeEntity::PRIVATE => DebtorEndUserTypeTranslationEntity::PRIVATE_EN,
            //DebtorEndUserTypeEntity::RESALE => DebtorEndUserTypeTranslationEntity::RESALE_EN,
            DebtorEndUserTypeEntity::COMPANY => DebtorEndUserTypeTranslationEntity::COMPANY_EN
        ],
        LocaleEntity::DUTCH => [
            DebtorEndUserTypeEntity::PRIVATE => DebtorEndUserTypeTranslationEntity::PRIVATE_NL,
            //DebtorEndUserTypeEntity::RESALE => DebtorEndUserTypeTranslationEntity::RESALE_NL,
            DebtorEndUserTypeEntity::COMPANY => DebtorEndUserTypeTranslationEntity::COMPANY_NL
        ],
        LocaleEntity::GERMAN => [
            DebtorEndUserTypeEntity::PRIVATE => DebtorEndUserTypeTranslationEntity::PRIVATE_GE,
            //DebtorEndUserTypeEntity::RESALE => DebtorEndUserTypeTranslationEntity::RESALE_GE,
            DebtorEndUserTypeEntity::COMPANY => DebtorEndUserTypeTranslationEntity::COMPANY_GE
        ]
    ];
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\Debtors\FixedTables\DebtorEndUserTypeEntity", inversedBy="debtor_end_user_type_translations")
     * @ORM\JoinColumn(name="debtor_end_user_type_id", referencedColumnName="id")
     */
    private $debtor_end_user_type;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\FixedTables\LocaleEntity")
     * @ORM\JoinColumn(name="locale_id", referencedColumnName="id")
     */
    private $locale;
    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    /**
     * @return DebtorEndUserTypeEntity
     */
    public function getDebtorEndUserType() : DebtorEndUserTypeEntity
    {
        return $this->debtor_end_user_type;
    }
    /**
     * @param DebtorEndUserTypeEntity $debtor_end_user_type
     */
    public function setDebtorEndUserType(DebtorEndUserTypeEntity $debtor_end_user_type)
    {
        $this->debtor_end_user_type = $debtor_end_user_type;
    }
    /**
     * @return LocaleEntity
     */
    public function getLocale() : LocaleEntity
    {
        return $this->locale;
    }
    /**
     * @param LocaleEntity $locale
     */
    public function setLocale(LocaleEntity $locale)
    {
        $this->locale = $locale;
    }
    /**
     * @param string $name
     * @param string $locale
     * @return string
     */
    public static function get_translation(string $name, string $locale) : string
    {
        return DebtorEndUserTypeTranslationEntity::TRANSLATIONS[$locale][$name];
    }
    /**
     * DebtorEndUserTypeEntity constructor.
     */
    public function __construct()
    {

    }
}