<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 10/30/2017
 * Time: 11:22 AM
 */

namespace AppBundle\Entity\Grant;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="app_grants")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Grant\GrantRepository")
 */
class GrantEntity
{
    const DEBTOR_GRANT = 'debtor';
    const ARTICLE_GRANT = 'article';
    const ORDER_GRANT = 'order';
    const SUPPLIER_GRANT = 'supplier';


    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $_grant;

    /**
     * @ORM\Column(type="text", length=400)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function get_grant() : string
    {
        return $this->_grant;
    }

    /**
     * @param string $grant
     */
    public function set_grant(string $grant)
    {
        $this->_grant = $grant;
    }

    /**
     * @return bool
     */
    public function getEnabled() : bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    public function toggle()
    {
        $this->enabled = !($this->enabled);
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated() : \DateTime
    {
        return $this->updated;
    }

}