<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 1/6/2018
 * Time: 10:07 PM
 */

namespace AppBundle\Utils;

use Symfony\Component\Validator\Constraints\Date;

class DateHelper
{
    const MONTHS_EN = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'
    ];
    const MONTHS_NL = [
        'Jan', 'Feb', 'Maart', 'Apr', 'Mei', 'Juni', 'Juli', 'Aug', 'Sept', 'Okt', 'Nov', 'Dec'
    ];
    const MONTHS_GE = [
        'Jan', 'Feb', 'Marz', 'Apr', 'Mai', 'Juni', 'Juli', 'Aug', 'Sept', 'Okt', 'Nov', 'Dez'
    ];

    public static function get_month_with_translation(int $month, string $locale)
    {
        switch($locale) {
            case 'en':
                return DateHelper::MONTHS_EN[$month];
                break;
            case 'nl':
                return DateHelper::MONTHS_NL[$month];
                break;
            case 'ge':
                return DateHelper::MONTHS_GE[$month];
                break;
            default:
                return DateHelper::MONTHS_EN[$month];
        }
    }
}